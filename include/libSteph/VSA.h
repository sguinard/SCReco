#ifndef VSA_H
#define VSA_H

#include "Color.h"
#include "Triangle.h"
#include "Triangle_Coords.h"
#include "Typedefs.h"
#include "Proxy.h"
#include "MTriCoords.h"
#include "Mtri.h"
#include <unordered_map>
#include <tuple>

namespace std{

template <class T>
        inline void hash_combine(std::size_t& seed, T const& v)
        {
            seed ^= std::hash<T>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }

        // Recursive template code derived from Matthieu M.
        template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1>
        struct HashValueImpl
        {
          static void apply(size_t& seed, Tuple const& tuple)
          {
            HashValueImpl<Tuple, Index-1>::apply(seed, tuple);
            hash_combine(seed, std::get<Index>(tuple));
          }
        };

        template <class Tuple>
        struct HashValueImpl<Tuple,0>
        {
          static void apply(size_t& seed, Tuple const& tuple)
          {
            hash_combine(seed, std::get<0>(tuple));
          }
        };
    

    template <typename ... TT>
    struct hash<std::tuple<TT...>> 
    {
        size_t
        operator()(std::tuple<TT...> const& tt) const
        {                                              
            size_t seed = 0;                             
            HashValueImpl<std::tuple<TT...> >::apply(seed, tt);    
            return seed;                                 
        }                                              

    };


class VSA
{
private:
  //Mtri _M;
  std::map<uint32_t, std::vector<uint32_t> > M;
  std::unordered_map<tuple<uint32_t, uint32_t, uint32_t>, uint32_t> tri_idx;
	double _tresh;
	uint32_t _nb_regions;
	const vP3 _points;
	const vT _triangles;
	std::vector<std::pair<uint32_t,uint32_t> > tri_adj;
	std::vector<Proxy> _proxies;
	std::vector<int32_t> _component;

public:
	//VSA();
 VSA(uint32_t nb_regions, double tresh, vP3* points, vT* triangles) : _tresh(tresh), _nb_regions(nb_regions), _points(*points), _triangles(*triangles)
	  {
	    _component = std::vector<int32_t>(triangles->size(), -1);
	    /*for (uint32_t p = 0; p != _points.size(); ++p)
	      //if (_M.find(p) != _M.end())
		{
		  vT vtri = _M[p];
		  for (uint32_t i = 0; i != vtri.size(); ++i)
		    for (uint32_t j = i+1; j < vtri.size(); ++j)
		      {
			uint32_t pos1 = std::find( _triangles.begin(), _triangles.end(), vtri[i]) - _triangles.begin();
			uint32_t pos2 = std::find( _triangles.begin(), _triangles.end(), vtri[j]) - _triangles.begin();
			if (adjacent(&_triangles[pos1],&_triangles[pos2]))
			  tri_adj.emplace_back(std::make_pair(pos1,pos2));
		      }
		      }*/
	    for (uint32_t t_idx = 0; t_idx != _triangles.size(); ++t_idx)
	      {
		tri_idx[tuple<uint32_t, uint32_t, uint32_t>(_triangles[t_idx].i, _triangles[t_idx].j, _triangles[t_idx].k)] = t_idx;
		M[_triangles[t_idx].i].emplace_back(t_idx);
		M[_triangles[t_idx].j].emplace_back(t_idx);
		M[_triangles[t_idx].k].emplace_back(t_idx);
	      }
	    for (uint32_t p_idx = 0; p_idx != _points.size(); ++p_idx)
	      {
		std::vector<uint32_t> vtri_idx = M[p_idx];
		for (uint32_t idx1 = 0; idx1 != vtri_idx.size(); ++idx1)
		  for (uint32_t idx2 = idx1+1; idx2 < vtri_idx.size(); ++idx2)
		    if (adjacent(&_triangles[vtri_idx[idx1]],&_triangles[vtri_idx[idx2]]))
			  tri_adj.emplace_back(std::make_pair(vtri_idx[idx1],vtri_idx[idx2]));
	      }

	    std::sort(tri_adj.begin(), tri_adj.end());
	    tri_adj.erase( unique(tri_adj.begin(), tri_adj.end()), tri_adj.end());
	    }
	//std::cout << tri_adj.size() << " ftu,k;l" << std::endl;
	//for (auto & pair_adj : tri_adj) std::cout << pair_adj.first << " " << pair_adj.second << std::endl;}
	
	  ~VSA(){};

	bool is_valid();

	void solve();
	void propagation();
	double iteration(bool first_iter = false);
	double compute_error_all();
	double compute_error_l2(int32_t id_reg, Triangle* t);
	double compute_error_point();
	double compute_error_l21(uint32_t id_reg, Triangle* t);
	std::vector<std::vector<std::pair<Triangle, Color> > > color_points();
	void store();
	void store_projections();
	void store_points_colored();
};
}
#endif VSA_H

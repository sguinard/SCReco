#ifndef WEDGESTUFF
#define WEDGESTUFF

#include "Edge.h"
#include "Mw.h"

/// \file WedgeStuff.h
///
/// \brief Toolbox for wedges

std::vector<std::pair<Wedge,edge_number> > find_wedge_m(const Mw* M, Wedge w);

std::vector<Wedge> wedge_thresholding(XMls &mls, std::vector<Wedge> vw, double omega);

std::vector<Wedge> *convert_edge_list_to_wedge(std::vector<std::map<uint, std::vector<SCR::edge> > *> *v_edges, XMls& mls);

std::vector<Wedge> convert_triangle_list_to_wedge(std::vector<Triangle> vtri, XMls &mls);

std::tuple<std::vector<Triangle>, std::vector<std::tuple<uint, uint, uint> >, std::vector<uint> > convert_wedge_list_to_triangle(XMls &mls, std::vector<Wedge> vw, std::map<uint, std::vector<SCR::edge> > *Edges, XBlockIndex b, Mtri mt, double eps);

#endif // WEDGESTUFF


#ifndef KMEANS_H
#define KMEANS_H

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "GlobalTools.h"
#include "MPvI.h"

/// \file KMeans header
///
/// \brief this file contains the kmeans class for the resolution of kmeans problem with planes

class KMeans
{

private:
    /// \brief data for kmeans
    vP3 points;
    /// \brief clusters
    vP  planes;
    /// \brief energy if data is clustered in 1 plane
    double energy_trivial;
    /// \brief energy if data is clustered in nb. of planes regions
    double energy;

public:
    KMeans();
    KMeans(vP3* v_points);

    ~KMeans();

    bool find_point_in_map_of_planes_and_vi(MPvI* m, int p);

    void nth_iteration(int n);
    void nth_iteration_ransac(int n);
    void nth_iteration_ransac_custom(int n, double d_tresh, bool verbose);
    void solve(double treshold);
    void store(XMls &mls, param params);
    void color_points(XMls& mls, param params, std::vector<uint32> inComponent, std::vector<Triangle> vt);

};

#endif // KMEANS_H

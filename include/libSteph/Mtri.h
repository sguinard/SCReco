#ifndef MTRI_H
#define MTRI_H

#include "Triangle.h"
#include "libXMls/XMls.h"

/// \file Mtri.h
/// @brief  custom class for managing maps of triangles
///         not very useful for now,
///         but cleaner than just in harcode and can have utility in some future


namespace SCR{

/// \brief class for managing maps of triangles
class Mtri
{
public:
    std::map<uint,std::vector<Triangle> > M;

    Mtri();
    Mtri(std::vector<Triangle> vtri);
    Mtri(std::vector<Triangle> *vtri);
    ~Mtri(){}

    /// \brief access operator
    std::vector<Triangle>& operator[](uint i)
    {
        return M[i];
    }

    /// \brief iterators
    typedef std::map<uint,std::vector<Triangle> >::iterator iterator;
        iterator begin() { return M.begin(); }
        iterator end()   { return M.end();   }

};

}


#endif // MTRI_H

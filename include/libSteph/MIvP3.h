#ifndef MPVI_H
#define MPVI_H

#include "Typedefs.h"
#include "map"

/// \file MIvP3.h
/// @brief  custom class for managing maps of planes with their associated vector of points
///         not very useful for now,
///         but cleaner than just in harcode and can have utility in some future


/// \brief class for managing maps of triangles
class MIvP3
{
public:
    std::map<int,vP3> M;

    MIvP3();
    ~MIvP3(){}

    /// \brief access operator
    vP3& operator[](int i)
    {
        return M[i];
    }

    /*/// \brief access operator
    vP3 at(int i)
    {
        return M.at(i);
    }*/

    /// \brief iterators
    typedef std::map<int,vP3>::iterator iterator;
        iterator begin() { return M.begin(); }
        iterator end()   { return M.end();   }

    /// \brief enables find
    iterator find(int key)
    {
        return M.find(key);
    }

};


#endif // MPVI_H

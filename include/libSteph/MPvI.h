#ifndef MPVI_H
#define MPVI_H

#include "Typedefs.h"
#include "map"

/// \file MPvI.h
/// @brief  custom class for managing maps of planes with their associated vector of points
///         not very useful for now,
///         but cleaner than just in harcode and can have utility in some future


/// \brief class for managing maps of triangles
class MPvI
{
public:
    std::map<Plane,vI> M;

    MPvI();
    ~MPvI(){}

    /// \brief access operator
    vI& operator[](Plane p)
    {
        return M[p];
    }

    /// \brief iterators
    typedef std::map<Plane,vI>::iterator iterator;
        iterator begin() { return M.begin(); }
        iterator end()   { return M.end();   }

    /// \brief enables find
    iterator find(Plane key)
    {
        return M.find(key);
    }

};


#endif // MPVI_H

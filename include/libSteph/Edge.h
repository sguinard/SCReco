#ifndef EDGE_H
#define EDGE_H

#include <ctime>
#include <iostream>
#include <limits>
#include <stdio.h>
#include <thread>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "maxFlow/graph.h"

#include "Sensortools.h"

/*!
 *  Edge header
 *
 *  @brief defines classes edge and edge_map
 *  @warning everything is in the namespace SCR
 */

namespace SCR {
/*! Specific class for Simplicial Complex Reconstruction
 *  This class defines edges connecting 2 echos for "neighbouring" pulses
 *  For a define pulse, given its acquisition information (t,theta),
 *  we call the neighbours of the pulse, the 2 points with the closest t
 *  + the 2 points with the closest theta in the precedent and following acquisition lines
 *  which gives for each pulse a 6-neighbouring relationship
 *  Knowing that each pulse can have from 0 to 8 echos,
 *  each echo has between 0 and 48 neighbours (not including other echos for his own pulse)
 *  This class create edges linking 2 echos inside this neighbourhood relationship.
 */
class edge
{
    public:
        XBlockIndex block;      // index of block
        XEchoIndex node_begin;  // beginning of edge
        XEchoIndex node_end;    // end of edge

        edge();

        edge(const edge & e);

        edge(XEchoIndex n1, XEchoIndex n2);

        edge(XBlockIndex b, XEchoIndex n1, XEchoIndex n2);

        ~edge(){}

        /// don't ever try to modify this sh*t ...
        ///
        /// @brief These operators are here to facilitate some operations ...
        bool operator==(edge rhs) {return ((node_begin == rhs.node_begin && node_end == rhs.node_end) ||
                                           (node_begin == rhs.node_end && node_end == rhs.node_begin) && block == rhs.block) ? true : false;}
        bool operator!=(edge rhs) {return operator ==(rhs) ? false : true;}
        bool operator<(edge rhs) {return (node_begin < rhs.node_begin ? true : node_end < rhs.node_end ? true : false);}

        inline edge operator*() const {return (*this);}

        void print_edge();

        bool is_valid();

        double length(XMls &mls);
};

class edge_map
{
    public:
        edge e;
        edge_map(edge e) {this->e=e;}
    };

    struct order_edge_map: public std::binary_function<edge_map, edge_map, bool>
    {
        bool operator()(edge_map lhs, edge_map rhs) const
        {
            return  (lhs.e.node_begin < rhs.e.node_begin ||
                    (lhs.e.node_begin == rhs.e.node_begin && lhs.e.node_end < lhs.e.node_end)) ?
                true : false;
        }
    };

// compute euclidian distance between 2 echos thaks to spherical coordinatess
float d_spherical(XEchoIndex n1, XEchoIndex n2, XBlockIndex block_idx, XFloatAttrib* range, XFloatAttrib* theta, XFloatAttrib* phi);

// compute euclidian distance
double d2(XPt3D p1, XPt3D p2);
double d2(edge e1, XMls& mls);

// compute c0 regularity for 2 echos
double c0(XPt3D Pivot, XPt3D e1, XPt3D e2);

// compute c1 regularity for 3 echos
double c1(XPt3D e1, XPt3D e2, XPt3D e3);
double c1(edge e1, edge e2, XMls &mls);

// compute angle between 2 edges
double angle(XPt3D v1, XPt3D v2);

// convert edge in vector stored as xpt3d
XPt3D e2p (edge e, XMls &mls);

std::tuple<uint,uint,uint> e2t (edge e);
edge t2e (std::tuple<uint,uint,uint> t);

//Print echos linked to a given echo
void print_edges_by_echo(std::map<uint, std::vector<edge> > edges, XEchoIndex ei);

// Find if 2 edges have a common node
bool have_common_node(edge e1, edge e2);

// Find if 2 edges have a common node and return it
XEchoIndex find_common_node(edge e1, edge e2);

// Find if 3 edges form a triangle
bool is_triangle(edge e1, edge e2, edge e3);

// Find if a given echo is in maxflow output
bool find_echo(std::vector<std::pair<uint, uint> > maxflow, XPulseIndex pulse);

// Find whether an edge is in a vector of edges or not
bool find_edge(std::vector<edge> edges, edge e);
std::vector<edge>::iterator find_edge_it(std::vector<edge> edges, edge e);

// Check whether some particular points are in the cloud
bool check_points_existence (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i);
int  check_points_existence_multiblock (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i);
bool check_inferior_existence (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i);
bool check_superior_existence (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i);
bool check_next_existence (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i);

// Compute score and check whether edge should be kept or not
bool check_score (double c0, double c1_ijk, double c1_jkl, double tresh, double lambda);

// Do some stuff
void remove_edge(std::map<uint,std::vector<edge> > edges, edge e);

// Nodes of the graph are edges linking "neighbouring" echos
std::pair<std::vector<edge>,std::vector<double> > fill_Nodes(XMls & mls, param params);
std::pair<std::vector<edge>,std::vector<double> > fill_Nodes(XMls &mls, std::vector<std::vector<int> >vv_pulse_with_echo_idx, param params);
std::vector<edge> filtering(XMls &mls, param params, std::vector<std::vector<int> > vv_pulse_with_echo_idx,
                                                             double tresh, double lambda, bool length_tresh = false);
std::vector<edge> naive_filtering(XMls &mls, param params, std::vector<std::vector<int> > vv_pulse_with_echo_idx, double thresh);

// Compute c1 regularity and add it to "Nodes"
std::pair<std::vector<edge>,std::pair<std::vector<double>,std::vector<double> > > fill_c1(XMls & mls, param params, std::pair<std::vector<edge>,std::vector<double> > Nodes);

// edges of the graph corresponds to nodes with 1 common echo
std::vector<std::map<int, std::vector<std::pair<edge, double> > > > fill_edges(std::vector<edge> Nodes, XMls &mls, param params);
std::pair<std::vector<std::pair<edge, edge> >, std::vector<double> > fill_edges_vec(std::vector<edge> Nodes, XMls &mls, param params);

// Compute score knowing c0 and c1
std::vector<edge> score(std::pair<std::vector<edge>, std::vector<double> > pairN,
           std::vector<std::map<int, std::vector<std::pair<edge, double> > > > pairE,
           XMls &mls);

// Find triangles in the reconstruction, call it and pass output to write_ply function
std::vector<Triangle> compute_mesh(std::map<uint,std::vector<edge> > edges, XMls &mls);
// New one
std::vector<Triangle> compute_mesh(std::map<uint,std::vector<edge> > edges, XMls &mls, vector<vector<int> > vv_pulse_with_echo_idx, param params);
// Newest
std::vector<Triangle> compute_mesh(std::map<uint,std::vector<edge> > edges, XMls &mls, vector< vector<int> > vv_pulse_with_echo_idx);
//
std::tuple<std::vector<std::vector<Triangle> >, std::vector<std::tuple<uint, uint, uint> >, std::vector<std::vector<uint> > > compute_mesh_and_segments(std::vector<std::map<uint,std::vector<edge> > > data,XMls& mls);
std::tuple<std::vector<std::vector<Triangle> >*,std::vector<std::tuple<uint,uint,uint> >*,std::vector<std::vector<uint> >* >* compute_mesh_and_segments_fast(std::vector<std::map<uint,std::vector<edge> >* >* data,XMls& mls);

// Create graph for MaxFlow optimisation
// cf. Boykov & Kolmogorov software
// "An Experimental Comparison of Min-Cut/Max-Flow Algorithms for Energy Minimization in Vision."
// Yuri Boykov and Vladimir Kolmogorov.
// In IEEE Transactions on Pattern Analysis and Machine Intelligence (PAMI),
// September 2004
std::pair<std::vector<edge>, std::vector<edge> > create_graph(std::pair<std::vector<edge>, std::vector<double> > pair,
                                                 std::pair<std::vector<std::pair<SCR::edge,SCR::edge> >,std::vector<double> > pairE,
                                                 XMls &mls);
std::pair<std::vector<edge>, std::vector<edge> > create_graph(std::pair<std::vector<edge>, std::vector<double> > pairN,
                                                                std::vector<std::map<int, std::vector<std::pair<edge, double> > > > pairE,
                                                                XMls &mls);
}   // Namespace


#endif // EDGE_H

#ifndef MW_H
#define MW_H

#include "Wedge.h"

/// \file Mw.h

/// \brief custom class for managing maps of wedges
class Mw
{
public:
    std::map<uint,std::vector<Wedge> > M;

    Mw();
    Mw(std::vector<Wedge> vw);
    ~Mw(){}

    /// \brief iterators
    typedef std::map<uint,std::vector<Wedge> >::iterator iterator;
        iterator begin() { return M.begin(); }
        iterator end()   { return M.end();   }

    /*std::vector<Wedge>& at(uint i)
    {
        return M::const_iterator();
    }*/

    /// \brief access operator
    const std::vector<Wedge>& operator[](uint i) const
    {
        //return M[i];
        return M.at(i);
    }

};

#endif // MW_H

#ifndef MTRICOORDS_H
#define MTRICOORDS_H

#include "Triangle_Coords.h"

typedef TriangleCoords TC;
typedef std::vector<TC> VTC; // haha
typedef std::pair<XPt3D,XPt3D> pPP;

using namespace SCR;

struct cmpP {
	
	bool operator()(const XPt3D &p1, const XPt3D &p2) const
  {
		return p1.X < p2.X ? true : p1.Y < p2.Y ? true : p1.Z < p2.Z ? true: false;
	}
	
};


struct cmpMTC {
  bool operator()(const pPP &pp1, const pPP &pp2) const
    {
      XPt3D p11 = pp1.first;
      XPt3D p12 = pp1.second;
      XPt3D p21 = pp1.first;
      XPt3D p22 = pp1.second;

      XPt3D meanp1 = (p11.operator +=(p12));
      XPt3D meanp2 = (p21.operator +=(p22));

      /*if (meanp1.X < meanp2.X || meanp1.Y < meanp2.Y || meanp1.Z < meanp2.Z) std::cout << 1 << " " << std::flush;
	else std::cout << pp2.first.X << " " << pp2.second.X << " " << std::endl;*/

      return sqrt(meanp1.X*meanp1.X + meanp1.Y*meanp1.Y + meanp1.Z*meanp1.Z) < sqrt(meanp2.X*meanp2.X + meanp2.Y*meanp2.Y + meanp2.Z*meanp2.Z) ? true : false;
    }
};

class MTriCoords
{
 public:
  std::map<pPP,VTC,cmpMTC> M;
  
  MTriCoords();
  MTriCoords(VTC *vtc);
  ~MTriCoords(){}

  const VTC& operator[](pPP p) const
    {
      return M.at(p);
    }

  typedef std::map<pPP,VTC,cmpMTC>::iterator iterator;
  iterator begin() { return M.begin(); }
  iterator end()   { return M.end();   }
};

#endif MTRICOORDS_H

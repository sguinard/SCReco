#ifndef SEMANTICSEGMENTATION
#define SEMANTICSEGMENTATION

#include "Plane.h"
#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

using namespace SCR;

double mean_distance_point_to_plane(XMls &mls, std::vector<Plane> planes, param params);

double quadratic_distance_point_point(XPt3D p1, XPt3D p2);

#endif // SEMANTICSEGMENTATION


#ifndef POLYGON_H
#define POLYGON_H

#include "Plane.h"

namespace SCR{

class Polygon
{
private:
    vP3* points;

public:
    Polygon();
    Polygon(vP3* input_points);

    ~Polygon();
};

}

#endif // POLYGON_H

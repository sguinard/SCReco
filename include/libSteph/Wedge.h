#ifndef WEDGE
#define WEDGE

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "Mtri.h"

/// \file Wedge.h
///
/// \brief file containing the wedge class

/// @brief just to know where 2 wedges intersect
enum edge_number {Left, Right, Top, Bottom, None=-1};

/// @brief A Wedge is a merge of 2 adjacent triangles
///
/// @warning Points shall be ordered by acquisition order !!!
///
/// @remark edges of wedges are referenced as follow :
///
///                  top
///             e2---------e4
///             /          /
///            /          /
///     left  /          /  right
///          /          /
///         /          /
///        e1---------e3
///           bottom
///
class Wedge
{

public:
    //Triangle t1;
    //Triangle t2;
    //XPt3D p1;
    //XPt3D p2;
    //XPt3D p3;
    //XPt3D p4;
    /// \brief 1st node
    uint e1;
    /// \brief 2nd node
    uint e2;
    /// \brief 3rd node
    uint e3;
    /// \brief 4th node
    uint e4;

    Wedge();
    //Wedge(Triangle t1, Triangle t2);
    //Wedge(XPt3D p1, XPt3D p2, XPt3D p3, XPt3D p4);
    //Wedge(XMls &mls, XBlockIndex b, XEchoIndex e1, XEchoIndex e2, XEchoIndex e3, XEchoIndex e4);
    Wedge(XEchoIndex e1, XEchoIndex e2, XEchoIndex e3, XEchoIndex e4);

    ~Wedge();

    bool operator == (Wedge w);
    bool operator != (Wedge w);
    bool operator <(Wedge w);

    bool is_adjacent (Wedge w);
    edge_number place_adjacent(Wedge w);

    const void print() const;
};

bool find_wedge_in_vec(std::vector<Wedge> *vw, Wedge w);

std::vector<std::pair<Wedge, edge_number> > find_wedge(std::vector<Wedge> vw, Wedge w);

XPt3D compute_normal(XMls &mls, Wedge w);

XPt3D cross_product(XPt3D v1, XPt3D v2);


#endif // WEDGE


#ifndef PARAM
#define PARAM

#include <ctime>
#include <iostream>
#include <limits>
#include <stdio.h>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#pragma once

using namespace std;

namespace SCR{

typedef std::vector<XPt3D> vP3;
typedef vP3::iterator vP3_iter;

typedef std::vector<XPt2D> vP2;
typedef vP2::iterator vP2_iter;

typedef std::vector<vP3*> vvP3;
typedef vvP3::iterator vvP3_iter;

struct param
{
    string sbet_folder;
    string sbet_mission;
    string laser_calib;
    string ept_folder;
    string output, format;

    // optional
    float tri_threshold;
    float max_DP_error;
    XSecond i_start, i_end;
    int pivot_E, pivot_N, pivot_H;
    int add_rgb;

    // header
    string datetime_;
    param():tri_threshold(0.5), max_DP_error(0), i_start(-1), i_end(-1), pivot_E(-1), pivot_N(-1), pivot_H(0), add_rgb(false){}
    string Format()
    {
        string format = output.substr(output.size()-3,3);
        cout << "format: " << format << endl;
        return format;
    }
};

}

#endif // PARAM


#ifndef CUTPURSUIT_PLANE_H
#define CUTPURSUIT_PLANE_H

#include "Ransac.h"
#include "time.h"

#include "../extern/cut-pursuit-master/include/CutPursuit_Linear.h"
//#include "../extern/cut-pursuit-master/include/CutPursuit_L2.h"
//#include "../extern/cut-pursuit-master/include/Graph.h"
#include "../extern/cut-pursuit-master/include/Common.h"
#include "../extern/cut-pursuit-master/include/CutPursuit.h"


#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include <iostream>
#include <fstream>


namespace CP {

template<typename T>
class ComponentsFusionPlane
{//this class encode a potential fusion between two cadjacent component
 //and is ordered wrt the merge_gain
public:
    std::size_t comp1, comp2; //index of the components
    std::size_t border_index; //index of the border-edge
    T merge_gain; //gain obtained by mergeing the components
    Plane merged_plane; //value of the new components when they are merged
    ComponentsFusionPlane(std::size_t c1, std::size_t c2, std::size_t ind = 0, T gain = 0.)
    {
        this->comp1 = c1;
        this->comp2 = c2;
        this->border_index = ind;
        this->merge_gain = gain;
    }
};


template<typename T>
struct lessComponentsFusionPlane: public std::binary_function<ComponentsFusionPlane<T>, ComponentsFusionPlane<T>, bool>
{
    bool operator()(const ComponentsFusionPlane<T> lhs, const ComponentsFusionPlane<T> rhs) const
    {
        return lhs.merge_gain < rhs.merge_gain;
    }
};


template <typename T>
class CutPursuit_Plane : public CutPursuit_Linear<T>
{
    /// Data: points (uint)
    /// Observations: coordinates vector
    /// Value: projection of point on plane
public:
    ~CutPursuit_Plane(){
     };
    std::vector<std::vector<T>> componentVector;
        // only used with backward step - the sum of all observation in the component
    CutPursuit_Plane(uint32_t nbVertex = 1) : CutPursuit_Linear<T>(nbVertex)
    {
        this->componentVector  = std::vector<std::vector<T>>(1);
    }
    CutPursuit_Plane(const CutPursuit_Plane<T>& cpp)
    {
		this->main_graph     = cpp.main_graph;
        this->reduced_graph  = cpp.reduced_graph;
        this->components     = cpp.components;
	}

    // hack - just don't ask why
    //friend double Plane::x;//access_x(){double _x =x; return _x;}
    /*friend double access_y(){double _y =y; return _y;}
    friend double access_z(){double _z =z; return _z;}
    friend double access_d(){double _d =d; return _d;}*/


    //=============================================================================================
    //=============================     COMPUTE ENERGY  ===========================================
    //=============================================================================================
     std::pair<double,double> compute_energy_plane()
    {
        VertexAttributeMap<double> vertex_attribute_map
                = boost::get(boost::vertex_bundle, this->main_graph);
        EdgeAttributeMap<double> edge_attribute_map
                = boost::get(boost::edge_bundle, this->main_graph);
        //the first element pair_energy of is the fidelity and the second the penalty
        std::pair<double,double> pair_energy;
        double energy = 0;
        //std::cout << "DIMENSION: " << this->dim << std::endl;
        //#pragma omp parallel for private(i_dim) if (this->parameter.parallel) schedule(static) reduction(+:energy,i)
        for (uint32_t ind_ver = 0; ind_ver < this->nVertex; ind_ver++)
        {
            VertexDescriptor<double> i_ver = boost::vertex(ind_ver, this->main_graph);
            for(uint32_t i_dim=0; i_dim<this->dim; i_dim++)
            {
//                std::cout << "weight: " << vertex_attribute_map(i_ver).weight << " "
//                          << "observation: " << vertex_attribute_map(i_ver).observation[i_dim] << " "
//                          << "value: " << vertex_attribute_map(i_ver).value[i_dim] << std::endl;
                energy += .5*vertex_attribute_map(i_ver).weight
                        * pow(vertex_attribute_map(i_ver).observation[i_dim]
                            - vertex_attribute_map(i_ver).value[i_dim],2);
            }
        }
        pair_energy.first = energy;
        energy = 0;
        EdgeIterator<double> i_edg, i_edg_end = boost::edges(this->main_graph).second;
        for (i_edg = boost::edges(this->main_graph).first; i_edg != i_edg_end; i_edg++)
        {
            if (!edge_attribute_map(*i_edg).realEdge)
            {
                continue;
            }
            energy +=  edge_attribute_map(*i_edg).isActive * this->parameter.reg_strenth
                    * edge_attribute_map(*i_edg).weight;    // test other weighting on edges
        }
        pair_energy.second = energy;
	std::cout << "point energy: " << pair_energy.first << "\tedges energy: " << pair_energy.second << std::endl;
        return pair_energy;
    }



    //=============================================================================================
    //=============================     find_corner        =======================================
    //=============================================================================================
    std::pair<Plane, Plane> find_plane(const uint32_t & i_com)
    {
        // given a component will output the pairs of the two most likely labels
        VertexAttributeMap<double> vertex_attribute_map
                                    = boost::get(boost::vertex_bundle, this->main_graph);
        //std::vector<double> average_vector(this->dim,0);
        std::pair<Plane,Plane> planes2;
        Plane best_plane = Plane();
        Plane snd_best_plane = Plane();
        Plane plane1 = Plane();
        Plane plane2 = Plane();
        //vI* vp;

        double error = 0.1;

        //std::cout << "NUMBER OF POINTS IN COMPONENT: " << i_com << " = " << this->components[i_com].size() << std::endl;

        // select subset
        if (this->components[i_com].size() >= 3)    // if we can find enough points to define 2 independent planes
        {
            VertexIterator<double> ite_ver = std::get<0>(boost::vertices(this->main_graph));

            vvD* comp_obs = new vvD;
            for (int i = 0; i != this->components[i_com].size(); ++i)
            {
                comp_obs->emplace_back(std::vector<double> {vertex_attribute_map[this->components[i_com][*ite_ver+i]].observation[0],
                                                            vertex_attribute_map[this->components[i_com][*ite_ver+i]].observation[1],
                                                            vertex_attribute_map[this->components[i_com][*ite_ver+i]].observation[2] });
            }
//            clock_t start = clock();
            planes2 = ransac2_from_3d_points_unparallelized(comp_obs);
//            std::cout << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << std::endl;
            delete comp_obs;

        }

        //std::cout << best_plane_pts << " " << snd_best_plane_pts << std::endl;
        //best_plane.print();
//        planes2.first.print(); planes2.second.print();
        return planes2;
//        return std::make_pair(best_plane,snd_best_plane);
    }




    //=============================================================================================
    //=============================      COMPUTE CORNERS        ===================================
    //=============================================================================================
    inline void compute_corners_plane(std::vector< std::vector< Plane > > & corners)
    { //-----compute the 2 most populous labels------------------------------
         //#pragma omp parallel for if (this->parameter.parallel) schedule(dynamic)

        //std::cout << "NUMBER OF COMPONENTS: " << this->components.size() << std::endl;
        //std::cout << "NUMBER OF CORNERS: " << corners.size() << std::endl;

        for (int i=0; i != (int)this->components.size(); i++)
        {
            std::vector<Plane> vp;
            vp.emplace_back(Plane());
            vp.emplace_back(Plane());
            corners.emplace_back(vp);
        }

        for (uint32_t  i_com =0;i_com < this->components.size(); i_com++)
        {
            if (this->saturated_components[i_com])
            {
                continue;
            }
            std::pair<Plane, Plane> corners_pair = find_plane(i_com);
            corners[i_com][0] = corners_pair.first;
            corners[i_com][1] = corners_pair.second;

        }
        return;
    }




    //=============================================================================================
    //=============================       SET_CAPACITIES    =======================================
    //=============================================================================================
    inline void set_capacities_plane(const std::vector< std::vector< Plane > > & corners)
    {
        VertexDescriptor<double> desc_v;
        EdgeDescriptor   desc_source2v, desc_v2sink, desc_v2source;
        VertexAttributeMap<double> vertex_attribute_map
                = boost::get(boost::vertex_bundle, this->main_graph);
        EdgeAttributeMap<double> edge_attribute_map
                = boost::get(boost::edge_bundle, this->main_graph);
        double cost_B, cost_notB; //the cost of being in B or not B, local for each component
        //----first compute the capacity in sink/node edges------------------------------------
         //#pragma omp parallel for if (this->parameter.parallel) schedule(dynamic)
        for (uint32_t i_com = 0; i_com < this->components.size(); i_com++)
        {
            if (this->saturated_components[i_com])
            {
                //std::cout << "Component " << i_com << " is saturated." << std::endl;
                continue;
            }
            for (uint32_t i_ver = 0;  i_ver < this->components[i_com].size(); i_ver++)
            {
                desc_v    = this->components[i_com][i_ver];
                // because of the adjacency structure NEVER access edge (source,v) directly!
                desc_v2source = boost::edge(desc_v, this->source,this->main_graph).first;
                desc_source2v = edge_attribute_map(desc_v2source).edge_reverse; //use edge_reverse instead
                desc_v2sink   = boost::edge(desc_v, this->sink,this->main_graph).first;
                cost_B    = 0;
                cost_notB = 0;
                if (vertex_attribute_map(desc_v).weight==0)
                {
                    edge_attribute_map(desc_source2v).capacity = 0;
                    edge_attribute_map(desc_v2sink).capacity   = 0;
                    continue;
                }
                /// \todo quadratic distance to both planes
                XPt3D p;
                p.X = vertex_attribute_map[this->components[i_com][i_ver]].observation[0];
                p.Y = vertex_attribute_map[this->components[i_com][i_ver]].observation[1];
                p.Z = vertex_attribute_map[this->components[i_com][i_ver]].observation[2];
                cost_B    += corners[i_com][0].quadratic_distance_point_plane(p);
                //vertex_attribute_map(desc_v).observation[corners[i_com][0]]; // quadratic distance to plane 1
                cost_notB += corners[i_com][1].quadratic_distance_point_plane(p);
                //vertex_attribute_map(desc_v).observation[corners[i_com][1]]; // quadrativ distance to plane 2
                //std::cout << cost_B << " " << cost_notB << std::endl;
                if (cost_B>cost_notB)
                {
                    edge_attribute_map(desc_source2v).capacity = cost_B - cost_notB;
                    edge_attribute_map(desc_v2sink).capacity   = 0.;
                }
                else
                {
                    edge_attribute_map(desc_source2v).capacity = 0.;
                    edge_attribute_map(desc_v2sink).capacity   = cost_notB - cost_B;
                }
            }
        }
        //----then set the vertex to vertex edges ---------------------------------------------
        EdgeIterator<double> i_edg, i_edg_end;
        for (boost::tie(i_edg, i_edg_end) = boost::edges(this->main_graph);
             i_edg != i_edg_end; ++i_edg)
        {
            if (!edge_attribute_map(*i_edg).realEdge)
            {
                //std::cout << "Edge " << *i_edg << " is not a real edge." << std::endl;
                continue;
            }
            if (!edge_attribute_map(*i_edg).isActive)
            {
                //std::cout << "Edge " << *i_edg << " is a real edge, not activated. Its capacity is: " << edge_attribute_map(*i_edg).weight * this->parameter.reg_strenth << std::endl;
                edge_attribute_map(*i_edg).capacity
                        = edge_attribute_map(*i_edg).weight * this->parameter.reg_strenth;
            }
            else
            {
                //std::cout << "Edge " << *i_edg << " is a real edge, already activated. Its capacity is: 0" << std::endl;
                edge_attribute_map(*i_edg).capacity = 0;
            }
        }
    }


    //=============================================================================================
    //==============================  compute_connected_components=========================================
    //=============================================================================================
    void compute_connected_components_plane()
    {  //this function compute the connected components of the graph with active edges removed
        //the boolean vector indicating wether or not the edges and vertices have been seen already
        //the root is the first vertex of a component
        //this function is written such that the new components are appended at the end of components
        //this allows not to recompute saturated component
        VertexAttributeMap<T> vertex_attribute_map
            = boost::get(boost::vertex_bundle, this->main_graph);
        VertexIndexMap<T> vertex_index_map =get(boost::vertex_index, this->main_graph);
        //indicate which edges and nodes have been seen already by the dpsearch
        std::vector<bool> edges_seen (this->nEdge, false);
        std::vector<bool> vertices_seen (this->nVertex+2, false);
        vertices_seen[vertex_index_map(this->source)] = true;
        vertices_seen[vertex_index_map(this->sink)]   = true;
        //-------- start with the known roots------------------------------------------------------
        //#pragma omp parallel for if (this->parameter.parallel) schedule(dynamic)
        //int i=0;
        for (uint32_t ind_com = 0; ind_com < this->root_vertex.size(); ind_com++)
        {
            //std::cout << ind_com << " " << std::flush;
            VertexDescriptor<T> root = this->root_vertex[ind_com]; //the first vertex of the component
            if (this->saturated_components[ind_com])
            {   //this component is saturated, we don't need to recompute it
                for (uint32_t ind_ver = 0; ind_ver < this->components[ind_com].size(); ++ind_ver)
                {
                    vertices_seen[vertex_index_map(this->components[ind_com][ind_ver])] = true;
                    //std::cout << i++ << " " << std::flush;
                }
            }
            else
            {   //compute the new content of this component
                this->components.at(ind_com) = this->connected_comp_from_root(root, this->components.at(ind_com).size()
                                          , vertices_seen , edges_seen);
             }
        }

        //----now look for components that did not already exists----
        VertexIterator<T> ite_ver;
        std::cout << this->lastIterator -  boost::vertices(this->main_graph).first << std::endl;//int prout=0;
        for (ite_ver = boost::vertices(this->main_graph).first;
             ite_ver != this->lastIterator; ite_ver++)
        {
            if (vertices_seen[vertex_index_map(*ite_ver)])
            {
                 continue;
            } //this vertex is not currently in a connected component
            VertexDescriptor<T> root = *ite_ver; //we define it as the root of a new component
            uint32_t current_component_size =
                    this->components[vertex_attribute_map(root).in_component].size();
            this->components.push_back(
                    this->connected_comp_from_root(root, current_component_size
                  , vertices_seen, edges_seen));
            this->root_vertex.push_back(root);
            this->saturated_components.push_back(false);
        }
        this->components.shrink_to_fit();
    }


    //=============================================================================================
    //=============================        REDUCE       ===========================================
    //=============================================================================================
    void reduce_plane(std::vector< std::vector< Plane > > corners)
    {   //compute the reduced graph, and if need be performed a backward check
        this->compute_connected_components_plane();
        this->compute_corners_plane(corners);
        if (this->parameter.backward_step)
        {
            //compute the structure of the reduced graph
            this->compute_reduced_graph_plane(corners);
            //check for beneficial merges
            this->merge_plane();
        }
        else
        {   //compute only the value associated to each connected components
            this->compute_reduced_value_plane(corners);
        }
    }



    //=============================================================================================
    //================================  COMPUTE_REDUCE_GRAPH   ====================================
    //=============================================================================================
    void compute_reduced_graph_plane(std::vector< std::vector< Plane > > corners)
    {   //compute the adjacency structure between components as well as weight and value of each component
        //this is stored in the reduced graph structure
        EdgeAttributeMap<T> edge_attribute_map
            = boost::get(boost::edge_bundle, this->main_graph);
        VertexAttributeMap<T> vertex_attribute_map
            = boost::get(boost::vertex_bundle, this->main_graph);
        this->reduced_graph = Graph<T>(this->components.size());
        VertexAttributeMap<T> component_attribute_map = boost::get(boost::vertex_bundle, this->reduced_graph);
        //----fill the value sof the reduced graph----
        #pragma omp parallel for schedule(dynamic)
        for (uint32_t ind_com = 0;  ind_com < this->components.size(); ind_com++)
        {
            std::pair<std::vector<T>, T> component_values_and_weight = this->compute_value_plane(ind_com,corners);
            //----fill the value and weight field of the reduced graph-----------------------------
            VertexDescriptor<T> reduced_vertex = boost::vertex(ind_com, this->reduced_graph);
            component_attribute_map[reduced_vertex] = VertexAttribute<T>(this->dim);
            component_attribute_map(reduced_vertex).weight
                    = component_values_and_weight.second;
            for(uint32_t i_dim=0; i_dim<this->dim; i_dim++)
            {
                //std::cout << component_values_and_weight.first[i_dim] << " " << std::flush;
                component_attribute_map(reduced_vertex).value[i_dim]
                        = component_values_and_weight.first[i_dim];
                //std::cout << component_attribute_map(reduced_vertex).value[i_dim] << " " << std::flush;
            }//std::cout << std::endl;
        }
        //------compute the edges of the reduced graph
        EdgeAttributeMap<T> border_edge_attribute_map = boost::get(boost::edge_bundle, this->reduced_graph);
        this->borders.clear();
        EdgeDescriptor edge_current, border_edge_current;
        uint32_t ind_border_edge = 0, comp1, comp2, component_source, component_target;
        VertexDescriptor<T> source_component, target_component;
        bool reducedEdgeExists;
        typename boost::graph_traits<Graph<T>>::edge_iterator ite_edg, ite_edg_end;
        for (boost::tie(ite_edg,ite_edg_end) = boost::edges(this->main_graph); ite_edg !=  ite_edg_end; ++ite_edg)
        {
            if (!edge_attribute_map(*ite_edg).realEdge)
            {   //edges linking the source or edge node do not take part
                continue;
            }
            edge_current = *ite_edg;
            //compute the connected components of the source and target of current_edge
            comp1       = vertex_attribute_map(boost::source(edge_current, this->main_graph)).in_component;
            comp2       = vertex_attribute_map(boost::target(edge_current, this->main_graph)).in_component;
            if (comp1==comp2)
            {   //this edge links two nodes in the same connected component
                continue;
            }
            //by convention we note component_source the smallest index and
            //component_target the largest
            component_source  = std::min(comp1,comp2);
            component_target  = std::max(comp1,comp2);
            //retrieve the corresponding vertex in the reduced graph
            source_component = boost::vertex(component_source, this->reduced_graph);
            target_component = boost::vertex(component_target, this->reduced_graph);
            //try to add the border-edge linking those components in the reduced graph
            boost::tie(border_edge_current, reducedEdgeExists)
                    = boost::edge(source_component, target_component, this->reduced_graph);
            if (!reducedEdgeExists)
            {   //this border-edge did not already existed in the reduced graph
                //border_edge_current = boost::add_edge(source_component, target_component, this->reduced_graph).first;
                border_edge_current = boost::add_edge(source_component, target_component, this->reduced_graph).first;
                border_edge_attribute_map(border_edge_current).index  = ind_border_edge;
                border_edge_attribute_map(border_edge_current).weight = 0;
                ind_border_edge++;
                //create a new entry for the borders list containing this border
                this->borders.push_back(std::vector<EdgeDescriptor>(0));
            }
            //add the weight of the current edge to the weight of the border-edge
            border_edge_attribute_map(border_edge_current).weight += 0.5*edge_attribute_map(edge_current).weight;
            this->borders[border_edge_attribute_map(border_edge_current).index].push_back(edge_current);
        }
    }









     //=============================================================================================
    //================================          MERGE_RESPLIT          ====================================
    //=============================================================================================
    void merge_resplit_plane()
    {
        // TODO: right now we only do one loop through the heap of potential mergeing, and only
        //authorize one mergeing per component. We could update the gain and merge until it is no longer
        //beneficial
        //check wether the energy can be decreased by removing edges from the reduced graph
        //----load graph structure---
        VertexAttributeMap<T> vertex_attribute_map
                = boost::get(boost::vertex_bundle, this->main_graph);
        VertexAttributeMap<T> component_attribute_map
                = boost::get(boost::vertex_bundle, this->reduced_graph);
        EdgeAttributeMap<T> border_edge_attribute_map
                = boost::get(boost::edge_bundle, this->reduced_graph);
        EdgeAttributeMap<T> edge_attribute_map
                = boost::get(boost::edge_bundle, this->main_graph);
        VertexIndexMap<T> component_index_map = boost::get(boost::vertex_index, this->reduced_graph);
        std::map<uint32,Plane > new_comps_planes;
        //-----------------------------------
        EdgeDescriptor border_edge_current;
        typename boost::graph_traits<Graph<T>>::edge_iterator ite_border, ite_border_end;
        typename std::vector<EdgeDescriptor>::iterator ite_border_edge;
        VertexDescriptor<T> source_component, target_component;
        uint32_t ind_source_component, ind_target_component, border_edge_currentIndex;
        //gain_current is the vector of gains associated with each mergeing move
        std::vector<T> gain_current(boost::num_edges(this->reduced_graph));
        //we store in merge_queue the potential mergeing with a priority on the potential gain
        std::priority_queue<ComponentsFusionPlane<T>, std::vector<ComponentsFusionPlane<T>>, lessComponentsFusionPlane<T>> merge_queue;
        T gain; // the gain obtained by removing the border corresponding to the edge in the reduced graph



	/// RESPLIT_BEGIN

	std::vector<bool> to_resplit = std::vector<bool>(boost::num_vertices(this->reduced_graph),false);
	std::vector<uint32_t> ind_resplit;
	std::vector<XPt3D> centroids;
	std::map<std::pair<uint32_t, uint32_t>, double> edge_adjacency;
	
	/// RESPLIT_END


	
        for (boost::tie(ite_border,ite_border_end) = boost::edges(this->reduced_graph); ite_border !=  ite_border_end; ++ite_border)
        {
            //a first pass go through all the edges in the reduced graph and compute the gain obtained by
            //mergeing the corresponding vertices
            border_edge_current      = *ite_border;
            border_edge_currentIndex = border_edge_attribute_map(border_edge_current).index;
            //retrieve the two components corresponding to this border
            source_component = boost::source(border_edge_current, this->reduced_graph);
            target_component = boost::target(border_edge_current, this->reduced_graph);
            ind_source_component = component_index_map(source_component);
            ind_target_component = component_index_map(target_component);

            //----now compute the gain of mergeing those two components-----
            // compute the fidelity lost by mergeing the two connected components
            std::pair<Plane, T> merge_gain = compute_merge_gain_plane(source_component, target_component);
            // the second part is due to the removing of the border
            gain = merge_gain.second
                 + border_edge_attribute_map(border_edge_current).weight * this->parameter.reg_strenth;

            //std::cout << "gain : " << gain << std::endl;
            //mergeing_information store the indexes of the components as well as the edge index and the gain
            //in a structure ordered by the gain
            ComponentsFusionPlane<T> mergeing_information(ind_source_component, ind_target_component, border_edge_currentIndex, gain);
            //mergeing_information.merged_value = merge_gain.first;
            mergeing_information.merged_plane = merge_gain.first;

            new_comps_planes[ind_source_component] = mergeing_information.merged_plane;
            new_comps_planes[ind_target_component] = mergeing_information.merged_plane;






	    /// RESPLIT_BEGIN

	    if (to_resplit[ind_source_component] == false && to_resplit[ind_target_component] == false)
	      {
		to_resplit[ind_source_component] = true;
		to_resplit[ind_target_component] = true;
		ind_resplit.emplace_back(ind_source_component);
		ind_resplit.emplace_back(ind_target_component);
		std::cout << "Resplit " << ind_source_component << " and " << ind_target_component << std::endl;
	      }

	    /// RESPLIT_END


	    
//            std::cout << merge_gain.first[0] << " " << merge_gain.first[1] << " " << merge_gain.first[2] << std::endl;
            if (gain>0)
            {   //it is beneficial to merge those two components
                //we add them to the merge_queue
                merge_queue.push(mergeing_information);
                gain_current.at(border_edge_currentIndex) = gain;
            }
        }





	/// RESPLIT_BEGIN

	for (auto& ind_com: ind_resplit)
	  {
	    XPt3D centroid = XPt3D(0,0,0);
	    for (uint32_t ind_ver = 0; ind_ver < this->components[ind_com].size(); ++ind_ver)
                {
		  XPt3D old_point;
                    old_point.X =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[0];
                    old_point.Y =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[1];
                    old_point.Z =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[2];
		    centroid.operator +=(old_point);
		}
	    centroid.operator /=(this->components[ind_com].size());
	    centroids.emplace_back(centroid);
	  }

	std::ofstream ply_edges;
	ply_edges.open("../../_edges.ply");
	ply_edges << "ply\nformat ascii 1.0\ncomment adjacency edges to resplit\ncomment made by SG\nelement vertex ";
	ply_edges << centroids.size();
	ply_edges << "\nproperty float x\nproperty float y\nproperty float z\nelement edge ";
	ply_edges << centroids.size()/2;
	ply_edges << "\nproperty int vertex1\nproperty int vertex2\nend_header\n";

	for (auto& p3: centroids)
	  {
	    ply_edges << p3.X << " " << p3.Y << " " << p3.Z << "\n";
	  }
	for (uint32_t i = 0; i < ind_resplit.size(); i+=2)
	  {
	    ply_edges << i << " " << i+1 << "\n";
	  }
	
	ply_edges.close();

	/// RESPLIT_END



	/// RESPLIT_DUMMY_BEGIN

	/*	for (boost::tie(ite_border,ite_border_end) = boost::edges(this->reduced_graph); ite_border !=  ite_border_end; ++ite_border)
        {
            //a first pass go through all the edges in the reduced graph and compute the gain obtained by
            //mergeing the corresponding vertices
            border_edge_current      = *ite_border;
            border_edge_currentIndex = border_edge_attribute_map(border_edge_current).index;
            //retrieve the two components corresponding to this border
            source_component = boost::source(border_edge_current, this->reduced_graph);
            target_component = boost::target(border_edge_current, this->reduced_graph);
            ind_source_component = component_index_map(source_component);
            ind_target_component = component_index_map(target_component);

	    vvD* points = new vvD;
	    for (uint32_t ind_ver = 0; ind_ver < this->components[ind_source_component].size(); ++ind_ver)
                {
		  points->emplace_back(std::vector<double>{
				       vertex_attribute_map(this->components[ind_source_component][ind_ver]).observation[0],
				       vertex_attribute_map(this->components[ind_source_component][ind_ver]).observation[1],
					 vertex_attribute_map(this->components[ind_source_component][ind_ver]).observation[2]});
		}
	    for (uint32_t ind_ver = 0; ind_ver < this->components[ind_target_component].size(); ++ind_ver)
                {
		  points->emplace_back(std::vector<double>{
				       vertex_attribute_map(this->components[ind_target_component][ind_ver]).observation[0],
					 vertex_attribute_map(this->components[ind_target_component][ind_ver]).observation[1],
					 vertex_attribute_map(this->components[ind_target_component][ind_ver]).observation[2]});
		}
	    std::pair<Plane,Plane> planes = ransac2_from_3d_points_unparallelized(points);

	    for (uint32_t ind_ver = 0; ind_ver < this->components[ind_source_component].size(); ++ind_ver)
               {
		 XPt3D old_point;
                    old_point.X =   vertex_attribute_map(this->components[ind_source_component][ind_ver]).observation[0];
                    old_point.Y =   vertex_attribute_map(this->components[ind_source_component][ind_ver]).observation[1];
                    old_point.Z =   vertex_attribute_map(this->components[ind_source_component][ind_ver]).observation[2];
		    XPt3D projete =
		      planes.first.quadratic_distance_point_plane(old_point) < planes.second.quadratic_distance_point_plane(old_point) ? planes.first.project_point_plane(old_point) : planes.second.project_point_plane(old_point);
		    vertex_attribute_map(this->components[ind_source_component][ind_ver]).value[0] = projete.X;
		       vertex_attribute_map(this->components[ind_source_component][ind_ver]).value[1] = projete.Y;
		       vertex_attribute_map(this->components[ind_source_component][ind_ver]).value[2] = projete.Z;
		    
	       }

	    for (uint32_t ind_ver = 0; ind_ver < this->components[ind_target_component].size(); ++ind_ver)
               {
		 XPt3D old_point;
                    old_point.X =   vertex_attribute_map(this->components[ind_target_component][ind_ver]).observation[0];
                    old_point.Y =   vertex_attribute_map(this->components[ind_target_component][ind_ver]).observation[1];
                    old_point.Z =   vertex_attribute_map(this->components[ind_target_component][ind_ver]).observation[2];
		    XPt3D projete =
		      planes.first.quadratic_distance_point_plane(old_point) < planes.second.quadratic_distance_point_plane(old_point) ? planes.first.project_point_plane(old_point) : planes.second.project_point_plane(old_point);
		    vertex_attribute_map(this->components[ind_target_component][ind_ver]).value[0] = projete.X;
		       vertex_attribute_map(this->components[ind_target_component][ind_ver]).value[1] = projete.Y;
		       vertex_attribute_map(this->components[ind_target_component][ind_ver]).value[2] = projete.Z;
		    
	       }

	       }*/

	/// RESPLIT_DUMMY_END


	
//        std::cout << "GAIN: " << gain << std::endl;
        //----go through the priority queue of merges and perform them as long as it is beneficial---
        //is_merged indicate which components no longer exists because they have been merged with a neighboring component
	/*        std::vector<bool> is_merged(this->components.size(), false);
        //to_destroy indicates the components that are needed to be removed
        std::vector<bool> to_destroy(this->components.size(), false);
        while(merge_queue.size()>0)
        {   //loop through the potential mergeing and accept the ones that decrease the energy
            ComponentsFusionPlane<T> mergeing_information = merge_queue.top();
            if (mergeing_information.merge_gain<=0)
            {   //no more mergeing provide a gain in energy
                break;
            }
            merge_queue.pop();
            if (is_merged.at(mergeing_information.comp1) || is_merged.at(mergeing_information.comp2))
            {
                //at least one of the components have already been merged
                continue;
            }
            //---proceed with the fusion of comp1 and comp2----
            //add the vertices of comp2 to comp1
            this->components[mergeing_information.comp1].insert(this->components[mergeing_information.comp1].end()
                ,this->components[mergeing_information.comp2].begin(), this->components[mergeing_information.comp2].end());
            //if comp1 was saturated it might not be anymore
            this->saturated_components[mergeing_information.comp1] = false;
            //the new weight is the sum of both weights
            component_attribute_map(mergeing_information.comp1).weight
                           += component_attribute_map(target_component).weight;
            //the new value is already computed in mergeing_information
            //we deactivate the border between comp1 and comp2
            for (ite_border_edge = this->borders.at(mergeing_information.border_index).begin();
                ite_border_edge != this->borders.at(mergeing_information.border_index).end() ; ++ite_border_edge)
            {
                 edge_attribute_map(*ite_border_edge).isActive = false;
            }
            is_merged.at(mergeing_information.comp1)  = true;
            is_merged.at(mergeing_information.comp2)  = true;
            to_destroy.at(mergeing_information.comp2) = true;
        }
        //we now rebuild the vectors components, rootComponents and saturated_components
        std::vector<std::vector<VertexDescriptor<T>>> new_components;
        std::vector<VertexDescriptor<T>> new_root_vertex;
        std::vector<bool> new_saturated_components;
        uint32_t ind_new_component = 0;
        for (uint32_t ind_com = 0; ind_com < this->components.size(); ind_com++, ind_new_component++)
        {
            if (to_destroy.at(ind_com))
            {   //this component has been removed
                continue;
            }//this components is kept
            new_components.push_back(this->components.at(ind_com));
            new_root_vertex.push_back(this->root_vertex.at(ind_com));
            new_saturated_components.push_back(this->saturated_components.at(ind_com));
	    
//            new_comps_planes[ind_com].print();
            if (is_merged.at(ind_com))
            {   //we need to update the value of the vertex in this component
//                std::cout << "mergeing " << ind_com << " in " << ind_new_component << std::endl;
	      vP3* points = new vP3;
	       for (uint32_t ind_ver = 0; ind_ver < this->components[ind_com].size(); ++ind_ver)
                {
		  XPt3D old_point;
                    old_point.X =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[0];
                    old_point.Y =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[1];
                    old_point.Z =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[2];
		    points->emplace_back(old_point);
		}
	       Plane p = Plane(points);
                for (uint32_t ind_ver = 0; ind_ver < this->components[ind_com].size(); ++ind_ver)
                {
                    XPt3D old_point;
                    old_point.X =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[0];
                    old_point.Y =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[1];
                    old_point.Z =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[2];
                    XPt3D new_point = p.project_point_plane(old_point);
//std::cout << dist2(new_point, old_point) << " " << std::flush;
//std::cout << new_point.X << " " << old_point.X << " " << new_point.Y << " " << old_point.Y << " " << new_point.Z << " " << old_point.Z << std::endl;
                    if (operator !=(new_point,XPt3D(~0,~0,~0)))
                    {
                        vertex_attribute_map(this->components[ind_com][ind_ver]).value[0] = new_point.X;
                        vertex_attribute_map(this->components[ind_com][ind_ver]).value[1] = new_point.Y;
                        vertex_attribute_map(this->components[ind_com][ind_ver]).value[2] = new_point.Z;
                    }
                    vertex_attribute_map(this->components[ind_com][ind_ver]).in_component
                        = ind_new_component;//ind_com;
                }
            }
//            ind_new_component++;
        }
        this->components           = new_components;
        this->root_vertex          = new_root_vertex;
        this->saturated_components = new_saturated_components;*/
    }











    



    //=============================================================================================
    //================================          MERGE          ====================================
    //=============================================================================================
    void merge_plane()
    {
        // TODO: right now we only do one loop through the heap of potential mergeing, and only
        //authorize one mergeing per component. We could update the gain and merge until it is no longer
        //beneficial
        //check wether the energy can be decreased by removing edges from the reduced graph
        //----load graph structure---
        VertexAttributeMap<T> vertex_attribute_map
                = boost::get(boost::vertex_bundle, this->main_graph);
        VertexAttributeMap<T> component_attribute_map
                = boost::get(boost::vertex_bundle, this->reduced_graph);
        EdgeAttributeMap<T> border_edge_attribute_map
                = boost::get(boost::edge_bundle, this->reduced_graph);
        EdgeAttributeMap<T> edge_attribute_map
                = boost::get(boost::edge_bundle, this->main_graph);
        VertexIndexMap<T> component_index_map = boost::get(boost::vertex_index, this->reduced_graph);
        std::map<uint32,Plane > new_comps_planes;
        //-----------------------------------
        EdgeDescriptor border_edge_current;
        typename boost::graph_traits<Graph<T>>::edge_iterator ite_border, ite_border_end;
        typename std::vector<EdgeDescriptor>::iterator ite_border_edge;
        VertexDescriptor<T> source_component, target_component;
        uint32_t ind_source_component, ind_target_component, border_edge_currentIndex;
        //gain_current is the vector of gains associated with each mergeing move
        std::vector<T> gain_current(boost::num_edges(this->reduced_graph));
        //we store in merge_queue the potential mergeing with a priority on the potential gain
        std::priority_queue<ComponentsFusionPlane<T>, std::vector<ComponentsFusionPlane<T>>, lessComponentsFusionPlane<T>> merge_queue;
        T gain; // the gain obtained by removing the border corresponding to the edge in the reduced graph

        for (boost::tie(ite_border,ite_border_end) = boost::edges(this->reduced_graph); ite_border !=  ite_border_end; ++ite_border)
        {
            //a first pass go through all the edges in the reduced graph and compute the gain obtained by
            //mergeing the corresponding vertices
            border_edge_current      = *ite_border;
            border_edge_currentIndex = border_edge_attribute_map(border_edge_current).index;
            //retrieve the two components corresponding to this border
            source_component = boost::source(border_edge_current, this->reduced_graph);
            target_component = boost::target(border_edge_current, this->reduced_graph);
            ind_source_component = component_index_map(source_component);
            ind_target_component = component_index_map(target_component);

            //----now compute the gain of mergeing those two components-----
            // compute the fidelity lost by mergeing the two connected components
            std::pair<Plane, T> merge_gain = compute_merge_gain_plane(source_component, target_component);
            // the second part is due to the removing of the border
            gain = merge_gain.second
                 + border_edge_attribute_map(border_edge_current).weight * this->parameter.reg_strenth;

            //std::cout << "gain : " << gain << std::endl;
            //mergeing_information store the indexes of the components as well as the edge index and the gain
            //in a structure ordered by the gain
            ComponentsFusionPlane<T> mergeing_information(ind_source_component, ind_target_component, border_edge_currentIndex, gain);
            //mergeing_information.merged_value = merge_gain.first;
            mergeing_information.merged_plane = merge_gain.first;

            new_comps_planes[ind_source_component] = mergeing_information.merged_plane;
            new_comps_planes[ind_target_component] = mergeing_information.merged_plane;
//            std::cout << merge_gain.first[0] << " " << merge_gain.first[1] << " " << merge_gain.first[2] << std::endl;
            if (gain>0)
            {   //it is beneficial to merge those two components
                //we add them to the merge_queue
                merge_queue.push(mergeing_information);
                gain_current.at(border_edge_currentIndex) = gain;
            }
        }
//        std::cout << "GAIN: " << gain << std::endl;
        //----go through the priority queue of merges and perform them as long as it is beneficial---
        //is_merged indicate which components no longer exists because they have been merged with a neighboring component
        std::vector<bool> is_merged(this->components.size(), false);
        //to_destroy indicates the components that are needed to be removed
        std::vector<bool> to_destroy(this->components.size(), false);
        while(merge_queue.size()>0)
        {   //loop through the potential mergeing and accept the ones that decrease the energy
            ComponentsFusionPlane<T> mergeing_information = merge_queue.top();
            if (mergeing_information.merge_gain<=0)
            {   //no more mergeing provide a gain in energy
                break;
            }
            merge_queue.pop();
            if (is_merged.at(mergeing_information.comp1) || is_merged.at(mergeing_information.comp2))
            {
                //at least one of the components have already been merged
                continue;
            }
            //---proceed with the fusion of comp1 and comp2----
            //add the vertices of comp2 to comp1
            this->components[mergeing_information.comp1].insert(this->components[mergeing_information.comp1].end()
                ,this->components[mergeing_information.comp2].begin(), this->components[mergeing_information.comp2].end());
            //if comp1 was saturated it might not be anymore
            this->saturated_components[mergeing_information.comp1] = false;
            //the new weight is the sum of both weights
            component_attribute_map(mergeing_information.comp1).weight
                           += component_attribute_map(target_component).weight;
            //the new value is already computed in mergeing_information
            //we deactivate the border between comp1 and comp2
            for (ite_border_edge = this->borders.at(mergeing_information.border_index).begin();
                ite_border_edge != this->borders.at(mergeing_information.border_index).end() ; ++ite_border_edge)
            {
                 edge_attribute_map(*ite_border_edge).isActive = false;
            }
            is_merged.at(mergeing_information.comp1)  = true;
            is_merged.at(mergeing_information.comp2)  = true;
            to_destroy.at(mergeing_information.comp2) = true;
        }
        //we now rebuild the vectors components, rootComponents and saturated_components
        std::vector<std::vector<VertexDescriptor<T>>> new_components;
        std::vector<VertexDescriptor<T>> new_root_vertex;
        std::vector<bool> new_saturated_components;
        uint32_t ind_new_component = 0;
        for (uint32_t ind_com = 0; ind_com < this->components.size(); ind_com++, ind_new_component++)
        {
            if (to_destroy.at(ind_com))
            {   //this component has been removed
                continue;
            }//this components is kept
            new_components.push_back(this->components.at(ind_com));
            new_root_vertex.push_back(this->root_vertex.at(ind_com));
            new_saturated_components.push_back(this->saturated_components.at(ind_com));
//            new_comps_planes[ind_com].print();
            if (is_merged.at(ind_com))
            {   //we need to update the value of the vertex in this component
//                std::cout << "mergeing " << ind_com << " in " << ind_new_component << std::endl;
	      vP3* points = new vP3;
	       for (uint32_t ind_ver = 0; ind_ver < this->components[ind_com].size(); ++ind_ver)
                {
		  XPt3D old_point;
                    old_point.X =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[0];
                    old_point.Y =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[1];
                    old_point.Z =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[2];
		    points->emplace_back(old_point);
		}
	       Plane p = Plane(points);
                for (uint32_t ind_ver = 0; ind_ver < this->components[ind_com].size(); ++ind_ver)
                {
                    XPt3D old_point;
                    old_point.X =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[0];
                    old_point.Y =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[1];
                    old_point.Z =   vertex_attribute_map(this->components[ind_com][ind_ver]).observation[2];
                    XPt3D new_point = p.project_point_plane(old_point);
//std::cout << dist2(new_point, old_point) << " " << std::flush;
//std::cout << new_point.X << " " << old_point.X << " " << new_point.Y << " " << old_point.Y << " " << new_point.Z << " " << old_point.Z << std::endl;
                    if (operator !=(new_point,XPt3D(~0,~0,~0)))
                    {
                        vertex_attribute_map(this->components[ind_com][ind_ver]).value[0] = new_point.X;
                        vertex_attribute_map(this->components[ind_com][ind_ver]).value[1] = new_point.Y;
                        vertex_attribute_map(this->components[ind_com][ind_ver]).value[2] = new_point.Z;
                    }
                    vertex_attribute_map(this->components[ind_com][ind_ver]).in_component
                        = ind_new_component;//ind_com;
                }
            }
//            ind_new_component++;
        }
        this->components           = new_components;
        this->root_vertex          = new_root_vertex;
        this->saturated_components = new_saturated_components;
    }




    //=============================================================================================
    //================================  COMPUTE_REDUCE_VALUE  ====================================
    //=============================================================================================
    void compute_reduced_value_plane(std::vector< std::vector< Plane > > corners)
    {//std::cout << "Connected components: " << this->components.size() << std::endl;
        //vP3 vpp;
        for (uint32_t ind_com = 0;  ind_com < this->components.size(); ind_com++)
        {   //compute the reduced value of each component
            compute_value_plane(ind_com,corners);
        }
        //StorePlanes(vpp,params,mls);
    }



    //=============================================================================================
    //=================================   COMPUTE_VALUE   =========================================
    //=============================================================================================
    std::pair<std::vector<double>, double> compute_value_plane(const uint32_t & i_com, std::vector< std::vector< Plane > > corners)
    {

        //XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
        VertexAttributeMap<double> vertex_attribute_map
                                    = boost::get(boost::vertex_bundle, this->main_graph);

//        if (i_com == 0)
//        {  // we allocate the space necessary for the component vector at the first read of the component
//           this-> componentVector = std::vector<std::vector<T>>(this->components.size());
//        }
        std::vector<double> average_vector(this->dim), component_value(this->dim);
        double total_weight = 0;
        for(uint32_t i_dim=0; i_dim < this->dim; i_dim++)
        {
            average_vector[i_dim] = 0;
        }

        if (i_com >= corners.size())
        {
            for (int i=0; i != (int)i_com; i++)
            {
                std::vector<Plane> vp;
                vp.emplace_back(Plane());
                vp.emplace_back(Plane());
                corners.emplace_back(vp);
            }
        }

            //std::cout << i_com << " " << corners.size() << " " << this->components.size() << std::endl;
            int plane1 = 0;
            int plane2 = 0;

            for (uint32_t ind_ver = 0; ind_ver < this->components[i_com].size(); ++ind_ver)
            {
                //corners[i_com][0].print();
                XPt3D point;
                point.X = vertex_attribute_map[this->components[i_com][ind_ver]].observation[0];
                point.Y = vertex_attribute_map[this->components[i_com][ind_ver]].observation[1];
                point.Z = vertex_attribute_map[this->components[i_com][ind_ver]].observation[2];
                if (corners[i_com][0].is_valid() && corners[i_com][1].is_valid() ) corners[i_com][0].quadratic_distance_point_plane(point) < corners[i_com][1].quadratic_distance_point_plane(point) ? ++plane1 : ++plane2;
                else if (corners[i_com][0].is_valid()) ++plane1;
                else if (corners[i_com][1].is_valid()) ++plane2;
                std::vector<double> projete = corners[i_com][0].quadratic_distance_point_plane(point) < corners[i_com][1].quadratic_distance_point_plane(point) ?
                                corners[i_com][0].project_vector_plane(point) : corners[i_com][1].project_vector_plane(point);
//std::cout << projete[0] << " " << point.X << " " << projete[1] << " " << point.Y << " " << projete[2] << " " << point.Z << std::endl;
                for(uint32_t i_dim=0; i_dim < this->dim; i_dim++)
                { // projeté du point dans le plan

                    component_value[i_dim] = projete[i_dim];
//vertex_attribute_map(this->components[i_com][ind_ver]).value[i_dim] = projete[i_dim];
                    average_vector[i_dim] = vertex_attribute_map[this->components[i_com][ind_ver]].observation[i_dim]
                                    *  vertex_attribute_map[this->components[i_com][ind_ver]].weight;
                }

                total_weight += vertex_attribute_map[this->components[i_com][ind_ver]].weight;
                vertex_attribute_map(this->components[i_com][ind_ver]).in_component = i_com;
            }


			vP3* points = new vP3;
			for (uint32_t ind_ver = 0; ind_ver < this->components[i_com].size(); ind_ver++)
                {
                    XPt3D point;
                    point.X = vertex_attribute_map[this->components[i_com][ind_ver]].observation[0];
                    point.Y = vertex_attribute_map[this->components[i_com][ind_ver]].observation[1];
                    point.Z = vertex_attribute_map[this->components[i_com][ind_ver]].observation[2];
                    points->emplace_back(point);
                }
            plane1 > plane2 ? corners[i_com][0] = Plane(points) : corners[i_com][1] = Plane(points);
			
			
            if (plane1 > plane2)
            {
                for (uint32_t ind_ver = 0; ind_ver < this->components[i_com].size(); ind_ver++)
                {
                    XPt3D point;
                    point.X = vertex_attribute_map[this->components[i_com][ind_ver]].observation[0];
                    point.Y = vertex_attribute_map[this->components[i_com][ind_ver]].observation[1];
                    point.Z = vertex_attribute_map[this->components[i_com][ind_ver]].observation[2];
                    std::vector<double> projete = corners[i_com][0].project_vector_plane(point);
                    XPt3D proj = corners[i_com][0].project_point_plane(point);
                    /*if (i_com == 0)
                    {
                        corners[i_com][0].print();
                        std::cout << projete[0] - point.X << " " << projete[1] - point.Y << " " << projete[2] - point.Z << std::endl;
                    }*/
//                    std::cout << "plane 1 " << projete[0] /*- point.X*/ << " " << projete[1] /*- point.Y*/ << " " << projete[2] /*- point.Z*/ << std::endl; // OK HERE
                    if (proj != XPt3D(~0,~0,~0))
                    for(uint32_t i_dim=0; i_dim<this->dim; i_dim++)
                    {
                        component_value[i_dim] = projete[i_dim];
                        //if (corners[i_com][0].is_valid())
                        vertex_attribute_map(this->components[i_com][ind_ver]).value[i_dim] = projete[i_dim];
//                        std::cout << i_com << " " << ind_ver << " " << i_dim << " " << vertex_attribute_map(this->components[i_com][ind_ver]).value[i_dim] << " " << std::endl;
                    }
                }
            }
            else
            {
                for (uint32_t ind_ver = 0; ind_ver < this->components[i_com].size(); ind_ver++)
                {
                    XPt3D point;
                    point.X = vertex_attribute_map[this->components[i_com][ind_ver]].observation[0];
                    point.Y = vertex_attribute_map[this->components[i_com][ind_ver]].observation[1];
                    point.Z = vertex_attribute_map[this->components[i_com][ind_ver]].observation[2];
                    std::vector<double> projete = corners[i_com][1].project_vector_plane(point);
                    XPt3D proj = corners[i_com][1].project_point_plane(point);
                    /*if (i_com == 0)
                    {
                        corners[i_com][1].print();
                        std::cout << projete[0] - point.X << " " << projete[1] - point.Y << " " << projete[2] - point.Z << std::endl;
                    }*/
//                    std::cout << "plane 2 " << projete[0] /*- point.X*/ << " " << projete[1] /*- point.Y*/ << " " << projete[2] /*- point.Z*/ << std::endl;
                    if (proj != XPt3D(~0,~0,~0))
                    for(uint32_t i_dim=0; i_dim<this->dim; i_dim++)
                    {
                        component_value[i_dim] = projete[i_dim];
                        //if (corners[i_com][1].is_valid())
                        vertex_attribute_map(this->components[i_com][ind_ver]).value[i_dim] = projete[i_dim];
//                        std::cout << vertex_attribute_map(this->components[i_com][ind_ver]).value[i_dim] << " " << std::flush;
//                        std::cout << i_com << " " << ind_ver << " " << i_dim << " " << vertex_attribute_map(this->components[i_com][ind_ver]).value[i_dim] << " " << std::endl;
                    }
//                    std::cout << std::endl;
                }
            }

		corners[i_com].clear();
		corners[i_com] = std::vector<Plane>(1,Plane(points));
		delete points;

        return std::pair<std::vector<double>, double>(component_value, total_weight);
    }



    //=============================================================================================
    //=============================        SPLIT        ===========================================
    //=============================================================================================
    std::pair<uint32_t,std::vector< std::vector< Plane > > > split_plane()
    { // split the graph by trying to find the best binary partition
      // each components is split into B and notB
        uint32_t saturation;
        //initialize h_1 and h_2 with kmeans
        //--------initilializing labels------------------------------------------------------------
        //corner contains the two most likely class for each component
        std::vector< std::vector< Plane > > corners =
                std::vector< std::vector< Plane > >(this->components.size(),
                std::vector< Plane >(2,Plane()));
        //std::cout << this->components.size() << " components" << std::endl;
        this->compute_corners_plane(corners);
        this->set_capacities_plane(corners);
        //compute flow
        boost::boykov_kolmogorov_max_flow(
                   this->main_graph,
                   get(&EdgeAttribute<T>::capacity        , this->main_graph),
                   get(&EdgeAttribute<T>::residualCapacity, this->main_graph),
                   get(&EdgeAttribute<T>::edge_reverse     , this->main_graph),
                   get(&VertexAttribute<T>::color         , this->main_graph),
                   get(boost::vertex_index                , this->main_graph),
                   this->source,
                   this->sink);
        saturation = this->activate_edges();
        //std::cout << saturation << " saturation" << std::endl;
        //std::cout << this->components.size() << " components" << std::endl;
        return std::make_pair(saturation,corners);
    }


    //=============================================================================================
    //=================================   COMPUTE_MERGE_GAIN   =========================================
    //=============================================================================================
    std::pair<Plane, T> compute_merge_gain_plane(const VertexDescriptor<T> & comp1, const VertexDescriptor<T> & comp2)
    {
        VertexAttributeMap<T> vertex_attribute_map
                = boost::get(boost::vertex_bundle, this->main_graph);
        //VertexIndexMap<T> reduced_vertex_vertex_index_map = get(boost::vertex_index, this->reduced_graph);
        std::vector<T> merge_value(this->dim), mergedVector(this->dim);
        T gain = 0;

        Plane best_plane = Plane();

        //std::cout << this->components[comp1].size() << " " << this->components[comp2].size() << std::endl;


        if (this->components[comp1].size() + this->components[comp2].size() >= 6)    // if we can find enough points to define 2 independent planes
        {

//std::cout << "Je suis l'alpha" << std::flush;
            VertexIterator<double> ite_ver = std::get<0>(boost::vertices(this->main_graph));

            vP3* comp_obs = new vP3;
            for (int i = 0; i != (int)this->components[comp1].size(); i++)   // create vector of 3d points
            {
                XPt3D fuck_it;
                fuck_it.X = vertex_attribute_map[this->components[comp1][*ite_ver+i]].observation[0];
                fuck_it.Y = vertex_attribute_map[this->components[comp1][*ite_ver+i]].observation[1];
                fuck_it.Z = vertex_attribute_map[this->components[comp1][*ite_ver+i]].observation[2];
                comp_obs->emplace_back(fuck_it);
            }
            for (int i = 0; i != (int)this->components[comp2].size(); i++)   // create vector of 3d points
            {
                XPt3D fuck_it;
                fuck_it.X = vertex_attribute_map[this->components[comp2][*ite_ver+i]].observation[0];
                fuck_it.Y = vertex_attribute_map[this->components[comp2][*ite_ver+i]].observation[1];
                fuck_it.Z = vertex_attribute_map[this->components[comp2][*ite_ver+i]].observation[2];
                comp_obs->emplace_back(fuck_it);
            }
//std::cout << " et l'omega," << std::flush;

            best_plane = Plane(comp_obs);
            delete comp_obs;
        }
//int i=0;
//        std::cout << " le premier et le dernier," << std::flush;
        for (uint32_t i_ver = 0;  i_ver < this->components[comp1].size(); i_ver++)
        {
            VertexIterator<double> ite_ver = std::get<0>(boost::vertices(this->main_graph));

            XPt3D p;
            p.X = vertex_attribute_map[this->components[comp1][*ite_ver+i_ver]].observation[0];
            p.Y = vertex_attribute_map[this->components[comp1][*ite_ver+i_ver]].observation[1];
            p.Z = vertex_attribute_map[this->components[comp1][*ite_ver+i_ver]].observation[2];

            XPt3D merge = //best_plane.quadratic_distance_point_plane(p) < snd_best_plane.quadratic_distance_point_plane(p) ?
                       best_plane.project_point_plane(p);// : snd_best_plane.project_point_plane(p);

            XPt3D value;
            value.X = vertex_attribute_map[this->components[comp1][*ite_ver+i_ver]].observation[0];
            value.Y = vertex_attribute_map[this->components[comp1][*ite_ver+i_ver]].observation[1];
            value.Z = vertex_attribute_map[this->components[comp1][*ite_ver+i_ver]].observation[2];
//std::cout << merge.X << " " << p.X << " " << merge.Y << " " << p.Y << " " << merge.Z << " " << p.Z << std::endl;

            //if (operator !=(merge,p)) merge = value;
			if (operator !=(merge,XPt3D(~0,~0,~0)))
            gain += dist2(value,p) - dist2(merge,p);
//            std::cout << dist2(value,p) << " " << dist2(merge,p) << " - " << std::flush;
        }
//        std::cout << " le commencement et la fin." << std::endl;
        //i = 0;
        for (uint32_t i_ver = 0;  i_ver < this->components[comp2].size(); i_ver++)
        {
            VertexIterator<double> ite_ver = std::get<0>(boost::vertices(this->main_graph));
            XPt3D p;
            p.X = vertex_attribute_map[this->components[comp2][*ite_ver+i_ver]].observation[0];
            p.Y = vertex_attribute_map[this->components[comp2][*ite_ver+i_ver]].observation[1];
            p.Z = vertex_attribute_map[this->components[comp2][*ite_ver+i_ver]].observation[2];

            XPt3D merge = //best_plane.quadratic_distance_point_plane(p) < snd_best_plane.quadratic_distance_point_plane(p) ?
                       best_plane.project_point_plane(p);// : snd_best_plane.project_point_plane(p);

            XPt3D value;
            value.X = vertex_attribute_map[this->components[comp2][*ite_ver+i_ver]].observation[0];
            value.Y = vertex_attribute_map[this->components[comp2][*ite_ver+i_ver]].observation[1];
            value.Z = vertex_attribute_map[this->components[comp2][*ite_ver+i_ver]].observation[2];
//std::cout << merge.X << " " << p.X << " " << merge.Y << " " << p.Y << " " << merge.Z << " " << p.Z << std::endl;

            //if (operator !=(merge,p)) merge = value;
			if (operator !=(merge,XPt3D(~0,~0,~0)))
            gain += dist2(value,p) - dist2(merge,p);
//            std::cout << dist2(value,p) << " " << dist2(merge,p) << " - " << std::flush;
        }
//std::cout << "Apocalypse 22:13" << std::endl;

//        best_plane.print();
//std::cout << gain << std::endl;
        return std::pair<Plane, T>(best_plane, gain);
    }




    void initialize_plane(std::vector<uint32> inComponent)
    {
        this->parameter.stopping_ratio = 1e-8;
        uint nb_comps_default = *std::max_element(inComponent.begin(),inComponent.end())+1;
        //build the reduced graph with one component, fill the first vector of components
        //and add the sink and source nodes
        VertexIterator<T> ite_ver, ite_ver_end;
        EdgeAttributeMap<T> edge_attribute_map
            = boost::get(boost::edge_bundle, this->main_graph);
        //std::cout << *std::max_element(inComponent.begin(),inComponent.end()) << std::endl;
        this->components.resize(nb_comps_default);//this->components[0]  = std::vector<VertexDescriptor<T>> (0);//(this->nVertex);
        this->root_vertex.resize(nb_comps_default);
        this->saturated_components.resize(nb_comps_default);
        for (int i = 0; i != nb_comps_default; ++i) this->saturated_components.at(i) = false;
        //this->root_vertex[0] = *boost::vertices(this->main_graph).first;
        this->nVertex = boost::num_vertices(this->main_graph);
        this->nEdge   = boost::num_edges(this->main_graph);
        int i=0;
        //--------compute the first reduced graph----------------------------------------------------------
        std::vector<bool> comp_seen {nb_comps_default,false};
        for (int i = 0; i != nb_comps_default; ++i) comp_seen[i] = false;
        for (boost::tie(ite_ver, ite_ver_end) = boost::vertices(this->main_graph);
             ite_ver != ite_ver_end; ++ite_ver, ++i)
        {
            if (!comp_seen[inComponent[i]])
            {
                this->root_vertex.at(inComponent[i]) = *ite_ver;
                comp_seen[inComponent[i]] = true;
            }
            this->components[inComponent[i]].emplace_back(*ite_ver);
        }
        this->lastIterator = ite_ver;
        VertexAttributeMap<double> vertex_attribute_map = boost::get(boost::vertex_bundle, this->main_graph);

        std::vector<std::vector<Plane> > vplane (nb_comps_default,std::vector<Plane>(2,Plane()));
        for (int i = 0; i != this->components.size(); ++i)
        {
            vP3* points_in_comp = new vP3;
            for (uint32_t ind_ver = 0; ind_ver < this->components[i].size(); ++ind_ver)
            {
                //corners[i_com][0].print();
                XPt3D point;
                point.X = vertex_attribute_map[this->components[i][ind_ver]].observation[0];
                point.Y = vertex_attribute_map[this->components[i][ind_ver]].observation[1];
                point.Z = vertex_attribute_map[this->components[i][ind_ver]].observation[2];
                points_in_comp->emplace_back(point);
            }
            Plane p = Plane(points_in_comp);
            vplane[i] = std::vector<Plane> {p,p};
            delete points_in_comp;
            //this->compute_value_plane(i,Plane(points_in_comp));    // with best plane
        }
        for (int i = 0; i != this->components.size(); ++i)
        {
            this->compute_value_plane(i,vplane);
        }
        for (int i = 0; i != this->components.size(); ++i) std::cout << this->root_vertex[i] << " " << std::flush;
        std::cout << std::endl;
        //--------build the link to source and sink--------------------------------------------------------
        this->source = boost::add_vertex(this->main_graph);
        this->sink   = boost::add_vertex(this->main_graph);
        uint32_t eIndex = boost::num_edges(this->main_graph);
        ite_ver = boost::vertices(this->main_graph).first;
        for (uint32_t ind_ver = 0;  ind_ver < this->nVertex ; ind_ver++)
        {
            // note that source and edge will have many nieghbors, and hence boost::edge should never be called to get
            // the in_edge. use the out_edge and then reverse_Edge
            addDoubledge<T>(this->main_graph, this->source, boost::vertex(ind_ver, this->main_graph), 0.,
                         eIndex, edge_attribute_map , false);
            eIndex +=2;
            addDoubledge<T>(this->main_graph, boost::vertex(ind_ver, this->main_graph), this->sink, 0.,
                         eIndex, edge_attribute_map, false);
            eIndex +=2;
            ++ite_ver;
        }

    }




    std::pair<std::vector<T>, std::vector<T>> run_plane(std::vector<uint32> inComponent)
    {
        //first initilialize the structure
        this->initialize_plane(inComponent);
        if (this->parameter.verbose > 0)
        {
            std::cout << "Graph "  << boost::num_vertices(this->main_graph) << " vertices and "
             <<   boost::num_edges(this->main_graph)  << " edges and observation of dimension "
             << this->dim << '\n';
        }
        T energy_zero = this->compute_energy_plane().first; //energy with 1 component
        T old_energy = energy_zero; //energy at the previous iteration
        //vector with time and energy, useful for benchmarking
        std::vector<T> energy_out(this->parameter.max_ite_main ),time_out(this->parameter.max_ite_main);
        TimeStack ts; ts.tic();
        //the main loop
        for (uint32_t ite_main = 1; ite_main <= this->parameter.max_ite_main; ite_main++)
        {
	  //CutPursuit_Plane cpp_save = *this;
            std::cout << this->nVertex << " points" << std::endl;
            //--------those two lines are the whole iteration-------------------------
            std::pair<uint32_t,std::vector< std::vector< Plane > > > split = this->split_plane(); //compute optimal binary partition
            uint32_t saturation = split.first;
            std::vector< std::vector< Plane > > corners = split.second;
            this->reduce_plane(corners); //compute the new reduced graph
            //-------end of the iteration - rest is stopping check and display------
            
            std::pair<T,T> energy = this->compute_energy_plane();
            energy_out.push_back((energy.first + energy.second));
            time_out.push_back(ts.tocDouble());
            if (this->parameter.verbose > 1)
            {
                printf("Iteration %3i - %4i components - ", ite_main, (int)this->components.size());
                printf("Saturation %5.1f %% - ",100*saturation / (double) this->nVertex);
                switch (this->parameter.fidelity)
                {
                    case L2:
                    {
                        printf("Quadratic Energy %4.3f %% - ", 100 * (energy.first + energy.second) / energy_zero);
                        break;
                    }
                    case linear:
                    {
                        printf("Linear Energy %10.1f - ", energy.first + energy.second);
                        break;
                    }
                    case KL:
                    {
                        printf("KL Energy %4.3f %% - ", 100 * (energy.first + energy.second) / energy_zero);
                        break;
                    }
                }
                std::cout << "Timer  " << ts.toc() << std::endl;
            }
            //std::cout << "coucou" << std::endl;
            //----stopping checks-----
            if (saturation == (double) this->nVertex)
            {   //all components are saturated
                if (this->parameter.verbose > 1)
                {
                    std::cout << "All components are saturated" << std::endl;
                }
		//compute the structure of the reduced graph
            this->compute_reduced_graph_plane(corners);
            //check for beneficial merges
            //this->merge_resplit_plane();
                store_CP_planes(corners);
                break;
            }
            if ((old_energy - energy.first - energy.second) / (energy_zero - energy.first - energy.second)
               < this->parameter.stopping_ratio)
            {   //relative energy progress stopping criterion
				//*this = cpp_save;
                if (this->parameter.verbose > 1)
                {
                    std::cout << "Stopping criterion reached" << std::endl;
                }
		//compute the structure of the reduced graph
            this->compute_reduced_graph_plane(corners);
            //check for beneficial merges
            //this->merge_resplit_plane();
                store_CP_planes(corners);
                break;
            }
            if (ite_main>=this->parameter.max_ite_main)
            {   //max number of iteration
                if (this->parameter.verbose > 1)
                {
                    std::cout << "Max number of iteration reached" << std::endl;
                }
		//compute the structure of the reduced graph
            this->compute_reduced_graph_plane(corners);
            //check for beneficial merges
            //this->merge_resplit_plane();
                store_CP_planes(corners);
                break;
            }
            old_energy = energy.first + energy.second;
        }
        return std::pair<std::vector<T>, std::vector<T>>(energy_out, time_out);
    }
    
    
    void store_CP_planes(std::vector< std::vector< Plane> > planes)
    {
		std::ofstream output;
		output.open ("/home/sguinard/dev/libxmls/planes.csv");
		VertexAttributeMap<T> vertex_attribute_map
                = boost::get(boost::vertex_bundle, this->main_graph);
                
		for (uint32_t ind_com = 0;  ind_com < this->components.size(); ind_com++)
        {
			vP3* points = new vP3;
			for (uint32_t ind_ver = 0; ind_ver < this->components[ind_com].size(); ind_ver++)
                {
                    XPt3D point;
                    point.X = vertex_attribute_map[this->components[ind_com][ind_ver]].observation[0];
                    point.Y = vertex_attribute_map[this->components[ind_com][ind_ver]].observation[1];
                    point.Z = vertex_attribute_map[this->components[ind_com][ind_ver]].observation[2];
                    points->emplace_back(point);
                }
                Plane p = Plane(points);
		delete points;
                output << ind_com << "," << p.X() << "," << p.Y() << "," << p.Z() << "," << p.D() << "\n";
		}
		/*for (int i = 0; i != planes.size(); ++i)
			for (auto& p: planes[i])
				{
					output << i << "," << p.X() << "," << p.Y() << "," << p.Z() << "," << p.D() << "\n";
				}*/
		output.close();
	}


};

}

#endif // CUTPURSUIT_PLANE_H

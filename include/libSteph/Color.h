#ifndef COLOR_H
#define COLOR_H


class Color
{
public:
    unsigned char red;
    unsigned char green;
    unsigned char blue;

    Color();
    Color(unsigned char r, unsigned char g, unsigned char b);

    ~Color();
};

#endif // COLOR_H

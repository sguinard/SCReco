#ifndef SCRECO_H
#define SCRECO_H

/** \brief tentative to create a unique structure containing all aour informations
 *  \warning NOT COMPLETED
 */

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "Edge.h"
#include "Wedge.h"

class SCReco
{
public:
    std::vector<uint> vp;   // points
    std::vector<std::tuple<uint,uint,uint> > ve;    // edges
    std::vector<Triangle> vt;   // triangles

    SCReco();
    SCReco(std::vector<Triangle> t, std::vector<std::tuple<uint,uint,uint> > e, std::vector<uint> p);

    std::vector<uint> points_used();
    void find_triangles_and_edges(std::vector<std::map<uint,std::vector<SCR::edge> > > data, XMls& mls);
    void find_remaining_points();
    void wedge_filtering();
};

#endif // SCRECO_H

#ifndef PLANE_H
#define PLANE_H


#include "Triangle.h"
#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "param.h"

// !!!!!!!! DO NOT INCLUDE SVD.H !!!!!!!!!
// !! FIRST IT DOES NOT WORK WITH EIGEN !!
// !!!! NEXT, IT DOES NOT WORK AT ALL !!!!
//extern "C"
//{
//#include "SVD.h"
//}


/** Define a plane
 *  by its 4 parameters x,y,z,d
 *  so that man can easily compute distance
 *  from point to plane
 *
 *  (x,y,z) is the normal of the plane
 *  and d a distance
 *
 *  \usepackage{amsmath}
 *
 *  \remark
 *  from 3 points p1, p2, p3 (eventually forming a triangle)
 *  we find the normal vector of the plane (p1,p2,p3)
 *  we define the 2 vectors
 *  \f$ \vec{u} = p2 - p1 \f$ and
 *  \f$ \vec{v} = p2 - p3 \f$
 *  the normal vector \f$ \vec{n} \f$ of the plane P
 *  is defined as \f$ \vec{n} = \vec{u} \wedge \vec{v} \f$
 *  for \f$ \vec{n} = \begin{bmatrix} a \\ b \\ c \end{bmatrix} \f$
 *      - the first 3 parameters are defined as:
 *          -# \f$ x = a \f$
 *          -# \f$ y = b \f$
 *          -# \f$ z = c \f$
 *      - the last parameter d is defined by considering a point (\f$ p1 = \begin{bmatrix} p1_x \\ p1_y \\ p1_z \end{bmatrix} \f$) :
 *          -# \f$ d = - p1_x \cdot x - p1_y \cdot y - p1_z \cdot z \f$
 *
 */

namespace SCR{

class Plane
{
    double x,y,z,d;

public:

    /// \remark ok, so here's the deal:
    /// - i want to build a map with planes
    /// - BUT the map needs to know how to order the planes
    ///     - so, i have to overload the '<' operator
    /// - BUT it does not make sens to say plane1 < plane2
    ///     - so, i create a false inferiority relationship, just to separate planes (i don't care of they order)
    ///     - usually, for simplicity, these overloadings are outside of the class
    /// - BUT planes parameters are private
    ///     - so, i overloaded the operator inside the class
    /// - BUT planes have to be const
    ///     - so, i made the overloading const as well
    ///
    /// \warning do not touch this
    inline bool operator< (const Plane p2) const
    {
        if (this->x < p2.x) return true;
        else if (this->x == p2.x)
        {
            if (this->y < p2.y) return true;
            else if (this->y == p2.y)
            {
                if (this->z < p2.z) return true;
                else if (this->z == p2.z)
                    return this->d < p2.d ? true : false;
            }
            else return false;
        }
        else return false;
    }

    bool is_valid();

    /*double _x_;
    double _y_;
    double _z_;
    double _d_;*/
    int p1,p2,p3;//p4;

    Plane();
    Plane(vP3 *vp);
    Plane(double _x,double _y,double _z,double _d);
    Plane(XPt3D p_1, XPt3D p_2, XPt3D p_3);
    ~Plane();

	double X(){return x;}
	double Y(){return y;}
	double Z(){return z;}
	double D(){return d;}

    void print();
    vP3 compute_3_points();
    vP3 compute_orthonormal_base();

    XPt3D find_centroid_set_of_points(vP3 *vp);
    XPt3D project_point_plane(XPt3D p);
    std::vector<double> project_vector_plane(XPt3D p);
    double quadratic_distance_point_plane(XPt3D p) const;
    double distance_point_plane(XPt3D p);
    double distance_triangle_plane(Triangle t, XMls &mls, param params);
    double custom_ransac_distance(Triangle t, XMls &mls, param params);
    double custom_ransac_distance(uint p, XMls &mls, param params);
    XPt3D barycenter(Triangle t, XMls& mls, param params);
};

}

#endif // PLANE_H

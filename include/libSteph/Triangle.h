#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <vector>
#include "iostream"

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

namespace SCR{

struct Triangle
{
    unsigned int i,j,k; // pulse with return idx
    Triangle(unsigned int i_=0, unsigned int j_=0, unsigned int k_=0):
        i(i_), j(j_), k(k_) {}
    bool operator==(Triangle t) {return (t.i == i || t.i == j || t.i == k) &&
                                        (t.j == i || t.j == j || t.j == k) &&
                                        (t.k == i || t.k == j || t.k == k) ? true : false;}
    bool operator!=(Triangle t) {return (t.i == i || t.i == j || t.i == k) &&
                                        (t.j == i || t.j == j || t.j == k) &&
                                        (t.k == i || t.k == j || t.k == k) ? false : true;}
	bool operator==(const Triangle t) const {return (t.i == i || t.i == j || t.i == k) &&
                                        (t.j == i || t.j == j || t.j == k) &&
                                        (t.k == i || t.k == j || t.k == k) ? true : false;}
    bool operator!=(const Triangle t) const {return (t.i == i || t.i == j || t.i == k) &&
                                        (t.j == i || t.j == j || t.j == k) &&
                                        (t.k == i || t.k == j || t.k == k) ? false : true;}
    void print();

    XPt3D normal(XMls &mls);

    float perimeter(XMls &mls);

    float area(XMls& mls);
};

/*struct triangle_sort
{
    inline bool operator() (const Triangle t1, const Triangle t2)
    {
        std::vector<unsigned int> v1;
        std::vector<unsigned int> v2;

        v1.emplace_back(t1.i);
        v1.emplace_back(t1.j);
        v1.emplace_back(t1.k);
        v2.emplace_back(t2.i);
        v2.emplace_back(t2.j);
        v2.emplace_back(t2.k);

        std::sort (v1.begin(), v1.end());
        std::sort (v2.begin(), v2.end());

        if (v1[0] < v2[0]) return true;
        else if (v1[0] == v2[0])
        {
            if (v1[1] < v2[1]) return true;
            else if (v1[1] == v2[1])
                return v1[2] < v2[2] ? true : false;
            else return false;
        }
        else return false;
    }
};*/

inline bool operator< (const Triangle t1, const Triangle t2)
{
    if (t1.i < t2.i) return true;
    else if (t1.i == t2.i)
    {
        if (t1.j < t2.j) return true;
        else if (t1.j == t2.j)
            return t1.k < t2.k ? true : false;
        else return false;
    }
    else return false;
}

bool adjacent (Triangle t1, Triangle t2);
bool adjacent (const Triangle* t1, const Triangle* t2);

bool find_triangle (std::vector<Triangle> vt, Triangle t);

}

#endif // TRIANGLE_H


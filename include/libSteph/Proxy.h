#ifndef PROXY_H
#define PROXY_H

#include "Plane.h"
#include "Triangle.h"
#include "Typedefs.h"
#include "MTriCoords.h"

class Proxy
{
private:
    XPt3D _x;    // barycenter
    XPt3D _n;    // normal
    double _d;

public:
    Proxy();
    Proxy(XPt3D x, XPt3D n);
    Proxy(vP3 *points);

    double X();
    double Y();
    double Z();
    double NX();
    double NY();
    double NZ();
    double D();
};

#endif // PROXY_H

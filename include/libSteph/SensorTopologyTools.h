#ifndef SENSORTOPOLOGY_H
#define SENSORTOPOLOGY_H

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "Edge.h"
#include "WedgeStuff.h"

#include "Voronoi.h"
#include "Ransac.h"

/// \file SensorTopologyTools.h
///
/// \details This file contains the prototype of the SensorTopology class


using namespace SCR;


/// \class SensorTopology
///
/// \details This class contains all the informations from the sensor topology of a fixed lidar laser scanner:
class SensorTopology
{
private:
    XPt3D pivot;                        /*!< place of the sensor*/
    vvD observations;                  /*!< points acquired*/
    std::vector<edge> edges;           /*!< edges between points*/
    std::vector<Triangle> triangles;   /*!< triangles between points*/
    std::vector<Wedge> wedges;         /*!< wedges are sets of 4 self-connected edges*/

public:
    SensorTopology();
    SensorTopology(vvD* _observations);
    SensorTopology(XPt3D _pivot, vvD* _observations);
    SensorTopology(XPt3D _pivot, vvD* _observations, std::vector<edge> *_edges);
    SensorTopology(XPt3D _pivot, vvD* _observations, std::vector<Triangle> *_triangles);
    SensorTopology(XPt3D _pivot, vvD* _observations, std::vector<edge> *_edges, std::vector<Triangle> *_triangles);

    ~SensorTopology();

    XPt3D find_centroid();
    double compute_standard_deviation();
    void get_triangles(std::vector<Triangle>* _triangles);

    bool has_valid_observations();
    bool has_valid_edges();
    bool has_valid_triangles();

    XPt3D compute_wedge_normal(Wedge w);

    void compute_simplicial_complexes_reconstruction_from_observations(double c0_tresh, double c1_tresh, double wedge_tresh, double edge_tresh, uint32 nb_pulses_per_line);

    void compute_edges(double c0_tresh, double c1_tresh, uint32 nb_pulses_per_line);
    void compute_triangles_from_edges();
    void compute_wedges();
    void filter_wedges(double wedge_tresh);
    void compute_triangles_from_wedges();
};

#endif // SENSORTOPOLOGY_H

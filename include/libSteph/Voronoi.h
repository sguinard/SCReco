#ifndef VORONOI_H
#define VORONOI_H


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/squared_distance_3.h> //for 3D functions
#include "Typedefs.h"
#include <iostream>
#include <fstream>
#include <cassert>
#include <list>
#include <vector>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
//typedef CGAL::Triangulation_3<K>      Triangulation;
typedef CGAL::Delaunay_triangulation_3<K> Triangulation;
typedef Triangulation::Cell_handle    Cell_handle;
typedef Triangulation::Vertex_handle  Vertex_handle;
typedef Triangulation::Locate_type    Locate_type;


std::list<Triangulation::Point> addToList(std::list<Triangulation::Point> L, vvD *observations);

std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* create_3d_delaunay(std::list<Triangulation::Point> L, vvD *&observations);
std::vector<Triangle>* convert_3d_delaunay(std::list<Triangulation::Point> L, vvD *&observations);
std::vector<Triangle>* convert_3d_delaunay(std::list<Triangulation::Point> L, vvD* &observations, float max_edge_length);


#endif // VORONOI_H

#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>
#include <algorithm>

//void prout()
//{
//    char red[] = { 0x1b, '[', '1', ';', '3', '1', 'm', 0 };
//    char normal[] = { 0x1b, '[', '0', ';', '3', '9', 'm', 0 };
//    std::cout << red << "prout" << normal << std::endl;
//}


//template <typename T>
//typename std::vector<T>::const_iterator find( const std::vector<T>& v, const T& value )
// {
//     return std::find( v.begin(), v.end(), value );
// }


template <typename T>
void remove_from_vec(std::vector<T>*& vec, size_t pos)
{
    //std::cout << pos << " " << std::flush;
    if (pos < vec->size())
    {
        auto it = vec->begin();
        std::advance(it, pos);
        vec->erase(it);
    }
}


template <typename T>
void remove_from_vec(std::vector<T>*& vec, std::vector<T>*& vec_orig)
{
    typename vector<T>::const_iterator it;
    for (it = vec->begin(); it != vec->end(); ++it)
    {
        if (std::find(vec->begin(),vec->end(),*it) != vec->end())
            remove_from_vec(vec,distance(vec_orig->begin(), find(vec_orig->begin(), vec_orig->end(), *std::find(vec->begin(),vec->end(),*it))));
    }
    //std::cout << "fin " << std::flush;
}





#endif // CONFIG_H


#ifndef TRIANGLE_COORDS_H
#define TRIANGLE_COORDS_H

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "Edge.h"
#include "WedgeStuff.h"

#include "Voronoi.h"
#include "Ransac.h"
#include "Triangle.h"

using namespace SCR;

class TriangleCoords
{
private:
  vP3 _points;

public:
  /// constructors
  TriangleCoords();
  TriangleCoords(vP3 *points);

  /// destructor
  ~TriangleCoords();

  /// operators
  bool operator <(const TriangleCoords t) const
  {
    TriangleCoords me = *this;
    TriangleCoords tt = t;
    XPt3D c1 = me.centroid();
    XPt3D c2 = tt.centroid();
    return c1.X < c2.X ? true : c1.Y < c2.Y ? true : c1.Z < c2.Z ? true : false;
  }
  bool operator >(const TriangleCoords t) const { TriangleCoords me = *this;
	TriangleCoords tt = t; return me.operator ==(tt) ? false : true;}
  bool operator ==(TriangleCoords t)
  {
    if (this->p1() == t.p1())
		if (this->p2() == t.p2())
			return this->p3() == t.p3();
		if (this->p2() == t.p3())
			return this->p3() == t.p2();
			
	if (this->p1() == t.p2())
		if (this->p1() == t.p2())
			return this->p3() == t.p3();
		if (this->p1() == t.p3())
			return this->p3() == t.p2();
			
	if (this->p3() == t.p1())
		if (this->p2() == t.p2())
			return this->p1() == t.p3();
		if (this->p2() == t.p3())
			return this->p1() == t.p2();
			
	return false;
  }
  bool operator !=(TriangleCoords t) { return !(this->operator ==(t));}

  TriangleCoords min(TriangleCoords tc)
  {
    XPt3D meant1 = (this->p1().operator +=(this->p2())).operator +=(this->p3());
    XPt3D meant2 = (tc.p1().operator +=(tc.p2())).operator +=(tc.p3());
    return meant1.X < meant2.X ? *this : meant1.Y < meant2.Y ? *this : meant1.Z < meant2.Z ? *this : tc;
  }

  TriangleCoords max(TriangleCoords tc)
  {
    XPt3D meant1 = (this->p1().operator +=(this->p2())).operator +=(this->p3());
    XPt3D meant2 = (tc.p1().operator +=(tc.p2())).operator +=(tc.p3());
    return meant1.X < meant2.X ? tc : meant1.Y < meant2.Y ? tc : meant1.Z < meant2.Z ? tc : *this;
  }
  
  /// getter
  XPt3D p1();
  XPt3D p2();
  XPt3D p3();
  std::vector<double> get_p1();
  std::vector<double> get_p2();
  std::vector<double> get_p3();

  /// setter
  void set_p1(XPt3D p);
  void set_p2(XPt3D p);
  void set_p3(XPt3D p);
  
  bool is_adjacent(TriangleCoords t);
  
  XPt3D centroid();
  
  bool is_valid();
  bool intersects(TriangleCoords tri);

  double area();
  std::vector<double> distance2plane(Plane p);

  double distance_triangle_plane(Plane p);
  
  TriangleCoords project_triangle_plane(Plane p);
};

#endif TRIANGLE_COORDS_H

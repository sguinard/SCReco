#ifndef CONVEXHULL_H
#define CONVEXHULL_H

#include "libXBase/XPt2D.h"
#include "Plane.h"
#include "MIvP3.h"
//#include "param.h"
//#include "Polyhedron.h"

// ---- STD ----
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <getopt.h>
#include <list>
#include <string.h>
#include "boost/filesystem.hpp"
#include <time.h>
#include <sstream>
#include <limits>
#include <algorithm>
#include <iterator>

// ---- CGAL ----
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/algorithm.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Alpha_shape_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/squared_distance_2.h>
#include <CGAL/Timer.h>
#include <CGAL/Polygon_2_algorithms.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/Homogeneous.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/intersections.h>
#include <cassert>

// ---- CGAL definition -----
// Kernel and geometry
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::FT FT;
typedef K::Point_2  Point_2_CGAL;
typedef K::Point_3  Point_3;
typedef K::Segment_2  Segment;
typedef CGAL::Polygon_2<K> Polygon_2;
typedef CGAL::Polygon_with_holes_2<K> Polygon_with_holes_2;

// Triangulatino and alphashape stuff
typedef CGAL::Alpha_shape_vertex_base_2<K> Vb;
typedef CGAL::Alpha_shape_face_base_2<K>  Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation_2;
typedef CGAL::Alpha_shape_2<Triangulation_2>  Alpha_shape_2;
typedef Alpha_shape_2::Alpha_shape_edges_iterator Alpha_shape_edges_iterator;
typedef Alpha_shape_2::Alpha_iterator Alpha_iterator;


// ----- Project definition -----
// A polygon is a list of segment
typedef std::list<Segment> Poly_seg;


// ---- Redefinition of the 2D point class of cgal to add the altitude and the time ---- //
class Point_2 : public Point_2_CGAL {

public:
    Point_2(double px, double py) : Point_2_CGAL(px,py), cc(1),z(0),t(0) {};
    Point_2(double px, double py, double pz,double pt) :  Point_2_CGAL(px,py), z(pz), t(pt) {
    };
    // bool operator<(Point_2 pp) const {
    //   return ((cc == 0) ? t > pp.t : z > pp.z);
    // }

    double val() const {
        return z;
    }
    int cc;
    double z;
    double t;
};


bool compare_ptsz (const Point_2& p1, const Point_2& p2);

std::list<Point_2_CGAL> cast2cgal(std::list<Point_2> const & lp);

std::vector<double> alpha_shape_1d(std::list<Point_2>& lp, double alpha);



// ---- Class for storing one ids point cloud set information ---- //
class Multipoly{

public:
    Multipoly() : seg_id(-1), class_id(-1) {
        filename = "err";
    }

    int nb_segs(){
        int acc = 0;
        std::list<Poly_seg>::iterator it;
        for(it = list_poly.begin(); it != list_poly.end(); ++it){
            acc += it->size();
        }
        return acc;
    }
    int nb_poly(){
        return list_poly.size();
    }

    //gettet and setter
    int get_seg_id() const{return seg_id;}
    void set_seg_id(int v){seg_id = v;}

    int get_class_id() const{return class_id;}
    void set_class_id(int v){class_id = v;}

    const std::vector<double> get_z() const {return z;}
    void set_z(const std::vector<double>& v) {z = v;}

    // Id of the set
    int seg_id;
    // Class of the set
    int class_id;

    std::vector<double> z;
    // Name of the file
    std::string filename;

    // Point of the set
    std::list<Point_2>  points;

    // Result of the alphashape
    // List of Polygons. (The set of point can be divized in multiple polygon by the alphashape algorithm)
    std::list<Poly_seg>  list_poly;
};


template <class OutputIterator>
        void
        alpha_edges( const Alpha_shape_2&  A,
                     OutputIterator out)
{//int i=0;
    for(Alpha_shape_edges_iterator it =  A.alpha_shape_edges_begin();
    it != A.alpha_shape_edges_end();
    ++it){
        *out++ = A.segment(*it);//std::cout << i++ << " " << std::flush;
    }
}


class ConvexHull {
private:
    SCR::vP2* projected_points;
    SCR::vP3* points;
    SCR::Plane plane;
    //Polyhedron hull;

public:
    ConvexHull();
    ConvexHull(SCR::vP3* input_points);
    ConvexHull(SCR::vP3* input_points, SCR::Plane input_plane);

    ~ConvexHull();

    Multipoly compute_convex_hull();

};

void write_output(boost::filesystem::path path,std::string filename, std::vector<Multipoly> & map_Multipoly);

void write_alpha_shape_ply(XMls &mls, SCR::param params, std::vector<Multipoly> & map_Multipoly);

SCR::vP2 *project_points_2d(SCR::Plane plane, SCR::vP3 *points);

SCR::vP2 *project_points_2d(SCR::vP3 base, SCR::vP3 *points);

SCR::vP3 *project_points_3d(SCR::vP3 base, SCR::vP2 *points);


#endif // CONVEXHULL_H


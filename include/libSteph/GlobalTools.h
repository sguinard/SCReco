#ifndef GLOBALTOOLS
#define GLOBALTOOLS

/// \file GlobalTools.h
///
/// \brief Toolbox with essentially searching tools
///
/// @note should find a real name for this file, but i have no good idea (toolbox ? searchtools ?)

#include "Edge.h"
#include "Typedefs.h"

using namespace SCR;

bool point_in_vI(vI* points, int p);

bool search_point_in_triangle(Triangle t, uint p);

//bool find_edge(std::tuple<uint,uint,uint> ENIT, int b, int p);

bool find_edge(std::vector<Triangle> v_tri, uint p);

bool find_edge_in_triangles(std::vector<Triangle> *vt, SCR::edge e);

bool find_edge_in_triangle(Triangle vt, SCR::edge e);


#endif // GLOBALTOOLS


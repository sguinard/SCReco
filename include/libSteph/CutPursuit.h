#ifndef CUTPURSUIT_H
#define CUTPURSUIT_H

#include <iostream>
#include <cstdio>
#include <vector>
#include "boost/tuple/tuple.hpp"
#include "../extern/cut-pursuit-master/include/API.h"

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "CutPursuit_Plane.h"
//#include "Triangle_Coords.h"
#include "CutPursuit_Plane_Triangle.h"

std::vector<std::vector<double> > find_observations_mesh(XMls& mls, param params, std::vector<Triangle> vtri);
std::vector<std::vector<double> > find_observations(XMls& mls, param params);

std::pair<std::vector<uint32_t>,std::vector<uint32_t> > find_edges(std::vector<Triangle> vtri, std::vector<std::tuple<uint,uint,uint> > edges);
std::pair<std::vector<uint32_t>,std::vector<uint32_t> > find_edges(std::vector<Triangle> vtri);
std::pair<std::vector<uint32_t>,std::vector<uint32_t> > find_edges(XMls& mls, param params, double tresh);
//std::pair<std::vector<uint32_t>,std::vector<uint32_t> > find_edges(XMls& mls, param params, double tresh);

std::vector<std::vector<std::pair<XPt3D, Color> > > color_points(std::vector<std::vector<double> > solution, std::vector<uint32> inComponent);
std::pair<std::vector<std::vector<std::pair<XPt3D, Color> > >,std::vector<std::vector<std::pair<XPt3D, Color> > > > color_points(std::vector<std::vector<double> > solution, std::vector<std::vector<double> > observation, std::vector<uint32> inComponent, uint32 nb_comps);
std::pair<std::vector<std::vector<std::pair<XPt3D, uint32> > >,std::vector<std::vector<std::pair<XPt3D, uint32> > > > color_points(std::vector<std::vector<double> > solution, std::vector<std::vector<double> > observation, std::vector<uint32> inComponent);
void color_triangles(std::vector<std::vector<double> > observation, std::vector<std::vector<uint32_t> > triangles, vvD points, std::vector<uint32> inComponent, uint32_t ncomps);
void store_plane_pursuit_tri(vvD *points, std::vector<std::vector<uint32_t> > *triangles, std::vector<Color> comp_colors, std::string name, std::vector<uint32_t> inComponent);

void cut_pursuit(const uint32_t n_nodes, const uint32_t n_edges, const uint32_t nObs
          , std::vector< std::vector<double> > & observation
          , const std::vector<uint32_t> & Eu, const std::vector<uint32_t> & Ev
          , const std::vector<double> & edgeWeight, const std::vector<double> & nodeWeight
          , std::vector< std::vector<double> > & solution,  const double lambda, const double mode, const double speed
          , const double verbose, XMls& mls, param params, std::vector<uint32> inComponent);
void cut_pursuit
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<std::vector<double> > &observation, // coordinates
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<std::vector<double> > &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent,
		 const std::string out);
void cut_pursuit
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 vvD &observation, // coordinates
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 vvD &solution,    // void vector
		 vvD &points,
		 std::vector<std::vector<uint32_t> > &triangles,
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent);
void cut_pursuit_tri
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<std::vector<double> > &observation, // coordinates
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<std::vector<double> > &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent);
void cut_pursuit_tri
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<std::vector<double> > &observation, // coordinates
                 vvD *pts,
                 std::vector<std::vector<uint32_t> > *tris,
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<std::vector<double> > &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent);
#endif // CUTPURSUIT_H


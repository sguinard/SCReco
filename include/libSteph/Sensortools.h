#ifndef SENSORTOOLS_H
#define SENSORTOOLS_H

#include <ctime>
#include <iostream>
#include <limits>
#include <stdio.h>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "Plane.h"
#include "Color.h"

using namespace std;

using namespace SCR;

/// \file Sensortools.h
///
/// \brief toolbox with storage functions + low level functions

/// \brief write function templated
template <typename T> void Write(char * & it, T data)
{
    *reinterpret_cast<T*>(it) = data;
    it += sizeof(T);
}

// count number of vertex
int count_vertex(XMls & mls);

void WritePlyEdge(XMls & mls, std::vector<std::pair<uint,uint> >edges,
              int n_vertex, param params);



void WritePlySegment(XMls & mls, std::vector<std::tuple<uint,uint,uint> >edges,
              int n_vertex, param params);


void WritePlyPoint(XMls & mls, std::vector<std::vector<XPt3D> > points, param params);
void WritePlyPoint(std::vector<std::vector<double> > points);
void WritePlyPointColor(XMls & mls, std::vector<std::vector<std::pair<XPt3D, Color> > > points, param params, std::string filename="_points_colored.ply");
void WritePlyPointColor(std::vector<std::vector<std::pair<XPt3D, Color> > > points, std::string filename);
void WritePlyPointColor(std::vector<std::vector<std::pair<XPt3D,uint32> > > points, std::string filename);


void StorePlanes(std::vector<XPt3D> points, param params, XMls &mls);


//void StorePlanes(XMls & mls, vector<Plane> v_plane,
//              int n_vertex, param params);

void WritePly(XMls & mls, vector<vector<Triangle> > v_tri,
              int n_vertex, param params);

void WritePly(XMls & mls, std::vector<std::pair<uint,uint> >edges, vector<Triangle> v_tri,
              int n_vertex, param params);


/// @bug vertex coordinates are trunkated or rounded to 2/3 digits in x and y
void WritePly(XMls & mls, std::pair<std::vector<std::pair<uint,uint> >,std::vector<double> >edges_score, vector<Triangle> v_tri,
              int n_vertex, param params);


void WriteOff(XMls & mls, vector<Triangle> v_tri,
              int n_vertex, param params);

void WriteOff(XMls & mls, std::vector<std::pair<uint,uint> >edges,
              int n_vertex, param params);

float MaxEdgeSize_echo(XMls & mls, XBlockIndex b1, XEchoIndex id1, XBlockIndex b2, XEchoIndex id2, XBlockIndex b3, XEchoIndex id3);

float MaxEdgeSize(XMls & mls, XBlockIndex b1, XPulseIndex id1, XBlockIndex b2, XPulseIndex id2, XBlockIndex b3, XPulseIndex id3);

float MaxEdgeSize(XMls & mls, XBlockIndex b, XPulseIndex id1, XPulseIndex id2, XPulseIndex id3);

// make a wedge based on the four vertices ABXY = (AXB)+(ABY) triangles
void MakeWedge(XMls & mls, vector<Triangle> & v_tri, vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
               XBlockIndex ba, XPulseIndex pa, XBlockIndex bb, XPulseIndex pb,
               XBlockIndex bx, XPulseIndex px, XBlockIndex by, XPulseIndex py);


// mono block version
inline void MakeWedge(XMls & mls, vector<Triangle> & v_tri, vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
                      XBlockIndex b, XPulseIndex pa, XPulseIndex pb, XPulseIndex px, XPulseIndex py)
{
    MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, tri_threshold, b, pa, b, pb, b, px, b, py);
}

// make a wedge based on the four vertices ABXY = (AXB)+(ABY) triangles
void MakeWedge(XMls & mls, vector<Triangle> & v_tri, vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
               XBlockIndex ba, XEchoIndex pa, XBlockIndex bb, XEchoIndex pb,
               XBlockIndex bx, XEchoIndex px, XBlockIndex by, XEchoIndex py);

// mono block version
inline void MakeWedge(XMls & mls, vector<Triangle> & v_tri, vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
                      XBlockIndex b, XEchoIndex pa, XEchoIndex pb, XEchoIndex px, XEchoIndex py)
{
    MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, tri_threshold, b, pa, b, pb, b, px, b, py);
}

float Maxedgesize(XMls & mls, XBlockIndex b, XPulseIndex id1, XPulseIndex id2, XPulseIndex id3);

float Maxedgesize(XMls & mls, XBlockIndex b1, XPulseIndex id1, XBlockIndex b2, XPulseIndex id2, XBlockIndex b3, XPulseIndex id3);

void progress (int progression, int max);

void maxflow_help();


#endif // SENSORTOOLS_H


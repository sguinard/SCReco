#ifndef TINYPLYMANAGEMENT_H
#define TINYPLYMANAGEMENT_H

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "Typedefs.h"
#include "WedgeStuff.h"
#include "KMeans.h"
#include "Triangle_Coords.h"

#include "ConvexHull.h"

#include "TinyPlyIO.h"

using namespace tinyply;
//using namespace SCR;

void PlyFile2Vector(const string &filename, PlyFile *file, SCR::vvD* observations, std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* edges);
void PlyFile2Vector(const string &filename, PlyFile *file, SCR::vvD* points, SCR::vvD* observations, std::vector<std::vector<uint32_t> >* triangles);
void PlyFile2Vector(const std::string & filename, PlyFile* file, SCR::vvD* points, SCR::vvD* observations, std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* edges);
void PlyFile2Vector(const std::string & filename, PlyFile* file, SCR::vP3* observations, SCR::vT* triangles);
void PlyFile2Vector(const std::string & filename, PlyFile* file, SCR::vvD* observations);
void PlyFile2Vector(const std::string & filename, PlyFile* file, std::vector<TriangleCoords> *observations);

void createmesh(const string &filename, PlyFile *file, SCR::vvD* observations, std::vector<Triangle> *v_tri);


#endif // TINYPLYMANAGEMENT_H


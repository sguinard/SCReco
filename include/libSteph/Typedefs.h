#ifndef TYPEDEFS
#define TYPEDEFS

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "Sensortools.h"

namespace SCR{

/// \brief vector of vectors of doubles
typedef std::vector<std::vector<double> > vvD;

/// \brief iterator on vector of vectors of doubles
typedef vvD::iterator vvD_iter;

/// \brief vector of Triangles
typedef std::vector<Triangle> vT;

/// \brief iterator on vector of triangles
typedef vT::iterator vT_iter;

/// \brief vector of references to Triangles
//typedef std::vector<std::reference_wrapper<Triangle> > vT_ref;

/// \brief vector of Planes
typedef std::vector<Plane> vP;

// \brief vector of Colors
typedef std::vector<Color> vC;

/// \brief vector of Floats
typedef std::vector<float> vF;

/// \brief vector of Ints
typedef std::vector<int> vI;

/// \brief iterator on vector of ints
typedef vI::iterator vI_iter;

/// \brief vector of references to ints
typedef std::vector<std::reference_wrapper<int> > vI_ref;

/// \brief iterator on vector of Triangles
typedef vT::iterator Tit;

/// \brief iterator on vector of Triangles*
//typedef vT_ref::iterator Tit_ref;

/// \brief iterator on vector of Planes
typedef vI::iterator Iit;

/// \brief iterator on vector of Planes
typedef vP::iterator vP_iter;


/// \brief tentative to use reference wrappers
///
/// \warning uncomplete - do not use
struct vT_ref
{
public:
    std::vector<std::reference_wrapper<Triangle> > v;

/// \brief iterators
typedef std::vector<std::reference_wrapper<Triangle> >::iterator iterator;
    iterator begin() { return v.begin(); }
    iterator end()   { return v.end();   }
};

}

#endif // TYPEDEFS


#ifndef RANSAC
#define RANSAC

#include "Automatic_Pointers.h"

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "GlobalTools.h"
#include "SemanticSegmentation.h"
#include "Config.h"

/********************************************//**
 * \file Ransac.h
 * \brief
 * File for all functions concerning RANSAC algorithm computing
 *
 * RANSAC algorithm:
 *  - input:
 *              -# M: mesh
 *              -# n: nb. of planes to retrieve
 *              -# k: nb. of iterations
 *              -# tresh: treshold
 *              -# e: error
 *  - output:
 *              -# P: vector of best planes parameters
 *              -# \epsilon: vector of errors
 *  - algorithm:
 *                  - P = vector()
 *                  - \f$\epsilon\f$ = vector()
 *                  - for 0<i<k
 *                      - p := random_plane_based_on_triangle(list_of_triangles)
 *                      - e = compute_error(p,list_of_triangles)
 *                      - if (e < tresh)
 *                          - P <- p
 *                          - \f$\epsilon\f$ <- e
 *                          - if (len(P) = n)
 *                              - return P,\f$\epsilon\f$
 *                  - return P,\f$\epsilon\f$
 *
 ***********************************************/


bool triangle_in_vT(vT *triangles, Triangle t);

XPt3D point_mutation(XPt3D point, float mutation, XPt3D Pivot, XPt3D barycenter);

float distance_triangle_plane(Triangle t, Plane p, XMls &mls);

vI* find_points_in_mesh(vT* vtri, vI* vint);
vI_ref* find_points_in_mesh(vT_ref* vt, vI_ref* vint);

vI *find_points_far_from_plane(vI *points_in_mesh, vI *points_near_plane);
vT *find_triangles_far_from_plane(vT* triangles, vT *triangles_near_plane);

Plane random_plane(vvD* points, double tresh);
Plane random_plane_based_on_triangle(vT vtri, XMls &mls);
std::pair<Triangle,Plane> random_plane(vI* vp, XMls& mls, param params);

vI *compute_error(Plane p, XMls &mls, vI* points_in_mesh, float e, param params);
vT* compute_error(Plane p, XMls &mls, vT* triangles, float e, param params);
vI* compute_error_pt(Plane p, XMls &mls, vT* triangles, float e, param params);
vI* compute_error(Plane p, vvD* points, float tresh);
std::pair<vI *, double> compute_error_d(Plane p, vvD* points, float tresh);

std::pair<Plane,vI> ransac(vT* vtri, float n, float k, float e, XMls &mls, param params);

std::pair<std::vector<XPt3D>, std::vector<std::pair<Plane, Triangle> > > ransacN(vT *vtri, float np, float n, float k, float e, XMls &mls, param params);
std::tuple<std::vector<XPt3D>, std::vector<std::pair<Plane, Triangle> >, std::vector<std::pair<XPt3D, Color> > > ransacN_colorized(vT* vtri, float np, float n, float k, float e, XMls &mls, param params);
std::tuple<std::vector<XPt3D>, std::vector<std::pair<Plane, Triangle> >, std::vector<std::pair<XPt3D, Color> >, std::vector<std::pair<XPt3D, Color> > > ransac_segmentation(vT* vtri, float tresh, float k, float e, XMls &mls, param params);

std::vector<uint32> ransac_from_3d_points(std::vector<std::vector<double> >* points, int nb_planes);
std::pair<std::vector<uint32>,uint32> ransac_from_3d_points_fast(vvD *points, vvD *solution, double energy_tresh);
std::pair<std::vector<uint32>,uint32> ransac_from_3d_points_unparallelized(vvD *points, vvD* solution, double energy_tresh);
std::pair<Plane,Plane> ransac2_from_3d_points_unparallelized(vvD *points);
std::pair<Plane, Plane> ransac2_from_3d_points_fast(vvD *points);

std::pair<std::vector<XPt3D>, std::vector<std::pair<Plane, Triangle> > > ransacN_from_triangles(vT *vtri, float np, float n, float k, float e, XMls &mls, param params);

vT store_planes_as_triangles(std::vector<std::pair<Plane, vI> > best_planes, XMls &mls);
//vP store_planes(std::vector<std::pair<Plane, vI> > best_planes, XMls &mls);
//vP store_planes(std::vector<std::pair<Plane, Triangle> > best_planes, XMls &mls);

vI find_bounding_box(Plane P, vI points, XMls& mls);

std::pair<vI*,std::tuple<Plane,Triangle,vI*> > ransac_parallel_loop(vI* vp, XMls& mls, param params, int nb_iter, float error);

#endif // RANSAC




#define WHAT "SCReco: performs a 3D Reconstruction based on Simplicial Complex and outputs 3 .ply files, \n\t1st one (ending with '_triangles') contains the triangles, \n\t2nd one (ending with '_edges') contains the edges that are not in any triangle, \n\t3rd one (ending with '_points') contains the points that are neither in any triangles nor in any edge."

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/WedgeStuff.h"
#include "libSteph/KMeans.h"

#include "libSteph/ConvexHull.h"

#include "libSteph/SC.h"

#include "libSteph/CutPursuit.h"
#include "../extern/cut-pursuit-master/include/API.h"

using namespace SCR;

struct comp
{
    comp(const int & s) : _s(s) { }

    int _s;

    bool operator () (std::pair<int, int> const & p)
    {
        return (p.first == _s || p.second == _s) ? true : false;
    }
};

//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    std::cout << std::unitbuf;
    cout << WHAT << endl;
    if(argc < 6)
    {
        cout << "Usage: " << argv[0] << "  sbet_folder sbet_mission laser_calib.xml ept_folder output [tri_threshold=0.5 (int)start_time (int)end_time pivot_E=-1 pivot_N=-1 pivot_H=0 add_rgb=0]" << endl;
        cout << "sbet_folder: folder where the sbet files are" << endl;
        cout << "sbet_mission: common part of all filenames relevant to the same trajectory" << endl;
        cout << "laser_calib.xml: calibration file for the laser" << endl;
        cout << "ept_folder: folder containing the echo pulse tables (generated with EptExport)" << endl;
        cout << "output: name of the output mesh file (.ply or .off only)" << endl;
        cout << "tri_threshold: threshold on max triangle edge size to add the triangle (default=0.5m)" << endl;
        cout << "(start|end)_time: start and end time of the laser points to export (default=everything)" << endl;
        cout << "pivot_(E|N|H): optionally set manually the pivot point (default=-1=first point of the trajectory rounded at 100m for EN, 0 for H)" << endl;
        cout << "add_rgb: if not 0, add r,g, b attributes in .ply mode based on reflectance (default=0=don't)" << endl;
        return 0;
    }

    int i_arg=1;
    param params;
    // required
    params.sbet_folder = string((argv[i_arg++]));
    params.sbet_mission = string(argv[i_arg++]);
    params.laser_calib = string(argv[i_arg++]);
    params.ept_folder = string(argv[i_arg++]);
    params.output = string(argv[i_arg++]);

    // optional
    if(i_arg < argc) params.tri_threshold = atof(argv[i_arg++]);
    //if(i_arg < argc) params.max_DP_error = atof(argv[i_arg++]);
    if(i_arg < argc) params.i_start = atoi(argv[i_arg++]);
    if(i_arg < argc) params.i_end   = atoi(argv[i_arg++]);
    if(i_arg < argc) params.pivot_E = atoi(argv[i_arg++]);
    if(i_arg < argc) params.pivot_N = atoi(argv[i_arg++]);
    if(i_arg < argc) params.pivot_H = atoi(argv[i_arg++]);
    if(i_arg < argc) params.add_rgb = atoi(argv[i_arg++]);


    //clock_t start = clock();
    // constructor and infos accessible after construction
    XMls mls(params.ept_folder, params.laser_calib, params.sbet_folder, params.sbet_mission);
    mls.Select(params.i_start, params.i_end);
    cout << mls.NBlock() << " block(s) selected" << endl;

    if(params.pivot_E == -1)
    {
        XPt3D Pivot = mls.m_trajecto.SbetSeries().GetGeoref(0).Translation();
        params.pivot_E = 100*(int)(Pivot.X/100);
        params.pivot_N = 100*(int)(Pivot.Y/100);
    }

    std::vector<XAbstractAttrib*> attribs = mls.AttribList();
    for (std::vector<XAbstractAttrib*>::iterator it = attribs.begin(); it != attribs.end(); ++it)
        std::cout << (*it)->m_attribname << " ";
    std::cout << std::endl;

    //start = clock();
    //int PPL = mls.PulsePerLine();
    clock_t start = clock();
    int idx=0;
    vector< vector<int> > vv_pulse_with_echo_idx(mls.NBlock()); // unique indexation of pulses with at least one echo across blocks
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        // index pulses with at leat one echo
        vv_pulse_with_echo_idx[block_idx] = vector<int>(mls.NPulse(block_idx), -1);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++)
            if(mls.NbOfEcho(block_idx, pulse_idx)>0)
            {
                /// browse echos
                //for (int i=0; i<mls.NbOfEcho(block_idx,pulse_idx);i++)
                //    std::cout << XEchoIndex(mls.IdxFirstEcho(block_idx, pulse_idx) + i - 1) << " " << std::flush;

                //std::cout << mls.IdxFirstEcho(block_idx,pulse_idx) << " " << std::flush;
                vv_pulse_with_echo_idx[block_idx][pulse_idx] = idx++;
            }
        mls.Free(block_idx);
    }

    // create triangles list
    //vector<Triangle> v_tri;
    //mls.Load(0);


    /// @param block8 : thresh = 0.025 && lambda = 0.01
    /// @param limite d'acquisition : faire monter lambda à 0.5 voire 0.8 (au delà pas d'apport significatif)
    std::vector<SCR::edge> edges = SCR::filtering(mls,params,vv_pulse_with_echo_idx,(double).05,(double)1e-4,true);
    //std::pair<std::vector<SCR::edge>,std::vector<double> > edges_all = SCR::filtering(mls,params,vv_pulse_with_echo_idx,(double)1);

    std::vector<std::tuple<uint,uint,uint> > scr;
    for (std::vector<SCR::edge>::iterator it = edges.begin(); it != edges.end(); ++it)
    {
        //if (it->node_begin >= 73670 && it->node_begin <= 73680)
        //std::cout << it->node_begin << " " << it->node_end << " - ";
        scr.emplace_back(std::make_tuple(it->block,it->node_begin,it->node_end));
    }
    std::vector<std::map<uint,std::vector<SCR::edge> >* >* data_blocks = new std::vector<std::map<uint,std::vector<SCR::edge> >* >;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        std::map<uint,std::vector<SCR::edge> >* Edges = new std::map<uint,std::vector<SCR::edge> >;
        for (std::vector<SCR::edge>::iterator it = edges.begin(); it != edges.end(); ++it)
        {
            //if (abs((**it).node_begin - (**it).node_end) < 10) (**it).print_edge();
            if ((Edges->operator[]((**it).node_begin).size() == 0 ||
                 Edges->operator[]((**it).node_begin).back() != (**it)) &&
                 (**it).is_valid()  &&
                 (**it).block == block_idx)          /// Look at this f*cking syntax WTH
            {
                Edges->operator[]((**it).node_begin).emplace_back(**it);
                Edges->operator[]((**it).node_end).emplace_back(**it);
            }
        }
        data_blocks->emplace_back(Edges);
        mls.Free(block_idx);
    }
    //std::vector<std::map<uint,std::vector<SCR::edge> > > data_blocks_save = data_blocks;


    // Remember to check the storage inside the map
    std::tuple<std::vector<std::vector<Triangle> >*,std::vector<std::tuple<uint,uint,uint> >*,std::vector<std::vector<uint> >* >* out = SCR::compute_mesh_and_segments_fast(data_blocks,mls);
    std::vector<std::vector<Triangle> > v_tri = *(std::get<0>(*out));
    std::vector<std::tuple<uint,uint,uint> > edges_not_in_triangles = *std::get<1>(*out);
    std::cout << "edges not in triangle " << edges_not_in_triangles.size() << std::endl;
    std::vector<std::vector<uint> > rem_points = *std::get<2>(*out);

    std::vector<std::vector<XPt3D> > points_not_in_edges_nor_in_triangles;
    int b = 0;


    /*for (int i=0; i!=v_tri.size(); ++i)
    {
        for (auto& tri:v_tri[i])
            tri.print();
        std::sort(v_tri[i].begin(), v_tri[i].end());
        v_tri[i].erase( unique( v_tri[i].begin(), v_tri[i].end() ), v_tri[i].end() );
    }*/


    /*for (std::vector<std::vector<int> >::iterator blk = vv_pulse_with_echo_idx.begin(); blk != vv_pulse_with_echo_idx.end(); ++blk, ++b)
    {
        mls.Load(b);
        std::cout << "Computing remaining points for block " << b <<  " containing " << mls.NEcho(b) << " points." << std::endl;
        for (std::vector<int>::iterator p = blk->begin(); p != blk->end(); ++p)
        {
            std::vector<XPt3D> points_not_in_edges_nor_in_triangles_temp;
            std::map<uint,std::vector<SCR::edge> > Edges = data_blocks[b];
            if (*p > 0 && *p < (int)mls.NEcho(b))
                if (Edges.find((uint)*p) == Edges.end())
                {
                    //std::cout << *p << " " << std::flush;
                    points_not_in_edges_nor_in_triangles_temp.emplace_back(mls.Pworld(b,*p));
                }
            points_not_in_edges_nor_in_triangles.emplace_back(points_not_in_edges_nor_in_triangles_temp);
        }
        mls.Free(b);
    }*/

    cout << "Done in " << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl;

    /*for (XBlockIndex b = 0; b != mls.NBlock(); ++b)
    {
        std::map<uint,std::vector<SCR::edge> > Edges = data_blocks[b];
        for (std::map<uint,std::vector<SCR::edge> >::iterator it = Edges.begin(); it != Edges.end(); ++it)
            for (std::vector<SCR::edge>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
                std::cout << it2->block << " " << it2->node_begin << " " << it2->node_end << " - " << std::flush;
    }*/

    /// @note work well for retrieving points for 1 block but doesn't work for more blocks ...
    uint NPS = 0;

//    for (XBlockIndex b = 0; b != mls.NBlock(); ++b)
//    {
//        mls.Load(b);
//        std::vector<XPt3D> points_not_in_edges_nor_in_triangles_temp;
//        std::vector<uint> rem_points_block = rem_points[b];
//        uint NPS_temp = 0;
//        for (std::vector<int>::iterator it = vv_pulse_with_echo_idx[b].begin(); it != vv_pulse_with_echo_idx[b].end(); ++it)
//        {
//            ++NPS_temp;
//            if (*it - NPS > 0 && *it - NPS < mls.NEcho(b))
//            if (std::find(rem_points_block.begin(),rem_points_block.end(),*it-NPS) == rem_points_block.end())
//                points_not_in_edges_nor_in_triangles_temp.emplace_back(mls.Pworld(b,*it-NPS));
//        }
//        NPS += NPS_temp;
//        std::cout << points_not_in_edges_nor_in_triangles_temp.size() << " points alone in block " << b << std::endl;
//        points_not_in_edges_nor_in_triangles.emplace_back(points_not_in_edges_nor_in_triangles_temp);
//        mls.Free(b);
//    }

//    std::cout << "Working on wedges " << std::endl;

    /// Wedge stuff
    //std::vector<Wedge> vw = convert_triangle_list_to_wedge(v_tri[0],mls);
    std::vector<Wedge>* vw = convert_edge_list_to_wedge(data_blocks,mls);
    std::vector<Wedge> svw = wedge_thresholding(mls,*vw,1e-3);
    std::tuple<std::vector<Triangle>,std::vector<std::tuple<uint,uint,uint> >, std::vector<uint> > w_tresh = convert_wedge_list_to_triangle(mls,svw,data_blocks->operator [](0),0,Mtri(v_tri[0]),5e-3);
    std::vector<Triangle> v_triw = std::get<0>(w_tresh);
    std::vector<std::vector<Triangle> > vt;
    vt.emplace_back(v_triw);
    /// @note add function to compute not used edges

    std::vector<uint> rem_points_block = std::get<2>(w_tresh);
    points_not_in_edges_nor_in_triangles.clear();
    std::cout << rem_points_block.size() << " points used on " << vv_pulse_with_echo_idx[0].size() << std::endl;
    mls.Load(0);
    std::vector<XPt3D> points_not_in_edges_nor_in_triangles_temp;

    uint NPS_temp = 0;
    for (std::vector<int>::iterator it = vv_pulse_with_echo_idx[b].begin(); it != vv_pulse_with_echo_idx[b].end(); ++it)
    {
        ++NPS_temp;
        if (*it - NPS > 0 && *it - NPS < mls.NEcho(b))
        if (std::find(rem_points_block.begin(),rem_points_block.end(),*it-NPS) == rem_points_block.end())
            points_not_in_edges_nor_in_triangles_temp.emplace_back(mls.Pworld(b,*it-NPS));
    }
    NPS += NPS_temp;
    std::cout << points_not_in_edges_nor_in_triangles_temp.size() << " points alone in block " << b << std::endl;
    points_not_in_edges_nor_in_triangles.emplace_back(points_not_in_edges_nor_in_triangles_temp);
    mls.Free(0);
cout << "Done in " << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl;

//    /** KMeans tests **/
//    srand(time(NULL));
//    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
//    vP3* v_points = new vP3;
//    mls.Load(0);
//    for (XEchoIndex echo = 0; echo != mls.NEcho(0); echo++)
//        v_points->emplace_back(mls.Pworld(0,echo)-Pivot);
//    KMeans* kmeans = new KMeans;
//    *kmeans = KMeans(v_points);
//    kmeans->solve(.1);
//    kmeans->store(mls,params);
//    mls.Free(0);

//    /** RANSAC tests **/
//    vT* vtri = new vT;
//    vtri = &vt[0];
//    //std::pair<Plane,vI> best_plane = ransac(vtri,2000,1000,1,mls,params);
//    //std::pair<std::vector<XPt3D>,std::vector<std::pair<Plane,Triangle> > > best_planes = ransacN(vtri,10,2000,200,.3,mls,params);
//    //std::tuple<std::vector<XPt3D>, std::vector<std::pair<Plane, Triangle> >, std::vector<std::pair<XPt3D, Color> > > best_planes = ransacN_colorized(vtri,3,2000,200,.3,mls,params);
//    std::tuple<std::vector<XPt3D>, std::vector<std::pair<Plane, Triangle> >, std::vector<std::pair<XPt3D, Color> >, std::vector<std::pair<XPt3D, Color> > > best_planes = ransac_segmentation(vtri,0.001,1000,.1,mls,params);
////    std::tuple<std::vector<XPt3D>, std::vector<std::pair<Plane, Triangle> >, std::vector<std::pair<XPt3D, Color> > > best_planes = ransacN_colorized(vtri,3,2000,200,.3,mls,params);
//    //std::cout << "tyfughjnk," << std::endl;
//    /*for (auto& best_plane:best_planes)
//    {
//        best_plane.first.print();
//        std::cout << "with " << best_plane.second.size() << " close points" << std::endl;
//    }*/
//    StorePlanes(std::get<0>(best_planes),params,mls);
//    std::vector<std::vector<std::pair<XPt3D,Color> > > points_colored;
//    std::vector<std::vector<std::pair<XPt3D,Color> > > project_colored;
//    points_colored.emplace_back(std::get<2>(best_planes));
//    project_colored.emplace_back(std::get<3>(best_planes));
//    WritePlyPointColor(mls,points_colored,params,"_observations.ply");
//    WritePlyPointColor(mls,project_colored,params,"_values.ply");
//    //vP planes = store_planes(best_planes,mls);
//    //std::vector<vT> planes_to_store;
//    //planes_to_store.emplace_back(planes);
//    //StorePlanes(mls, planes, mls.NTotalEcho() /*planes.size()*3*/, params); // Write Ply with planes as pairs of triangles



    /** Segmentation **/
    mls.Load(0);
//    std::cout << "Cate now wants to segment the point cloud of dust" << std::endl;
    std::vector<std::vector<double> > observations = find_observations_mesh(mls,params,vt[0]);
    //std::vector<std::vector<double> > observations = find_observations(mls,params);
//    std::cout << "Cate found observations by opening his eyes" << std::endl;
    //std::pair<std::vector<uint32_t>,std::vector<uint32_t> > EuEv = find_edges(vt[0], std::get<1>(w_tresh));
    std::pair<std::vector<uint32_t>,std::vector<uint32_t> > EuEv = find_edges(vt[0]);//(mls,params,1);
//    std::cout << "Cate found edges while falling of the table" << std::endl;
    std::vector<std::vector<double> > solution = observations;
    std::vector<uint32> inComponent;
    inComponent.resize(observations.size());

    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    //int ind_obs = 0;
    std::cout << vt[0].size() << " " << observations.size() << " " << EuEv.first.size() << std::endl;
    solution = observations;

    std::vector<double> edges_weight;
    for (uint i=0; i!=EuEv.first.size(); ++i) edges_weight.emplace_back(1);
    std::vector<double> nodes_weight;
    for (uint i=0; i!=observations.size(); ++i) nodes_weight.emplace_back(1);
    cut_pursuit(observations.size(),EuEv.first.size(),3,observations,EuEv.first,EuEv.second,edges_weight,nodes_weight,solution,.003,99,1,2,mls,params,inComponent);
    std::cout << "finished" << std::endl;



    //std::cout << solution[0][0] << " " << solution[0][1] << " " << solution[0][2] << std::endl;
    //WritePlyPointColor(mls,color_points(solution,inComponent),params);
    mls.Free(0);


    //if(params.Format() == "off") WriteOff(mls, scr, mls.NTotalEcho(), params);
    //else
//    WritePly(mls, v_tri/*vt*/, mls.NTotalEcho(), params); // Write Ply with triangles
//    //WritePlyEdge(mls, data_blocks, mls.NTotalEcho(), params);
//    WritePlySegment(mls, /*std::get<1>(w_tresh)*/edges_not_in_triangles, mls.NTotalEcho(), params); // Write Ply with edges
//    WritePlyPoint(mls,points_not_in_edges_nor_in_triangles,params);

    std::cout << mls.NEcho(0) << std::endl;

    cout << "Done in " << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl;
    return 0;
}


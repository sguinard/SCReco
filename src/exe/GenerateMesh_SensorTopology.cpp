#define WHAT "GenerateMesh: creates a mesh from a 3d point cloud by computing its delaunay tetrahedralization"

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/WedgeStuff.h"
#include "libSteph/SensorTopologyTools.h"

#include "libSteph/TinyPlyIO.h"
#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Voronoi.h"
#include "libSteph/Ransac.h"

using namespace std;
using namespace SCR;


/// \brief writes .ply file with a mesh
///
/// \param mls
/// \param v_tri: vector contianing for each block the liste of triangles
/// \param n_vertex: number of vertices
/// \param params
///
/// \warning doc is not up to date
void WritePly(vvD* observations, vector<Triangle>* v_tri, string out)
{
    int tri_size = 0;
    tri_size += v_tri->size();

    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_STmesh.ply");

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << observations->size() << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "element face " << tri_size << endl;
    fileOut << "property list uchar int32 vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * observations->size();

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for (auto& point:*observations)
        for (auto& dim:point)
        Write<float>(it,dim);

    cout << "Writing " << observations->size() << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*tri_size;
    buffer = new char[tri_buffer_size]; it = buffer;

    for(auto & tri:*v_tri)
    {
//        std::cout << tri.i << " " << tri.j << " " << tri.k << " - " << std::flush;
        Write<unsigned char>(it, 3);
        Write<int>(it, tri.i);
        Write<int>(it, tri.j);
        Write<int>(it, tri.k);
    }
    cout << "Writing " << tri_size << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}


/// \warning 02/09/18 - i'm really sorry about this one
uint32 get_nb_pulses_per_line(const std::string & filename, PlyFile* file)
{
    uint32 nppl = 0;

    std::ifstream ss(filename, std::ios::binary);

    if (ss.fail()) throw std::runtime_error("failed to open " + filename);

    for (auto c : file->get_comments())
    {
        /// \warning 02/09/18 - not optimal at all - but we assume that there won't be 10e9 comments so that's not too serious i guess
        std::size_t found_width = c.find("height");
        if (found_width != std::string::npos)
        {
            // find nb of spaces before 'width'
            uint32 nspaces_before_width = 0;

            for (uint32 i=0; i!=found_width; ++i)
                if (c.at(i) == ' ')
                    nspaces_before_width++;
            // find nb of spaces before 1st digit
            std::size_t found_digit = c.find_first_of("0123456789");
            uint32 nspaces_before_digit= 0;

            for (uint32 i=0; i!=found_digit; ++i)
                if (c.at(i) == ' ')
                    nspaces_before_digit++;
            // find all digit between nspaces_before_width + nspaces_before_digit and next space
            uint32 n = 0;
            uint32 i = 0;
            std::string number = " ";
            while (n != nspaces_before_digit+nspaces_before_width && i < c.size())
            {
                if (c.at(i) == ' ')
                    ++n;
                ++i;
            }
            // collect all digits before next space
            while (c.at(i) != ' ' && i < c.size())
                number += c.at(i++);
            nppl = atoi(const_cast<char*>(number.c_str()));
//            std::cout << "first 'width' found at: " << found_width << '\n'
//                      << "nb spaces before 'width': " << nspaces_before_width << "\n"
//                      << "nb spaces before digit: " << nspaces_before_digit << "\n"
//                      << "width: " << nppl << std::endl;
        }
    }

    return nppl;
}


void mesh_using_sensor_topology_and_naive_filtering(std::vector<Triangle> *v_tri, vvD *observations, uint32 nb_pulses_per_line)
{
    /// \details sensor topology
    ///
    /// \details
    ///         i + 1
    ///           |
    /// i-nppl    |      i+nppl+1
    ///       \   |   /
    ///           i
    ///        /  |  \
    ///       /   |   \
    /// i-nppl-1  |      i+nppl
    ///           |
    ///         i - 1

    // for each point
    for (int i=0; i!=observations->size()-nb_pulses_per_line-1; ++i)
    {
        v_tri->emplace_back(Triangle(i,i+nb_pulses_per_line,i+nb_pulses_per_line+1));
        v_tri->emplace_back(Triangle(i,i+nb_pulses_per_line,i+1));
    }
}




void mesh_using_sensor_topology_and_naive_filtering(std::vector<Triangle> *v_tri, vvD *observations, uint32 nb_pulses_per_line, double max_edge_length)
{
    /// \details sensor topology
    ///
    /// \details
    ///         i + 1
    ///           |
    /// i-nppl    |      i+nppl+1
    ///       \   |   /
    ///           i
    ///        /  |  \
    ///       /   |   \
    /// i-nppl-1  |      i+nppl
    ///           |
    ///         i - 1

    // for each point
    for (int i=0; i!=observations->size()-nb_pulses_per_line-1; ++i)
    {
        XPt3D p1 = XPt3D(observations->at(i)[0],observations->at(i)[1],observations->at(i)[2]);
        XPt3D p2 = XPt3D(observations->at(i+1)[0],observations->at(i+1)[1],observations->at(i+1)[2]);
        XPt3D p3 = XPt3D(observations->at(i+nb_pulses_per_line)[0],observations->at(i+nb_pulses_per_line)[1],observations->at(i+nb_pulses_per_line)[2]);
        XPt3D p4 = XPt3D(observations->at(i+nb_pulses_per_line+1)[0],observations->at(i+nb_pulses_per_line+1)[1],observations->at(i+nb_pulses_per_line+1)[2]);
//std::cout << dist(p1,p2) << " " << dist(p1,p3) << " " << dist(p2,p3) << " " << dist(p3,p4) << std::endl;
        if (max(max(dist(p1,p3),dist(p1,p4)),dist(p3,p4)) < max_edge_length)
            v_tri->emplace_back(Triangle(i,i+nb_pulses_per_line,i+nb_pulses_per_line+1));

        if (max(max(dist(p1,p4),dist(p1,p2)),dist(p4,p2)) < max_edge_length)
            v_tri->emplace_back(Triangle(i,i+nb_pulses_per_line,i+1));
    }
}



int main(int argc, char **argv)
{
    std::string first_arge;
    first_arge = argv[1];

    std::cout << first_arge << std::endl;

    tinyply::PlyFile* file = new tinyply::PlyFile;

    vvD* observations = new vvD;
    PlyFile2Vector(first_arge, file, observations);

    vector<Triangle>* v_tri = new std::vector<Triangle>;

    uint32 nppl = get_nb_pulses_per_line(first_arge, file);

    SensorTopology* st = new SensorTopology;
    *st = SensorTopology(observations);
    st->compute_edges(.05,1e-4,nppl);
    st->compute_wedges();
    st->filter_wedges(1e-3);
    st->compute_triangles_from_wedges();
    //st->compute_triangles_from_edges();
    st->get_triangles(v_tri);

//    mesh_using_sensor_topology_and_naive_filtering(v_tri,observations,nppl);

    WritePly(observations,v_tri,first_arge);

    delete observations;
    delete v_tri;

    return 0;
}




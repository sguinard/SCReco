#define WHAT "VSA: Variational Shape Approximation"

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/WedgeStuff.h"
#include "libSteph/KMeans.h"

#include "libSteph/ConvexHull.h"

#include "libSteph/SC.h"

#include "libSteph/CutPursuit.h"
#include "../extern/cut-pursuit-master/include/API.h"

#include "libSteph/TinyPlyIO.h"
#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Voronoi.h"
#include "libSteph/Ransac.h"

#include "libSteph/Triangle.h"
#include "libSteph/Triangle_Coords.h"

#include "libSteph/VSA.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

using namespace std;
using namespace SCR;

int main(int argc, char **argv)
{

    //srand(0);
  double tresh = argc > 3 ? atof(argv[3]) : 0.9;
    std::cout << WHAT << std::endl;

    std::string first_arge;
    first_arge = argv[1];

    int nb_regions = argc > 2 ? atoi(argv[2]) : 10;

    std::cout << first_arge << std::endl;

    tinyply::PlyFile* file = new tinyply::PlyFile;

    std::vector<XPt3D>    *points    = new std::vector<XPt3D>;
    std::vector<Triangle> *triangles = new std::vector<Triangle>;
    
    PlyFile2Vector(first_arge, file, points, triangles);

    std::cout << "Got " << triangles->size() << " triangles as input" << std::endl;

    VSA* vsa = new VSA(nb_regions,tresh,points,triangles);
    vsa->solve();
    vsa->store();
    vsa->store_projections();
    vsa->store_points_colored();
    
    return 0;

}

#define WHAT "WHAAAT WHAAAT"

#include <ctime>
#include <iostream>
#include <limits>
#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
//#include "libSteph/TinyPlyIO.h"
#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Voronoi.h"
#include "libSteph/Proxy.h"

using namespace std;
using namespace SCR;


void load(const std::string &filename, PlyFile *file, std::map<uint32_t, XPt3D> *id2pts, std::map<uint32_t, std::vector<uint32_t> > *reg2id)
{

  std::ifstream ss(filename, std::ios::binary);

  if (ss.fail()) throw std::runtime_error("failed to open" + filename);

  file->parse_header(ss);

  std::cout << ".......................................................................\n";
  for (auto c : file->get_comments()) std::cout << "Comment: " << c << std::endl;
  for (auto e : file->get_elements())
    {
      std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
      for (auto p : e.properties) std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")" << std::endl;
      std::cout << "...................................................................\n";
    }
  // Tinyply 2.0 treats incoming data as untyped byte buffers. It's now
    // up to users to treat this data as they wish. See below for examples.
    std::shared_ptr<PlyData> vertices_region, normals, faces, texcoords;

    // The header information can be used to programmatically extract properties on elements
    // known to exist in the file header prior to reading the data. For brevity of this sample, properties
    // like vertex position are hard-coded:
    try { vertices_region = file->request_properties_from_element("vertex", { "x", "y", "z", "component" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    try { faces = file->request_properties_from_element("face", { "vertex_indices" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    manual_timer read_timer;

    read_timer.start();
    file->read(ss);
    read_timer.stop();

    std::cout << "Parsing took " << read_timer.get() / 1000.f << " seconds: " << std::endl;
    if (vertices_region) std::cout << "\tRead " << vertices_region->count << " total vertices "<< std::endl;
    if (normals) std::cout << "\tRead " << normals->count << " total vertex normals " << std::endl;
    if (texcoords) std::cout << "\tRead " << texcoords->count << " total vertex texcoords " << std::endl;
    if (faces) std::cout << "\tRead " << faces->count << " total faces (triangles) " << std::endl;

    // Example: type 'conversion' to your own native types - Option A

    const size_t numVerticesBytes = vertices_region->buffer.size_bytes();
    std::vector<float4> verts(vertices_region->count);
    std::memcpy(verts.data(), vertices_region->buffer.get(), numVerticesBytes);

    uint32_t id = 0;
    for (auto& vertex:verts)
      {
	id2pts->insert( std::pair<uint32_t,XPt3D>(id++, XPt3D((double)vertex.x, (double)vertex.y, (double)vertex.z)) );
	// add pts ids to reg2id map
	if (reg2id->find(vertex.component) != reg2id->end())
	    reg2id->at(vertex.component).emplace_back(id-1);
	else
	    reg2id->insert( std::pair<uint32_t,std::vector<uint32_t> >(vertex.component, std::vector<uint32_t>{id-1}));
      }
      //observations->emplace_back(std::vector<double> {(double)vertex.x, (double)vertex.y, (double)vertex.z});

    //const size_t numFacesBytes = faces->buffer.size_bytes();
    //std::vector<mesh_triangle> facs(faces->count);
    //std::memcpy(facs.data(), faces->buffer.get(), numFacesBytes);
}


void find_proxies(std::map<uint32_t, std::vector<uint32_t> > *reg2id, std::map<uint32_t, XPt3D> *id2pts, std::vector<Proxy> *proxies)
{

  for (auto& reg : *reg2id)
    {
      vP3* pts = new vP3;
      for (auto p: reg.second)
	pts->emplace_back(id2pts->at(p));
      proxies->emplace_back(Proxy(pts));
    }
  
}


int main (int argc, char **argv)
{
  std::cout << WHAT << std::endl;

  std::string first_arge;
  first_arge = argv[1];

  std::cout << first_arge << std::endl;

  tinyply::PlyFile* file = new tinyply::PlyFile;

  std::map <uint32_t, XPt3D> *id2pts = new std::map <uint32_t, XPt3D>;
  std::map <uint32_t, std::vector<uint32_t> > *reg2id = new std::map <uint32_t, std::vector<uint32_t > >;
  std::vector<Proxy> *proxies = new std::vector<Proxy>;

  std::cout << "...... LOADING ......" << std::endl;
  load(first_arge, file, id2pts, reg2id);

  std::cout << id2pts->at(0).X << " " << id2pts->at(0).Y << " " << id2pts->at(0).Z << std::endl;
  std::cout << reg2id->at(1)[0] << std::endl;

  std::cout << "...... COMPUTING PROXIES ......" << std::endl;
  find_proxies(reg2id, id2pts, proxies);

  std::cout << proxies->at(0).NX() << " " << proxies->at(0).NY() << " " << proxies->at(0).NZ() << std::endl;
  //std::cout << proxies->at(1).NX() << " " << proxies->at(1).NY() << " " << proxies->at(1).NZ() << std::endl;
  
  return 0;
}

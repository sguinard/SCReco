#define WHAT "SCSimplification: performs a simplification of simplicial complexes"

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/WedgeStuff.h"
#include "libSteph/KMeans.h"

#include "libSteph/ConvexHull.h"

#include "libSteph/SC.h"

#include "libSteph/CutPursuit.h"
#include "../extern/cut-pursuit-master/include/API.h"

#include "libSteph/TinyPlyIO.h"
#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Voronoi.h"
#include "libSteph/Ransac.h"

#include "libSteph/Triangle_Coords.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Timer.h>

using namespace std;
using namespace SCR;

int main(int argc, char **argv)
{

    //srand(0);

    std::cout << WHAT << std::endl;

    if (argc < 3 || argc > 5)
    {
        std::cerr << "SCSimplification needs 2 (+2 optional) arguments: "
                     "\n\tthe filename of the processed .ply file (string)"
                     "\n\tthe mode used for graph creation (int):"
                     "\n\t\t- 0 for plane-based Ransac computation and segmentation"
                     "\n\t\t- 1 for plane-based Ransac computation + segmentation using Cut-Pursuit structured with mesh adjacency"
                     "\n\t\t- 2 for plane-based Ransac computation + segmentation using Cut-Pursuit structured with delaunay triangulation"
                     "\n\tRansac regularization strength (float) (optional - default = 0.05)"
                     "\n\tCut-Pursuit regularization strength (float) (optional - default = 0.001)" << std::endl;
        return -1;  /// \warning btw did you know that return code must be positive ? ie. -1 returns 255 which an error about pointer non initialization! :D
    }

    std::string first_arge;
    first_arge = argv[1];
    int mode = atoi(argv[2]);
    double ransac_reg = argc > 3 ? atof(argv[3]) : 0.05;

    std::cout << first_arge << std::endl;

    tinyply::PlyFile* file = new tinyply::PlyFile;

    vvD* observations = new vvD;
    std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* edges = new std::pair<std::vector<uint32_t>, std::vector<uint32_t> >;
    std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* e_vor = new std::pair<std::vector<uint32_t>, std::vector<uint32_t> >;

    vvD* solution = new vvD;
    *solution = *observations;
    std::pair<std::vector<uint32>,uint32> inComponent;

    double d0 = 1; // mean distance between 2 points

    if (mode < 3)
      {
	PlyFile2Vector(first_arge, file, observations, *&edges);
	//    PlyFile2Vector(first_arge, file, observations);

	std::cout << "Initialization with RANSAC..." << std::flush;
	CGAL::Timer t;
	t.start();
	inComponent = ransac_from_3d_points_unparallelized(observations,solution,ransac_reg);
	t.stop();
	std::cout << " Done. Time: " << t.time() << " sec." << std::endl;
      }

    if (mode == 0)  // ransac only
    {
        std::cout << "Mesh segmentation using RANSAC only\n........................................................................" << std::endl;

        std::pair<std::vector<std::vector<std::pair<XPt3D, Color> > >,std::vector<std::vector<std::pair<XPt3D, Color> > > > points_colored = color_points(*solution,*observations,
                                                                                                                                                          inComponent.first,inComponent.second);
        WritePlyPointColor(points_colored.first,"_values.ply");
        WritePlyPointColor(points_colored.second,"_observations.ply");
    }
    if (mode == 1)  // edges = mesh adjacency
    {
        double cp_reg = argc == 5 ? atof(argv[4]) : 0.001;
        std::cout <<
                     "Mesh segmentation using \ell_0-Plane Pursuit\n"
                     "\tgraph is structured with mesh adjacency"
                     "\n........................................................................"
                  << std::endl;
        e_vor = edges;

        //std::cout << e_vor->first.size() << std::endl;
        //std::cout << e_vor->first[0] << " " << e_vor->second[0] << std::endl;

        //for (uint8_t i = 0; i != e_vor->first.size(); ++i)
        //    std::cout << e_vor->first[i] << " " << e_vor->second[i] << " - " << std::flush;

	/*XPt3D center = XPt3D(0,0,0);
	for (auto& p:*observations)
	  {
	    center.X += p[0];
	    center.Y += p[1];
	    center.Z += p[2];
	  }
        center.operator /=(observations->size());
	for (auto& p:*observations)
	  {
	    XPt3D p3;
	    p3.X = p[0];
	    p3.Y = p[1];
	    p3.Z = p[2];
	    d0 += d2(center,p3);
	    }*/
	for (uint32_t i = 0; i != e_vor->first.size(); ++i)
	  {
	    XPt3D p1;
	    p1.X = observations->at(e_vor->first[i])[0];
	    p1.Y = observations->at(e_vor->first[i])[1];
	    p1.Z = observations->at(e_vor->first[i])[2];
	    XPt3D p2;
	    p2.X = observations->at(e_vor->second[i])[0];
	    p2.Y = observations->at(e_vor->second[i])[1];
	    p2.Z = observations->at(e_vor->second[i])[2];
	    d0 += d2(p1,p2);
	  }
	//d0 *= 1e4;
	d0 /= e_vor->first.size();

	std::cout << "mean distance is: " << d0 << std::endl;

        std::vector<double> edge_weight(e_vor->first.size(),1);
	for (uint32_t i = 0; i !=e_vor->first.size(); ++i)
	  {
	    XPt3D p1;
	    p1.X = observations->at(e_vor->first[i])[0];
	    p1.Y = observations->at(e_vor->first[i])[1];
	    p1.Z = observations->at(e_vor->first[i])[2];
	    XPt3D p2;
	    p2.X = observations->at(e_vor->second[i])[0];
	    p2.Y = observations->at(e_vor->second[i])[1];
	    p2.Z = observations->at(e_vor->second[i])[2];
	    edge_weight[i] = 100/(2+d2(p1,p2)/d0); 
      //std::cout << edge_weight[i] << " " << std::flush;
	    //std::cout << d2(p1,p2)*100 << " " << std::flush;
	  }
        std::vector<double> node_weight(observations->size(),1);

        cut_pursuit(observations->size(),e_vor->first.size(),3,*observations,e_vor->first,e_vor->second,edge_weight,node_weight,*solution,cp_reg,2,1,2,inComponent.first, first_arge);
    }

    if (mode == 2)  // edges = delaunay triangulation
    {
        double cp_reg = argc == 5 ? atof(argv[4]) : 0.001;
        std::cout <<
                     "Mesh segmentation using \ell_0-Plane Pursuit\n"
                     "\tgraph is structured with a 3D Delaunay triangulation"
                     "\n........................................................................"
                  << std::endl;
        vector<Triangle>* v_tri = new std::vector<Triangle>;

        std::list<Triangulation::Point> L;
        L = addToList(L, observations);

        v_tri = convert_3d_delaunay(L,observations,.5);
//        std::pair<std::vector<uint32_t>,std::vector<uint32_t> >* e_vor = new std::pair<std::vector<uint32_t>,std::vector<uint32_t> >;
        *e_vor = find_edges(*v_tri);
        
//        e_vor = create_3d_delaunay(L,observations);

        std::cout << e_vor->first.size() << " " << e_vor->second.size() << std::endl;
//        for (int i=0; i != e_vor->first.size(); ++i)
//        std::cout << e_vor->first[i] << " " << e_vor->second[i] << std::endl;

        std::vector<double> edge_weight(e_vor->first.size(),1);
        std::vector<double> node_weight(observations->size(),1);

        cut_pursuit(observations->size(),e_vor->first.size(),3,*observations,e_vor->first,e_vor->second,edge_weight,node_weight,*solution,cp_reg,2,1,2,inComponent.first, first_arge);

        delete v_tri;
    }

    if (mode == 3)
      {
	std::vector<TriangleCoords>* vtri_coords = new std::vector<TriangleCoords>;
	PlyFile2Vector(first_arge, file, vtri_coords);

	//cut_pursuit_tri(observations->size(),e_vor->first.size(),3,*observations,e_vor->first,e_vor->second,edge_weight,node_weight,*solution,cp_reg,99,1,2,inComponent.first);
      }

    /*delete edges;
    delete e_vor;
    delete solution;
    delete observations;*/

    return 0;
}

#define WHAT "SCSimplification: performs a simplification of simplicial complexes"

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/WedgeStuff.h"
#include "libSteph/KMeans.h"

#include "libSteph/ConvexHull.h"

#include "libSteph/SC.h"

#include "libSteph/CutPursuit.h"
#include "../extern/cut-pursuit-master/include/API.h"

#include "libSteph/TinyPlyIO.h"
#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Voronoi.h"
#include "libSteph/Ransac.h"
#include "libSteph/VSA.h"

#include "libSteph/Triangle_Coords.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

using namespace std;
using namespace SCR;

/// custom adjacency function to compare vectors of 3 points indeces (triangles)
    /// and return whether the vectors have 2 points (an edge) in common
    bool adjacent(std::vector<uint32_t>* t1, std::vector<uint32_t>* t2)
    {
      uint8_t adj = 0;
      for (auto &p: *t1)
	if (std::find(t2->begin(), t2->end(), p) != t2->end())
	  ++adj;
      return adj == 2 ? true : false;
    }

double adjacency_length(std::vector<uint32_t>* t1, std::vector<uint32_t>* t2, vP3* points)
{
  if (!adjacent(t1,t2)) return 0;
  // now find edge along adjacency
  uint8_t source = -1;
  uint8_t target = -1;
  for (uint8_t p = 0; p != t1->size(); ++p)
  if (std::find(t2->begin(), t2->end(), t1->at(p)) != t2->end())
    source == (uint8_t)-1 ? source = p : target = p;
  if (source == (uint8_t)-1 || target == (uint8_t)-1) return 0;
  //std::cout << d2(points->at(source), points->at(target)) << " " << std::flush;
  return d2(points->at(source), points->at(target));
}

int main(int argc, char **argv)
{

    //srand(0);

    std::cout << WHAT << std::endl;

    if (argc < 3 || argc > 5)
    {
        std::cerr << "SCSimplification needs 2 (+2 optional) arguments: "
                     "\n\tthe filename of the processed .ply file (string)"
                     "\n\tthe mode used for graph creation (int):"
                     "\n\t\t- 0 for plane-based Ransac computation and segmentation"
                     "\n\t\t- 1 for plane-based Ransac computation + segmentation using Cut-Pursuit structured with mesh adjacency"
                     "\n\t\t- 2 for plane-based Ransac computation + segmentation using Cut-Pursuit structured with delaunay triangulation"
                     "\n\tRansac regularization strength (float) (optional - default = 0.05)"
                     "\n\tCut-Pursuit regularization strength (float) (optional - default = 0.001)" << std::endl;
        return -1;  /// \warning btw did you know that return code must be positive ? ie. -1 returns 255 which an error about pointer non initialization! :D
    }
std::cout << "coucou" << std::endl;
    std::string first_arge;
    first_arge = argv[1];
std::cout << argc << std::endl;
    //int mode = atoi(argv[2]);
    double ransac_reg = argc > 3 ? atof(argv[3]) : 0.05;
std::cout << "hohoho" << std::endl; 
    std::cout << first_arge << std::endl;

    tinyply::PlyFile* file = new tinyply::PlyFile;

    vvD* observations = new vvD;
    vvD* points = new vvD;
    std::vector<std::vector<uint32_t> > *triangles = new std::vector<std::vector<uint32_t> >;
    std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* edges = new std::pair<std::vector<uint32_t>, std::vector<uint32_t> >;
    std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* e_vor = new std::pair<std::vector<uint32_t>, std::vector<uint32_t> >;

    vvD* solution = new vvD;
    *solution = *observations;
    std::pair<std::vector<uint32>,uint32> inComponent;

    double d0 = 1; // mean distance between 2 points

    std::cout << "\n................. LOADING .................\n" << std::endl;

    PlyFile2Vector(first_arge, file, points, observations, triangles);
    //    PlyFile2Vector(first_arge, file, observations);

    std::cout << "\n.......... RANSAC INITIALIZATION ..........\n" << std::endl;
    
    inComponent = ransac_from_3d_points_unparallelized(observations,solution,ransac_reg);

    std::cout << "\n...... COMPUTING TRIANGLES ADJACENCY ......\n" << std::endl;


    // find triangle adjacency
    std::map<uint32_t, std::vector<uint32_t> > *M = new std::map<uint32_t, std::vector<uint32_t> >;
    std::unordered_map<tuple<uint32_t, uint32_t, uint32_t>, uint32_t> *tri_idx = new std::unordered_map<tuple<uint32_t, uint32_t, uint32_t>, uint32_t>;
    std::vector<std::pair<uint32_t,uint32_t> > *tri_adj = new std::vector<std::pair<uint32_t, uint32_t> >;
    std::cout << triangles->size() << " triangles" << std::endl;
    for (uint32_t t_idx = 0; t_idx != triangles->size(); ++t_idx)
      {
	//if (t_idx%100 == 0) std::cout << t_idx << " " << std::flush;
	//std::cout << triangles->at(t_idx)[0] << " " << triangles->at(t_idx)[1] << " " << triangles->at(t_idx)[2] << std::endl;
	if (tri_idx->find(tuple<uint32_t, uint32_t, uint32_t>(triangles->at(t_idx)[0], triangles->at(t_idx)[1], triangles->at(t_idx)[2])) != tri_idx->end()) tri_idx->at(tuple<uint32_t, uint32_t, uint32_t>(triangles->at(t_idx)[0], triangles->at(t_idx)[1], triangles->at(t_idx)[2])) = t_idx;
	else tri_idx->insert(std::pair<tuple<uint32_t, uint32_t, uint32_t>, uint32_t>( tuple<uint32_t, uint32_t, uint32_t>(triangles->at(t_idx)[0], triangles->at(t_idx)[1], triangles->at(t_idx)[2]), t_idx));
	if (M->find(triangles->at(t_idx)[0]) != M->end()) M->at(triangles->at(t_idx)[0]).emplace_back(t_idx);
	else M->insert(std::pair<uint32_t, std::vector<uint32_t> >(triangles->at(t_idx)[0], std::vector<uint32_t>{t_idx}));
	if (M->find(triangles->at(t_idx)[1]) != M->end()) M->at(triangles->at(t_idx)[1]).emplace_back(t_idx);
	else M->insert(std::pair<uint32_t, std::vector<uint32_t> >(triangles->at(t_idx)[1], std::vector<uint32_t>{t_idx}));
	if (M->find(triangles->at(t_idx)[2]) != M->end()) M->at(triangles->at(t_idx)[2]).emplace_back(t_idx);
	else M->insert(std::pair<uint32_t, std::vector<uint32_t> >(triangles->at(t_idx)[2], std::vector<uint32_t>{t_idx}));
      }
    std::cout << M->size() << std::endl;
    //for (auto& prout: *M) std::cout << prout.first << " " << std::endl;
    for (uint32_t p_idx = 0; p_idx != points->size(); ++p_idx)
      if (M->find(p_idx) != M->end())
      {
	std::vector<uint32_t> *vtri_idx = new std::vector<uint32_t>;
	*vtri_idx = M->at(p_idx);
	//std::cout << p_idx << " " << std::flush;
	for (uint32_t idx1 = 0; idx1 != vtri_idx->size(); ++idx1)
	  for (uint32_t idx2 = idx1+1; idx2 < vtri_idx->size(); ++idx2)
	    if (adjacent(&triangles->at(vtri_idx->at(idx1)),&triangles->at(vtri_idx->at(idx2))))
	      tri_adj->emplace_back(std::make_pair(vtri_idx->at(idx1),vtri_idx->at(idx2)));
	}
    
    std::sort(tri_adj->begin(), tri_adj->end());
    tri_adj->erase( unique(tri_adj->begin(), tri_adj->end()), tri_adj->end());

    for (auto& e: *tri_adj)
      {
	edges->first.emplace_back(e.first);
	edges->second.emplace_back(e.second);
      }

    //for (uint32_t i = 0; i != edges->first.size(); ++i)
    //  std::cout << edges->first[i] << " " << edges->second[i] << " - " << std::flush;
      

    /// adding triangle adjacency in edges

    //if (mode == 1)  // edges = mesh adjacency
    {
        double cp_reg = argc == 4 ? atof(argv[3]) : 0.001;
        std::cout <<
                     "Mesh segmentation using \ell_0-Cut-Pursuit\n"
                     "\tgraph is structured with mesh adjacency"
                     "\n........................................................................"
                  << std::endl;
        e_vor = edges;

        std::cout << e_vor->first.size() << std::endl;
        std::cout << e_vor->first[0] << " " << e_vor->second[0] << std::endl;

	std::vector<double> edge_weight(e_vor->first.size(),1);
	for (uint32_t i = 0; i != e_vor->first.size(); ++i)
	  {
	    std::vector<uint32_t> t1 = triangles->at(e_vor->first[i]);
	    std::vector<uint32_t> t2 = triangles->at(e_vor->second[i]);
	    //std::cout << t1[0] << " " << t1[1] << " " << t1[2] << " - " << t2[0] << " " << t2[1] << " " << t2[2] << std::endl;
	    vP3 pts = vP3{XPt3D(points->at(t1[0])[0], points->at(t1[0])[1], points->at(t1[0])[2]), XPt3D(points->at(t1[1])[0], points->at(t1[1])[1], points->at(t1[1])[2]), XPt3D(points->at(t1[2])[0], points->at(t1[2])[1], points->at(t1[2])[2])};
	    //std::cout << adjacency_length(&t1, &t2, &pts) << " " << std::flush;
	    edge_weight[i] = 10 * (adjacency_length(&t1, &t2, &pts));
	  }

        std::vector<double> node_weight(observations->size(),1);

	for (uint32_t i = 0; i != observations->size(); ++i)
	  {
	    Triangle t = Triangle(triangles->at(i)[0], triangles->at(i)[1], triangles->at(i)[2]);
	    XPt3D p1 = XPt3D(points->at(t.i)[0], points->at(t.i)[1], points->at(t.i)[2]);
	    XPt3D p2 = XPt3D(points->at(t.j)[0], points->at(t.j)[1], points->at(t.j)[2]);
	    XPt3D p3 = XPt3D(points->at(t.k)[0], points->at(t.k)[1], points->at(t.k)[2]);
	    XPt3D p12 = p2.operator -=(p1);
	    XPt3D p13 = p3.operator -=(p1);
	    node_weight[i] = 1e10 * 0.5 * sqrt(std::pow(p12.Y * p13.Z - p12.Z * p13.Y, 2) + std::pow(p12.Z * p13.X - p12.X * p13.Z, 2) + std::pow(p12.X * p13.Y - p12.Y * p13.X, 2));
	    //std::cout << node_weight[i]*1e-10 << " " << std::flush;
	  }
	
	cut_pursuit_tri(observations->size(),e_vor->first.size(),3,*observations,points,triangles,e_vor->first,e_vor->second,edge_weight,node_weight,*solution,cp_reg,3,1,2,inComponent.first);
    }

    return 0;
}

#define WHAT "GenerateMesh: creates a mesh from a 3d point cloud by computing its delaunay tetrahedralization"

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/WedgeStuff.h"
#include "libSteph/KMeans.h"

#include "libSteph/ConvexHull.h"

#include "libSteph/SC.h"

#include "libSteph/CutPursuit.h"
#include "../extern/cut-pursuit-master/include/API.h"

#include "libSteph/TinyPlyIO.h"
#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Voronoi.h"
#include "libSteph/Ransac.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

using namespace std;
using namespace SCR;


/// \brief writes .ply file with a mesh
///
/// \param mls
/// \param v_tri: vector contianing for each block the liste of triangles
/// \param n_vertex: number of vertices
/// \param params
///
/// \warning doc is not up to date
void WritePly(vvD* observations, vector<Triangle>* v_tri, string out)
{
    int tri_size = 0;
    tri_size += v_tri->size();

    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_mesh.ply");

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << observations->size() << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "element face " << tri_size << endl;
    fileOut << "property list uchar int32 vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * observations->size();

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for (auto& point:*observations)
        for (auto& dim:point)
        Write<float>(it,dim);

    cout << "Writing " << observations->size() << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*tri_size;
    buffer = new char[tri_buffer_size]; it = buffer;

    for(auto & tri:*v_tri)
    {
//        std::cout << tri.i << " " << tri.j << " " << tri.k << " - " << std::flush;
        Write<unsigned char>(it, 3);
        Write<int>(it, tri.i);
        Write<int>(it, tri.j);
        Write<int>(it, tri.k);
    }
    cout << "Writing " << tri_size << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}



int main(int argc, char **argv)
{
    std::string first_arge;
    first_arge = argv[1];

    std::cout << first_arge << std::endl;

    tinyply::PlyFile* file = new tinyply::PlyFile;

    vvD* observations = new vvD;
    PlyFile2Vector(first_arge, file, observations);

    vector<Triangle>* v_tri = new std::vector<Triangle>;

    std::list<Triangulation::Point> L;
    L = addToList(L, observations);

    v_tri = convert_3d_delaunay(L,observations,.1);

    WritePly(observations,v_tri,first_arge);

    delete observations;
    delete v_tri;

    return 0;
}




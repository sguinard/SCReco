/** @brief
 * Same as SensorMesh2
 * Except storage part is modified
 * To output a simplicial complex
 */


#define WHAT "SensorMesh2: create a single .ply mesh for all the specified time interval using sensor topology"

#include <ctime>
#include <iostream>
#include <limits>
#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/GlobalTools.h"


using namespace std;

/// \warning ONLY MONOBLOCK

void WritePly(XMls & mls, vector<Triangle> v_tri,
              int n_vertex, param params)
{
    ofstream fileOut(params.output);
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }
    fileOut << "element face " << v_tri.size() << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    XFloatAttrib * p_refl=NULL;
    if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            XEchoIndex last_echo_idx = mls.IdxLastEcho(block_idx, pulse_idx);
            XPt3D Pw = mls.Pworld(block_idx, last_echo_idx);
            float e=Pw.X-params.pivot_E, n=Pw.Y-params.pivot_N, h=Pw.Z-params.pivot_H;
            //cout << e << " " << n << " " << h << endl;
            Write<float>(it, e);
            Write<float>(it, n);
            Write<float>(it, h);
            if(params.add_rgb == 1) // r g b mode
            {
                float g = 12.75f*(p_refl->at(block_idx)[last_echo_idx]+20.f); // rescale [-20,0] to [0,256] TODO: parameters
                if(g<0.f) g=0.f; else if(g>255.f) g=255.f;
                unsigned char ug = g;
                Write<unsigned char>(it, ug);
                Write<unsigned char>(it, ug);
                Write<unsigned char>(it, ug);
            }
            else if(params.add_rgb == 2) // quality mode
            {
                Write<float>(it, p_refl->at(block_idx)[last_echo_idx]);
            }
        }
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*v_tri.size();
    buffer = new char[tri_buffer_size]; it = buffer;
    for(auto & tri:v_tri)
    {
        //std::cout << tri.i << " " << tri.j << " " << tri.k << " - " << std::flush;
        Write<unsigned char>(it, 3);
        Write<int>(it, tri.i);
        Write<int>(it, tri.j);
        Write<int>(it, tri.k);
    }
    cout << "Writing " << v_tri.size() << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}


// make a wedge based on the four vertices ABXY = (AXB)+(ABY) triangles
void MakeWedge(XMls & mls, vector<Triangle> & v_tri, std::vector<std::tuple<uint,uint,uint> > & v_edge, std::vector<uint> & v_points_used,
               vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
               XBlockIndex ba, XPulseIndex pa, XBlockIndex bb, XPulseIndex pb,
               XBlockIndex bx, XPulseIndex px, XBlockIndex by, XPulseIndex py)
{
    if(mls.NbOfEcho(ba, pa)==0 || mls.NbOfEcho(bb, pb) == 0) return;
    //cout << "ABXY (" << ba << "," << pa << ")("<< bb << "," << pb << ")("<< bx << "," << px << ")("<< by << "," << py << ")" << endl;
    int A=vv_pulse_with_echo_idx[ba][pa], B=vv_pulse_with_echo_idx[bb][pb];
    int X=vv_pulse_with_echo_idx[bx][px], Y=vv_pulse_with_echo_idx[by][py];
    bool ab = false;
    if(mls.NbOfEcho(bx, px)>0)
    {
        if(Maxedgesize(mls, ba, pa, bb, pb, bx, px) < tri_threshold)
        {
            ab = true;
            v_tri.push_back(Triangle(A, B, X));
            v_points_used.push_back(A);
            v_points_used.push_back(B);
            v_points_used.push_back(X);
        }
        else
        {
            if (SCR::edge(ba,A,X).length(mls) < tri_threshold)
            {
                v_edge.push_back(std::make_tuple(ba,A,X));
                v_points_used.push_back(A);
                v_points_used.push_back(X);
            }
            if (SCR::edge(ba,X,B).length(mls) < tri_threshold)
            {
                v_edge.push_back(std::make_tuple(ba,X,B));
                v_points_used.push_back(X);
                v_points_used.push_back(B);
            }
        }
    }
    if(mls.NbOfEcho(by, py)>0)
    {
        if(Maxedgesize(mls, ba, pa, bb, pb, by, py) < tri_threshold)
        {
            ab = true;
            v_tri.push_back(Triangle(A, Y, B));
            v_points_used.push_back(A);
            v_points_used.push_back(Y);
            v_points_used.push_back(B);
        }
        else
        {
            if (SCR::edge(ba,A,Y).length(mls) < tri_threshold)
            {
                v_edge.push_back(std::make_tuple(ba,A,Y));
                v_points_used.push_back(A);
                v_points_used.push_back(Y);
            }
            if (SCR::edge(ba,Y,B).length(mls) < tri_threshold)
            {
                v_edge.push_back(std::make_tuple(ba,Y,B));
                v_points_used.push_back(Y);
                v_points_used.push_back(B);
            }
        }
    }
    if (!ab)
        if (SCR::edge(ba,A,B).length(mls) < tri_threshold)
        {
            v_edge.push_back(std::make_tuple(ba,A,B));
            v_points_used.push_back(A);
            v_points_used.push_back(B);
        }
    v_points_used.erase(unique(v_points_used.begin(),v_points_used.end()),v_points_used.end());
}


/*// make a wedge based on the four vertices ABXY = (AXB)+(ABY) triangles
void MakeWedge(XMls & mls, vector<Triangle> & v_tri, std::vector<std::tuple<uint,uint,uint> >edges, std::vector<std::vector<XPt3D> > points,
               vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
               XBlockIndex ba, XEchoIndex pa, XBlockIndex bb, XEchoIndex pb,
               XBlockIndex bx, XEchoIndex px, XBlockIndex by, XEchoIndex py)
{
    //if(mls.NbOfEcho(ba, pa)==0 || mls.NbOfEcho(bb, pb) == 0) return;
    //cout << "ABXY (" << ba << "," << pa << ")("<< bb << "," << pb << ")("<< bx << "," << px << ")("<< by << "," << py << ")" << endl;
    int A=pa, B=pb;
    int X=px, Y=py;
    //if(mls.NbOfEcho(bx, px)>0)
    //{
    //    if(Maxedgesize(mls, ba, pa, bb, pb, bx, px) < tri_threshold)
            v_tri.push_back(Triangle(A, B, X));
    //}
    //if(mls.NbOfEcho(by, py)>0)
    //{
    //    if(Maxedgesize(mls, ba, pa, bb, pb, by, py) < tri_threshold)
            v_tri.push_back(Triangle(A, Y, B));
    //}
}*/


void WritePlyTri(XMls & mls, vector<Triangle> v_tri,
              int n_vertex, param params)
{
    std::string out = params.output;
    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_triangles.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }
    fileOut << "element face " << v_tri.size() << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    XFloatAttrib * p_refl=NULL;
    if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            XEchoIndex last_echo_idx = mls.IdxLastEcho(block_idx, pulse_idx);
            XPt3D Pw = mls.Pworld(block_idx, last_echo_idx);
            float e=Pw.X-params.pivot_E, n=Pw.Y-params.pivot_N, h=Pw.Z-params.pivot_H;
            //cout << e << " " << n << " " << h << endl;
            Write<float>(it, e);
            Write<float>(it, n);
            Write<float>(it, h);
            if(params.add_rgb == 1) // r g b mode
            {
                float g = 12.75f*(p_refl->at(block_idx)[last_echo_idx]+20.f); // rescale [-20,0] to [0,256] TODO: parameters
                if(g<0.f) g=0.f; else if(g>255.f) g=255.f;
                unsigned char ug = g;
                Write<unsigned char>(it, ug);
                Write<unsigned char>(it, ug);
                Write<unsigned char>(it, ug);
            }
            else if(params.add_rgb == 2) // quality mode
            {
                Write<float>(it, p_refl->at(block_idx)[last_echo_idx]);
            }
        }
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*v_tri.size();
    buffer = new char[tri_buffer_size]; it = buffer;
    for(auto & tri:v_tri)
    {
        //std::cout << tri.i << " " << tri.j << " " << tri.k << " - " << std::flush;
        Write<unsigned char>(it, 3);
        Write<int>(it, tri.i);
        Write<int>(it, tri.j);
        Write<int>(it, tri.k);
    }
    cout << "Writing " << v_tri.size() << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}



int main(int argc, char **argv)
{
    cout << WHAT << endl;
    if(argc < 6)
    {
        cout << "Usage: " << argv[0] << "  sbet_folder sbet_mission laser_calib.xml ept_folder output [tri_threshold=0.5 (int)start_time (int)end_time pivot_E=-1 pivot_N=-1 pivot_H=0 add_rgb=0]" << endl;
        cout << "sbet_folder: folder where the sbet files are" << endl;
        cout << "sbet_mission: common part of all filenames relevant to the same trajectory" << endl;
        cout << "laser_calib.xml: calibration file for the laser" << endl;
        cout << "ept_folder: folder containing the echo pulse tables (generated with EptExport)" << endl;
        cout << "output: name of the output mesh file (.ply or .off only)" << endl;
        cout << "tri_threshold: threshold on max triangle edge size to add the triangle (default=0.5m)" << endl;
        //cout << "max_DP_error: maximum Douglas-Peucker error (default=0m=do not decimate)" << endl;
        cout << "(start|end)_time: start and end time of the laser points to export (default=everything)" << endl;
        cout << "pivot_(E|N|H): optionally set manually the pivot point (default=-1=first point of the trajectory rounded at 100m for EN, 0 for H)" << endl;
        cout << "add_rgb: if not 0, add r,g, b attributes in .ply mode based on reflectance (default=0=don't)" << endl;
        return 0;
    }

    int i_arg=1;
    param params;
    // required
    params.sbet_folder = string((argv[i_arg++]));
    params.sbet_mission = string(argv[i_arg++]);
    params.laser_calib = string(argv[i_arg++]);
    params.ept_folder = string(argv[i_arg++]);
    params.output = string(argv[i_arg++]);

    // optional
    if(i_arg < argc) params.tri_threshold = atof(argv[i_arg++]);
    //if(i_arg < argc) params.max_DP_error = atof(argv[i_arg++]);
    if(i_arg < argc) params.i_start = atoi(argv[i_arg++]);
    if(i_arg < argc) params.i_end = atoi(argv[i_arg++]);
    if(i_arg < argc) params.pivot_E = atoi(argv[i_arg++]);
    if(i_arg < argc) params.pivot_N = atoi(argv[i_arg++]);
    if(i_arg < argc) params.pivot_H = atoi(argv[i_arg++]);
    if(i_arg < argc) params.add_rgb = atoi(argv[i_arg++]);


    clock_t start = clock();
    // constructor and infos accessible after construction
    XMls mls(params.ept_folder, params.laser_calib, params.sbet_folder, params.sbet_mission);
    mls.Select(params.i_start, params.i_end);
    cout << mls.NBlock() << " block(s) selected" << endl;

    if(params.pivot_E == -1)
    {
        XPt3D Pivot = mls.m_trajecto.SbetSeries().GetGeoref(0).Translation();
        params.pivot_E = 100*(int)(Pivot.X/100);
        params.pivot_N = 100*(int)(Pivot.Y/100);
    }

    start = clock();
    int PPL = mls.PulsePerLine();
    int idx=0;
    vector< vector<int> > vv_pulse_with_echo_idx(mls.NBlock()); // unique indexation of pulses with at least one echo across blocks
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        // index pulses with at leat one echo
        vv_pulse_with_echo_idx[block_idx] = vector<int>(mls.NPulse(block_idx), -1);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++)
            if(mls.NbOfEcho(block_idx, pulse_idx)>0)
                vv_pulse_with_echo_idx[block_idx][pulse_idx] = idx++;
        mls.Free(block_idx);
    }

    /*std::vector<SCR::edge> edges = SCR::filtering(mls,params,vv_pulse_with_echo_idx,(double)1,(double)1,true);
    //std::pair<std::vector<SCR::edge>,std::vector<double> > edges_all = SCR::filtering(mls,params,vv_pulse_with_echo_idx,(double)1);
    std::vector<std::tuple<uint,uint,uint> > scr;
    for (std::vector<SCR::edge>::iterator it = edges.begin(); it != edges.end(); ++it)
    {
        //if (it->node_begin >= 73670 && it->node_begin <= 73680)
        //std::cout << it->node_begin << " " << it->node_end << " - ";
        scr.emplace_back(std::make_tuple(it->block,it->node_begin,it->node_end));
    }*/

    /*std::vector<std::map<uint,std::vector<SCR::edge> > > data_blocks;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        std::map<uint,std::vector<SCR::edge> >Edges;
        for (std::vector<SCR::edge>::iterator it = edges.begin(); it != edges.end(); ++it)
        {
            //if (abs((**it).node_begin - (**it).node_end) < 10) (**it).print_edge();
            if ( (**it).length(mls) < params.tri_threshold &&
                (Edges[(**it).node_begin].size() == 0 ||
                 Edges[(**it).node_begin].back() != (**it)) &&
                 (**it).is_valid()  &&
                 (**it).block == block_idx)          /// Look at this f*cking syntax WTH
            {
                Edges[(**it).node_begin].emplace_back(**it);
                Edges[(**it).node_end].emplace_back(**it);
            }
        }
        data_blocks.emplace_back(Edges);
        mls.Free(block_idx);
    }

    // create triangles list
    std::vector<std::vector<XPt3D> > v_points;
    std::vector<uint> v_points_used;
    std::vector<std::tuple<uint,uint,uint> > v_edges;
    std::vector<std::tuple<uint,uint,uint> > v_ed;
    vector<Triangle> v_tri;
    mls.Load(0);
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        std::vector<XPt3D> v_points_temp;
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<n_pulse-PPL-1; pulse_idx++)
        {
            MakeWedge(mls, v_tri, v_edges, v_points_used, vv_pulse_with_echo_idx, params.tri_threshold,
                      block_idx, pulse_idx, block_idx, pulse_idx+PPL+1, block_idx, pulse_idx+PPL, block_idx, pulse_idx+1);
        }
        for (auto& point:vv_pulse_with_echo_idx[block_idx])
        {
            //std::cout << point << " " << std::flush;
            if (std::find(v_points_used.begin(),v_points_used.end(),point)==v_points_used.end() && point > 0)
                v_points_temp.push_back(mls.Pworld(block_idx,point));
        }
        v_points.push_back(v_points_temp);

        // triangles between current block and next one
        /*if(block_idx<mls.NBlock()-1)
        {
            mls.Load(block_idx+1);
            cout << "PPL " << PPL << " n_pulse " << n_pulse << " block " << block_idx << "/" << mls.NBlock() << endl;
            // first wedge (3 vertices in block, 1 in block+1)
            MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, params.tri_threshold,
                      block_idx, n_pulse-PPL-1, block_idx+1, 0,
                      block_idx, n_pulse-1, block_idx, n_pulse-PPL);
            for(XPulseIndex pulse_idx=n_pulse-PPL; pulse_idx<n_pulse-1; pulse_idx++)
            {
                // strip wedges (2 vertices in block, 2 in block+1)
                MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, params.tri_threshold,
                          block_idx, pulse_idx, block_idx+1, pulse_idx+PPL+1-n_pulse,
                          block_idx+1, pulse_idx+PPL-n_pulse, block_idx, pulse_idx+1);
            }
            // last wedge (1 vertex in block, 3 in block+1)
            MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, params.tri_threshold,
                      block_idx, n_pulse-1, block_idx+1, PPL,
                      block_idx+1, PPL-1, block_idx+1, 0);
        }*/
        /*for (auto it:data_blocks[block_idx])
            for (auto & e:it.second)
            {
                //std::clock_t start;
                //start = std::clock();
                if (e.length(mls) < params.tri_threshold && !find_edge_in_triangles(&v_tri,e))
                    v_edges.push_back(std::make_tuple(block_idx,e.node_begin,e.node_end));
                //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
            }*/
        /*mls.Free(block_idx);
    }
    for (auto& e:v_edges)
        if (!find_edge_in_triangles(&v_tri,SCR::edge(std::get<0>(e),std::get<1>(e),std::get<2>(e))))
            v_ed.push_back(e);*/




    std::vector<SCR::edge> edges = SCR::naive_filtering(mls,params,vv_pulse_with_echo_idx,(double)0.5);
    //std::pair<std::vector<SCR::edge>,std::vector<double> > edges_all = SCR::filtering(mls,params,vv_pulse_with_echo_idx,(double)1);
    //std::vector<std::tuple<uint,uint,uint> > scr;
    /*for (std::vector<SCR::edge>::iterator it = edges.begin(); it != edges.end(); ++it)
    {
        //if (it->node_begin >= 73670 && it->node_begin <= 73680)
        //std::cout << it->node_begin << " " << it->node_end << " - ";
        scr.emplace_back(std::make_tuple(it->block,it->node_begin,it->node_end));
    }*/
    std::vector<std::map<uint,std::vector<SCR::edge> >* >* data_blocks = new std::vector<std::map<uint,std::vector<SCR::edge> >* >;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        std::map<uint,std::vector<SCR::edge> >* Edges = new std::map<uint,std::vector<SCR::edge> >;
        for (std::vector<SCR::edge>::iterator it = edges.begin(); it != edges.end(); ++it)
        {
            //if (abs((**it).node_begin - (**it).node_end) < 10) (**it).print_edge();
            if ((Edges->operator[]((**it).node_begin).size() == 0 ||
                 Edges->operator[]((**it).node_begin).back() != (**it)) &&
                 (**it).is_valid()  &&
                 (**it).block == block_idx)          /// Look at this f*cking syntax WTH
            {
                Edges->operator[]((**it).node_begin).emplace_back(**it);
                Edges->operator[]((**it).node_end).emplace_back(**it);
            }
        }
        data_blocks->emplace_back(Edges);
        mls.Free(block_idx);
    }

    //std::vector<std::map<uint,std::vector<SCR::edge> > > data_blocks_save = data_blocks;

    std::vector<Triangle> v_tri;

    // Remember to check the storage inside the map
    std::tuple<std::vector<std::vector<Triangle> >*,std::vector<std::tuple<uint,uint,uint> >*,std::vector<std::vector<uint> >* >* out = SCR::compute_mesh_and_segments_fast(data_blocks,mls);
    cout << "Done in " << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl;
    //std::vector<Triangle> v_tri = (*(std::get<0>(*out)))[0];
    std::vector<std::tuple<uint,uint,uint> > v_ed = *std::get<1>(*out);
    std::cout << "edges not in triangle " << v_ed.size() << std::endl;
    std::vector<std::vector<uint> > rem_points = *std::get<2>(*out);

    mls.Load(0);
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<n_pulse-PPL-1; pulse_idx++)
        {
            MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, params.tri_threshold,
                      block_idx, pulse_idx, block_idx, pulse_idx+PPL+1, block_idx, pulse_idx+PPL, block_idx, pulse_idx+1);
        }
    }
    mls.Free(0);

    uint NPS = 0;
    std::vector<std::vector<XPt3D> > v_points;
    //int b = 0;
    for (XBlockIndex b = 0; b != mls.NBlock(); ++b)
    {
        mls.Load(b);
        std::vector<XPt3D> v_points_temp;
        std::vector<uint> rem_points_block = rem_points[b];
        uint NPS_temp = 0;
        for (std::vector<int>::iterator it = vv_pulse_with_echo_idx[b].begin(); it != vv_pulse_with_echo_idx[b].end(); ++it)
        {
            ++NPS_temp;
            if (*it - NPS > 0 && *it - NPS < mls.NEcho(b))
            if (std::find(rem_points_block.begin(),rem_points_block.end(),*it-NPS) == rem_points_block.end())
                v_points_temp.emplace_back(mls.Pworld(b,*it-NPS));
        }
        NPS += NPS_temp;
        std::cout << v_points_temp.size() << " points alone in block " << b << std::endl;
        v_points.emplace_back(v_points_temp);
        mls.Free(b);
    }




    cout << "Creating " << params.output << endl;
//    //if(params.Format() == "off") WriteOff(mls, v_tri, idx, params);
//    //else
    WritePlyTri(mls, v_tri, idx, params);
    WritePlySegment(mls, v_ed, mls.NTotalEcho(), params); // Write Ply with edges
    WritePlyPoint(mls,v_points,params);
    cout << "Done in " << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl;
    return 0;
}



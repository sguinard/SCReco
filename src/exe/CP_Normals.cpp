#define WHAT "Computes Cut-Pursuit algorithm with normals"

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/WedgeStuff.h"
#include "libSteph/KMeans.h"

#include "libSteph/ConvexHull.h"

#include "libSteph/SC.h"

#include "libSteph/CutPursuit.h"
#include "../extern/cut-pursuit-master/include/API.h"

#include "libSteph/TinyPlyIO.h"
#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Voronoi.h"
#include "libSteph/Ransac.h"

#include "libSteph/Triangle.h"
#include "libSteph/Triangle_Coords.h"

#include "libSteph/VSA.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

#include <tuple>

using namespace std;
using namespace SCR;
using namespace CP;





std::tuple<vvD,std::vector<uint32_t> > cut_pursuit_norm
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<std::vector<double> > &observation, // coordinates
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<std::vector<double> > &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent)
{   //C-style ++ interface
    std::srand (time(NULL));
    if (verbose > 0)
    {
        std::cout << "L0-CUT PURSUIT";
    }
    //--------parameterization---------------------------------------------
    //CutPursuit<double> * cpl = create_CP(mode, verbose);
    //CutPursuit_Plane<double> * cp = reinterpret_cast<CutPursuit_Plane<double> *>(cpl);
    CutPursuit<double>* cp = create_CP(mode, verbose);  // ok
    set_speed(cp, speed, verbose);
    set_up_CP(cp, n_nodes, n_edges, nObs, observation, Eu, Ev
             ,edgeWeight, nodeWeight);
    cp->parameter.reg_strenth = lambda;
    cp->parameter.max_ite_main = 10;
    cp->parameter.backward_step = true;
    //-------run the optimization------------------------------------------
    cp->run();
    //------------write the solution-----------------------------
    VertexAttributeMap<double> vertex_attribute_map = boost::get(
            boost::vertex_bundle, cp->main_graph);
    //std::cout << inComponent.size() << std::endl;

    VertexIterator<double> ite_nod = boost::vertices(cp->main_graph).first;
    //for(uint32_t ind_nod = 0; ind_nod < n_nodes; ind_nod++ )
    //{
    //  std::cout << ind_nod << " " << std::flush;
    //  inComponent[*ite_nod] = 0;
      //vertex_attribute_map[*ite_nod].in_component;
        //std::cout << inComponent[*ite_nod] << " " << std::flush;

        /*for(uint32_t ind_dim=0; ind_dim < nObs; ind_dim++)
        {
            solution[ind_nod][ind_dim] = vertex_attribute_map[*ite_nod].value[ind_dim];
            //std::cout << solution[ind_nod][ind_dim] - observation[ind_nod][ind_dim] << " " ; // like 10s meter aparts   // -3X.XXX -X.XXX -5X.XXX
	    }*/
        //std::cout << std::endl;
    //    ite_nod++;
    //}




    delete cp;
    return std::make_tuple(solution,inComponent);
}








int main(int argc, char **argv)
{

    //srand(0);
  double tresh = argc > 3 ? atof(argv[3]) : 0.1;
    std::cout << WHAT << std::endl;

    std::string first_arge;
    first_arge = argv[1];

    int nb_regions = argc > 2 ? atoi(argv[2]) : 10;

    std::cout << first_arge << std::endl;

    tinyply::PlyFile* file = new tinyply::PlyFile;

    vvD* points = new vvD;    
    vvD* observations = new vvD;
    std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* edges = new std::pair<std::vector<uint32_t>, std::vector<uint32_t> >;
    std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* e_vor = new std::pair<std::vector<uint32_t>, std::vector<uint32_t> >;

    vvD* solution = new vvD;
    *solution = *observations;
    std::vector<uint32> inComponent (0,observations->size());
    
    PlyFile2Vector(first_arge, file, points, observations, *&edges);
    //for (auto& p: *points) std::cout << p[0] << " " << p[1] << " " << p[2] << std::endl;
    //for (auto& obs: *observations) std::cout << obs[0] << " " << obs[1] << " " << obs[2] << std::endl;

    //inComponent = ransac_from_3d_points_unparallelized(observations,observations,0.05);


    double cp_reg = argc == 3 ? atof(argv[2]) : 0.001;
    std::cout <<
      "Mesh segmentation using \ell_0-Cut-Pursuit\n"
      "\tgraph is structured with mesh adjacency"
      "\n........................................................................"
	      << std::endl;
    

    *e_vor = *edges;

    std::cout << e_vor->first.size() << std::endl;
    std::cout << e_vor->first[0] << " " << e_vor->second[0] << std::endl;

    double d0 = 0;
    for (uint32_t i = 0; i != e_vor->first.size(); ++i)
      {
	XPt3D p1;
	p1.X = points->at(e_vor->first[i])[0];
	p1.Y = points->at(e_vor->first[i])[1];
	p1.Z = points->at(e_vor->first[i])[2];
	XPt3D p2;
	p2.X = points->at(e_vor->second[i])[0];
	p2.Y = points->at(e_vor->second[i])[1];
	p2.Z = points->at(e_vor->second[i])[2];
	d0 += d2(p1,p2);
      }
    //d0 *= 1e4;
    d0 /= e_vor->first.size();
    
    std::cout << "mean distance is: " << d0 << std::endl;
    
    std::vector<double> edge_weight(e_vor->first.size(),1);
    for (uint32_t i = 0; i !=e_vor->first.size(); ++i)
      {
	XPt3D p1;
	p1.X = points->at(e_vor->first[i])[0];
	p1.Y = points->at(e_vor->first[i])[1];
	p1.Z = points->at(e_vor->first[i])[2];
	XPt3D p2;
	p2.X = points->at(e_vor->second[i])[0];
	p2.Y = points->at(e_vor->second[i])[1];
	p2.Z = points->at(e_vor->second[i])[2];
	edge_weight[i] = 1/(2+d2(p1,p2)/d0); // todo: compute d0 as the mean of ditances between 2 points
	//std::cout << d2(p1,p2)*100 << " " << std::flush;
      }
    std::vector<double> node_weight(observations->size(),1);
    std::vector<uint32_t> comps;
    uint32_t n_nodes_red = 0;
    uint32_t n_edges_red = 0;
    std::vector<std::vector<uint32_t> > borders;
    std::vector<uint32_t> Eu_red;
    std::vector<uint32_t> Ev_red;
    std::vector<double> edgeWeight_red;
    std::vector<double> nodeWeight_red;

    //inComponent.reserve(observations->size());
    //std::fill(inComponent.begin(), inComponent.end(), 0);
    std::vector<std::vector<uint32_t> > Components;
    
    //std::tuple<vvD, std::vector<uint32_t> > cpn = cut_pursuit_norm(observations->size(),e_vor->first.size(),3,*observations,e_vor->first,e_vor->second,edge_weight,node_weight,*solution,cp_reg,2,1,2,inComponent);

    cut_pursuit(observations->size(),e_vor->first.size(),3,*observations,e_vor->first,e_vor->second,edge_weight,node_weight,*solution,inComponent,Components,borders,n_nodes_red, n_edges_red,Eu_red,Ev_red,edgeWeight_red,nodeWeight_red,cp_reg,(double)2,(double)1,(double)2);

    /// compute best fitting plane for each region
    uint32_t nb_planes = Components.size();
    vP* planes = new vP;
    for (uint32_t i = 0; i != nb_planes; ++i)
      {
	vP3* pts = new vP3;
	for (uint32_t j = 0; j != inComponent.size(); ++j)
	  {
	    if (inComponent[j] == i) pts->emplace_back(XPt3D(points->at(inComponent[j])[0], points->at(inComponent[j])[1], points->at(inComponent[j])[2]));
	  }
	planes->emplace_back(Plane(pts));
	delete pts;
      }

    /// project each point to its associated plane
    vvD* projections = new vvD;
    for (uint32_t i = 0; i != inComponent.size(); ++i)
      {
	XPt3D p = XPt3D(points->at(i)[0], points->at(i)[1], points->at(i)[2]);
	XPt3D proj = planes->at(inComponent[i]).project_point_plane(p);
	projections->emplace_back(std::vector<double>{proj.X, proj.Y, proj.Z});
	//std::cout << p.X << " " << p.Y << " " << p.Z << " - " << proj.X << " " << proj.Y << " " << proj.Z << std::endl;
      }
    
    std::pair<std::vector<std::vector<std::pair<XPt3D, uint32> > >,std::vector<std::vector<std::pair<XPt3D, uint32> > > > points_colored = color_points(*points,*projections,inComponent);
    
    WritePlyPointColor(points_colored.second,"_values.ply");
    WritePlyPointColor(points_colored.first,"_observations.ply");
    
    return 0;

}

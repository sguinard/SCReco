#define WHAT "BigCube: creates a big cube"

#include <ctime>
#include <iostream>
#include <limits>
#include <typeinfo>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/WedgeStuff.h"
#include "libSteph/KMeans.h"

#include "libSteph/ConvexHull.h"

#include "libSteph/SC.h"

#include "libSteph/CutPursuit.h"
#include "../extern/cut-pursuit-master/include/API.h"

#include "libSteph/TinyPlyIO.h"
#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Voronoi.h"
#include "libSteph/Ransac.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

using namespace std;
using namespace SCR;


void make_cube(vvD* observations, vector<Triangle>* v_tri, int nb_points_per_face)
{
    // 6 faces
    // face 1: 000 - 001 - 011 - 010
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{0,(float)rand()/RAND_MAX,(float)rand()/RAND_MAX});
    // face 2: 100 - 101 - 001 - 000
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{(float)rand()/RAND_MAX,0,(float)rand()/RAND_MAX});
    // face 3: 100 - 101 - 111 - 110
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{1,(float)rand()/RAND_MAX,(float)rand()/RAND_MAX});
    // face 4: 110 - 111 - 011 - 010
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{(float)rand()/RAND_MAX,1,(float)rand()/RAND_MAX});
    // face 5: 001 - 011 - 111 - 101
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{(float)rand()/RAND_MAX,(float)rand()/RAND_MAX,1});
    // face 6: 000 - 010 - 110 - 100
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{(float)rand()/RAND_MAX,(float)rand()/RAND_MAX,0});
}

void make_cube_noisy(vvD* observations, vector<Triangle>* v_tri, int nb_points_per_face,float noise)
{
    // 6 faces
    // face 1: 000 - 001 - 011 - 010
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{0+((float)rand()/RAND_MAX-0.5)*noise,(float)rand()/RAND_MAX,(float)rand()/RAND_MAX});
    // face 2: 100 - 101 - 001 - 000
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{(float)rand()/RAND_MAX,0+((float)rand()/RAND_MAX-0.5)*noise,(float)rand()/RAND_MAX});
    // face 3: 100 - 101 - 111 - 110
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{1+((float)rand()/RAND_MAX-0.5)*noise,(float)rand()/RAND_MAX,(float)rand()/RAND_MAX});
    // face 4: 110 - 111 - 011 - 010
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{(float)rand()/RAND_MAX,1+((float)rand()/RAND_MAX-0.5)*noise,(float)rand()/RAND_MAX});
    // face 5: 001 - 011 - 111 - 101
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{(float)rand()/RAND_MAX,(float)rand()/RAND_MAX,1+((float)rand()/RAND_MAX-0.5)*noise});
    // face 6: 000 - 010 - 110 - 100
    for (int i=0; i!=nb_points_per_face; ++i)
        observations->emplace_back(std::vector<double>{(float)rand()/RAND_MAX,(float)rand()/RAND_MAX,0+((float)rand()/RAND_MAX-0.5)*noise});
}


/// \brief writes .ply file with a mesh
///
/// \param mls
/// \param v_tri: vector contianing for each block the liste of triangles
/// \param n_vertex: number of vertices
/// \param params
///
/// \warning doc is not up to date
void WritePly(vvD* observations, vector<Triangle>* v_tri, string out)
{
    int tri_size = 0;
    tri_size += v_tri->size();

    ofstream fileOut(out+"_mesh.ply");

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << observations->size() << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "element face " << tri_size << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * observations->size();

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for (auto& point:*observations)
        for (auto& dim:point)
        Write<float>(it,dim);

    cout << "Writing " << observations->size() << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*tri_size;
    buffer = new char[tri_buffer_size]; it = buffer;

    for(auto & tri:*v_tri)
    {
        //std::cout << (uint)tri.i << " " << (uint)tri.j << " " << (uint)tri.k << " - " << std::flush;
        Write<unsigned char>(it, 3);
        Write<int>(it, tri.i);
        Write<int>(it, tri.j);
        Write<int>(it, tri.k);
    }
    cout << "Writing " << tri_size << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}



int main(int argc, char **argv)
{
    std::cout << WHAT << std::endl;

    vvD* observations = new vvD;
    vector<Triangle>* v_tri = new std::vector<Triangle>;

//    make_cube(observations, v_tri, 1000);
    make_cube_noisy(observations, v_tri,1000,10);

    std::list<Triangulation::Point> L;
    L = addToList(L, observations);

    v_tri = convert_3d_delaunay(L,observations,0.1);

    //for (auto& tri: *v_tri)
    //    std::cout << tri.i << " " << tri.j << " " << tri.k << " - " << std::flush;

    WritePly(observations,v_tri,"cube1000");

    delete observations;
    delete v_tri;

    return 0;
}



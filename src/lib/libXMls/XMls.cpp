
#include "libXMls/XMls.h"
#include "libXBase/XArchiXMLBaseTools.h"
#include "libXBase/XArchiXMLTools.h"
#include "libXBase/XArchiXMLException.h"
#include "libXBase/XArchiGeorefXML.h"
#include "tinyxml.h"
#include <boost/filesystem.hpp>

using namespace std;

XMls::XMls(Param param):
    XEchoPulseTable(param.ept_folder),
    m_trajecto(param.sbet_folder, param.sbet_mission)
{
    // read laser calib
    try
    {
        //validation du formalisme XML
        TiXmlDocument doc( param.laser_calib.c_str() );
        if (!doc.LoadFile())
            cout << "ERROR: Invalid XML format in " << param.laser_calib << " Error=" << doc.ErrorDesc() << endl;
        XArchiXML::XArchiGeoref_LoadFromNode(&m_sensor_georef, XArchiXML::FindSubSubNode(doc.RootElement(), "georef"));
    }
    catch(XArchiXML::XmlException e)
    {
        cout << "Reading " << param.laser_calib << " raised " << e.Erreur() << endl;
        return;
    }

    // read time pivots
    m_ept_time_pivot.XmlLoad(param.ept_folder+".pvt.xml");

    boost::filesystem::path sbet_pivot_path(param.sbet_folder), sbet_pivot_name = "sbet_" + param.sbet_mission + ".pvt.xml";
    sbet_pivot_path /= sbet_pivot_name;
    m_trajecto_time_pivot.XmlLoad(sbet_pivot_path.string());

    // compute shift
    m_time_shift = m_ept_time_pivot.Diff(m_trajecto_time_pivot);
}

// in seconds from rxp file, trajecto time is transformed
void XMls::Select()
{
    XSecond start_second=-1, end_second=-1;
    Select(start_second, end_second);
}

// in seconds from rxp file, trajecto time is transformed
void XMls::Select(XSecond & start_second, XSecond & end_second)
{
    // load echos
    XEchoPulseTable::Select(start_second, end_second);
    cout << "Select (" << start_second << "," << end_second << ")+" << m_time_shift << endl;

    // load trajecto
    m_trajecto.Load(start_second+m_time_shift, end_second+m_time_shift); // todo: look for all trajectory parts of the same trajectory
    m_trajecto.SbetSeries().PreComputeGeoref(); // todo: optionnally change coord system, precompute with laser calib
}

// in absolute UTC time
void XMls::Select(const XAbsoluteTime & start_time, const XAbsoluteTime & end_time)
{
    XSecond start_second=floor(start_time.Diff(m_ept_time_pivot));
    XSecond end_second=ceil(end_time.Diff(m_ept_time_pivot));
    cout << "start_second " << start_second << endl;
    cout << "end_second " << end_second << endl;
    Select(start_second, end_second);
}

XPt3D XMls::Oworld(double ins_time, bool precomputed)
{
    XPt3D O;
    bool OK = m_trajecto.SbetSeries().GetTranslation(ins_time, O, precomputed);
    if(!OK)
        cout << "Warning: Oworld(" << ins_time << ") outside bounds " << m_trajecto.StartTime() << "," << m_trajecto.EndTime() << endl;
    return O;
}

XArchiGeoref XMls::Ins(double ins_time, bool precomputed)
{
    XArchiGeoref sbet_georef;
    bool OK = false;
    if(precomputed)
        OK = m_trajecto.SbetSeries().GetGeoref_precomputed(ins_time, sbet_georef);
    else OK = m_trajecto.SbetSeries().GetGeoref(ins_time, sbet_georef);
    if(!OK)
        cout << "Warning: Ins(" << ins_time << ") outside bounds " << m_trajecto.StartTime() << "," << m_trajecto.EndTime() << endl;
    return sbet_georef;
}

XPt3D XMls::Pworld(XBlockIndex block_idx, XEchoIndex echo_idx)
{
    return m_trajecto.SbetSeries().ApplyGeoref_precomputed(Time(block_idx, echo_idx) + m_time_shift, Pins(block_idx, echo_idx));
}

XPt3D XMls::Pworld(XBlockIndex block_idx, XPulseIndex pulse_idx, double range)
{
    return Cworld(block_idx, pulse_idx) + range * RayWorld(block_idx, pulse_idx);
}

XPt3D XMls::Pworld_interpol_frame(XBlockIndex block_idx, XEchoIndex echo_idx)
{
    XArchiGeoref sbet_georef = Ins(block_idx, echo_idx, true);
    return sbet_georef.Applique_transfo(Pins(block_idx, echo_idx));
}

XPt3D XMls::Pworld_interpol_angles(XBlockIndex block_idx, XEchoIndex echo_idx)
{
    XArchiGeoref sbet_georef = Ins(block_idx, echo_idx, false);
    return sbet_georef.Applique_transfo(Pins(block_idx, echo_idx));
}

XPt3D XMls::Cworld(XBlockIndex block_idx, XEchoIndex echo_idx)
{
    XArchiGeoref sbet_georef = Ins(block_idx, echo_idx);
    return sbet_georef.Applique_transfo(Cins());
}

XPt3D XMls::Cworld(XBlockIndex block_idx, XPulseIndex pulse_idx)
{
    XArchiGeoref sbet_georef = Ins(block_idx, pulse_idx);
    return sbet_georef.Applique_transfo(Cins());
}

/// Ray direction in Lamb93
XPt3D XMls::RayWorld(XBlockIndex block_idx, XPulseIndex pulse_idx)
{
    XArchiGeoref sbet_georef = Ins(block_idx, pulse_idx);
    return sbet_georef.Rotation()*RayIns(block_idx, pulse_idx);
}

SbetEvent XMls::Sbet(XBlockIndex block_idx, XPulseIndex pulse_idx)
{
    SbetEvent sbet_event;
    double trajecto_time = Time(block_idx, pulse_idx) + m_time_shift;
    if(!m_trajecto.SbetSeries().Interpol_event(sbet_event, trajecto_time))
        cout << "Warning: Interpol_event outside bounds for time " << trajecto_time << endl;
    return sbet_event;
}

SbetEvent XMls::Sbet(XBlockIndex block_idx, XEchoIndex echo_idx)
{
    SbetEvent sbet_event;
    double trajecto_time = Time(block_idx, echo_idx) + m_time_shift;
    if(!m_trajecto.SbetSeries().Interpol_event(sbet_event, trajecto_time))
        cout << "Warning: Interpol_event outside bounds for time " << trajecto_time << endl;
    return sbet_event;
}

AccuracyEvent XMls::Accuracy(XBlockIndex block_idx, XPulseIndex pulse_idx)
{
    AccuracyEvent acc_event;
    double trajecto_time = Time(block_idx, pulse_idx) + m_time_shift;
    if(!m_trajecto.AccuracySeries().Interpol_event(acc_event, trajecto_time))
        cout << "Warning: Interpol_event outside bounds for time " << trajecto_time << endl;
    return acc_event;
}

AccuracyEvent XMls::Accuracy(XBlockIndex block_idx, XEchoIndex echo_idx)
{
    AccuracyEvent acc_event;
    double trajecto_time = Time(block_idx, echo_idx) + m_time_shift;
    if(!m_trajecto.AccuracySeries().Interpol_event(acc_event, trajecto_time))
        cout << "Warning: Interpol_event outside bounds for time " << trajecto_time << endl;
    return acc_event;
}


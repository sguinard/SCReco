#include "libSteph/ConvexHull.h"

SCR::vP2* project_points_2d(SCR::vP3 base, SCR::vP3* points)
{
    SCR::vP2* projected_points = new SCR::vP2;
    for (SCR::vP3_iter point_iter = points->begin(); point_iter != points->end(); ++point_iter)
    {
        projected_points->emplace_back(
        XPt2D ( point_iter->X * base[0].X + point_iter->Y * base[0].Y + point_iter->Z * base[0].Z,
                point_iter->X * base[1].X + point_iter->Y * base[1].Y + point_iter->Z * base[1].Z)  );
    }
    return projected_points;
}

SCR::vP2 *project_points_2d(SCR::Plane plane, SCR::vP3 *points)
{
    return project_points_2d(plane.compute_orthonormal_base(),points);
}

SCR::vP3 *project_points_3d(SCR::vP3 base, SCR::vP2 *points)
{
    SCR::vP3* projected_points = new SCR::vP3;
    for (SCR::vP2_iter point_iter = points->begin(); point_iter != points->end(); ++point_iter)
    {
        projected_points->emplace_back(
        XPt3D ( point_iter->X * base[0].X + point_iter->Y * base[1].X,
                point_iter->X * base[0].Y + point_iter->Y * base[1].Y,
                point_iter->X * base[0].Z + point_iter->Y * base[1].Z)  );
    }
    return projected_points;
}




bool compare_ptsz (const Point_2& p1, const Point_2& p2){
    return p1.val() < p2.val();
}

std::list<Point_2_CGAL> cast2cgal(std::list<Point_2> const & lp){
    std::list<Point_2_CGAL> lpc;
    for(std::list<Point_2>::const_iterator pit = lp.begin(); pit != lp.end(); pit++){
        lpc.push_back(Point_2_CGAL(pit->x(),pit->y()));
    }
    return lpc;
}


std::vector<double> alpha_shape_1d(std::list<Point_2>& lp, double alpha){
    lp.sort(compare_ptsz);
    std::vector<double> res;
    for(std::list<Point_2>::iterator pit = lp.begin(); pit != lp.end(); pit++){
        double z = pit->val();
        if(res.empty() || res.back() + alpha < z) {
            res.push_back(z);
            res.push_back(z);
        } else {
            res.back() = z;
        }
    }
    return res;
}




ConvexHull::ConvexHull()
{

}

ConvexHull::ConvexHull(SCR::vP3 *input_points)
{
    points = input_points;
    plane = SCR::Plane(input_points);
}

ConvexHull::~ConvexHull()
{

}

Multipoly ConvexHull::compute_convex_hull()
{

    //SCR::vP2* point_2 = new vP2;
    //point_2 = project_points_2d(plane,points);

    // Compute Alpha Expension
                // ---- Parse parameters -----
                //std::cout << "Parse parameters ..." << std::endl;
                // Model parameters
                double alpha_xy = 100;
                double alpha_z = 100;
                double zmin = -100;
                double zmax = 100;
//                int cl = 1;
//                int c;
//                int errflg = 0;
//                bool is_output_dir = true;
//                int acc = 0;
//                int option_index = 0;
                std::list<std::string> listname;

                std::cout << "\t\tParameters :" << std::endl
                    << "\t\t\talpha_xy : " << alpha_xy << std::endl
                    << "\t\t\talpha_z : " << alpha_z << std::endl;
                if(zmin == std::numeric_limits<double>::min()) std::cout << "\t\t\tzmin :-inf"; else  std::cout << "\t\t\tzmin : " <<  zmin; std::cout << std::endl;
                if(zmax == std::numeric_limits<double>::max()) std::cout << "\t\t\tzmax :+inf"; else  std::cout << "\t\t\tzmax : " << zmax ; std::cout << std::endl;

                double minval = std::numeric_limits<double>::infinity();
                double maxval =-std::numeric_limits<double>::infinity();

                // ----- Loop over each point of the file ----
                Multipoly M;
                int acc2 = 0;
                for(SCR::vP3_iter it = points->begin(); it != points->end(); ++it)
                {
                    XPt3D p = *it;
                    Point_2 p2(p.X,p.Y,p.Z,acc2++);

                    M.points.push_back(p2);

                }

                int acc_poly = 0;
                int acc_segs = 0;
//                for(std::map<int,Multipoly>::iterator iter = map_Multipoly.begin(); iter != map_Multipoly.end(); ++iter)
//                {
//                    // The id of the set
//                    int k =  iter->first;

                // ----- Alpha shape ----- //
                std::list<Point_2> lpc = M.points;

std::cout << lpc.size() << std::endl;
                Alpha_shape_2 A(lpc.begin(), lpc.end(),
                        FT(alpha_xy),
                        Alpha_shape_2::REGULARIZED);
//std::cout << A.size_type << std::endl;
                // Storing the edges  in segments.
                Poly_seg segments;
                alpha_edges(A, std::back_inserter(segments));

                // ----- Fill the structure of the *k* set ----- //
                // If the set is not empty
                //    if(segments.size() == 0) continue;
                if (segments.size() > 0)
                {
                    M.list_poly.push_front(Poly_seg());
                    // Add the first segment
                    M.list_poly.front().push_front(segments.front());
    //std::cout << segments.size() << std::endl;          /// \bug 0
                    // Extract Polygon from the set of segment
                    segments.pop_front();               /// \bug crash here
                    // While some segments are not affected to a polygon
                    while(segments.size() != 0){
                            // Take the next segment
                            Segment s_acc = M.list_poly.front().front();
                            Poly_seg::iterator  final_iter = segments.end();
                            final_iter --;
                            // For the last segment added, try to find a segment which has a common point
                            for(Poly_seg::iterator  lit = segments.begin(); lit != segments.end(); ++lit){
                                // If found
                                if(s_acc.source() == lit->target()){
                                    // Add the the current polygon
                                    M.list_poly.front().push_front(*lit);
                                    segments.erase(lit);
                                    // And do it again for the new sgment
                                    break;
                                }
                                // Else
                                if(lit == final_iter){
                                    // Create a new polygon
                                    M.list_poly.push_front(Poly_seg());
                                    // Start with the new segments
                                    M.list_poly.front().push_front(*lit);
                                    segments.erase(lit);
                                    break;
                                }
                            }
                    }
//    std::cout << "BAAAAAAH" << std::endl;
                        // Alphashape 1d
                        std::vector<double> alpha_shape_z = alpha_shape_1d(lpc,alpha_z);
                        M.set_z(alpha_shape_z);

                    // For each created
                    acc_poly += M.nb_poly();
                    acc_segs += M.nb_segs();
    //                }
                    //std::cout << "\t        sets : " << M.size() << std::endl;
                    std::cout << "\t    polygons : " << acc_poly << std::endl;
                    std::cout << "\t    segments : " << acc_segs << std::endl;
                }

    return M;
}




void write_output(boost::filesystem::path path,std::string filename, std::vector<Multipoly> & map_Multipoly){

    // ----- Creating outputs filename -----
    //path = path.stem();
    std::size_t lastindex = filename.find_last_of(".");
    std::string rawname = filename.substr(0, lastindex);
    std::string name_csv = path.string();
    std::string name_vrt = path.string();
    name_csv += ".csv";
    name_vrt += ".vrt";
    boost::filesystem::path full_csv(path);
    boost::filesystem::path full_vrt(path);
    full_csv += filename+".csv";
    full_vrt += filename+".vrt";

    // File stream, fo1 for the csv file, fo2 for the vrt file
    std::ofstream fo1,fo2;

    // ----- VRT File (Header) -----
    std::cout << "\t writing : " << full_vrt << " ..." << std::endl;
    fo2.open(std::string(full_vrt.string()).c_str());
    fo2 << "<OGRVRTDataSource>" << std::endl;
    fo2 << " <OGRVRTLayer name=\""<< (path.stem()).string()+filename << "\">" << std::endl;
    fo2 << " <SrcDataSource relativeToVRT=\"1\">" << (path.stem()).string()+".csv"+name_csv << "</SrcDataSource>" << std::endl;
    fo2 << " <LayerSRS>IGNF:LAMB93</LayerSRS> " << std::endl;
    fo2 << " <GeometryType>wkbMultiPolygon</GeometryType> " << std::endl;
    fo2 << "<Field name=\"source\" type=\"String\"/>" << std::endl;
    fo2 << "<Field name=\"sourceid\" type=\"Integer\"/>" << std::endl;
    fo2 << "<Field name=\"class\" type=\"Integer\"/>" << std::endl;
    fo2 << "<Field name=\"zmin\" type=\"Real\"/>" << std::endl;
    fo2 << "<Field name=\"zmax\" type=\"Real\"/>" << std::endl;
    fo2 << "<Field name=\"count\" type=\"Integer\"/>" << std::endl;
    fo2 << "<GeometryField encoding=\"WKT\" field=\"geom\"/> " << std::endl;
    fo2 << "</OGRVRTLayer>" << std::endl;
    fo2 << "</OGRVRTDataSource>" << std::endl;
    fo2.close();

    // ----- CSV File (List of multipolygone -----
    std::cout << "\t writing : "   << full_csv  << " ..." << std::endl;
    fo1.open(std::string(full_csv.string()).c_str());
    fo1 << "source,sourceid,class,zmin,zmax,count,geom" << std::endl;
    Poly_seg::iterator sit;
    // Loop over each Polygone
    for(std::vector<Multipoly>::iterator iter = map_Multipoly.begin(); iter != map_Multipoly.end(); ++iter)
    {
        int acc1 = 0;
        Multipoly & M = *iter;
        if(M.nb_segs() == 0) continue;
        //int k =  iter->first;
        std::list<Poly_seg> meta_list = M.list_poly;

        // Each  set of polygon has the following forme
        // ID ; CLASS ; FILE ID ; FILE NAME ; Zmin ; Zmax ; tmin ; tmax ; Multipolygon structure
        fo1 << M.filename << "," << M.get_seg_id() << "," << M.get_class_id()  << "," << M.get_z().front() << "," << M.get_z().back()   << "," << M.points.size() << ",\"MULTIPOLYGON((";
        fo1 << std::fixed << std::setprecision(3);
        // Writing the multipolygon structure
        // For each polygon of the multipolygon set
        for(std::list<Poly_seg>::iterator lit = meta_list.begin(); lit != meta_list.end();++lit){
            // The current polygon
            Poly_seg ls = *lit;
            int acc = 0;
            if(acc1 != 0)
                fo1 << ",";
            fo1 << "(";
            Poly_seg::iterator sit_last = ls.end();
            sit_last--;

            // For each segment of the polygon
            for(sit = ls.begin(); sit != ls.end();++sit){
                if(acc != 0)
                    fo1 << ",";
                fo1 << sit->source().x() << " " << sit->source().y();// << " "  << "1";
                if(sit == sit_last)
                    fo1  << "," << sit->target().x() << " " << sit->target().y();// << " " << "1";
                acc++;
            }
            fo1 << ")";
            acc1++;
        }
        fo1 << "))\"" << std::endl;
    }
    fo1.close();
}




void write_alpha_shape_ply(XMls& mls, SCR::param params, std::vector<Multipoly> & map_Multipoly){

    // ----- Creating outputs filename -----
    int n_vertex = 0;
    MIvP3* set_of_points = new MIvP3;
    //for (vI_iter it = comp_idx.begin(); it != comp_idx.end(); it++)
    //    set_of_points[*it]->emplace_back(new vP3);
    for(std::vector<Multipoly>::iterator it = map_Multipoly.begin(); it != map_Multipoly.end(); ++it)
    {
        int acc1 = 0;
        Multipoly & M = *it;
        if(M.nb_segs() == 0) continue;
        std::list<Poly_seg> meta_list = M.list_poly;

        for(std::list<Poly_seg>::iterator lit = meta_list.begin(); lit != meta_list.end();++lit)
        {
            // The current polygon
            Poly_seg ls = *lit;
            for (Poly_seg::iterator iter = ls.begin(); iter != ls.end(); ++iter)
            {
                //iter->;
            }
        }
    }

    //for (std::vector<std::vector<XPt3D> >::iterator it = points.begin(); it != points.end(); ++it)
    //    n_vertex += it->size();

    std::string out = params.output;
    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_convex_hull.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "convexhull generated by sguinard" << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "element face " << map_Multipoly.size() << endl;
    fileOut << "property list uchar int vertex_index" << endl;

    // ----- CSV File (List of multipolygone -----
//    std::cout << "\t writing : "   << full_csv  << " ..." << std::endl;
//    fo1.open(std::string(full_csv.string()).c_str());
//    fo1 << "source,sourceid,class,zmin,zmax,count,geom" << std::endl;
    Poly_seg::iterator sit;
    // Loop over each Polygone
    for(std::vector<Multipoly>::iterator iter = map_Multipoly.begin(); iter != map_Multipoly.end(); ++iter)
    {
        int acc1 = 0;
        Multipoly & M = *iter;
        if(M.nb_segs() == 0) continue;
        //int k =  iter->first;
        std::list<Poly_seg> meta_list = M.list_poly;

        // Each  set of polygon has the following forme
        // ID ; CLASS ; FILE ID ; FILE NAME ; Zmin ; Zmax ; tmin ; tmax ; Multipolygon structure
        //fo1 << M.filename << "," << M.get_seg_id() << "," << M.get_class_id()  << "," << M.get_z().front() << "," << M.get_z().back()   << "," << M.points.size() << ",\"MULTIPOLYGON((";
        //fo1 << std::fixed << std::setprecision(3);
        // Writing the multipolygon structure
        // For each polygon of the multipolygon set
        for(std::list<Poly_seg>::iterator lit = meta_list.begin(); lit != meta_list.end();++lit){
            // The current polygon
            Poly_seg ls = *lit;
            int acc = 0;
            //if(acc1 != 0)
            //    fo1 << ",";
            //fo1 << "(";
            Poly_seg::iterator sit_last = ls.end();
            sit_last--;

//            // For each segment of the polygon
//            for(sit = ls.begin(); sit != ls.end();++sit){
//                if(acc != 0)
//                    fo1 << ",";
//                fo1 << sit->source().x() << " " << sit->source().y();// << " "  << "1";
//                if(sit == sit_last)
//                    fo1  << "," << sit->target().x() << " " << sit->target().y();// << " " << "1";
//                acc++;
//            }
//            //fo1 << ")";
//            acc1++;
        }
        //fo1 << "))\"" << std::endl;
    }
    fileOut.close();
}


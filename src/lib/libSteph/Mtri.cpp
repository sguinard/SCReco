#include "libSteph/Mtri.h"

namespace SCR{

/// \brief default constructor
Mtri::Mtri()
{

}

/// \brief constructor from vector of triangles
///
/// \param vtri: vector of triangles
Mtri::Mtri(std::vector<Triangle> vtri)
{
    for (auto & tri:vtri)
    {
        M[tri.i].push_back(tri);
        M[tri.j].push_back(tri);
        M[tri.k].push_back(tri);
    }
}


/// \brief constructor from vector of triangles
///
/// \param vtri: vector of triangles
Mtri::Mtri(std::vector<Triangle> *vtri)
{
    for (auto & tri:*vtri)
    {
        M[tri.i].push_back(tri);
        M[tri.j].push_back(tri);
        M[tri.k].push_back(tri);
    }
}

}


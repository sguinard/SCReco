#include "libSteph/Triangle_Coords.h"
#include "math.h"
#include "limits.h"

TriangleCoords::TriangleCoords()
{
  _points = vP3(1,XPt3D());
}

TriangleCoords::TriangleCoords(vP3 *points)
{
  for (auto& p: *points)
    _points.emplace_back(p);
}

TriangleCoords::~TriangleCoords()
{
  
}

XPt3D TriangleCoords::p1()
{
  return this->is_valid() ? this->_points[0]: XPt3D(std::numeric_limits<double>::max(),std::numeric_limits<double>::max(),std::numeric_limits<double>::max());
}

XPt3D TriangleCoords::p2()
{
  return this->is_valid() ? this->_points[1]: XPt3D(std::numeric_limits<double>::max(),std::numeric_limits<double>::max(),std::numeric_limits<double>::max());
}

XPt3D TriangleCoords::p3()
{
  return this->is_valid() ? this->_points[2]: XPt3D(std::numeric_limits<double>::max(),std::numeric_limits<double>::max(),std::numeric_limits<double>::max());
}

std::vector<double> TriangleCoords::get_p1()
{
	std::vector<double> p;
	if (this->is_valid())
	{
		p.emplace_back(this->_points[0].X);
		p.emplace_back(this->_points[0].Z);
		p.emplace_back(this->_points[0].Y);
	}
	return p;
}

std::vector<double> TriangleCoords::get_p2()
{
	std::vector<double> p;
	if (this->is_valid())
	{
		p.emplace_back(this->_points[1].X);
		p.emplace_back(this->_points[1].Z);
		p.emplace_back(this->_points[1].Y);
	}
	return p;
}


std::vector<double> TriangleCoords::get_p3()
{
	std::vector<double> p;
	if (this->is_valid())
	{
		p.emplace_back(this->_points[2].X);
		p.emplace_back(this->_points[2].Z);
		p.emplace_back(this->_points[2].Y);
	}
	return p;
}


void TriangleCoords::set_p1(XPt3D p)
{
  this->_points[0] = p;
}

void TriangleCoords::set_p2(XPt3D p)
{
  this->_points[1] = p;
}

void TriangleCoords::set_p3(XPt3D p)
{
  this->_points[2] = p;
}

bool TriangleCoords::is_adjacent(TriangleCoords t)
{
		if (this->p1() == t.p1())
			if (this->p2() == t.p2() || this->p2() == t.p3() || this-> p3() == t.p2() || this->p3() == t.p3())
				return true;
		else if (this->p1() == t.p2())
			if (this->p2() == t.p1() || this->p2() == t.p3() || this-> p3() == t.p1() || this->p3() == t.p3())
				return true;
		else if (this->p1() == t.p3())
			if (this->p2() == t.p1() || this->p2() == t.p2() || this-> p3() == t.p1() || this->p3() == t.p2())
				return true;
				
		if (this->p2() == t.p1())
			if (this->p1() == t.p2() || this->p1() == t.p3() || this-> p3() == t.p2() || this->p3() == t.p3())
				return true;
		else if (this->p2() == t.p2())
			if (this->p1() == t.p1() || this->p1() == t.p3() || this-> p3() == t.p1() || this->p3() == t.p3())
				return true;
		else if (this->p2() == t.p3())
			if (this->p1() == t.p1() || this->p1() == t.p2() || this-> p3() == t.p1() || this->p3() == t.p2())
				return true;
				
		if (this->p3() == t.p1())
			if (this->p2() == t.p2() || this->p2() == t.p3() || this-> p1() == t.p2() || this->p1() == t.p3())
				return true;
		else if (this->p3() == t.p2())
			if (this->p2() == t.p1() || this->p2() == t.p3() || this-> p1() == t.p1() || this->p1() == t.p3())
				return true;
		else if (this->p3() == t.p3())
			if (this->p2() == t.p1() || this->p2() == t.p2() || this-> p1() == t.p1() || this->p1() == t.p2())
				return true;
}

XPt3D TriangleCoords::centroid()
{
	return this->is_valid() ? operator /(operator +(operator +(p1(), p2()), p3()), 3) : 
							  XPt3D (std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
}

bool TriangleCoords::is_valid()
{
  return _points.size() == 3 ? true : false;
}

bool TriangleCoords::intersects(TriangleCoords tri)
{
	if (this->is_valid() && tri.is_valid())
		if (this->_points[0] == tri._points[0] || this->_points[0] == tri._points[1] || this->_points[0] == tri._points[2] ||
			this->_points[1] == tri._points[0] || this->_points[1] == tri._points[1] || this->_points[1] == tri._points[2] ||
			this->_points[2] == tri._points[0] || this->_points[2] == tri._points[1] || this->_points[2] == tri._points[2])
	return false;
}

double TriangleCoords::area()
{
  if (this->is_valid())
    {
      XPt3D p1 = this->_points[2];
      XPt3D p2 = operator /(operator +(this->_points[0], this->_points[1]),2);
      double base = dist2(this->_points[0],this->_points[1]);
      double height = dist2(p1,p2);
      return base*height/2;
    }
  else return -1;
}

std::vector<double> TriangleCoords::distance2plane(Plane p)
{
  if (this->is_valid())
    {
      std::vector<double> distances;
      distances.emplace_back(p.quadratic_distance_point_plane(this->_points[0]));
      distances.emplace_back(p.quadratic_distance_point_plane(this->_points[1]));
      distances.emplace_back(p.quadratic_distance_point_plane(this->_points[2]));
      return distances;
    }
  return std::vector<double>(1,0);
}

double TriangleCoords::distance_triangle_plane(Plane p)
{
  if (this->is_valid())
    {
      double triangle_area = this->area();
      std::vector<double> distances = this->distance2plane(p);
      return triangle_area*( pow(distances[0],2.0) + pow(distances[1],2.0) + pow(distances[2],2.0)
		  + distances[0]*distances[1] + distances[0]*distances[2] + distances[1]*distances[2] ) /6;
    }
  return -1;
}

TriangleCoords TriangleCoords::project_triangle_plane(Plane p)
{
	TriangleCoords projete;
	if (this->is_valid())
	{
		vP3 newpts {p.project_point_plane(this->p1()), p.project_point_plane(this->p2()), p.project_point_plane(this->p3())};
		projete = TriangleCoords(&newpts);
	}
	return projete;
}

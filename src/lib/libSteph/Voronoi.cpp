#include "libSteph/Voronoi.h"



std::list<Triangulation::Point> addToList(std::list<Triangulation::Point> L, vvD* observations)
{
    for (auto& point:*observations)
        L.emplace_back(Triangulation::Point(point[0],point[1],point[2]));

    return L;
}



std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* create_3d_delaunay(std::list<Triangulation::Point> L, vvD* &observations)
{
    std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* edges = new std::pair<std::vector<uint32_t>, std::vector<uint32_t> >;

    Triangulation T(L.begin(), L.end()); // automatically make the triangulation
    observations->clear();

    // unused, just kept for the example
    Triangulation::size_type n = T.number_of_vertices();

// Construction of a map of points and their neighbors
    std::map<std::vector<double>,uint32_t> PointCoords; // key = coords & value = index (no link with point id)
    std::map<int,std::vector<int> > PointNeighbors; // key = index & value = vector of index of point's neighbors in Delaunay
    uint32_t index = 0;
    for (Triangulation::Finite_vertices_iterator it=T.finite_vertices_begin(); it!=T.finite_vertices_end(); ++it/*, ++index*/)
    {   // find all point's coords and store them with an internal index
        //++index;
        Triangulation::Vertex& V = *it;
        Triangulation::Point P = V.point();
        std::vector<double> Coords;
        Coords.push_back(P.x());
        Coords.push_back(P.y());
        Coords.push_back(P.z());
        if (PointCoords.find(Coords) == PointCoords.end())
        {
            PointCoords[Coords] = index;
            ++index;
            observations->emplace_back(std::vector<double>{P.x(),P.y(),P.z()});
        }
    }
    //observations->clear();
    for (std::map<std::vector<double>,uint32_t>::iterator it = PointCoords.begin(); it != PointCoords.end(); ++it)
    {
        if (it->second < observations->size())
        observations->at(it->second) = std::vector<double>{it->first[0],it->first[1],it->first[2]};
    }
    for (Triangulation::Finite_edges_iterator ei=T.finite_edges_begin(); ei!=T.finite_edges_end(); ++ei)
    {   // browse every edge and store them (source->target & target->source)
        const Triangulation::Edge& E = *ei;
        Triangulation::Segment S = T.segment(E);
        const Triangulation::Point& PSource = S.source();
        const Triangulation::Point& PTarget = S.target();
        // find coords to access point's internal index with PointCoords map
        std::vector<double> CoordsSource;
        CoordsSource.push_back(PSource.x());
        CoordsSource.push_back(PSource.y());
        CoordsSource.push_back(PSource.z());
        std::vector<double> CoordsTarget;
        CoordsTarget.push_back(PTarget.x());
        CoordsTarget.push_back(PTarget.y());
        CoordsTarget.push_back(PTarget.z());
        uint32_t indexSource = PointCoords.find(CoordsSource)->second;
        uint32_t indexTarget = PointCoords.find(CoordsTarget)->second;

//        std::cout << indexSource << " " << indexTarget << std::endl;

        edges->first.emplace_back(indexSource);
        edges->second.emplace_back(indexTarget);
        //PointNeighbors[indexSource].push_back(indexTarget);
        //PointNeighbors[indexTarget].push_back(indexSource);
    }

//    std::pair<std::map<std::vector<float>,int >,std::map<int,std::vector<int> > > Delaunay3D;
//    Delaunay3D.first=PointCoords;
//    Delaunay3D.second=PointNeighbors;

    //std::cout << edges->first[0] << " " << edges->second[0] << std::endl;

    return edges;

}



std::vector<Triangle>* convert_3d_delaunay(std::list<Triangulation::Point> L, vvD* &observations)
{
    std::vector<Triangle>* v_tri = new std::vector<Triangle>;
    std::map<Triangulation::Vertex_handle, uint> point2index;
    observations->clear();

    Triangulation T(L.begin(), L.end()); // automatically make the triangulation

    // unused, just kept for the example
    Triangulation::size_type n = T.number_of_vertices();

// Construction of a map of points and their neighbors
    std::map<Triangulation::Point,uint32_t> PointCoords; // key = coords & value = index (no link with point id)
    uint32_t index = 0;
    for (Triangulation::Finite_vertices_iterator it=T.finite_vertices_begin(); it!=T.finite_vertices_end(); ++it)
    {   // find all point's coords and store them with an internal index
        Triangulation::Vertex& V = *it;
        Triangulation::Point P = V.point();
        if (PointCoords.find(P) == PointCoords.end())
        {
            PointCoords[P] = index++;
            observations->emplace_back(std::vector<double>{P.x(),P.y(),P.z()});
        }
//        std::cout << P.x() << " " << P.y() << " " << P.z() << std::endl;
    }
    for (std::map<Triangulation::Point,uint32_t>::iterator it = PointCoords.begin(); it != PointCoords.end(); ++it)
    {
        if (it->second < observations->size())
        observations->at(it->second) = std::vector<double>{it->first.x(),it->first.y(),it->first.z()};
    }

    for (Triangulation::Finite_facets_iterator fit = T.finite_facets_begin(); fit != T.finite_facets_end(); ++fit)
    {
        const Triangulation::Facet& F = *fit;
        auto cit = fit->first;
        auto point1 = cit->vertex((fit->second+1)%4)->point();
        auto point2 = cit->vertex((fit->second+2)%4)->point();
        auto point3 = cit->vertex((fit->second+3)%4)->point();
        uint32_t p1 = (uint32)PointCoords.find(point1)->second;
        uint32_t p2 = (uint32)PointCoords.find(point2)->second;
        uint32_t p3 = (uint32)PointCoords.find(point3)->second; 
        std::vector<uint32_t> vp {p1,p2,p3};
        std::sort(vp.begin(), vp.end());
        Triangle tri(vp[0], vp[1], vp[2]);
//        if ((uint8)PointCoords.find(point1)->second == 3917 ||
//            (uint8)PointCoords.find(point1)->second == 3917 ||
//            (uint8)PointCoords.find(point1)->second == 3917)
//std::cout << PointCoords.find(point1)->second << " " << PointCoords.find(point2)->second << " " << PointCoords.find(point3)->second << std::endl;
        v_tri->emplace_back(tri);
    }

    return v_tri;
}



std::vector<Triangle>* convert_3d_delaunay(std::list<Triangulation::Point> L, vvD* &observations, float max_edge_length)
{
    std::vector<Triangle>* v_tri = new std::vector<Triangle>;
    std::map<Triangulation::Vertex_handle, uint> point2index;
    observations->clear();

    Triangulation T(L.begin(), L.end()); // automatically make the triangulation

    // unused, just kept for the example
    Triangulation::size_type n = T.number_of_vertices();

// Construction of a map of points and their neighbors
    std::map<Triangulation::Point,uint32_t> PointCoords; // key = coords & value = index (no link with point id)
    uint32_t index = 0;
    for (Triangulation::Finite_vertices_iterator it=T.finite_vertices_begin(); it!=T.finite_vertices_end(); ++it)
    {   // find all point's coords and store them with an internal index
        Triangulation::Vertex& V = *it;
        Triangulation::Point P = V.point();
        if (PointCoords.find(P) == PointCoords.end())
        {
            PointCoords[P] = index++;
            observations->emplace_back(std::vector<double>{P.x(),P.y(),P.z()});
        }
//        std::cout << P.x() << " " << P.y() << " " << P.z() << std::endl;
    }
    for (std::map<Triangulation::Point,uint32_t>::iterator it = PointCoords.begin(); it != PointCoords.end(); ++it)
    {
        if (it->second < observations->size())
        observations->at(it->second) = std::vector<double>{it->first.x(),it->first.y(),it->first.z()};
    }

    for (Triangulation::All_facets_iterator fit = T.all_facets_begin(); fit != T.all_facets_end(); ++fit)
    {
        const Triangulation::Facet& F = *fit;
        auto cit = fit->first;
        auto point1 = cit->vertex((fit->second+1)%4)->point();
        auto point2 = cit->vertex((fit->second+2)%4)->point();
        auto point3 = cit->vertex((fit->second+3)%4)->point();
        if (CGAL::squared_distance(point1,point2) < max_edge_length &&
            CGAL::squared_distance(point1,point3) < max_edge_length &&
            CGAL::squared_distance(point2,point3) < max_edge_length)
        {
        Triangle tri((uint32)PointCoords.find(point1)->second,(uint32)PointCoords.find(point2)->second,(uint32)PointCoords.find(point3)->second);
//        if ((uint8)PointCoords.find(point1)->second == 3917 ||
//            (uint8)PointCoords.find(point1)->second == 3917 ||
//            (uint8)PointCoords.find(point1)->second == 3917)
//std::cout << PointCoords.find(point1)->second << " " << PointCoords.find(point2)->second << " " << PointCoords.find(point3)->second << std::endl;
        v_tri->emplace_back(tri);
        }
    }

	std::sort (v_tri->begin(), v_tri->end());
	v_tri->erase( unique(v_tri->begin(), v_tri->end()), v_tri->end());

    return v_tri;
}

#include "libSteph/Triangle.h"

namespace SCR{

/// @brief Check whether 2 triangles are adjacent
///        Adjacency is caracterised by the fact that the 2 triangles have a common edge
///
/// @param Triangle t1 : 1st triangle
/// @param Triangle t2 : 2nd triangle
///
/// @return bool : true if triangles are adjacent, false else
bool adjacent(Triangle t1, Triangle t2)
{
    int cpt = 0;
    if (t1.i == t2.i || t1.i == t2.j || t1.i == t2.k) ++cpt;
    if (t1.j == t2.i || t1.j == t2.j || t1.j == t2.k) ++cpt;
    if (t1.k == t2.i || t1.k == t2.j || t1.k == t2.k) ++cpt;
    return cpt == 2 ? true : false;
}

bool adjacent(const Triangle* t1, const Triangle* t2)
{
    int cpt = 0;
    if (t1->i == t2->i || t1->i == t2->j || t1->i == t2->k) ++cpt;
    if (t1->j == t2->i || t1->j == t2->j || t1->j == t2->k) ++cpt;
    if (t1->k == t2->i || t1->k == t2->j || t1->k == t2->k) ++cpt;
    return cpt == 2 ? true : false;
}


bool find_triangle(std::vector<Triangle> vt, Triangle t)
{

    for (std::vector<Triangle>::iterator it = vt.begin(); it != vt.end(); ++it)
        if (*it == t) return true;
    return false;

}

void Triangle::print()
{
    std::cout << "Triangle: " << i << " " << j << " " << k << std::endl;
}

XPt3D Triangle::normal(XMls& mls)
{
    XPt3D p1 = mls.Pworld(0,i);
    XPt3D p2 = mls.Pworld(0,j);
    XPt3D p3 = mls.Pworld(0,k);
    XPt3D v1 = p2-p1;
    XPt3D v2 = p2-p3;
    XPt3D normale;
    normale.X = v1.Y * v2.Z - v1.Z * v2.Y;
    normale.Y = v1.Z * v2.X - v1.X * v2.Z;
    normale.Z = v1.X * v2.Y - v1.Y * v2.X;
    normale.Normalise();
    return normale;
}

float Triangle::perimeter(XMls& mls)
{
    XPt3D p1 = mls.Pworld(0,i);
    XPt3D p2 = mls.Pworld(0,j);
    XPt3D p3 = mls.Pworld(0,k);
    float d1 = dist2(p1,p2);
    float d2 = dist2(p1,p3);
    float d3 = dist2(p2,p3);
    return d1+d2+d3;
}

float Triangle::area(XMls &mls)
{
    XPt3D p1 = mls.Pworld(0,i);
    XPt3D p2 = mls.Pworld(0,j);
    XPt3D p3 = mls.Pworld(0,k);
    XPt3D v1 = p2-p1;
    XPt3D v2 = p3-p1;
    XPt3D normale;
    normale.X = v1.Y * v2.Z - v1.Z * v2.Y;
    normale.Y = v1.Z * v2.X - v1.X * v2.Z;
    normale.Z = v1.X * v2.Y - v1.Y * v2.X;
    return 0.5*normale.Norme();
}

}

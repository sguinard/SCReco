#include <ctime>
#include <cmath>
#include <math.h>
#include <iostream>
#include <map>
#include <limits>
#include <stdio.h>
#include <thread>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/Edge.h"
#include "libSteph/maxFlow/graph.h"

namespace SCR{

/// Constructors

/// @brief Default constructor
edge::edge()
{
    this->block = ~0;
    this->node_begin = ~0;
    this->node_end = ~0;
}

/// @brief Copy constructor
edge::edge(const edge &e)
{
    this->block = e.block;
    this->node_begin = e.node_begin;
    this->node_end = e.node_end;
}

/// @brief Constructor with block-independent echo numbers
///
/// @param n1: 1st echo
/// @param n2: 2nd echo
edge::edge(XEchoIndex n1, XEchoIndex n2)
{
    this->block = ~0;
    this->node_begin = n1;
    this->node_end = n2;
}

/// @brief Constructor with block and echo numbers
///
/// @param b: block to which edge belong
/// @param n1: 1st echo
/// @param n2: 2nd echo
edge::edge(XBlockIndex b, XEchoIndex n1, XEchoIndex n2)
{
    this->block = b;
    this->node_begin = n1;
    this->node_end = n2;
}

/// @brief Print method for edge
void edge::print_edge()
{
    std::cout << "Edge in block " << block << " between nodes " << node_begin << " and " << node_end << std::flush;
}

/// @brief Find if edge is valid
/// i.e. nodes are different
bool edge::is_valid()
{
    return (node_begin == node_end ? false : true);
}

double edge::length(XMls & mls)
{
    return d2(*this,mls);
}

/// @brief Compute euclidian distance between 2 points given their spherical coordinates
///
/// @param n1: 1st point index
/// @param n2: 2nd point index
/// @param block_idx: block index
/// @param range  |
/// @param theta  | => spherical parameters for points in block
/// @param phi    |
///
/// @return float value: euclidian distance between n1 and n2
float d_spherical(XEchoIndex n1, XEchoIndex n2, XBlockIndex block_idx, XFloatAttrib* range, XFloatAttrib* theta, XFloatAttrib* phi)
{

    float r1 = range->at(block_idx)[n1];
    float t1 = theta->at(block_idx)[n1];
    float p1 =   phi->at(block_idx)[n1];
    float r2 = range->at(block_idx)[n2];
    float t2 = theta->at(block_idx)[n2];
    float p2 =   phi->at(block_idx)[n2];

    float x1 = r1*sin(p1)*cos(t1);
    float y1 = r1*sin(p1)*sin(t1);
    float z1 = r1*sin(p1);
    float x2 = r2*sin(p2)*cos(t2);
    float y2 = r2*sin(p2)*sin(t2);
    float z2 = r2*sin(p2);

    return sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2) );

}

/// @brief Compute euclidian distance between 2 points given their cartesian coordinates
///
/// @param p1: 1st point
/// @param p2: 2nd point
///
/// @return double value: euclidian distance between p1 and p2
double d2(XPt3D p1, XPt3D p2)
{

    return sqrt( (p1.X-p2.X)*(p1.X-p2.X) + (p1.Y-p2.Y)*(p1.Y-p2.Y) + (p1.Z-p2.Z)*(p1.Z-p2.Z) );

}

/// @brief Compute euclidian distance between 2 points given their cartesian coordinates
///
/// @param p1: 1st point
/// @param p2: 2nd point
///
/// @return double value: euclidian distance between p1 and p2
double d2(edge e1, XMls& mls)
{

    return d2(mls.Pworld(e1.block,e1.node_begin),mls.Pworld(e1.block,e1.node_end));

}

/// @brief Compute 1st part of energy: C0 regularity
/// i.e. find scalar product between
///     laser to 1st point ray
///     1st to 2nd point ray
/// if the scalar product is low (~0), points are nearly perpendicular to laser beam
/// if the scalar product is high (~1), points are nearly aligned and we want to discard an edge that would link them
///
/// @param Pivot: laser place when beam to 1st point has been emited
/// @param e1: 1st point
/// @param e2: 2nd point
///
/// @return double value between 0 and 1 stating the alignment of the 3 points
double c0(XPt3D Pivot, XPt3D e1, XPt3D e2)
{

    XPt3D e12 = e2-e1;
    XPt3D eij = e12.operator /= (e12.Norme());

    XPt3D ep1 = e1-Pivot;
    XPt3D li = ep1.operator /= (ep1.Norme());

    return 1-abs(li.X * eij.X + li.Y * eij.Y + li.Z * eij.Z);

}

/// @brief Compute 2nd part of energy: C1 regularity
/// i.e. find scalar product between
///     1st to 2nd point ray
///     2nd to 3rd point ray
/// if the scalar product is low (~0), points are not aligned
/// if the scalar product is high (~1), points are nearly aligned, so this may correspond to a grazing surface and we want to keep it
///
/// @param e1: 1st point
/// @param e2: 2nd point
/// @param e3: 3rd point
///
/// @return double value between 0 and 1 stating the alignment of the 3 points
double c1(XPt3D e1, XPt3D e2, XPt3D e3)
{

    XPt3D e12 = e2-e1;
    XPt3D eij = e12.operator /= (e12.Norme());

    XPt3D e23 = e3-e2;
    XPt3D ejk = e23.operator /= (e23.Norme());

    return 1-abs(ejk.X * eij.X + ejk.Y * eij.Y + ejk.Z * eij.Z);

}

/// @brief Compute 2nd part of energy: C1 regularity
/// i.e. find scalar product between
///     1st to 2nd point ray
///     2nd to 3rd point ray
/// if the scalar product is low (~0), points are not aligned
/// if the scalar product is high (~1), points are nearly aligned, so this may correspond to a grazing surface and we want to keep it
///
/// @param e1: 1st edge
/// @param e2: 2nd edge
/// @param mls
///
/// @return double value between 0 and 1 stating the alignment of the 3 points
double c1(edge e1, edge e2, XMls &mls)
{

    double _c1 = 0;

    mls.Load(e1.block);

    if (have_common_node(e1,e2))
    {
        XEchoIndex n1 = find_common_node(e1,e2);
        XEchoIndex n2 = n1 == e1.node_begin ? e1.node_end : e1.node_begin;
        XEchoIndex n3 = n1 == e2.node_begin ? e2.node_end : e2.node_begin;
        _c1 = c1(mls.Pworld(e1.block,n1),mls.Pworld(e1.block,n2),mls.Pworld(e1.block,n3));
    }

    mls.Free(e1.block);

    return 1-_c1;

}

/// @brief Print all edges containing a given echo
///
/// @param edges: map linking each echo to its corresponding edges
/// @param ei: echo which neighbouring edges are desired
void print_edges_by_echo(std::map<uint,std::vector<SCR::edge> > edges, XEchoIndex ei)
{
    XBlockIndex current_index = 0;
    std::cout << "Block: " << current_index << "\t";
    for (uint i=0; i<edges[ei].size(); i++)
    {
        if (edges[ei][i].block > current_index)
        {
            current_index = edges[ei][i].block;
            std::cout << "\nBlock: " << current_index << "\t";
        }
        std::cout << edges[ei][i].node_begin << " " << std::flush;
        std::cout << edges[ei][i].node_end << " " << std::flush;
    }
}

/// @brief Find if 2 edges have a common node
///
/// @param e1: 1st edge
/// @param e2: 2nd edge
///
/// @return boolean true if intersection, false else
bool have_common_node(edge e1, edge e2)
{
    return (    e1.block      == e2.block && (
                e1.node_begin == e2.node_begin  ||
                e1.node_begin == e2.node_end    ||
                e1.node_end   == e2.node_begin    ||
                e1.node_end   == e2.node_end  ) ) ? true : false;
}

/// @brief Find if 2 edges have a common node and return it
///
/// @param e1: 1st edge
/// @param e2: 2nd edge
///
/// @return node index if intersection, 0xffffffff else
XEchoIndex find_common_node(edge e1, edge e2)
{
    if (e1.block == e2.block)
    {
        if (    e1.node_begin == e2.node_begin  ||
                e1.node_begin == e2.node_end    )
            return e1.node_begin;
        if (    e1.node_end == e2.node_begin  ||
                e1.node_end == e2.node_end    )
            return e1.node_end;
    }
    return ~0;  // max uint value, used when no common node
}

/// @brief Find if 3 edges form a triangle
///
/// @param e1: 1st edge
/// @param e2: 2nd edge
/// @param e3: 3rd edge
///
/// @return true if intersection, false else
bool is_triangle(edge e1, edge e2, edge e3)
{
    XEchoIndex i1 = find_common_node(e1,e2);
    XEchoIndex i2 = find_common_node(e1,e3);
    XEchoIndex i3 = find_common_node(e3,e2);
    return     e1.block == e2.block && e1.block == e3.block            // if edges are in the same block
        &&  i1 != ~0 && i2 != ~0 && i3 != ~0                           // && if each edge intersects
        && (i1 != i2) && (i1 != i3) && (i2 != i3)  ?                   // && if respective intersections are all different
        true : false;
}

/// @brief Find if an edge is in a vector of edges
///
/// @param edges: vector of edges
/// @param e: edge to check belonging to
///
/// @return true if e belong to edges, false else
bool find_edge(std::vector<edge> edges, edge e)
{

    for (std::vector<edge>::iterator it = edges.begin(); it != edges.end(); ++it)
        if (*it == e || *it == edge(e.block,e.node_end,e.node_begin)) return true;
    return false;

}

/// @brief Find if an edge is in a vector of edges
///
/// @param edges: vector of edges
/// @param e: edge to check belonging to
///
/// @return iterator
std::vector<edge>::iterator find_edge_it(std::vector<edge> edges, edge e)
{

    for (std::vector<edge>::iterator it = edges.begin(); it != edges.end(); ++it)
        if (*it == e || *it == edge(e.block,e.node_end,e.node_begin)) return it;
    return edges.end();

}

/// @brief Nodes of the graph are edges linking "neighbouring" echos
///
/// for each pulse (with echos) in each block
/// find neighbourhood of the point
/// and compute c0 regularity between laser, and points
///
/// @param mls: XMls container
/// @param params: parameters of mls
///
/// @return pair of vectors containing the links between nodes and their c0 regularity value
std::pair<std::vector<edge>,std::vector<double> > fill_Nodes(XMls & mls, param params)
{

    std::vector<edge> Nodes;
    std::vector<double> c0_reg;

    int PPL = mls.PulsePerLine();
    XEchoIndex last_echo = 0;
    //XPt3D Pivot = mls.m_trajecto.SbetSeries().GetGeoref(0).Translation();
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    std::cout << Pivot.X << " " << Pivot.Y << " " << Pivot.Z << std::endl;

    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        std::cout << n_pulse << " pulses in block " << block_idx << std::endl;
        for(XPulseIndex pulse_idx=0; pulse_idx<n_pulse; pulse_idx++)
        {
            for (int i=0; i<mls.NbOfEcho(block_idx,pulse_idx);i++)
            {
                if ((pulse_idx + PPL + 1 < n_pulse) && mls.NbOfEcho(block_idx,pulse_idx) > 0)
                {
                    // Same line, next pulse
                    XEchoIndex n1(mls.IdxFirstEcho(block_idx, pulse_idx)+i);
                    XPt3D n1_coords = mls.Pworld(block_idx,n1);
                    for (int j=0; j<mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+1));j++)
                    {
                        XEchoIndex n2(mls.IdxFirstEcho(block_idx, pulse_idx + 1)+j);

                        XPt3D n2_coords = mls.Pworld(block_idx,n2);

                        Nodes.emplace_back(edge(block_idx,n1 + last_echo, n2 + last_echo));
                        c0_reg.emplace_back(c0(Pivot,n1_coords,n2_coords));
                    }

                    // Next Line, pulse before
                    for (int j=0; j<mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx + PPL));j++)
                    {
                        XEchoIndex n3(mls.IdxFirstEcho(block_idx, pulse_idx + PPL)+j);
                        XPt3D n3_coords = mls.Pworld(block_idx,n3);

                        Nodes.emplace_back(edge(block_idx,n1 + last_echo, n3 + last_echo));
                        c0_reg.emplace_back(c0(Pivot,n1_coords,n3_coords));
                    }

                    // Next Line, pulse after
                    for (int j=0; j<mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx + PPL + 1));j++)
                    {
                        XEchoIndex n4(mls.IdxFirstEcho(block_idx, pulse_idx + PPL + 1)+j);
                        XPt3D n4_coords = mls.Pworld(block_idx,n4);

                        Nodes.emplace_back(edge(block_idx,n1 + last_echo, n4 + last_echo));
                        c0_reg.emplace_back(c0(Pivot,n1_coords,n4_coords));
                    }
                }
            }
        }
        for(XPulseIndex pulse_idx=0; pulse_idx<n_pulse; pulse_idx++)
            last_echo += mls.NbOfEcho(block_idx,pulse_idx);
        mls.Free(block_idx);
    }

    std::pair<std::vector<edge>,std::vector<double> > pair;
    pair.first = Nodes;
    pair.second = c0_reg;

    return pair;

}

/// Nodes of the graph are edges linking "neighbouring" echos
///
/// !!!!! TO COMMENT !!!!!
std::pair<std::vector<edge>,std::vector<double> > fill_Nodes(XMls &mls, std::vector<std::vector<int> >vv_pulse_with_echo_idx, param params)
{

    std::vector<edge> Nodes;
    std::vector<double> c0_reg;

    //std::cout << "NPulse " << mls.NPulse(0) << " PPL " << mls.PulsePerLine() << std::endl;

    int PPL = mls.PulsePerLine();
    //XPt3D Pivot = mls.m_trajecto.SbetSeries().GetGeoref(0).Translation();
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    std::cout << Pivot.X << " " << Pivot.Y << " " << Pivot.Z << std::endl;

    //mls.Load(0);
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        std::cout << block_idx << std::endl;
        mls.Load(block_idx);
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<n_pulse-1; pulse_idx++)
        {
            if (mls.NbOfEcho(block_idx,pulse_idx) >= 1)
            {
                int p1 = pulse_idx;
                //int p1 = vv_pulse_with_echo_idx[block_idx][pulse_idx];

                // same line, next pulse
                if (mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+1)) >= 1 && pulse_idx+1 <= mls.NPulse(block_idx))
                {
                    int p2 = pulse_idx+1;
                    //int p2 = vv_pulse_with_echo_idx[block_idx][pulse_idx+1];
                    Nodes.emplace_back(edge(block_idx,p1,p2));
                    c0_reg.emplace_back(c0(Pivot,
                                        mls.Pworld(block_idx,mls.IdxLastEcho(block_idx, pulse_idx)),
                                        mls.Pworld(block_idx,mls.IdxLastEcho(block_idx, pulse_idx+1))));
                }

                // next line, same pulse
                if (XPulseIndex(pulse_idx+PPL) < mls.NPulse(block_idx))
                if (mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+PPL)) >= 1 && pulse_idx+PPL <= mls.NPulse(block_idx))
                {
                    int p2 = pulse_idx+PPL;
                    //int p2 = vv_pulse_with_echo_idx[block_idx][pulse_idx+PPL];
                    Nodes.emplace_back(edge(block_idx,p1,p2));
                    c0_reg.emplace_back(c0(Pivot,
                                        mls.Pworld(block_idx,mls.IdxLastEcho(block_idx, pulse_idx)),
                                        mls.Pworld(block_idx,mls.IdxLastEcho(block_idx, pulse_idx+PPL))));
                }

                // next line, next pulse
                if (XPulseIndex(pulse_idx+PPL+1) < mls.NPulse(block_idx))
                if (mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+PPL+1)) >= 1 && pulse_idx+PPL+1 <= mls.NPulse(block_idx))
                {
                    int p2 = pulse_idx+PPL+1;
                    //int p2 = vv_pulse_with_echo_idx[block_idx][pulse_idx+PPL+1];
                    Nodes.emplace_back(edge(block_idx,p1,p2));
                    c0_reg.emplace_back(c0(Pivot,
                                        mls.Pworld(block_idx,mls.IdxLastEcho(block_idx, pulse_idx)),
                                        mls.Pworld(block_idx,mls.IdxLastEcho(block_idx, pulse_idx+PPL+1))));
                }

            }
        }
        mls.Free(block_idx);
    }

    std::pair<std::vector<edge>,std::vector<double> > pair;
    pair.first = Nodes;
    pair.second = c0_reg;

    return pair;

}


/// @brief check in cloud if points necessary to c0 and c1 computation exists and have at elast 1 echo
///
/// @param mls
/// @param block_idx : block to which points belong
/// @param pulse_idx : pulse studied
/// @param i : which line are we working on :   - acquisition line (then i=1)
///                                             - same pulse in next line (then i=PPL)
///                                             - next pulse in next line (then i=PPL+1)
///
/// @return boolean : true if edge can be computed, false else
bool check_points_existence (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i)
{
    return  pulse_idx+2*i < mls.NPulse(block_idx) &&                    // check if 2 next pulses exists
            (int)pulse_idx-i >= 0 &&                                     // && if precedent pulse exists
            mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+2*i)) >= 1 &&  // && if 2nd next pulse has an echo
            mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+i)) >= 1 &&    // && if next pulse has an echo
            mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx-i)) >= 1       // && if precedent pulse has an echo
            ? true : false;
}

/// @brief check in cloud if points necessary to c0 and c1 computation exists and have at elast 1 echo
///
/// @param mls
/// @param block_idx : block to which points belong
/// @param pulse_idx : pulse studied
/// @param i : which line are we working on :   - acquisition line (then i=1)
///                                             - same pulse in next line (then i=PPL)
///                                             - next pulse in next line (then i=PPL+1)
///
/// @return boolean : true if edge can be computed, false else
int check_points_existence_multiblock (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i)
{
    if (mls.NbOfEcho(block_idx,pulse_idx) >= 1)
    {
        uint n_pts_b = mls.NPulse(block_idx); // 291XXX
        //std::cout << pulse_idx + i << " " << std::flush;
        //std::cout << pulse_idx << " " << i << " " << block_idx << " " << mls.NBlock() << " - " << std::flush;
        if (pulse_idx + (uint)i >= n_pts_b && mls.NBlock() > block_idx+(uint)1)
        {
            mls.Load(block_idx+1);
            //std::cout << pulse_idx << " " << std::flush;
            // check points in next block
            if (    mls.NbOfEcho( block_idx+1, XPulseIndex(i-(mls.NPulse(block_idx)-pulse_idx)) ) >= 1 &&
                    mls.NbOfEcho( block_idx+1, XPulseIndex(2*i-(mls.NPulse(block_idx)-pulse_idx)) ) >= 1  &&
                    mls.NbOfEcho( block_idx, XPulseIndex(pulse_idx-i )) >= 1 )
            return 1;
            mls.Free(block_idx+1);
        }
        else if (pulse_idx + (uint)(2*i) >= n_pts_b && mls.NBlock() > block_idx+(uint)1)
        {
            mls.Load(block_idx+1);
            //std::cout << pulse_idx << " " << std::flush;
            //
            if (    mls.NbOfEcho( block_idx, XPulseIndex(pulse_idx+i) ) >= 1 &&
                    mls.NbOfEcho( block_idx+1, XPulseIndex(2*i-(mls.NPulse(block_idx)-pulse_idx)) ) >= 1  &&
                    mls.NbOfEcho( block_idx, XPulseIndex(pulse_idx-i) ) >= 1 )
            return 2;
            mls.Free(block_idx+1);
        }
        else if (pulse_idx - (uint)i < (uint)0 && block_idx-(uint)1 >= (uint)0)
        {
            mls.Load(block_idx-1);
            //std::cout << pulse_idx << " " << std::flush;
            //
            if (    mls.NbOfEcho( block_idx, XPulseIndex(pulse_idx+i) ) >= 1 &&
                    mls.NbOfEcho( block_idx, XPulseIndex(pulse_idx+2*i) ) >= 1  &&
                    mls.NbOfEcho( block_idx-1,XPulseIndex( mls.NPulse(block_idx-1)-(i-pulse_idx) )) >= 1 )
            return 3;
            mls.Free(block_idx-1);
        }
    }
    return -1;
}


/// @warning work in progress
bool check_inferior_existence (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i)
{
    return  pulse_idx+i < mls.NPulse(block_idx) &&                    // check if next pulses exists
            (int)pulse_idx-i > 0 &&                                     // && if precedent pulse exists
            mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+i)) >= 1 &&    // && if next pulse has an echo
            mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx-i)) >= 1       // && if precedent pulse has an echo
            ? true : false;
}

/// @warning work in progress
bool check_superior_existence (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i)
{
    return  pulse_idx+2*i < mls.NPulse(block_idx) &&                    // check if 2 next pulses exists
            mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+2*i)) >= 1 &&  // && if 2nd next pulse has an echo
            mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+i)) >= 1     // && if next pulse has an echo
            ? true : false;
}

/// @warning work in progress
bool check_next_existence (XMls &mls, XBlockIndex block_idx, XPulseIndex pulse_idx, int i)
{
    return  pulse_idx+i < mls.NPulse(block_idx) &&                    // check if next pulses exists
            mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+i)) >= 1     // && if next pulse has an echo
            ? true : false;
}


/// @brief check whether score is ok so that we could keep edge
///
/// @param c0 : c0 regularity
/// @param c1_ijk : c1 between nodes of edge and preceding node on the same line
/// @param c1_jkl : c1 between nodes of edge and next node on same line
/// @param tresh : trehsold for c0
/// @param lambda : coefficient for c1
///
/// @return boolean : true if we should keep the edge, false else
bool check_score (double c0, double c1_ijk, double c1_jkl, double tresh, double lambda)
{
    return c0 > tresh ? true :
                        min((c1_ijk),(c1_jkl)) < (lambda * c0 * tresh) / (tresh - c0) ?
                        true : false;
}


/// @brief Browse points of the cloud and search for edges that should be kept looking at their c0 / c1 regularity
///
/// @param mls
/// @param params
/// @param vv_pulse_woth_echo_idx: unique indexation of pulses with >= 1 echo
/// @param tresh: threshold for c0_regularity
/// @param lambda: threshold for c1 regularity
///
/// @return pair containing chosen edges and their corresponding c0_regularity
///
/// @todo remove all similar blocks of code and create a custom function with all the corresponding parameters
std::vector<edge> filtering(XMls &mls, param params, std::vector<std::vector<int> > vv_pulse_with_echo_idx,
                                                            double tresh, double lambda, bool length_tresh)
{
    double ponder = 0.;
    double lmax = 1000;

    std::vector<edge> Nodes;

    int PPL = mls.PulsePerLine();
    std::cout << "PPL : " << PPL << std::endl;
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);

    std::cout.precision(10);
    std::cout << Pivot.X << " " << Pivot.Y << " " << Pivot.Z << std::endl;

    //mls.Load(0);
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        std::cout << block_idx << std::endl;
        mls.Load(block_idx);
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        double dmax = 0;
        for(XPulseIndex pulse_idx=1; pulse_idx<n_pulse-1; pulse_idx++)
        {
            //std::cout << pulse_idx << " " << std::flush;
            if (mls.NbOfEcho(block_idx,pulse_idx) >= 1) // for all pulses having at least 1 echo
            {
                for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                    if (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej)) > dmax /*&& d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej)) < 50*/)
                        dmax = d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej));
            }
        }
        std::cout << "dmax = " << dmax << std::endl;
        //lmax = dmax;
        for(XPulseIndex pulse_idx=1; pulse_idx<n_pulse-1; pulse_idx++)
        {
            //std::cout << pulse_idx << " " << std::flush;
            if (mls.NbOfEcho(block_idx,pulse_idx) >= 1) // for all pulses having at least 1 echo
            {
                // same line, next pulse
                if (check_points_existence(mls,block_idx,pulse_idx,1))
                {
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                        {
                            //std::cout << c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) << " " << std::flush;
                            //std::cout << sin(d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax) << " " << std::flush;
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) + ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);//+ ponder * sin(d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);// compute c0_regularity
                            for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-1); ei <= mls.IdxLastEcho(block_idx,pulse_idx-1); ++ei)   // precedent pulse
                            {
                                double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2); el <= mls.IdxLastEcho(block_idx,pulse_idx+2); ++el)   // 2nd next pulse
                                {
                                    double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,el)); // compute c1 reg. with 2nd next pulse
                                    //std::cout << "prout " << std::flush;
                                    if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                         && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                    {
                                        Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                    }
                                    //std::cout << "prout " << std::flush;
                                }
                            }
                        }
                }


                else if (check_points_existence_multiblock(mls,block_idx,pulse_idx,1) > 0)
                {
                    int cas  = check_points_existence_multiblock(mls,block_idx,pulse_idx,1);
                    //std::cout << cas << " " << std::flush;
                    if (cas == 1) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx+1,1-(mls.NPulse(block_idx)-pulse_idx)); ek <= mls.IdxLastEcho(block_idx+1,1-(mls.NPulse(block_idx)-pulse_idx)); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx+1,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-1); ei <= mls.IdxLastEcho(block_idx,pulse_idx-1); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx+1,2-(mls.NPulse(block_idx)-pulse_idx)); el <= mls.IdxLastEcho(block_idx+1,2-(mls.NPulse(block_idx)-pulse_idx)); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,mls.NPulse(block_idx)+ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 2) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-1); ei <= mls.IdxLastEcho(block_idx,pulse_idx-1); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx+1,2-(mls.NPulse(block_idx)-pulse_idx)); el <= mls.IdxLastEcho(block_idx+1,2-(mls.NPulse(block_idx)-pulse_idx)); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 3) // next block
                    {
                        mls.Load(block_idx-1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx+1,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx-1,mls.NPulse(block_idx-1)-(1-pulse_idx)); ei <= mls.IdxLastEcho(block_idx-1,mls.NPulse(block_idx-1)-(1-pulse_idx)); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx-1,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2); el <= mls.IdxLastEcho(block_idx,pulse_idx+2); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx-1);
                    }
                }


                else if (check_inferior_existence(mls,block_idx,pulse_idx,1))
                {
                    //std::cout << "a " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-1); ei <= mls.IdxLastEcho(block_idx,pulse_idx-1); ++ei)   // precedent pulse
                            {
                                double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                if ( check_score(c0_jk,c1_ijk,1,tresh,lambda)
                                     && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                    //std::cout << "a " << std::flush;
                                    //std::cout << c0_jk << " " << std::flush;
                                }
                            }
                        }
                }
                else if (check_superior_existence(mls,block_idx,pulse_idx,1))
                {
                    //std::cout << "b " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2); el <= mls.IdxLastEcho(block_idx,pulse_idx+2); ++el)   // 2nd next pulse
                            {
                                double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,el)); // compute c1 reg. with 2nd next pulse
                                if ( check_score(c0_jk,1,c1_jkl,tresh,lambda)
                                     && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                    //std::cout << "b " << std::flush;
                                }
                            }
                        }
                }
                else if (check_next_existence(mls,block_idx,pulse_idx,1))
                {
                    //std::cout << "b " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            if ( check_score(c0_jk,1,1,tresh,lambda)
                                 && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                //std::cout << "b " << std::flush;
                            }
                        }
                }

                // next line, same pulse
                if (check_points_existence(mls,block_idx,pulse_idx,PPL))
                {
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);
                            for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-PPL); ei <= mls.IdxLastEcho(block_idx,pulse_idx-PPL); ++ei)
                            {
                                double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei));
                                for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2*PPL); el <= mls.IdxLastEcho(block_idx,pulse_idx+2*PPL); ++el)
                                {
                                    double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,el));
                                    if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                         && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                    {
                                        Nodes.emplace_back(edge(block_idx,ej,ek));
                                    }
                                }
                            }
                        }
                }

                else if (check_points_existence_multiblock(mls,block_idx,pulse_idx,PPL) > 0)
                {
                    int cas = check_points_existence_multiblock(mls,block_idx,pulse_idx,PPL);
                    //std::cout << cas << " " << std::flush;
                    if (cas == 1) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx+1,PPL-(mls.NPulse(block_idx)-pulse_idx)); ek <= mls.IdxLastEcho(block_idx+1,PPL-(mls.NPulse(block_idx)-pulse_idx)); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx+1,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-PPL); ei <= mls.IdxLastEcho(block_idx,pulse_idx-PPL); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx+1,2*PPL-(mls.NPulse(block_idx)-pulse_idx)); el <= mls.IdxLastEcho(block_idx+1,2*PPL-(mls.NPulse(block_idx)-pulse_idx)); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,mls.NPulse(block_idx)+ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 2) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-PPL); ei <= mls.IdxLastEcho(block_idx,pulse_idx-PPL); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx+1,2*PPL-(mls.NPulse(block_idx)-pulse_idx)); el <= mls.IdxLastEcho(block_idx+1,2*PPL-(mls.NPulse(block_idx)-pulse_idx)); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 3) // next block
                    {
                        mls.Load(block_idx-1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx+1,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx-1,mls.NPulse(block_idx-1)-(PPL-pulse_idx)); ei <= mls.IdxLastEcho(block_idx-1,mls.NPulse(block_idx-1)-(PPL-pulse_idx)); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx-1,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2*PPL); el <= mls.IdxLastEcho(block_idx,pulse_idx+2*PPL); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx-1);
                    }
                }

                else if (check_inferior_existence(mls,block_idx,pulse_idx,PPL))
                {
                    //std::cout << "c " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-PPL); ei <= mls.IdxLastEcho(block_idx,pulse_idx-PPL); ++ei)   // precedent pulse
                            {
                                double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                if ( check_score(c0_jk,c1_ijk,1,tresh,lambda)
                                     && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                    //std::cout << "c " << std::flush;
                                    //std::cout << c0_jk << " " << std::flush;
                                }
                            }
                        }
                }
                else if (check_superior_existence(mls,block_idx,pulse_idx,PPL))
                {
                    //std::cout << "d " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2*PPL); el <= mls.IdxLastEcho(block_idx,pulse_idx+2*PPL); ++el)   // 2nd next pulse
                            {
                                double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,el)); // compute c1 reg. with 2nd next pulse
                                if ( check_score(c0_jk,1,c1_jkl,tresh,lambda)
                                     && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                    //std::cout << "d " << std::flush;
                                }
                            }
                        }
                }
                else if (check_next_existence(mls,block_idx,pulse_idx,PPL))
                {
                    //std::cout << "b " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            if ( check_score(c0_jk,1,1,tresh,lambda)
                                 && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                //std::cout << "b " << std::flush;
                            }
                        }
                }

                // next line, next pulse
                if (check_points_existence(mls,block_idx,pulse_idx,PPL+1))
                {
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);
                            for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-PPL-1); ei <= mls.IdxLastEcho(block_idx,pulse_idx-PPL-1); ++ei)
                            {
                                double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei));
                                for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2*(PPL+1)); el <= mls.IdxLastEcho(block_idx,pulse_idx+2*(PPL+1)); ++el)
                                {
                                    double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,el));
                                    if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                         && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                    {
                                        Nodes.emplace_back(edge(block_idx,ej,ek));
                                    }
                                }
                            }
                        }
                }

                else if (check_points_existence_multiblock(mls,block_idx,pulse_idx,PPL+1) > 0)
                {
                    int cas = check_points_existence_multiblock(mls,block_idx,pulse_idx,PPL+1);
                    //std::cout << cas << " " << std::flush;
                    if (cas == 1) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx+1,PPL+1-(mls.NPulse(block_idx)-pulse_idx)); ek <= mls.IdxLastEcho(block_idx+1,PPL+1-(mls.NPulse(block_idx)-pulse_idx)); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx+1,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-PPL-1); ei <= mls.IdxLastEcho(block_idx,pulse_idx-PPL-1); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx+1,2*PPL+2-(mls.NPulse(block_idx)-pulse_idx)); el <= mls.IdxLastEcho(block_idx+1,2*PPL+2-(mls.NPulse(block_idx)-pulse_idx)); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,mls.NPulse(block_idx)+ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 2) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-PPL-1); ei <= mls.IdxLastEcho(block_idx,pulse_idx-PPL-1); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx+1,2*PPL+2-(mls.NPulse(block_idx)-pulse_idx)); el <= mls.IdxLastEcho(block_idx+1,2*PPL+2-(mls.NPulse(block_idx)-pulse_idx)); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 3) // next block
                    {
                        mls.Load(block_idx-1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                            {
                                double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx+1,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                                for (XEchoIndex ei = mls.IdxFirstEcho(block_idx-1,mls.NPulse(block_idx-1)-(PPL+1-pulse_idx)); ei <= mls.IdxLastEcho(block_idx-1,mls.NPulse(block_idx-1)-(PPL+1-pulse_idx)); ++ei)   // precedent pulse
                                {
                                    double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx-1,ei)); // compute c1_reg. with precedent pulse
                                    for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2*PPL+2); el <= mls.IdxLastEcho(block_idx,pulse_idx+2*PPL+2); ++el)   // 2nd next pulse
                                    {
                                        double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx+1,el)); // compute c1 reg. with 2nd next pulse
                                        if ( check_score(c0_jk,c1_ijk,c1_jkl,tresh,lambda)
                                             && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                        {
                                            Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                        }
                                    }
                                }
                            }
                        mls.Free(block_idx-1);
                    }
                }

                else if (check_inferior_existence(mls,block_idx,pulse_idx,PPL+1))
                {
                    //std::cout << "e " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            for (XEchoIndex ei = mls.IdxFirstEcho(block_idx,pulse_idx-PPL-1); ei <= mls.IdxLastEcho(block_idx,pulse_idx-PPL-1); ++ei)   // precedent pulse
                            {
                                double c1_ijk = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,ei)); // compute c1_reg. with precedent pulse
                                if ( check_score(c0_jk,c1_ijk,1,tresh,lambda)
                                     && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                    //std::cout << "e " << std::flush;
                                }
                            }
                        }
                }
                else if (check_superior_existence(mls,block_idx,pulse_idx,PPL+1))
                {
                    //std::cout << "f " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            for (XEchoIndex el = mls.IdxFirstEcho(block_idx,pulse_idx+2*(PPL+1)); el <= mls.IdxLastEcho(block_idx,pulse_idx+2*(PPL+1)); ++el)   // 2nd next pulse
                            {
                                double c1_jkl = c1(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek),mls.Pworld(block_idx,el)); // compute c1 reg. with 2nd next pulse
                                if ( check_score(c0_jk,1,c1_jkl,tresh,lambda)
                                     && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                    //std::cout << "f " << std::flush;
                                }
                            }
                        }
                }
                else if (check_next_existence(mls,block_idx,pulse_idx,PPL+1))
                {
                    //std::cout << "b " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                        {
                            double c0_jk = c0(mls.Cworld(block_idx,ej),mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek))+ ponder * (d2(mls.Pworld(block_idx,ej),mls.Cworld(block_idx,ej))/dmax);  // compute c0_regularity
                            if ( check_score(c0_jk,1,1,tresh,lambda)
                                 && d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < lmax) // c0 > tresh or min. of c1 < formula
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                //std::cout << "b " << std::flush;
                            }
                        }
                }


            }
        }
        mls.Free(block_idx);
    }

    //std::cout << "prout " << std::endl;

    std::sort( Nodes.begin(), Nodes.end(), order_edge_map());//std::cout << "prout " << std::endl;
    Nodes.erase( unique( Nodes.begin(), Nodes.end() ), Nodes.end() );

    //std::sort( score.begin(), score.end() );
    //score.erase( unique( score.begin(), score.end() ), score.end() );

    //std::pair<std::vector<edge>,std::vector<double> > p;
    //p.first = Nodes;
    //p.second = score;

    //std::cout << "prout " << std::endl;

    return Nodes;

}




/// @brief Browse points of the cloud and search for edges that should be kept looking at their c0 / c1 regularity
///
/// @param mls
/// @param params
/// @param vv_pulse_woth_echo_idx: unique indexation of pulses with >= 1 echo
/// @param tresh: threshold for c0_regularity
/// @param lambda: threshold for c1 regularity
///
/// @return pair containing chosen edges and their corresponding c0_regularity
///
/// @todo remove all similar blocks of code and create a custom function with all the corresponding parameters
std::vector<edge> naive_filtering(XMls &mls, param params, std::vector<std::vector<int> > vv_pulse_with_echo_idx,double thresh)
{

    std::vector<edge> Nodes;

    int PPL = mls.PulsePerLine();
    std::cout << "PPL : " << PPL << std::endl;
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);

    std::cout.precision(10);
    std::cout << Pivot.X << " " << Pivot.Y << " " << Pivot.Z << std::endl;

    //mls.Load(0);
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        std::cout << block_idx << std::endl;
        std::cout << mls.NPulse(block_idx) << std::endl;
        mls.Load(block_idx);
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        for(XPulseIndex pulse_idx=1; pulse_idx<n_pulse-1; pulse_idx++)
        {
            //std::cout << pulse_idx << " " << std::flush;
            if (mls.NbOfEcho(block_idx,pulse_idx) >= 1) // for all pulses having at least 1 echo
            {
                // same line, next pulse
                if (check_points_existence(mls,block_idx,pulse_idx,1))
                {
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                        {
                            double c0_jk = edge(block_idx,ej,ek).length(mls);  // compute c0_regularity
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }


                else if (check_points_existence_multiblock(mls,block_idx,pulse_idx,1) > 0)
                {
                    int cas  = check_points_existence_multiblock(mls,block_idx,pulse_idx,1);
                    //std::cout << cas << " " << std::flush;
                    if (cas == 1) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx+1,1-(mls.NPulse(block_idx)-pulse_idx)); ek <= mls.IdxLastEcho(block_idx+1,1-(mls.NPulse(block_idx)-pulse_idx)); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 2) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 3) // next block
                    {
                        mls.Load(block_idx-1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx-1);
                    }
                }


                else if (check_inferior_existence(mls,block_idx,pulse_idx,1))
                {
                    //std::cout << "a " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }
                else if (check_superior_existence(mls,block_idx,pulse_idx,1))
                {
                    //std::cout << "b " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }
                else if (check_next_existence(mls,block_idx,pulse_idx,1))
                {
                    //std::cout << "b " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }

                // next line, same pulse
                if (check_points_existence(mls,block_idx,pulse_idx,PPL))
                {
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }

                else if (check_points_existence_multiblock(mls,block_idx,pulse_idx,PPL) > 0)
                {
                    int cas = check_points_existence_multiblock(mls,block_idx,pulse_idx,PPL);
                    //std::cout << cas << " " << std::flush;
                    if (cas == 1) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx+1,PPL-(mls.NPulse(block_idx)-pulse_idx)); ek <= mls.IdxLastEcho(block_idx+1,PPL-(mls.NPulse(block_idx)-pulse_idx)); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 2) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 3) // next block
                    {
                        mls.Load(block_idx-1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx-1);
                    }
                }

                else if (check_inferior_existence(mls,block_idx,pulse_idx,PPL))
                {
                    //std::cout << "c " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }
                else if (check_superior_existence(mls,block_idx,pulse_idx,PPL))
                {
                    //std::cout << "d " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }
                else if (check_next_existence(mls,block_idx,pulse_idx,PPL))
                {
                    //std::cout << "b " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }

                // next line, next pulse
                if (check_points_existence(mls,block_idx,pulse_idx,PPL+1))
                {
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }

                else if (check_points_existence_multiblock(mls,block_idx,pulse_idx,PPL+1) > 0)
                {
                    int cas = check_points_existence_multiblock(mls,block_idx,pulse_idx,PPL+1);
                    //std::cout << cas << " " << std::flush;
                    if (cas == 1) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx+1,PPL+1-(mls.NPulse(block_idx)-pulse_idx)); ek <= mls.IdxLastEcho(block_idx+1,PPL+1-(mls.NPulse(block_idx)-pulse_idx)); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 2) // next block
                    {
                        mls.Load(block_idx+1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx+1);
                    }
                    if (cas == 3) // next block
                    {
                        mls.Load(block_idx-1);
                        for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                            for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                            {
                                if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                                {
                                    Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                                }
                            }
                        mls.Free(block_idx-1);
                    }
                }

                else if (check_inferior_existence(mls,block_idx,pulse_idx,PPL+1))
                {
                    //std::cout << "e " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }
                else if (check_superior_existence(mls,block_idx,pulse_idx,PPL+1))
                {
                    //std::cout << "f " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }
                else if (check_next_existence(mls,block_idx,pulse_idx,PPL+1))
                {
                    //std::cout << "b " << std::flush;
                    for (XEchoIndex ej = mls.IdxFirstEcho(block_idx,pulse_idx); ej <= mls.IdxLastEcho(block_idx,pulse_idx); ++ej)   // 1st pulse
                        for (XEchoIndex ek = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); ek <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++ek)   // next pulse
                        {
                            if ( d2(mls.Pworld(block_idx,ej),mls.Pworld(block_idx,ek)) < thresh)
                            {
                                Nodes.emplace_back(edge(block_idx,ej,ek)); // store node
                            }
                        }
                }


            }
        }
        mls.Free(block_idx);
    }
    std::cout << "tfgyhjn,k;:" << std::endl;
    std::sort( Nodes.begin(), Nodes.end(), order_edge_map() );
    std::cout << "tfgyhjn,k;:" << std::endl;
    Nodes.erase( unique( Nodes.begin(), Nodes.end() ), Nodes.end() );

    //std::sort( score.begin(), score.end() );
    //score.erase( unique( score.begin(), score.end() ), score.end() );

    //std::pair<std::vector<edge>,std::vector<double> > p;
    //p.first = Nodes;
    //p.second = score;

    return Nodes;

}




/// edges of the graph corresponds to nodes with 1 common echo
///
/// This method browse all neighbouring nodes
/// And create an "adjacency map" of all neighbouring edges
/// (i.e. pair of edges sharing a node)
///
/// For each pair of neighbouring edges
/// We compute the c1 regularity
/// Giving information whether the 2 edges are aligned
///
/// @param Nodes: vector of edges
/// @param mls
/// @param params
///
/// @return pair containing:
///             - a map with all edges for each echo
///             - a vector containing the c1 regularity
std::vector<std::map<int,std::vector<std::pair<edge,double> > > > fill_edges(std::vector<edge> Nodes, XMls &mls, param params)
{
    std::vector<std::map<int,std::vector<edge> > >edges;
    std::vector<std::map<int,std::vector<std::pair<edge,double> > > >pairE;
    std::vector<double> c1_reg;

    edges.reserve(mls.NBlock());

    for (XBlockIndex blkidx = 0; blkidx != mls.NBlock(); ++blkidx)
    {
        std::map<int,std::vector<edge> >  map;
        for (std::vector<edge>::iterator it = Nodes.begin(); it != Nodes.end(); ++it)
        {
            if ((**it).block == blkidx)
            {
                map[(**it).node_begin].emplace_back(**it);
                map[(**it).node_end].emplace_back(**it);
            }
        }
        edges.emplace_back(map);
    }

    int echos = 0;

    for (XBlockIndex blkidx = 0; blkidx != mls.NBlock(); ++blkidx)
    {
        std::cout << blkidx << " " << edges[blkidx].size() << std::endl;
        mls.Load(blkidx);

        std::map<int,std::vector<std::pair<edge,double> > > pair;

        int n1 = 0;

        for (std::map<int,std::vector<edge> >::iterator it = edges[blkidx].begin(); it != edges[blkidx].end(); ++it)
        {
            for (std::vector<edge>::iterator eit = it->second.begin(); eit != it->second.end(); ++eit)
            {
                pair[it->first].emplace_back(std::make_pair(*eit,0));
                for (std::vector<edge>::iterator eit2 = eit.operator +(1); eit2 != it->second.end(); ++eit2) //node_numbers[edge_map(*eit)];//
                {
                    //std::cout << eit->node_begin - echos << " " << std::flush;
                    if (    eit->node_begin  < edges[blkidx].size() &&
                            eit->node_end    < edges[blkidx].size() &&
                            eit2->node_begin < edges[blkidx].size() &&
                            eit2->node_end   < edges[blkidx].size())
                    {
                        double n = c1(mls.Pworld(blkidx,eit->node_begin),mls.Pworld(blkidx,eit->node_end),mls.Pworld(blkidx,eit2->node_end));
                        //if (n<0.1)
                        //{
                            pair[it->first].emplace_back(std::make_pair(*eit2,n));
                            ++n1;
                        //}
                    }
                }
            }
        }

        pairE.emplace_back(pair);
        echos+=edges[blkidx].size();
        std::cout << n1 << std::endl;

        mls.Free(blkidx);
    }

    return pairE;

}


/// edges of the graph corresponds to nodes with 1 common echo
///
/// This method browse all neighbouring nodes
/// And create an "adjacency map" of all neighbouring edges
/// (i.e. pair of edges sharing a node)
///
/// For each pair of neighbouring edges
/// We compute the c1 regularity
/// Giving information whether the 2 edges are aligned
///
/// @param Nodes: vector of edges
/// @param mls
/// @param params
///
/// @return pair containing:
///             - a vector of adjacent edges
///             - a vector containing the c1 regularity
std::pair<std::vector<std::pair<edge,edge> >,std::vector<double> > fill_edges_vec(std::vector<edge> Nodes, XMls &mls, param params)
{

    std::vector<std::pair<std::pair<XEchoIndex,XEchoIndex>,std::pair<XEchoIndex,XEchoIndex> > > e;
    e.reserve(Nodes.size()*10);
    std::vector<std::pair<edge,edge> > edges;
    std::vector<double> c1_reg;
    //c1_reg.reserve(Nodes.size()*10);
    int i=0;

    std::vector<edge>::iterator it_end = Nodes.end();

    for (XBlockIndex blkidx = 0; blkidx != mls.NBlock(); ++blkidx)
    {
        mls.Load(blkidx);

        for (std::vector<edge>::iterator it = Nodes.begin(); it != it_end; ++it)
        {
            //std::cout << i << " " << std::flush;++i;
            for (std::vector<edge>::iterator it2 = it.operator +(1); it2 != it_end; ++it2)
            {
                if (it->block == it2->block)
                if (it->node_begin == it2->node_begin || it->node_begin == it2->node_end || it->node_end == it2->node_begin || it->node_end == it2->node_end)
                {
                    //std::cout << i << " " << std::flush;
                    std::pair<std::pair<XEchoIndex,XEchoIndex>,std::pair<XEchoIndex,XEchoIndex> > p =
                        std::make_pair(std::make_pair(it->node_begin,it->node_end),std::make_pair(it2->node_begin,it2->node_end));
                    e[i]=p;
                    XEchoIndex n1 = find_common_node(**it,**it2);
                    XEchoIndex n2 = n1 == (**it).node_begin ? (**it).node_end : (**it).node_begin;
                    XEchoIndex n3 = n1 == (**it2).node_begin ? (**it2).node_end : (**it2).node_begin;
                    c1_reg.emplace_back(c1(mls.Pworld((**it).block,n1),mls.Pworld((**it).block,n2),mls.Pworld((**it).block,n3)));
                    //c1_reg[i]=c1(**it,**it2,mls);
                    //++i;
                }
                // if we have 2 edges sharing a node
                /*if  (have_common_node(*it,*it2))
                {
                    edges.emplace_back(std::pair<edge,edge>(**it,**it2));
                    c1_reg.emplace_back(c1(**it,**it2,mls));
                }*/

            }
        }

        mls.Free(blkidx);
    }

    std::cout << i << std::endl;

    return std::pair<std::vector<std::pair<edge,edge> >,std::vector<double> >(edges,c1_reg);

}


/// Compute c1 regularity
/// and add it to neighbouring vector and c0 regularity
///
/// !!!!! TO COMMENT !!!!!
std::pair<std::vector<edge>,std::pair<std::vector<double>,std::vector<double> > > fill_c1(XMls & mls, param params, std::pair<std::vector<edge>,std::vector<double> > Nodes)
{

    std::vector<edge> adjacency;
    std::vector<double> c0_vec;
    std::vector<double> c1_vec;

    // for each edge
    for (std::vector<edge>::iterator it = adjacency.begin(); it != adjacency.end(); ++it)
    {
        edge e1 = **it;

        // find an edge that starts by the end of the current one
        for (std::vector<edge>::iterator it2 = it; it2 != adjacency.end(); ++it2)
        {
            edge e2 = **it2;
            XEchoIndex n3 = find_common_node(e1,e2);
            if (n3 != 0xffffffff)
            {
                XEchoIndex n1 = e1.node_begin;
                XEchoIndex n2 = e1.node_end;

                XBlockIndex block_idx = e1.block;

                XPt3D n1_coords = mls.Pworld(block_idx,n1);
                XPt3D n2_coords = mls.Pworld(block_idx,n2);
                XPt3D n3_coords = mls.Pworld(block_idx,n3);

                double c1_value = c1(n1_coords,n2_coords,n3_coords);
                c1_vec.emplace_back(c1_value);
            }
        }
    }

    std::pair<std::vector<double>,std::vector<double> > energy;
    energy.first = c0_vec;
    energy.second = c1_vec;

    std::pair<std::vector<edge>,std::pair<std::vector<double>,std::vector<double> > > fill_c1;
    fill_c1.first = adjacency;
    fill_c1.second = energy;

    return fill_c1;

}




/// @brief Compute score for each node knowing neighbourhing edges
///
/// for each edge jk, compute a score as:
/// c0(jk)*(1-c1(ijk))*(1-c1(jkl))
///
/// @param std::pair<std::vector<edge>, std::vector<double> > pairN
/// @param std::vector<std::map<int, std::vector<std::pair<edge, double> > > > pairE
/// @param mls
///
/// @return std::vector<edge>: remaining edges
std::vector<edge> score(std::pair<std::vector<edge>, std::vector<double> > pairN,
           std::vector<std::map<int, std::vector<std::pair<edge, double> > > > pairE,
           XMls &mls)
{

    std::vector<edge> remaining;

    /*for (std::pair<std::vector<edge>, std::vector<double> >::iterator it = pairN.begin(); it != pairN.end(); ++it)
    {
        // it->first = edge
        // it->second = c0
        // map = pairE[0]
        //pairE[0].find(it->first);
    }*/

    return remaining;

}


/// Find triangles in the reconstruction and add them to a vector of triangles used to write .ply file
///
/// @warning testing
std::vector<Triangle> compute_mesh(std::map<uint,std::vector<edge> > edges, XMls &mls, vector< vector<int> > vv_pulse_with_echo_idx)
{

    std::vector<Triangle> v_tri;

    for (std::map<uint,std::vector<edge> >::iterator it = edges.begin(); it != edges.end(); ++it)
        for (std::vector<edge>::iterator e1 = it->second.begin(); e1 != it->second.end(); ++e1)
            for (std::vector<edge>::iterator e2 = edges.at(e1->node_end).begin(); e2 != edges.at(e1->node_end).end(); ++e2)
                for (std::vector<edge>::iterator e3 = edges.at(e2->node_end).begin(); e3 != edges.at(e2->node_end).end(); ++e3)
                {
                    //std::cout << e1->node_begin << " " << e3->node_end << " - " << std::flush;
                    if (e3->node_end == e1->node_begin)
                        v_tri.emplace_back(Triangle(e1->node_begin,e1->node_end,e2->node_end));
                }

    return v_tri;

}

/// @brief Find triangles in the reconstruction and add them to a vector of triangles used to write .ply file
///
/// @warning deprecated - not annymore (01/09/2107)
/// @warning correct bug of triangle superposition ...
std::vector<Triangle> compute_mesh(std::map<uint,std::vector<edge> > edges,XMls& mls)
{

    ///
    /// \brief v_tri is the returned mesh
    ///
    /// \brief edges is a map linking to each node its corresponding edges
    /// i.e. edges starting or ending by this node
    ///

    std::vector<Triangle> v_tri;

    std::cout << edges.size() << std::endl;

    // browse point cloud
    for (std::map<uint,std::vector<edge> >::iterator it = edges.begin(); it!=edges.end(); ++it)
    {
        //std::sort(it->second.begin(),it->second.end());
        std::sort( it->second.begin(), it->second.end() );
        it->second.erase( unique( it->second.begin(), it->second.end() ), it->second.end() );
        // for each point, browse its edges
        for (std::vector<edge>::iterator e1 = it->second.begin(); e1 != it->second.end(); ++e1)
        {
            uint node2 = e1->node_end;

            std::sort(edges.at(node2).begin(),edges.at(node2).end());

            // 2 edges with a common node: it->first
            for (std::vector<edge>::iterator e2 = e1.operator +(1); e2 != it->second.end(); ++e2)
            {

                if (*e1 != *e2 && have_common_node(*e1,*e2))  /// @note OK , here we have 2 edges with 1 common node.
                {

                    uint cn = find_common_node(*e1,*e2); /// @note here is the common node.
                    uint o1 = cn == e1->node_end ? e1->node_begin : e1->node_end;
                    uint o2 = cn == e2->node_end ? e2->node_begin : e2->node_end;

                    //std::cout << cn << " " << o1 << " " << o2 << " - " << std::flush;
                    //edge(e1->block,o1,o2).print_edge(); std::cout << std::endl;

                    /// @note try with wedges ?
                    for (std::vector<edge>::iterator e3 = edges[o1].begin(); e3 != edges[o1].end(); ++e3)
                    {
                        uint o3 = o1 == e3->node_begin ? e3->node_end : e3->node_begin;
                        //std::cout << cn << " " << o1 << " " << o2 << " " << o3 << " - " << std::flush;
                        if (find_edge(edges[o2],edge(e1->block,o1,o3)))
                        {
                            v_tri.emplace_back(Triangle(cn,o1,o2));
                            v_tri.emplace_back(Triangle(o3,o1,o2));
                        }
                        //else if (o3 == cn && abs(o2-o1)<=ppl+1)
                        //    v_tri.emplace_back(Triangle(cn,o1,o2));
                    }

                }
            }
        }
    }

    std::vector<Triangle> tri_final;

    std::sort( v_tri.begin(), v_tri.end() );
    v_tri.erase( unique( v_tri.begin(), v_tri.end() ), v_tri.end() );

    for (std::vector<Triangle>::iterator it = v_tri.begin(); it != v_tri.end(); ++it)
        if (it->i != it->j && it->i != it->k && it->j != it->k)
            tri_final.emplace_back(*it);

    return tri_final;

}



void remove_edge (std::map<uint,std::vector<edge> > edges, edge e)
{
    edges[e.node_begin].erase(std::find(edges[e.node_begin].begin(),edges[e.node_begin].end(),edge(e.node_begin,e.node_end)));
    edges[e.node_end  ].erase(std::find(edges[e.node_end  ].begin(),edges[e.node_end  ].end(),edge(e.node_end,e.node_begin)));
}



/// @brief Find triangles in the reconstruction and add them to a vector of triangles used to write .ply file
///
/// @todo Check for triangles over different blocks
std::tuple<std::vector<std::vector<Triangle> >,std::vector<std::tuple<uint,uint,uint> >,std::vector<std::vector<uint> > > compute_mesh_and_segments(std::vector<std::map<uint,std::vector<edge> > > data,XMls& mls)
{

    ///
    /// \brief v_tri is the returned mesh
    ///
    /// \brief edges is a map linking to each node its corresponding edges
    /// i.e. edges starting or ending by this node
    ///

    std::vector<std::vector<Triangle> > v_tri;
    std::vector<std::vector<uint> >rem_points;

    std::cout << data.size() << std::endl;

    // add edges to vector of pairs
    // then remove edges when they are in a triangle
    // 11/10/2017

    std::vector<std::map<uint,std::vector<edge> > > data1;
    std::vector<std::tuple<uint,uint,uint> > edges_not_in_triangles;
    std::vector<std::tuple<uint,uint,uint> > edges_not_in_triangles_temp;

    for (std::vector<std::map<uint,std::vector<edge> > >::const_iterator bit = data.cbegin(); bit != data.cend(); ++bit)
    {
        std::map<uint,std::vector<edge> > edges = *bit;
        std::map<uint,std::vector<edge> > edges1;
        std::vector<Triangle> v_tri_temp;

        // browse point cloud
        for (std::map<uint,std::vector<edge> >::iterator it = edges.begin(); it!=edges.end(); ++it)
        {
            //std::sort(it->second.begin(),it->second.end());
            std::sort( it->second.begin(), it->second.end() );
            it->second.erase( unique( it->second.begin(), it->second.end() ), it->second.end() );
            // for each point, browse its edges
            for (std::vector<edge>::iterator e1 = it->second.begin(); e1 != it->second.end(); ++e1)
            {
                uint node2 = e1->node_end;

                std::sort(edges.at(node2).begin(),edges.at(node2).end());

                // 2 edges with a common node: it->first
                for (std::vector<edge>::iterator e2 = e1.operator +(1); e2 != it->second.end(); ++e2)
                {

                    if (*e1 != *e2 && have_common_node(*e1,*e2))  /// @note OK , here we have 2 edges with 1 common node.
                    {

                        uint cn = find_common_node(*e1,*e2); /// @note here is the common node.
                        uint o1 = cn == e1->node_end ? e1->node_begin : e1->node_end;
                        uint o2 = cn == e2->node_end ? e2->node_begin : e2->node_end;

                        //std::cout << cn << " " << o1 << " " << o2 << " - " << std::flush;
                        //edge(e1->block,o1,o2).print_edge(); std::cout << std::endl;

                        /// @note try with wedges ?
                        for (std::vector<edge>::iterator e3 = edges[o1].begin(); e3 != edges[o1].end(); ++e3)
                        {
                            uint o3 = o1 == e3->node_begin ? e3->node_end : e3->node_begin;
                            //std::cout << cn << " " << o1 << " " << o2 << " " << o3 << " - " << std::flush;
                            if (find_edge(edges[o2],edge(e1->block,o1,o3)))
                            {
                                v_tri_temp.emplace_back(Triangle(cn,o1,o2));
                                v_tri_temp.emplace_back(Triangle(o3,o1,o2));

                                // 5 edges in 2 triangles
                                // cn o1
                                edges1[cn].emplace_back(edge(e1->block,cn,o1));
                                // cn o2
                                edges1[cn].emplace_back(edge(e1->block,cn,o2));
                                // o1 o2
                                edges1[cn].emplace_back(edge(e1->block,o1,o2));
                                // o1 o3
                                edges1[cn].emplace_back(edge(e1->block,o1,o3));
                                // o2 o3
                                edges1[cn].emplace_back(edge(e1->block,o2,o3));
                            }
                            //else if (o3 == cn && abs(o2-o1)<=ppl+1)
                            //    v_tri.emplace_back(Triangle(cn,o1,o2));
                        }

                    }
                }
            }
        }
        v_tri.emplace_back(v_tri_temp);
        data1.emplace_back(edges1);
    }

    std::vector<std::vector<Triangle> > tri_final_temp;
    std::vector<std::vector<Triangle> > tri_final;

    int blk = 0;

    // unicity of each triangle ...
    for (std::vector<std::vector<Triangle> >::iterator vit = v_tri.begin(); vit != v_tri.end(); ++vit, ++blk)
    {
        std::vector<Triangle> v_tri_temp = *vit;
        std::sort( v_tri_temp.begin(), v_tri_temp.end() );
        v_tri_temp.erase( unique( v_tri_temp.begin(), v_tri_temp.end() ), v_tri_temp.end() );
        tri_final_temp.emplace_back(v_tri_temp);
    }

    // validity of each triangle
    int block = 0;
    for (std::vector<std::vector<Triangle> >::iterator vit = tri_final_temp.begin(); vit != tri_final_temp.end(); ++vit, ++block)
    {
        std::vector<Triangle> v_tri_temp;
        std::vector<uint> rem_points_temp;
        for (std::vector<Triangle>::iterator it = vit->begin(); it != vit->end(); ++it)
            if (it->i != it->j && it->i != it->k && it->j != it->k)
            {
                v_tri_temp.emplace_back(*it);
                rem_points_temp.emplace_back(it->i);
                rem_points_temp.emplace_back(it->j);
                rem_points_temp.emplace_back(it->k);
            }
        tri_final.emplace_back(v_tri_temp);
        rem_points.emplace_back(rem_points_temp);
    }

    // find edge that are not in any triangle
    std::vector<std::map<uint,std::vector<edge> > >::iterator  bit1 = data1.begin();
    for (std::vector<std::map<uint,std::vector<edge> > >::const_iterator bit = data.cbegin();bit != data.cend(); ++bit, ++bit1)
    {
        std::map<uint,std::vector<edge> > edges = *bit;
        std::map<uint,std::vector<edge> > edges1 = *bit1;
        for (std::map<uint,std::vector<edge> >::iterator it = edges.begin(); it!=edges.end(); ++it)
            for (std::vector<edge>::iterator e1 = it->second.begin(); e1 != it->second.end(); ++e1)
                if (    !find_edge(edges1[e1->node_begin],*e1) &&
                        !find_edge(edges1[e1->node_end],*e1) &&
                        !find_edge(edges1[e1->node_begin],edge(e1->node_end,e1->node_begin)) &&
                        !find_edge(edges1[e1->node_end],edge(e1->node_end,e1->node_begin) ))
                {
                    //e1->print_edge();
                    edges_not_in_triangles.emplace_back(std::make_tuple(e1->block,e1->node_begin,e1->node_end));
                    rem_points[e1->block].emplace_back((uint)e1->node_begin);
                    rem_points[e1->block].emplace_back((uint)e1->node_end);
                }
    }


    for (std::vector<std::vector<uint> >::iterator it = rem_points.begin(); it != rem_points.end(); ++it)
    {
        std::sort(it->begin(),it->end());
        it->erase( unique( it->begin(), it->end() ), it->end() );
    }

    return std::make_tuple(tri_final,edges_not_in_triangles,rem_points);

}


bool triangle_comparison(Triangle t1, Triangle t2) {return  (t1.i == t2.i || t1.i == t2.j || t1.i == t2.k) &&
                                                            (t1.j == t2.i || t1.j == t2.j || t1.j == t2.k) &&
                                                            (t1.k == t2.i || t1.k == t2.j || t1.k == t2.k) ? true : false;}

/// \brief Referenced version
/// @brief Find triangles in the reconstruction and add them to a vector of triangles used to write .ply file
///
/// @todo Check for triangles over different blocks
std::tuple<std::vector<std::vector<Triangle> > *, std::vector<std::tuple<uint, uint, uint> > *, std::vector<std::vector<uint> > *> *compute_mesh_and_segments_fast(std::vector<std::map<uint,std::vector<edge> >* >* data,XMls& mls)
{

    ///
    /// \brief v_tri is the returned mesh
    ///
    /// \brief edges is a map linking to each node its corresponding edges
    /// i.e. edges starting or ending by this node
    ///

    std::vector<std::vector<Triangle> >* v_tri = new std::vector<std::vector<Triangle> >;
    std::vector<std::vector<uint> >*rem_points = new std::vector<std::vector<uint> >;

    std::cout << data->size() << std::endl;

    // add edges to vector of pairs
    // then remove edges when they are in a triangle
    // 11/10/2017

    std::vector<std::map<uint,std::vector<edge> > >* data1 = new std::vector<std::map<uint,std::vector<edge> > >;
    std::vector<std::tuple<uint,uint,uint> >* edges_not_in_triangles = new std::vector<std::tuple<uint,uint,uint> >;

    for (std::vector<std::map<uint,std::vector<edge> >* >::iterator bit = data->begin(); bit != data->end(); ++bit)
    {
        std::map<uint,std::vector<edge> >* edges = *bit;
        std::map<uint,std::vector<edge> >* edges1 = new std::map<uint,std::vector<edge> >;
        std::vector<Triangle>* v_tri_temp = new std::vector<Triangle>;

        // browse point cloud
        for (std::map<uint,std::vector<edge> >::iterator it = edges->begin(); it!=edges->end(); ++it)
        {
            //std::sort(it->second.begin(),it->second.end());
            std::sort( it->second.begin(), it->second.end() );
            it->second.erase( unique( it->second.begin(), it->second.end() ), it->second.end() );
            // for each point, browse its edges
            for (std::vector<edge>::iterator e1 = it->second.begin(); e1 != it->second.end(); ++e1)
            {
                uint node2 = e1->node_end;

                std::sort(edges->at(node2).begin(),edges->at(node2).end());

                // 2 edges with a common node: it->first
                for (std::vector<edge>::iterator e2 = e1.operator +(1); e2 != it->second.end(); ++e2)
                {

                    if (*e1 != *e2 && have_common_node(*e1,*e2))  /// @note OK , here we have 2 edges with 1 common node.
                    {

                        uint cn = find_common_node(*e1,*e2); /// @note here is the common node.
                        uint o1 = cn == e1->node_end ? e1->node_begin : e1->node_end;
                        uint o2 = cn == e2->node_end ? e2->node_begin : e2->node_end;

                        //std::cout << cn << " " << o1 << " " << o2 << " - " << std::flush;
                        //edge(e1->block,o1,o2).print_edge(); std::cout << std::endl;

                        /// @note try with wedges ?
                        for (std::vector<edge>::iterator e3 = edges->operator [](o1).begin(); e3 != edges->operator [](o1).end(); ++e3)
                        {
                            uint o3 = o1 == e3->node_begin ? e3->node_end : e3->node_begin;
                            //std::cout << cn << " " << o1 << " " << o2 << " " << o3 << " - " << std::flush;
                            if (find_edge(edges->operator [](o2),edge(e1->block,o1,o3)))
                            {
                                std::vector<unsigned int> v1;
                                std::vector<unsigned int> v2;

                                v1.emplace_back(cn);
                                v1.emplace_back(o1);
                                v1.emplace_back(o2);
                                v2.emplace_back(o1);
                                v2.emplace_back(o2);
                                v2.emplace_back(o3);

                                std::sort (v1.begin(), v1.end());
                                std::sort (v2.begin(), v2.end());

                                //Triangle(v1[0],v1[1],v1[2]).print();

                                v_tri_temp->emplace_back(Triangle(v1[0],v1[1],v1[2]));

                                //v_tri_temp->back().print();

                                v_tri_temp->emplace_back(Triangle(v2[0],v2[1],v2[2]));

                                //v_tri_temp->emplace_back(Triangle(cn,o1,o2));
                                //v_tri_temp->emplace_back(Triangle(o3,o1,o2));

                                // 5 edges in 2 triangles
                                // cn o1
                                edges1->operator [](cn).emplace_back(edge(e1->block,cn,o1));
                                // cn o2
                                edges1->operator [](cn).emplace_back(edge(e1->block,cn,o2));
                                // o1 o2
                                edges1->operator [](cn).emplace_back(edge(e1->block,o1,o2));
                                // o1 o3
                                edges1->operator [](cn).emplace_back(edge(e1->block,o1,o3));
                                // o2 o3
                                edges1->operator [](cn).emplace_back(edge(e1->block,o2,o3));
                            }
                            //else if (o3 == cn && abs(o2-o1)<=ppl+1)
                            //    v_tri.emplace_back(Triangle(cn,o1,o2));
                        }

                    }
                }
            }
        }
        //std::cout << v_tri_temp->size() << " triangles" << std::endl;
        v_tri->emplace_back(*v_tri_temp);
        //for (std::vector<std::vector<Triangle> >::iterator vit = v_tri->begin(); vit != v_tri->end(); ++vit)
        //    for (std::vector<Triangle>::iterator it = vit->begin(); it != vit->end(); ++it)
        //        it->print();
        data1->emplace_back(*edges1);
    }

    std::vector<std::vector<Triangle> >* tri_final_temp = new std::vector<std::vector<Triangle> >;
    std::vector<std::vector<Triangle> >* tri_final = new std::vector<std::vector<Triangle> >;

    int blk = 0;

    // unicity of each triangle ...
    for (std::vector<std::vector<Triangle> >::iterator vit = v_tri->begin(); vit != v_tri->end(); ++vit, ++blk)
    {
        std::vector<Triangle>* v_tri_temp = &*vit;
        //std::cout << v_tri_temp->size() << " ";
        std::sort( v_tri_temp->begin(), v_tri_temp->end() );
        v_tri_temp->erase( unique( v_tri_temp->begin(), v_tri_temp->end() ), v_tri_temp->end() );
        //std::cout << v_tri_temp->size() << std::endl;
        tri_final_temp->emplace_back(*v_tri_temp);
        //for (std::vector<std::vector<Triangle> >::iterator vit = tri_final_temp->begin(); vit != tri_final_temp->end(); ++vit)
        //    for (std::vector<Triangle>::iterator it = vit->begin(); it != vit->end(); ++it)
        //        it->print();
    }

    // validity of each triangle
    int block = 0;
    for (std::vector<std::vector<Triangle> >::iterator vit = tri_final_temp->begin(); vit != tri_final_temp->end(); ++vit, ++block)
    {
        std::vector<Triangle>* v_tri_temp = new std::vector<Triangle>;
        std::vector<uint>* rem_points_temp = new std::vector<uint>;
        for (std::vector<Triangle>::iterator it = vit->begin(); it != vit->end(); ++it)
        {
            //it->print();
            if (it->i != it->j && it->i != it->k && it->j != it->k)
            {
                v_tri_temp->emplace_back(*it);
                rem_points_temp->emplace_back(it->i);
                rem_points_temp->emplace_back(it->j);
                rem_points_temp->emplace_back(it->k);
            }
        }
        std::cout << "added " << v_tri_temp->size() << " triangles" << std::endl;
        tri_final->emplace_back(*v_tri_temp);
        std::cout << tri_final->size() << " triangles" << std::endl;
        rem_points->emplace_back(*rem_points_temp);
    }

    // find edge that are not in any triangle
    std::vector<std::map<uint,std::vector<edge> > >::iterator  bit1 = data1->begin();
    for (std::vector<std::map<uint,std::vector<edge> >* >::const_iterator bit = data->cbegin();bit != data->cend(); ++bit, ++bit1)
    {
        std::map<uint,std::vector<edge> >* edges = *bit;
        std::map<uint,std::vector<edge> > edges1 = *bit1;
        for (std::map<uint,std::vector<edge> >::iterator it = edges->begin(); it!=edges->end(); ++it)
            for (std::vector<edge>::iterator e1 = it->second.begin(); e1 != it->second.end(); ++e1)
                if (    !find_edge(edges1[e1->node_begin],*e1) &&
                        !find_edge(edges1[e1->node_end],*e1) &&
                        !find_edge(edges1[e1->node_begin],edge(e1->node_end,e1->node_begin)) &&
                        !find_edge(edges1[e1->node_end],edge(e1->node_end,e1->node_begin) ))
                {
                    //e1->print_edge();
                    edges_not_in_triangles->emplace_back(std::make_tuple(e1->block,e1->node_begin,e1->node_end));
                    rem_points->operator [](e1->block).emplace_back((uint)e1->node_begin);
                    rem_points->operator [](e1->block).emplace_back((uint)e1->node_end);
                }
    }


    for (std::vector<std::vector<uint> >::iterator it = rem_points->begin(); it != rem_points->end(); ++it)
    {
        std::sort(it->begin(),it->end());
        it->erase( unique( it->begin(), it->end() ), it->end() );
    }

    std::tuple<std::vector<std::vector<Triangle> > *, std::vector<std::tuple<uint, uint, uint> > *, std::vector<std::vector<uint> > *> * out = new std::tuple<std::vector<std::vector<Triangle> > *, std::vector<std::tuple<uint, uint, uint> > *, std::vector<std::vector<uint> > *>;
    *out =  std::make_tuple(&*tri_final,&*edges_not_in_triangles,&*rem_points);

    std::cout << (std::get<0>(*out))->data()->size() << " triangles" << std::endl;
    std::cout << edges_not_in_triangles->size() << " edges" << std::endl;
    std::cout << rem_points->data()->size() << " points used" << std::endl;

    return out;

}









/// Find triangles in the reconstruction and add them to a vector of triangles used to write .ply file
///
/// !!!!! TO COMMENT !!!!!
std::vector<Triangle> compute_mesh(std::map<uint,std::vector<edge> > edges, XMls &mls, vector< vector<int> > vv_pulse_with_echo_idx, param params)
{

    std::vector<Triangle> v_tri;
    int PPL = mls.PulsePerLine();

    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        //std::cout << "block=" << block_idx << std::endl;
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<n_pulse-PPL-1; pulse_idx++)
        {
            if (    mls.NbOfEcho(block_idx,pulse_idx) > 0 &&
                    mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+1)) > 0 &&
                    mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+PPL)) > 0 &&
                    mls.NbOfEcho(block_idx,XPulseIndex(pulse_idx+PPL+1)) > 0)
            for (XEchoIndex e1 = mls.IdxFirstEcho(block_idx,pulse_idx); e1 != mls.IdxLastEcho(block_idx,pulse_idx); ++e1)
                for (XEchoIndex e2 = mls.IdxFirstEcho(block_idx,pulse_idx+1); e2 != mls.IdxLastEcho(block_idx,pulse_idx+1); ++e2)
                    for (XEchoIndex e3 = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); e3 != mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++e3)
                        for (XEchoIndex e4 = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); e4 != mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++e4)
                            if (    edges.find(e1) != edges.end() &&
                                    edges.find(e2) != edges.end() &&
                                    edges.find(e3) != edges.end() &&
                                    edges.find(e4) != edges.end())
                                MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, 100,block_idx, e1, e4, e3, e2);
        }
        mls.Free(block_idx);
    }

    return v_tri;

}

/// Create graph for MaxFlow optimisation
/// cf. Boykov & Kolmogorov software
/// "An Experimental Comparison of Min-Cut/Max-Flow Algorithms for Energy Minimization in Vision."
/// Yuri Boykov and Vladimir Kolmogorov.
/// In IEEE Transactions on Pattern Analysis and Machine Intelligence (PAMI),
/// September 2004
///
///
///
/// \todo : comment when it will be (more) stable
/// @warning deprecated
/// @warning not anymore
/*std::vector<std::pair<uint,uint> >*/ std::pair<std::vector<edge>,std::vector<edge> > create_graph(std::pair<std::vector<edge>,std::vector<double> > pairN,
                                                std::pair<std::vector<std::pair<edge,edge> >,std::vector<double> > pairE,
                                                XMls & mls)
{

    std::vector<edge> Nodes = pairN.first;
    std::vector<double> c0 = pairN.second;
    std::vector<std::pair<edge,edge> > edges = pairE.first;
    std::vector<double> c1 = pairE.second;
    //std::vector<std::pair<uint,uint> > graph_result;
    std::pair<std::vector<edge>,std::vector<edge> > graph_result;
    std::map<edge_map,int,order_edge_map> node_numbers;

    typedef Graph<double,double,double> GraphType;
        GraphType *g = new GraphType(/*estimated # of nodes*/ mls.NTotalEcho(), /*estimated # of edges*/ Nodes.size());

        // connect first scan line to source and last scan line to sink

        // Node = edge between 2 neighbourhing echos
        //int PPL = mls.PulsePerLine();

        g->add_node(Nodes.size());

        /**           source
         *            /    \
         *    c0(n0) /      \  c0(n1)
         *          /        \
         *        n0          |
         *         | \        |
         *         | c1(n0,n1)|
         *         |        \ |
         *         |          n1
         *          \        /
         *  1-c0(n0) \      / 1-c0(n1)
         *            \    /
         *             sink
         */

        int i=0;
        for (std::vector<edge>::iterator it = Nodes.begin(); it != Nodes.end(); ++it, ++i)
        {
            node_numbers[edge_map(*it)] = i;
            g->add_tweights(i,c0[i],1-c0[i]);
        }
        int c1_it = 0;
        int n_edges = 0;
        for (std::vector<std::pair<edge,edge> >::iterator it = edges.begin(); it != edges.end(); ++it)
        {
            int n1 = node_numbers[edge_map((*it).first)];
            int n2 = node_numbers[edge_map((*it).second)];
            g->add_edge(n1,n2,c1[c1_it],1-c1[c1_it]);
            ++n_edges;
        }

        std::cout << n_edges << std::endl;

        // Edge = 2 Graph Nodes (i.e. 2 edges linking each 2 neighbourhing echos) having a common echo
        //g -> add_tweights( 0,   /* capacities */  1, 5 );
        //g -> add_tweights( 1,   /* capacities */  2, 6 );
        //g -> add_edge( 0, 1,    /* capacities */  3, 4 );

        double flow = g -> maxflow();

        printf("Flow = %f\n", flow);
        printf("Minimum cut:\n");

        /// where is each node
        /*for (int i = 0; i < g->get_node_num(); i++)
        {
            if (g->what_segment(i) == GraphType::SOURCE)
                std::cout << "node " << i << " is in the SOURCE set\n";
            else
                std::cout << "node " << i << " is in the SINK set\n";
        }*/
        i=0;


        /// just trying, that won't work
        //int source_node = 0;
        //int sink_node = 0;
        std::vector<edge> src_node;
        std::vector<edge> snk_node;
        /*for (std::vector<edge>::iterator it = Nodes.begin(); it != Nodes.end(); ++it,++i)
            g->what_segment(i) == GraphType::SOURCE ? src_node.emplace_back(*it) : snk_node.emplace_back(*it);

        graph_result.first = src_node;
        graph_result.second = snk_node;*/

        i=0;

        for (std::vector<edge>::iterator it = Nodes.begin(); it != Nodes.end(); ++it, ++i)
        {
            if ((  g->what_segment((**it).node_begin) == GraphType::SINK &&
                   g->what_segment((**it).node_end)   == GraphType::SINK) )
                snk_node.emplace_back(*it);
            //{
                //std::pair<uint,uint> p;
                //p.first = (**it).node_begin;
                //p.second = (**it).node_end;
                //graph_result.emplace_back(p);
                //std::cout << c0[i] << " " << std::flush;
            //}
            else //(( g->what_segment((**it).node_begin) == GraphType::SOURCE &&
                 //   g->what_segment((**it).node_end)   == GraphType::SOURCE) )
                src_node.emplace_back(*it);
        }

        graph_result.first = src_node;
        graph_result.second = snk_node;

        /*if (g->what_segment(0) == GraphType::SOURCE)
                printf("node0 is in the SOURCE set\n");
        else
                printf("node0 is in the SINK set\n");
        if (g->what_segment(1000000) == GraphType::SOURCE)
                printf("node1 is in the SOURCE set\n");
        else
                printf("node1 is in the SINK set\n");*/

        delete g;

        return graph_result;
}



/// Better Maxflow method
///
/// @param pairN: pair containing the nodes of the graph (neighbourhing echos) and their c0 associated value
/// @param pairE: vector containing for each echo a pair of its adjacent edges and their respective c1 value
/// @param mls
///
/// @return pair containing a vector of nodes in the source and nodes in the sink
std::pair<std::vector<edge>, std::vector<edge> > create_graph(  std::pair<std::vector<edge>, std::vector<double> > pairN,
                                                                std::vector<std::map<int,std::vector<std::pair<edge,double> > > > pairE,
                                                                XMls &mls)
{

    std::vector<edge> Nodes = pairN.first;                                              // Nodes of the graph
    std::vector<double> c0 = pairN.second;                                              // c0 regularity
    std::vector<std::map<int,std::vector<std::pair<edge,double> > > > edges = pairE;    // edges.first = edges of the graph &&
                                                                                        // edges.second = c1 regularity
    std::pair<std::vector<edge>,std::vector<edge> > graph_result;                       // return pair containing source nodes && sink nodes
    std::map<edge_map,int,order_edge_map> node_numbers;                                 // Correspondancy between edges and nodes of the graph

    typedef Graph<double,double,double> GraphType;                                      // Initialize graph
    GraphType *g = new GraphType(Nodes.size(), edges.size());

    g->add_node(Nodes.size());

    int i=0;
    //double max_c0 = 0;
    //for (std::vector<double>::iterator it = c0.begin(); it != c0.end(); ++it)
    //    if (*it > max_c0) max_c0 = *it;
    for (std::vector<edge>::iterator it = Nodes.begin(); it != Nodes.end(); ++it, ++i)  // loop for adding nodes
    {
        node_numbers[edge_map(*it)] = i;                                                // Fill correspondancy map
        g->add_tweights(i,c0[i],1-c0[i]);                                               // Add nodes
    }

    for (XBlockIndex blkidx = 0; blkidx != mls.NBlock(); ++blkidx)                      // loop for adding edges
    {                                                                                   // for each block
        std::cout << "block= " << blkidx << std::endl;
        std::map<int,std::vector<std::pair<edge,double> > > map = edges[blkidx];        // select edges and c1 regularity for this block
        for (std::map<int,std::vector<std::pair<edge,double> > >::iterator it = map.begin(); it != map.end(); ++it)             // for each echo in the block
            for (std::vector<std::pair<edge,double> >::iterator eit = it->second.begin(); eit != it->second.end(); ++eit)       // double browse of all edges
                for (std::vector<std::pair<edge,double> >::iterator eit2 = eit.operator +(1); eit2 != it->second.end(); ++eit2) // with given echo
                    g->add_edge(node_numbers.at(eit->first),
                                node_numbers.at(eit2->first),
                                0.1*((double)1-eit2->second),
                                0.1*((double)eit2->second));                            // add edges
    }

    double flow = g->maxflow();                                                         // solve maxflow

    printf("Flow = %f\n", flow);
    printf("Minimum cut:\n");

    std::vector<edge> src_node;
    std::vector<edge> snk_node;

    for (std::vector<edge>::iterator it = Nodes.begin(); it != Nodes.end(); ++it, ++i)
        g->what_segment(node_numbers.at(*it)) == GraphType::SINK ?                      // find if each node is in source or sink set
            snk_node.emplace_back(*it):src_node.emplace_back(*it);

    std::cout << src_node.size() << " " << snk_node.size() << std::endl;

    graph_result.first = src_node;
    graph_result.second = snk_node;

    delete g;

    return graph_result;

}




bool find_echo(std::vector<std::pair<uint, uint> > maxflow, XPulseIndex pulse)
{
    //for (std::vector<std::pair<uint, uint> >::iterator it = maxflow.begin(); it != maxflow.end(); ++it)
    //    if ((*it).first == (int)pulse || (*it).second == (int)pulse)
    //        return true;
    return false;
}


XPt3D e2p (edge e, XMls& mls)
{
    XPt3D p;
    XPt3D x = mls.Pworld(0,e.node_end);
    XPt3D y = mls.Pworld(0,e.node_begin);
    p.X = x.X - y.X;
    p.Y = x.Y - y.Y;
    p.Z = x.Z - y.Z;
    return p;
}


double angle(XPt3D v1, XPt3D v2)
{
    double scalar_prod = v1.X*v2.X + v1.Y*v2.Y + v1.Z*v2.Z;
    double nv1 = sqrt(v1.X*2 + v1.Y*2 + v1.Z*2);
    double nv2 = sqrt(v2.X*2 + v2.Y*2 + v2.Z*2);
    return acos(scalar_prod/(nv1*nv2));
}

std::tuple<uint, uint, uint> e2t(edge e)
{
    return std::make_tuple(e.block,e.node_begin,e.node_end);
}

edge t2e(std::tuple<uint, uint, uint> t)
{
    return edge(std::get<0>(t),std::get<1>(t),std::get<2>(t));
}

} // Namespace

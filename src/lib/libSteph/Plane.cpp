#include "libSteph/Plane.h"
#include "libSteph/Wedge.h"


//#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
//#include <eigen3/Eigen/Eigenvalues>

using namespace Eigen;

namespace SCR{

/// \brief default constructor (initializes everything to 0
bool Plane::is_valid()
{
    return (x!=(double)0 || y!=(double)0 || z!=(double)0) ? true : false;
}

Plane::Plane()
{
    x=y=z=d=0;
}

/// \brief constructor taking the 4 parameters of a plane as arguments
///
/// \param _x: 1st coordinate of normal vector
/// \param _y: 2nd coordinate of normal vector
/// \param _z: 3rd coordinate of normal vector
/// \param _d: distance
Plane::Plane(double _x, double _y, double _z, double _d)
{
    x = _x;
    y = _y;
    z = _z;
    d = _d;
}

/// \brief constructor finding plane parameters from 3 points
///
/// \param p_1: 1st 3D point
/// \param p_2: 2nd 3D point
/// \param p_3: 3rd 3D point
Plane::Plane(XPt3D p_1, XPt3D p_2, XPt3D p_3)
{
    XPt3D v1 = p_1-p_2;
    XPt3D v2 = p_3-p_2;
    v1.Normalise();
    v2.Normalise();
    XPt3D N = cross_product(v1,v2);
    N.Normalise();
    x = N.X;
    y = N.Y;
    z = N.Z;
    d = -p_1.X*N.X-p_1.Y*N.Y-p_1.Z*N.Z;
}

/// \brief standard destructor
Plane::~Plane()
{

}

/// \brief constructor from vector of 3D points
/// this constructor computes a SVD to find the plane best approximating the point set
/// the SVD is computed with eigen
///
/// \note the normal vector of the plane is the left singular vector associated to the least singular value
///
/// \param vp: vector of 3D points
Plane::Plane(vP3* vp)
{
    //std::cout << vp->size() << " points" << std::endl;

    if (vp->size() >= 3)
    {

        MatrixXd M(3,vp->size());

        XPt3D centroid = find_centroid_set_of_points(vp);
        //std::cout << centroid.X << " " << centroid.Y << " " << centroid.Z << std::endl;

        int i = 0;
        // toy example
        /*MatrixXd M(3,3);
        M(0,0) = 1.5;     M(0,1) = 4;     M(0,2) = -3;
        M(1,0) = 1.5;     M(1,1) = 4.5;     M(1,2) = -3;
        M(2,0) = 2;     M(2,1) = 4.5;     M(2,2) = -2.5;*/
        for (vP3::iterator p = vp->begin(); p != vp->end(); ++p)
        {
            p->operator -=(centroid);
            //std::cout << p->X << " " << p->Y << " " << p->Z << " - " << std::flush;
            M(0,i) = p->X;
            M(1,i) = p->Y;
            M(2,i) = p->Z;
            ++i;
        }
        //cout << "Here is the matrix m:" << endl << M << endl;
        JacobiSVD<MatrixXd> svd(M, ComputeThinU | ComputeThinV);
        //std::cout << "Its left singular vectors are the columns of the thin U matrix:" << endl << svd.matrixU() << std::endl;
        //std::cout << svd.matrixU() << std::endl;
        //const Eigen::MatrixXd &U = svd.matrixU();
        double least_sv = 1e6;
        int least_sv_pos = 0;
        MatrixXd sv = svd.singularValues();
        for (int i = 0; i != sv.size(); i++)
        {
            //std::cout << sv(i) << " " << std::flush;
            if (sv(i) < least_sv)
            {
                least_sv = sv(i);
                least_sv_pos = i;
            }
        }
        //std::cout << std::endl;
        //std::cout << least_sv << " " << std::flush;
        XPt3D center (svd.matrixU()(0,least_sv_pos),svd.matrixU()(1,least_sv_pos),svd.matrixU()(2,least_sv_pos));   // inverting x and least_sv_pos - check if working ?
        //center.Normalise();
        x = center.X;
        y = center.Y;
        z = center.Z;
        d = -centroid.X*x-centroid.Y*y-centroid.Z*z;

        //if (x == 0 || y == 0 || z == 0)
        //if (vp->size() > 10000)
        //print();

    }
    else
    {
        //std::cout << "Plane with only " << vp->size() << " points" << std::endl;
        //std::cout << "Vector of points did not contain at least 3 points\nI can't coplute a plane." << std::endl;
        x=y=z=d=0;
    }

}

/// \brief quick method for printing plane parameters
void Plane::print()
{
    std::cout << "Plane parameters: \n\tx = " << x << "\n\ty = " << y << "\n\tz = " << z << "\n\td = " << d << std::endl;
}

/// \brief small routine for the computation of points belonging to a plane
/// this is used to store planes
///
/// \return vector of 3D points
///
/// \note Let \f$ \mathcal{P} \begin{bmatrix} x \\ y \\ z \\ d \end{bmatrix} \f$ be a plane.
/// the points computed are the ones solving plane's equation when to dimensions of the plane are null
/// this means that our three points solve the system:
/// \f[
/// \left\{
/// \begin{aligned}
/// a*x + 0*y + 0*z + d &= 0 \\
/// 0*x + b*y + 0*z + d &= 0 \\
/// 0*x + 0*y + c*z + d &= 0 \\
/// \end{aligned}
/// \right.
/// \Rightarrow
/// \left\{
/// \begin{aligned}
/// a &= -\frac{d}{x} \\
/// b &= -\frac{d}{y} \\
/// c &= -\frac{d}{z} \\
/// \end{aligned}
/// \right.
/// \f]
///
/// Thus we have
/// \f$
/// P1 = \begin{bmatrix} -\frac{d}{x} \\ 0 \\ 0 \end{bmatrix}
/// \f$,
/// \f$
/// P2 = \begin{bmatrix} 0 \\ -\frac{d}{y} \\ 0 \end{bmatrix}
/// \f$ and
/// \f$
/// P3 = \begin{bmatrix} 0 \\ 0 \\ -\frac{d}{z} \end{bmatrix}
/// \f$.
vP3 Plane::compute_3_points()
{
    if (this->is_valid())
    {
        vP3 vp {XPt3D(-d/x,0,0), XPt3D(0,-d/y,0), XPt3D(0,0,-d/z)};
        //std::cout << -d/x << " " << -d/y << " " << -d/z << std::endl;
        return vp;
    }
    vP3 vp;
    for (int i = 0; i != 3; i++)
        vp.emplace_back(0,0,0);
    return vp;
}

vP3 Plane::compute_orthonormal_base()
{
    vP3 base;
    XPt3D v1(y/x,-1,0);
    XPt3D v2(z/x,0,-1);

    v1.Normalise();
    v2 = v2.operator -=(v1.operator *=(abs(v1.X*v2.X + v1.Y*v2.Y + v1.Z*v2.Z)));
    v2.Normalise();

    base.emplace_back(v1);
    base.emplace_back(v2);

    return base;
}

XPt3D Plane::find_centroid_set_of_points(vP3* vp)
{
    XPt3D c;
    for (vP3::iterator p = vp->begin(); p != vp->end(); ++p)
        c.operator +=(*p);
    c.operator /=(vp->size());
    return c;
}

XPt3D Plane::project_point_plane(XPt3D p)
{
    //this->print();
    //std::cout << p.X << " " << p.Y << " " << p.Z << std::endl;
    if ( this->is_valid() )
    {
        XPt3D n(x,y,z);
        double prod = x*p.X + y*p.Y + z*p.Z + d;
        XPt3D projete = p.operator -=(n.operator *=(prod));
        //std::cout << p.X << " " << p.Y << " " << p.Z << " - " << projete.X << " " << projete.Y << " " << projete.Z << std::endl;
        return projete;
    }
    //else print();
    return XPt3D(~0,~0,~0);
}

std::vector<double> Plane::project_vector_plane(XPt3D p)
{
    //this->print();

    if ( this->is_valid() )
    {
        XPt3D n(x,y,z);
        double prod = x*p.X + y*p.Y + z*p.Z + d;
        XPt3D projete_p = p.operator -=(n.operator *=(prod));
        std::vector<double> projete;
        projete.push_back(   projete_p.X    );
        projete.push_back(   projete_p.Y    );
        projete.push_back(   projete_p.Z    );

        //std::cout << p.X  - projete[0] << " " << p.Y - projete[1] << " " << p.Z - projete[2] << std::endl;

        return projete;
    }
    //else print();
    std::vector<double> pp;
    pp.push_back(p.X);
    pp.push_back(p.Y);
    pp.push_back(p.Z);
    return pp;
}

/// \brief computes quadratic distance from point to plane
///
/// \details
/// the distance is formulated as following:
/// \f$ d(p,P) = {\frac{abs(p_x \cdot x + p_y \cdot y + p_z \cdot z + d)}{sqrt{x^2 + y^2 + z^2}}}^2
///
/// \return double: quadratic distance between point and plane
double Plane::quadratic_distance_point_plane(XPt3D p) const
{
    return std::pow((abs(p.X*x + p.Y*y + p.Z*z + d) / sqrt(x*x + y*y + z*z)),2);
}

/// \brief computes distance from point to plane
///
/// \details
/// the distance is formulated as following:
/// \f$ d(p,P) = \frac{abs(p_x \cdot x + p_y \cdot y + p_z \cdot z + d)}{\sqrt{x^2 + y^2 + z^2}} \f$
///
/// \return double: distance between point and plane
double Plane::distance_point_plane(XPt3D p)
{
    return /*std::pow(*/(abs(p.X*x + p.Y*y + p.Z*z + d) / sqrt(x*x + y*y + z*z))/*,2)*/;
}

/// \brief computes a custom distance between a triangle and a plane
///
/// \details we assume that the whole triangle should not be too far from the plane for RANSAC computation
/// => we search for the maximum distance between every point of the triangle and the plane
/// the farthest point is one of the vertex of the triangle
/// => we compute the distance between all its vertices and select the highest one
///
/// \param t: triangle
/// \param mls
/// \param params
///
/// \return double: max distance from triangle to plane
double Plane::distance_triangle_plane(Triangle t, XMls& mls, param params)
{
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    double d1 = distance_point_plane( mls.Pworld(0,t.i)-Pivot );
    double d2 = distance_point_plane( mls.Pworld(0,t.j)-Pivot );
    double d3 = distance_point_plane( mls.Pworld(0,t.k)-Pivot );
    return max(max(d1,d2),d3);
}

/// \brief computes our custom RANSAC distance
///
/// \details we think that, as every tringle generates a plane, we have to take into account the orientation of this plane
/// and weight triangle to plane distance by the scalar product of their respective orientations
///
/// \param t: triangle
/// \param mls
/// \param params
///
/// \return double: distance from triangle to plane over the scalar product of their respective orientations
double Plane::custom_ransac_distance(Triangle t, XMls& mls, param params)
{
    double d = distance_triangle_plane(t,mls,params);
    XPt3D tri_normale = t.normal(mls);
    XPt3D pla_normale(x,y,z);
    pla_normale.Normalise();
    double scalar_prod = abs(tri_normale.X*pla_normale.X + tri_normale.Y*pla_normale.Y + tri_normale.Z*pla_normale.Z);
    return d/(scalar_prod);
}

/// \brief computes distance between point (as index) and plane
///
/// \param p: index of point
/// \param mls
/// \param params
///
/// \return double: distance between point and plane
double Plane::custom_ransac_distance(uint p, XMls &mls, param params)
{
    XPt3D point = mls.Pworld(0,p);
    return abs(x*point.X + y*point.Y + z*point.Z + d) / sqrt(x*x + y*y + z*z);
}


/// \brief comutes barycenter of triangle (\f$ \frac{t1 + t2 + t3}{3} \f$)
///
/// \param t: triangle
/// \param mls
/// \param params
///
/// \return 3D point: barycenter of triangle
XPt3D Plane::barycenter(Triangle t, XMls& mls, param params)
{
    //mls.Load(0);
    XPt3D b;
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    XPt3D A = mls.Pworld(0,t.i);
    A -= Pivot;
    XPt3D B = mls.Pworld(0,t.j);
    B -= Pivot;
    XPt3D C = mls.Pworld(0,t.k);
    C -= Pivot;
    b.X = (A.X + B.X + C.X)/3;
    b.Y = (A.Y + B.Y + C.Y)/3;
    b.Z = (A.Z + B.Z + C.Z)/3;
    //mls.Free(0);
    return b;
}

}

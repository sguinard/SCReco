#include "libSteph/CutPursuit.h"

#include "../extern/cut-pursuit-master/include/API.h"

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "libSteph/MIvP3.h"
#include "libSteph/ConvexHull.h"

#include "time.h"

using namespace CP;

// observations
std::vector<std::vector<double> > find_observations_mesh(XMls& mls, param params, vT vtri)
{
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    std::vector<std::vector<double> > observations;

    std::map<uint,uint> index_order;
    uint idx = 0;
    for (auto& tri:vtri)
    {
        if (index_order.find(tri.i) == index_order.end())
            index_order[tri.i] = idx++;
        if (index_order.find(tri.j) == index_order.end())
            index_order[tri.j] = idx++;
        if (index_order.find(tri.k) == index_order.end())
            index_order[tri.k] = idx++;
    }

    std::vector<int> points_used;
    for (vT_iter vtri_iter = vtri.begin(); vtri_iter != vtri.end(); ++vtri_iter)
    {
        XPt3D point;
        std::vector<double> vp;

        if (std::find(points_used.begin(),points_used.end(),vtri_iter->i) == points_used.end())
        {
            point = mls.Pworld(0,vtri_iter->i)-Pivot;
            //point.Normalise();
            vp.emplace_back(point.X);
            vp.emplace_back(point.Y);
            vp.emplace_back(point.Z);
            observations.emplace_back(vp);
            vp.clear();
            points_used.emplace_back(vtri_iter->i);
        }
        if (std::find(points_used.begin(),points_used.end(),vtri_iter->j) == points_used.end())
        {
            point = mls.Pworld(0,vtri_iter->j)-Pivot;
            //point.Normalise();
            vp.emplace_back(point.X);
            vp.emplace_back(point.Y);
            vp.emplace_back(point.Z);
            observations.emplace_back(vp);
            vp.clear();
            points_used.emplace_back(vtri_iter->j);
        }
        if (std::find(points_used.begin(),points_used.end(),vtri_iter->k) == points_used.end())
        {
            point = mls.Pworld(0,vtri_iter->k)-Pivot;
            //point.Normalise();
            vp.emplace_back(point.X);
            vp.emplace_back(point.Y);
            vp.emplace_back(point.Z);
            observations.emplace_back(vp);
            points_used.emplace_back(vtri_iter->k);
        }
    }
    return observations;
}

std::vector<std::vector<double> > find_observations(XMls& mls, param params)
{
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    std::vector<std::vector<double> > observations;

    //observations.resize( mls.NEcho(0) );
        /*for( auto &it : observations )
        {
            it.resize( 3 );
        }*/

    for (uint node = 0; node != mls.NEcho(0); ++node)
    {
        std::vector<double> vp;
        XPt3D point = mls.Pworld(0,node)-Pivot;
        point.Normalise();
        vp.emplace_back(point.X);
        vp.emplace_back(point.Y);
        vp.emplace_back(point.Z);

        //std::cout << point.X << " " << point.Y << " " << point.Z << std::endl;

        /*observations[node][0] = (point.X);
        observations[node][1] = (point.Y);
        observations[node][2] = (point.Z);*/
        observations.emplace_back(vp);

        //std::cout << observations[node][0] << " " << observations[node][1] << " " << observations[node][2] << std::endl;
    }
    //std::sort( observations.begin(), observations.end() );
    //observations.erase( unique( observations.begin(), observations.end() ), observations.end() );
    return observations;
}


// Eu & Ev
std::pair<std::vector<uint32_t>,std::vector<uint32_t> > find_edges(std::vector<Triangle> vtri, std::vector<std::tuple<uint,uint,uint> > edges)
{
    std::vector<uint32_t> edges_begin;
    std::vector<uint32_t> edges_end;
    for (auto& tri:vtri)
    {
        std::vector<uint> v;
        v.emplace_back(tri.i);v.emplace_back(tri.j);v.emplace_back(tri.k);
        std::sort(v.begin(),v.end());

        edges_begin.emplace_back(v[0]);
        edges_begin.emplace_back(v[0]);
        edges_begin.emplace_back(v[1]);

        edges_end.emplace_back(v[1]);
        edges_end.emplace_back(v[2]);
        edges_end.emplace_back(v[2]);
    }
    for (auto& edge:edges)
    {
        edges_begin.emplace_back(std::min(std::get<1>(edge),std::get<2>(edge)));
        edges_end.emplace_back(std::max(std::get<1>(edge),std::get<2>(edge)));
    }
    return std::make_pair(edges_begin,edges_end);
}


// Eu & Ev
std::pair<std::vector<uint32_t>,std::vector<uint32_t> > find_edges(std::vector<Triangle> vtri)
{
    std::vector<uint32_t> edges_begin;
    std::vector<uint32_t> edges_end;

    std::map<uint,uint> index_order;
    uint idx = 0;
    for (auto& tri:vtri)
    {
        if (index_order.find(tri.i) == index_order.end())
            index_order[tri.i] = idx++;
        if (index_order.find(tri.j) == index_order.end())
            index_order[tri.j] = idx++;
        if (index_order.find(tri.k) == index_order.end())
            index_order[tri.k] = idx++;
    }

    for (auto& tri:vtri)
    {
        //std::vector<uint> v;
        //v.emplace_back(tri.i);v.emplace_back(tri.j);v.emplace_back(tri.k);
        //std::sort(v.begin(),v.end());

        //tri.print();

        //std::cout << index_order[tri.i] << " " << index_order[tri.j] << " " << index_order[tri.k] << std::endl;

        edges_begin.emplace_back(index_order[tri.i]);
        edges_begin.emplace_back(index_order[tri.i]);
        edges_begin.emplace_back(index_order[tri.j]);

        edges_end.emplace_back(index_order[tri.j]);
        edges_end.emplace_back(index_order[tri.k]);
        edges_end.emplace_back(index_order[tri.k]);
    }
    return std::make_pair(edges_begin,edges_end);
}


// Eu & Ev
std::pair<std::vector<uint32_t>,std::vector<uint32_t> > find_edges(XMls& mls, param params, double tresh)
{
    std::vector<uint32_t> edges_begin;
    std::vector<uint32_t> edges_end;

    int PPL = mls.PulsePerLine();
    uint n_pulse = (uint)mls.NPulse(0);
    int block_idx = 0;

    for(XPulseIndex pulse_idx=1; pulse_idx<n_pulse-1; pulse_idx++)
        if (mls.NbOfEcho(block_idx,pulse_idx) >= 1) // for all pulses having at least 1 echo
            for (XEchoIndex e1 = mls.IdxFirstEcho(block_idx,pulse_idx); e1 <= mls.IdxLastEcho(block_idx,pulse_idx); ++e1)   // 1st pulse
            {
                if (pulse_idx+1 < n_pulse)
                    for (XEchoIndex e2 = mls.IdxFirstEcho(block_idx,pulse_idx+1); e2 <= mls.IdxLastEcho(block_idx,pulse_idx+1); ++e2)   // 1st pulse
                    {
                        if (SCR::edge(0,e1,e2).length(mls) < tresh)
                        {
                            edges_begin.emplace_back(e1);
                            edges_end.emplace_back(e2);
                        }
                    }
                if (pulse_idx+PPL < n_pulse)
                    for (XEchoIndex e2 = mls.IdxFirstEcho(block_idx,pulse_idx+PPL); e2 <= mls.IdxLastEcho(block_idx,pulse_idx+PPL); ++e2)   // 1st pulse
                    {
                        if (SCR::edge(0,e1,e2).length(mls) < tresh)
                        {
                            edges_begin.emplace_back(e1);
                            edges_end.emplace_back(e2);
                        }
                    }
                if (pulse_idx+PPL+1 < n_pulse)
                    for (XEchoIndex e2 = mls.IdxFirstEcho(block_idx,pulse_idx+PPL+1); e2 <= mls.IdxLastEcho(block_idx,pulse_idx+PPL+1); ++e2)   // 1st pulse
                    {
                        if (SCR::edge(0,e1,e2).length(mls) < tresh)
                        {
                            edges_begin.emplace_back(e1);
                            edges_end.emplace_back(e2);
                        }
                    }
                /*if (pulse_idx-1 > 0 && pulse_idx-1 < n_pulse)
                    for (XEchoIndex e2 = mls.IdxFirstEcho(block_idx,pulse_idx-1); e2 <= mls.IdxLastEcho(block_idx,pulse_idx-1); ++e2)   // 1st pulse
                    {
                        edges_begin.emplace_back(e1);
                        edges_end.emplace_back(e2);
                    }
                if (pulse_idx-PPL > 0 && pulse_idx-PPL < n_pulse)
                    for (XEchoIndex e2 = mls.IdxFirstEcho(block_idx,pulse_idx-PPL); e2 <= mls.IdxLastEcho(block_idx,pulse_idx-PPL); ++e2)   // 1st pulse
                    {
                        edges_begin.emplace_back(e1);
                        edges_end.emplace_back(e2);
                    }
                if (pulse_idx-PPL-1 > 0 && pulse_idx-PPL-1 < n_pulse)
                    for (XEchoIndex e2 = mls.IdxFirstEcho(block_idx,pulse_idx-PPL-1); e2 <= mls.IdxLastEcho(block_idx,pulse_idx-PPL-1); ++e2)   // 1st pulse
                    {
                        edges_begin.emplace_back(e1);
                        edges_end.emplace_back(e2);
                    }*/
            }

    return std::make_pair(edges_begin,edges_end);
}


/*std::pair<std::vector<uint32_t>, std::vector<uint32_t> > find_edges(XMls &mls, param params, double tresh)
{

}*/

void cut_pursuit
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<std::vector<double> > &observation, // coordinates
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<std::vector<double> > &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 XMls& mls,
                 param params,
                 std::vector<uint32> inComponent)
{   //C-style ++ interface
    std::srand (time(NULL));
    if (verbose > 0)
    {
        std::cout << "L0-CUT PURSUIT";
    }
    //--------parameterization---------------------------------------------
    //CutPursuit<double> * cpl = create_CP(mode, verbose);
    //CutPursuit_Plane<double> * cp = reinterpret_cast<CutPursuit_Plane<double> *>(cpl);
    CutPursuit_Plane<double>* cp = dynamic_cast<CutPursuit_Plane<double>* >(create_CP(mode, verbose));  // ok
    set_speed(cp, speed, verbose);
    set_up_CP(cp, n_nodes, n_edges, nObs, observation, Eu, Ev
             ,edgeWeight, nodeWeight);
    cp->parameter.reg_strenth = lambda;
    cp->parameter.max_ite_main = 10;
    cp->parameter.backward_step = false;
    //-------run the optimization------------------------------------------
    cp->run_plane(inComponent);
    //------------write the solution-----------------------------
    VertexAttributeMap<double> vertex_attribute_map = boost::get(
            boost::vertex_bundle, cp->main_graph);


    VertexIterator<double> ite_nod = boost::vertices(cp->main_graph).first;
    for(uint32_t ind_nod = 0; ind_nod < n_nodes; ind_nod++ )
    {

        inComponent[*ite_nod] =
        vertex_attribute_map[*ite_nod].in_component;
        //std::cout << inComponent[*ite_nod] << " " << std::flush;

        for(uint32_t ind_dim=0; ind_dim < nObs; ind_dim++)
        {
            solution[ind_nod][ind_dim] = vertex_attribute_map[*ite_nod].value[ind_dim];
            //std::cout << solution[ind_nod][ind_dim] - observation[ind_nod][ind_dim] << " " ; // like 10s meter aparts   // -3X.XXX -X.XXX -5X.XXX
        }
        //std::cout << std::endl;
        ite_nod++;
    }


    std::cout << "finding convexhull" <<std::endl;
    vI comp_idx;
    for (int i=0; i != (int)inComponent.size(); i++)
    {
        bool found_comp = false;
        for (vI_iter it = comp_idx.begin(); it != comp_idx.end(); it++)
        {
            if (*it == (int)inComponent[i])
                found_comp = true;
        }
        if (!found_comp)
            comp_idx.push_back(i);
    }
    std::cout << "\tcreation of map of segments and their corresponding points" << std::endl;
    MIvP3* set_of_points = new MIvP3;
    //for (vI_iter it = comp_idx.begin(); it != comp_idx.end(); it++)
    //    set_of_points[*it]->emplace_back(new vP3);
    for(uint32_t ind_nod = 0; ind_nod < n_nodes; ind_nod++ )
    {
        //std::cout << solution[ind_nod][0] << " " << std::flush;
        XPt3D p (solution[ind_nod][0],solution[ind_nod][1],solution[ind_nod][2]);
        //std::cout << inComponent[ind_nod] << " " << p.X << " " << p.Y << " " << p.Z << " - " << std::flush;
        //if (set_of_points[(int)inComponent[ind_nod]].size() < 1) set_of_points[(int)inComponent[ind_nod]] = vP3();
        set_of_points->operator []((int)inComponent[ind_nod]).emplace_back(p);
        //std::cout << set_of_points->operator []((int)inComponent[ind_nod]).size() << " " << std::flush;
    }
    std::cout << set_of_points->operator [](0).size() << std::endl;
    std::cout << "\tconvex hull computation" << std::endl;
    std::vector<Multipoly> convexhulls;
    for (MIvP3::iterator it = set_of_points->begin(); it != set_of_points->end(); ++it)
    {
        std::cout << it->first <<" input points: " << it->second.size() << std::endl;
        if (it->second.size() > 0)
        {
            ConvexHull* ch = new ConvexHull;
            *ch = ConvexHull(&it->second);
            convexhulls.emplace_back(ch->compute_convex_hull());
        }
    }
    write_output("/home/sguinard/dev/libxmls/data/","convexhulls",convexhulls);


    std::pair<std::vector<std::vector<std::pair<XPt3D, Color> > >,std::vector<std::vector<std::pair<XPt3D, Color> > > > points_colored = color_points(solution,observation,inComponent,
                                                                                                                                                      cp->components.size());
    WritePlyPointColor(mls,points_colored.first,params,"_values.ply");
    WritePlyPointColor(mls,points_colored.second,params,"_observations.ply");

    delete cp;
    return;
}



void cut_pursuit
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<std::vector<double> > &observation, // coordinates
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<std::vector<double> > &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent,
		 const std::string out)
{   //C-style ++ interface
    //std::srand (time(NULL));
    if (verbose > 0)
    {
        std::cout << "L0-CUT PURSUIT";
    }
    //--------parameterization---------------------------------------------
//    for (auto& c:inComponent) std::cout << c << " " << std::flush;    // ok
    CutPursuit_Plane<double>* cp = dynamic_cast<CutPursuit_Plane<double>* >(create_CP(mode, verbose));  // ok
    set_speed(cp, speed, verbose);
    set_up_CP(cp, n_nodes, n_edges, nObs, observation, Eu, Ev
             ,edgeWeight, nodeWeight);
    cp->parameter.reg_strenth = lambda;
    cp->parameter.max_ite_main = 15;
    cp->parameter.backward_step = true;
    cp->parameter.parallel = true;
    //-------run the optimization------------------------------------------
    cp->run_plane(inComponent);
    //------------write the solution-----------------------------
    VertexAttributeMap<double> vertex_attribute_map = boost::get(
            boost::vertex_bundle, cp->main_graph);

//for (auto& c:inComponent) std::cout << c << " " << std::flush;
    VertexIterator<double> ite_nod = boost::vertices(cp->main_graph).first;
    for(uint32_t ind_nod = 0; ind_nod < n_nodes; ind_nod++ )
    {

        inComponent[*ite_nod] =
        vertex_attribute_map[*ite_nod].in_component;
        //std::cout << inComponent[*ite_nod] << " " << std::flush;

        for(uint32_t ind_dim=0; ind_dim < nObs; ind_dim++)
        {
            solution[ind_nod][ind_dim] = vertex_attribute_map[*ite_nod].value[ind_dim];
//            std::cout << solution[ind_nod][ind_dim] /*- observation[ind_nod][ind_dim]*/ << " " ; // like 10s meter aparts   // -3X.XXX -X.XXX -5X.XXX
        }
        //std::cout << std::endl;
        ite_nod++;
    }

    std::pair<std::vector<std::vector<std::pair<XPt3D, uint32> > >,std::vector<std::vector<std::pair<XPt3D, uint32> > > > points_colored = color_points(solution,observation,inComponent
                                                                                                                                                      /*cp->components.size()*/);
    //WritePlyPoint(solution);
    WritePlyPointColor(points_colored.first,out+"_values.ply");
    WritePlyPointColor(points_colored.second,out+"_observations.ply");

    delete cp;
    return;
}




void cut_pursuit_tri
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<std::vector<double> > &observation, // coordinates
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<std::vector<double> > &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent)
{   //C-style ++ interface
    //std::srand (time(NULL));
    if (verbose > 0)
    {
        std::cout << "L0-CUT PURSUIT";
    }
    //--------parameterization---------------------------------------------
//    for (auto& c:inComponent) std::cout << c << " " << std::flush;    // ok
    CutPursuit_Plane_Triangle<double>* cp = dynamic_cast<CutPursuit_Plane_Triangle<double>* >(create_CP(mode, verbose));  // ok
    set_speed(cp, speed, verbose);
    set_up_CP(cp, n_nodes, n_edges, nObs, observation, Eu, Ev
             ,edgeWeight, nodeWeight);
    cp->parameter.reg_strenth = lambda;
    cp->parameter.max_ite_main = 10;
    cp->parameter.backward_step = true;
    cp->parameter.parallel = true;
    //-------run the optimization------------------------------------------
    cp->run_plane(inComponent); // SG - 23-10-2018 correct thatx
    //------------write the solution-----------------------------
    VertexAttributeMap<double> vertex_attribute_map = boost::get(
            boost::vertex_bundle, cp->main_graph);

//for (auto& c:inComponent) std::cout << c << " " << std::flush;
    VertexIterator<double> ite_nod = boost::vertices(cp->main_graph).first;
    for(uint32_t ind_nod = 0; ind_nod < n_nodes; ind_nod++ )
    {

        inComponent[*ite_nod] =
        vertex_attribute_map[*ite_nod].in_component;
        //std::cout << inComponent[*ite_nod] << " " << std::flush;

        for(uint32_t ind_dim=0; ind_dim < nObs; ind_dim++)
        {
            solution[ind_nod][ind_dim] = vertex_attribute_map[*ite_nod].value[ind_dim];
//            std::cout << solution[ind_nod][ind_dim] /*- observation[ind_nod][ind_dim]*/ << " " ; // like 10s meter aparts   // -3X.XXX -X.XXX -5X.XXX
        }
        //std::cout << std::endl;
        ite_nod++;
    }

    std::pair<std::vector<std::vector<std::pair<XPt3D, uint32> > >,std::vector<std::vector<std::pair<XPt3D, uint32> > > > points_colored = color_points(solution,observation,inComponent
                                                                                                                                                      /*cp->components.size()*/);
    //WritePlyPoint(solution);
    WritePlyPointColor(points_colored.first,"_values.ply");
    WritePlyPointColor(points_colored.second,"_observations.ply");

    delete cp;
    return;
}





std::vector<std::vector<std::pair<XPt3D, Color> > > color_points(std::vector<std::vector<double> > solution, std::vector<uint32> inComponent)
{
    std::vector<std::vector<std::pair<XPt3D, Color> > > points_colored;

    //std::cout << "Number of points: " << solution.size() << std::endl;

    std::vector<std::pair<uint32,Color> > comp_color;
    for (int i=0; i != (int)inComponent.size(); i++)
    {
        //std::cout << inComponent[i] << " " << std::flush;
        bool found_comp = false;
        for (std::vector<std::pair<uint32,Color> >::iterator it = comp_color.begin(); it != comp_color.end(); it++)
        {
            if (it->first == inComponent[i])
                found_comp = true;
        }
        if (!found_comp)
        {
            unsigned char r = rand() % 255;
            unsigned char g = rand() % 255;
            unsigned char b = rand() % 255;
            comp_color.push_back(std::make_pair(inComponent[i],Color(r,g,b)));
        }
    }

    points_colored.resize(1);
    //int i=0;
    for (int p_index = 0; p_index != (int)solution.size(); p_index++)
    {
        if (solution[p_index].size() == 3)
        {
            Color color_of_comp;
            //std::cout << solution[p_index][0] << " " << solution[p_index][1] << " " << solution[p_index][2] << " " << inComponent[p_index] << std::endl;
            for (std::vector<std::pair<uint32,Color> >::iterator it = comp_color.begin(); it != comp_color.end(); it++)
            {
                if (it->first == inComponent[p_index])
                {
                    //std::cout << it->first << " " << static_cast<unsigned> ( it->second.red ) << " " << static_cast<unsigned> ( it->second.green ) << " " << static_cast<unsigned> ( it->second.blue ) << std::endl;

                    color_of_comp = it->second;
                    break;
                }
            }

            XPt3D p ((double)solution[p_index][0],(double)solution[p_index][1],(double)solution[p_index][2]);
            //std::cout << i << " " << std::flush;
            points_colored[0].emplace_back(std::make_pair(p,color_of_comp));
        }
    }
    std::cout << "Yay! I painted points     :hap:" << std::endl;
    return points_colored;
}




std::pair<std::vector<std::vector<std::pair<XPt3D, Color> > >,std::vector<std::vector<std::pair<XPt3D, Color> > > > color_points(std::vector<std::vector<double> > solution,
                                                                                                                                 std::vector<std::vector<double> > observation,
                                                                                                                                 std::vector<uint32> inComponent,
                                                                                                                                 uint32 nb_comps)
{
    //std::cout << "Cate enters the shed and see a paint bucket" << std::endl;
//std::cout << solution.size() - observation.size() << std::endl;
    //int nb_pts_obs = 0;

    std::vector<std::vector<std::pair<XPt3D, Color> > > values;
    std::vector<std::vector<std::pair<XPt3D, Color> > > observations;

    //std::cout << "Number of points: " << solution.size() << std::endl;

    std::vector<std::pair<uint32,Color> > comp_color;
    for (int i = 0; i != nb_comps; ++i)
        comp_color.emplace_back(std::make_pair(i,Color(rand() % 255,
                                                       rand() % 255,
                                                       rand() % 255)));

    std::cout << comp_color.size() << " colors" << std::endl;

    values.resize(1);
    observations.resize(1);

    for (int p_index = 0; p_index != (int)observation.size(); p_index++)
    {
        if (observation[p_index].size() == 3)
        {
            Color color_of_comp;
            for (std::vector<std::pair<uint32,Color> >::iterator it = comp_color.begin(); it != comp_color.end(); it++)
            {
                //std::cout << it->first << " " << static_cast<unsigned> ( it->second.red ) << " " << static_cast<unsigned> ( it->second.green ) << " " << static_cast<unsigned> ( it->second.blue ) << std::endl;
                //std::cout << it->first << " " << inComponent[i] << std::endl;
                if (it->first == inComponent[p_index])
                {
                    //std::cout << it->first << " " << " " << static_cast<unsigned> ( it->second.red ) << " " << static_cast<unsigned> ( it->second.green ) << " " << static_cast<unsigned> ( it->second.blue ) << std::endl;

                    color_of_comp = it->second;
                    break;
                }
            }


            XPt3D p (solution[p_index][0],solution[p_index][1],solution[p_index][2]);
            XPt3D p2 (observation[p_index][0],observation[p_index][1],observation[p_index][2]);
            //std::cout << i << " " << std::flush;
            values[0].emplace_back(std::make_pair(p,color_of_comp));
            observations[0].emplace_back(std::make_pair(p2,color_of_comp));
        }
    }
    //std::cout << "Cate is happy because he toppled the bucket and can now play with it" << std::endl;
//std::cout << "Yay! I painted points     :hap:" << std::endl;
    return std::make_pair(values,observations);
}



std::pair<std::vector<std::vector<std::pair<XPt3D, uint32> > >,std::vector<std::vector<std::pair<XPt3D, uint32> > > > color_points(
                                                                                                                                std::vector<std::vector<double> > solution,
                                                                                                                                std::vector<std::vector<double> > observation,
                                                                                                                                std::vector<uint32> inComponent)

{
	double err = 0;
    //std::cout << "Cate enters the shed and see a paint bucket" << std::endl;
//std::cout << solution.size() - observation.size() << std::endl;
    //int nb_pts_obs = 0;

    std::vector<std::vector<std::pair<XPt3D, uint32> > > values;
    std::vector<std::vector<std::pair<XPt3D, uint32> > > observations;

    values.resize(1);
    observations.resize(1);

    for (int p_index = 0; p_index != (int)observation.size(); p_index++)
    {
        if (observation[p_index].size() == 3)
        {
            XPt3D p (solution[p_index][0],solution[p_index][1],solution[p_index][2]);
            XPt3D p2 (observation[p_index][0],observation[p_index][1],observation[p_index][2]);
            err += d2(p,p2);
            //std::cout << p.X << " " << p.Y << " " << p.Z << " " << p2.X << " " << p2.Y << " " << p2.Z << std::endl;
            //std::cout << i << " " << std::flush;
            values[0].emplace_back(std::make_pair(p,inComponent[p_index]));
            observations[0].emplace_back(std::make_pair(p2,inComponent[p_index]));
        }
    }
    //std::cout << "Cate is happy because he toppled the bucket and can now play with it" << std::endl;
//std::cout << "Yay! I painted points     :hap:" << std::endl;
	std::cout << "\nERROR FROM POINTS TO THEIR PROJECTIONS: " << err << std::endl;
    return std::make_pair(values,observations);
}







void cut_pursuit_tri
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<std::vector<double> > &observation, // coordinates
                 vvD *pts,
                 std::vector<std::vector<uint32_t> > *tris,
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<std::vector<double> > &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent)
{
	//C-style ++ interface
    //std::srand (time(NULL));
    if (verbose > 0)
    {
        std::cout << "L0-CUT PURSUIT";
    }
    //--------parameterization---------------------------------------------
//    for (auto& c:inComponent) std::cout << c << " " << std::flush;    // ok
    CutPursuit_Plane_Triangle<double>* cp = dynamic_cast<CutPursuit_Plane_Triangle<double>* >(create_CP(mode, verbose));  // ok
    set_speed(cp, speed, verbose);
    cp->set_pts_tris(&observation, pts, tris);
    set_up_CP(cp, n_nodes, n_edges, nObs, observation, Eu, Ev
	      ,edgeWeight, nodeWeight);
    cp->parameter.reg_strenth = lambda;
    cp->parameter.max_ite_main = 10;
    cp->parameter.backward_step = true;
    cp->parameter.parallel = true;
    //-------run the optimization------------------------------------------
    cp->run_plane(inComponent); 
    //------------write the solution-----------------------------
    VertexAttributeMap<double> vertex_attribute_map = boost::get(
            boost::vertex_bundle, cp->main_graph);

//for (auto& c:inComponent) std::cout << c << " " << std::flush;
    VertexIterator<double> ite_nod = boost::vertices(cp->main_graph).first;
    for(uint32_t ind_nod = 0; ind_nod < n_nodes; ind_nod++ )
    {

        inComponent[*ite_nod] =
        vertex_attribute_map[*ite_nod].in_component;
        //std::cout << inComponent[*ite_nod] << " " << std::flush;

        for(uint32_t ind_dim=0; ind_dim < nObs; ind_dim++)
        {
            solution[ind_nod][ind_dim] = vertex_attribute_map[*ite_nod].value[ind_dim];
//            std::cout << solution[ind_nod][ind_dim] /*- observation[ind_nod][ind_dim]*/ << " " ; // like 10s meter aparts   // -3X.XXX -X.XXX -5X.XXX
        }
        //std::cout << std::endl;
        ite_nod++;
    }
    //for (auto& c: inComponent) std::cout << c << " " << std::flush;

    color_triangles(observation, *tris, *pts, inComponent, *std::max_element(inComponent.begin(), inComponent.end())/*cp->components.size()*/);
    
    delete cp;
    return;
}



void color_triangles(	std::vector<std::vector<double> > observation, 
						std::vector<std::vector<uint32_t> > triangles, 
						vvD points, 
						std::vector<uint32> inComponent, 
						uint32_t ncomps)
{//for (auto& p: points) std::cout << p[0] << " " << p[1] << " " << p[2] << std::endl;
	double err = 0;
	vP planes;
	std::map<uint32_t, std::vector<uint32_t> > comp2point_adjacency;
	std::map<uint32_t, std::vector<uint32_t> > point2comp_adjacency;
	
	// finding colors
	std::vector<Color> comp_color;
    for (int i = 0; i <= ncomps; ++i)
        comp_color.emplace_back(Color(rand() % 255, rand() % 255, rand() % 255));

    std::cout << comp_color.size() << " colors" << std::endl;

	// finding corresponding planes
	for (uint32_t i = 0; i <= ncomps; ++i)
	{
		vP3* pts = new vP3;
		for (uint32_t t_idx = 0; t_idx != triangles.size(); ++t_idx)
		{
			if (inComponent[t_idx] == i)
			{
				XPt3D p;
				p.X = (double)points[triangles[t_idx][0]][0];
				p.Y = (double)points[triangles[t_idx][0]][1];
				p.Z = (double)points[triangles[t_idx][0]][2];
				pts->emplace_back(p);
				p.X = (double)points[triangles[t_idx][1]][0];
				p.Y = (double)points[triangles[t_idx][1]][1];
				p.Z = (double)points[triangles[t_idx][1]][2];
				pts->emplace_back(p);
				p.X = (double)points[triangles[t_idx][2]][0];
				p.Y = (double)points[triangles[t_idx][2]][1];
				p.Z = (double)points[triangles[t_idx][2]][2];
				pts->emplace_back(p);
			}
		}
		planes.emplace_back(Plane(pts));
		delete pts;
	}
	
	// computing error
	vvD points_proj(points.size(),std::vector<double>{0,0,0});
	std::vector<uint32_t> points_adjacency(points.size(), 0);
	for (uint32_t t_idx = 0; t_idx != triangles.size(); ++t_idx)
	  { // compute point projection on intersection line between adjacent planes
		XPt3D p0;
		p0.X = (double)points[triangles[t_idx][0]][0];
		p0.Y = (double)points[triangles[t_idx][0]][1];
		p0.Z = (double)points[triangles[t_idx][0]][2];
		XPt3D p1;
		p1.X = (double)points[triangles[t_idx][1]][0];
		p1.Y = (double)points[triangles[t_idx][1]][1];
		p1.Z = (double)points[triangles[t_idx][1]][2];
		XPt3D p2;
		p2.X = (double)points[triangles[t_idx][2]][0];
		p2.Y = (double)points[triangles[t_idx][2]][1];
		p2.Z = (double)points[triangles[t_idx][2]][2];
		
		XPt3D proj0 = planes[inComponent[t_idx]].project_point_plane(p0);
		XPt3D proj1 = planes[inComponent[t_idx]].project_point_plane(p1);
		XPt3D proj2 = planes[inComponent[t_idx]].project_point_plane(p2);
		points_proj[triangles[t_idx][0]][0] += proj0.X;
		points_proj[triangles[t_idx][0]][1] += proj0.Y;
		points_proj[triangles[t_idx][0]][2] += proj0.Z;
		points_proj[triangles[t_idx][1]][0] += proj1.X;
		points_proj[triangles[t_idx][1]][1] += proj1.Y;
		points_proj[triangles[t_idx][1]][2] += proj1.Z;
		points_proj[triangles[t_idx][2]][0] += proj2.X;
		points_proj[triangles[t_idx][2]][1] += proj2.Y;
		points_proj[triangles[t_idx][2]][2] += proj2.Z;
		points_adjacency[triangles[t_idx][0]] += 1;
		points_adjacency[triangles[t_idx][1]] += 1;
		points_adjacency[triangles[t_idx][2]] += 1;
		
		if (comp2point_adjacency.find(inComponent[t_idx]) == comp2point_adjacency.end())
		  comp2point_adjacency.insert(std::pair<uint32_t, std::vector<uint32_t> >(inComponent[t_idx], std::vector<uint32_t>{triangles[t_idx][0], triangles[t_idx][1], triangles[t_idx][2]}));
		else
		  {
		    comp2point_adjacency[inComponent[t_idx]].emplace_back(triangles[t_idx][0]);
		    comp2point_adjacency[inComponent[t_idx]].emplace_back(triangles[t_idx][1]);
		    comp2point_adjacency[inComponent[t_idx]].emplace_back(triangles[t_idx][2]);
		  }

		if (point2comp_adjacency.find(triangles[t_idx][0]) == point2comp_adjacency.end())
		  point2comp_adjacency.insert(std::pair<uint32_t, std::vector<uint32_t> >(triangles[t_idx][0], std::vector<uint32_t>{inComponent[t_idx]}));
		else
		  {
		    point2comp_adjacency[triangles[t_idx][0]].emplace_back(inComponent[t_idx]);
		  }

		if (point2comp_adjacency.find(triangles[t_idx][1]) == point2comp_adjacency.end())
		  point2comp_adjacency.insert(std::pair<uint32_t, std::vector<uint32_t> >(triangles[t_idx][1], std::vector<uint32_t>{inComponent[t_idx]}));
		else
		  {
		    point2comp_adjacency[triangles[t_idx][1]].emplace_back(inComponent[t_idx]);
		  }

		if (point2comp_adjacency.find(triangles[t_idx][2]) == point2comp_adjacency.end())
		  point2comp_adjacency.insert(std::pair<uint32_t, std::vector<uint32_t> >(triangles[t_idx][2], std::vector<uint32_t>{inComponent[t_idx]}));
		else
		  {
		    point2comp_adjacency[triangles[t_idx][2]].emplace_back(inComponent[t_idx]);
		  }
		
		//std::cout << p0.X << " " << p0.Y << " " << p0.Z << " " << proj0.X << " " << proj0.Y << " " << proj0.Z << std::endl;
		//std::cout << d2(p0, proj0) << " " << d2(p1,proj1) << " " << d2(p2,proj2) << std::endl;
		
		/*double per = (d2(p0,p1) + d2(p1,p2) + d2(p2,p0)) / 2;
		double area = sqrt(per * (per - d2(p0,p1)) * (per - d2(p1,p2)) * (per -d2(p2,p0)));
		err += area * (std::pow(d2(p0,proj0),2) + std::pow(d2(p1,proj1),2) + std::pow(d2(p2,proj2),2) 
		+ d2(p0,proj0)*d2(p1,proj1) + d2(p0,proj0)*d2(p2,proj2) + d2(p1,proj1)*d2(p2,proj2)) / 6;*/
	}
	
	for (uint32_t i = 0; i != points_proj.size(); ++i)
	{
	  if (points_adjacency[i] > 0)
	    {
		points_proj[i][0] /= points_adjacency[i];
		points_proj[i][1] /= points_adjacency[i];
		points_proj[i][2] /= points_adjacency[i];

		XPt3D p_orig = XPt3D(points[i][0], points[i][1], points[i][2]);
		XPt3D p_proj = XPt3D(points_proj[i][0], points_proj[i][1], points_proj[i][2]);
		
		err += d2(p_orig, p_proj);
	    }
	}
	
	std::cout << "ERROR FROM POINTS TO PLANES: " << err << std::endl;	
	
	// storing
	store_plane_pursuit_tri(&points, &triangles, comp_color, "observations", inComponent);
	store_plane_pursuit_tri(&points_proj, &triangles, comp_color, "projections", inComponent);



	// re-poly -VSA-
	/*for (std::map<uint32_t, std::vector<uint32_t> >::iterator it = comp2point_adjacency.begin(); it != comp2point_adjacency.end(); ++it)
	  {
	    std::sort(it->second.begin(), it->second.end());
	    it->second.erase( unique(it->second.begin(), it->second.end()), it->second.end());
	  }
	for (std::map<uint32_t, std::vector<uint32_t> >::iterator it = point2comp_adjacency.begin(); it != point2comp_adjacency.end(); ++it)
	  {
	    std::sort(it->second.begin(), it->second.end());
	    it->second.erase( unique(it->second.begin(), it->second.end()), it->second.end());
	  }
	std::map<uint32_t, std::vector<uint32_t> > triple_adjacency;
	std::map<uint32_t, std::vector<uint32_t> > meta_adjacency;
	std::vector<uint32_t> anchor_vertices;
	for (uint32_t i = 0; i != points_adjacency.size(); ++i)
	  {
	    if (point2comp_adjacency[i].size() > 2)
	      {
		for (auto& r: point2comp_adjacency[i])
		  if (triple_adjacency.find(r) == triple_adjacency.end())
		    triple_adjacency.insert(std::pair<uint32_t, std::vector<uint32_t> >(r, std::vector<uint32_t>{i}));
		  else
		    triple_adjacency[r].emplace_back(i);
		anchor_vertices.emplace_back(i);
	      }
	    if (point2comp_adjacency[i].size() >= 2)
	      {
		for (auto& r: point2comp_adjacency[i])
		  if (meta_adjacency.find(r) == meta_adjacency.end())
		    meta_adjacency.insert(std::pair<uint32_t, std::vector<uint32_t> >(r, std::vector<uint32_t>{i}));
		  else
		    meta_adjacency[r].emplace_back(i);
	      }
	  }
	for (auto& m: meta_adjacency)
	  {
	    std::cout << "component: " << m.first << " - " << std::flush;
	    for (auto& p: m.second)
	      std::cout << p << " " << std::flush;
	    std::cout << std::endl;
	    }

	for (uint32_t i = 0; i != ncomps+1; ++i)
	  {
	    if (triple_adjacency.find(i) == triple_adjacency.end()) // in this case our region has no point adjacent to 3 regions
	                                         // so we select the furthest point from the center of the region and anchor it
	      {// compute region center
		std::cout << "region: " << i << " has no triple point" << std::endl;
		std::vector<uint32_t> pts_idx = comp2point_adjacency[i];
		XPt3D center = XPt3D(0,0,0);
		for (auto& p: pts_idx)
		  center.operator +=(XPt3D(points[p][0], points[p][1], points[p][2]));
		center.operator /=(pts_idx.size());
		double max_d = 0;
		uint32_t furthest_point = 0;
		for (auto& p: pts_idx)
		  {
		    double d = d2(center, XPt3D(points[p][0], points[p][1], points[p][2]));
		    if (d > max_d)
		      {
			max_d = d;
			furthest_point = p;
		      }
		  }
		anchor_vertices.emplace_back(furthest_point);
		triple_adjacency.insert(std::pair<uint32_t, std::vector<uint32_t> >(i, std::vector<uint32_t>{furthest_point}));
	      }
	    else
	      std::cout << "region: " << i << " has " << triple_adjacency[i].size() << " triple points" << std::endl;
	  }

	// test if we have at least 3 points per region
	for (uint32_t i = 0; i != triple_adjacency.size(); ++i)
	  {
	    if (triple_adjacency[i].size() == 1)
	      {// in this case, we need more anchor points
		std::vector<uint32_t> pts_idx = comp2point_adjacency[i];
		XPt3D center = XPt3D(points[triple_adjacency[i][0]][0], points[triple_adjacency[i][0]][1], points[triple_adjacency[i][0]][2]);
		//center.operator /=(pts_idx.size());
		double max_d = 0;
		uint32_t furthest_point = 0;
		for (auto& p: pts_idx)
		  {
		    double d = d2(center, XPt3D(points[p][0], points[p][1], points[p][2]));
		    if (d > max_d)
		      {
			max_d = d;
			furthest_point = p;
		      }
		  }
		anchor_vertices.emplace_back(furthest_point);
		triple_adjacency[i].emplace_back(furthest_point);
	      }
	    if (triple_adjacency[i].size() == 2)// need to add one more point
	      {// in this case, we need more anchor points
		std::vector<uint32_t> pts_idx = comp2point_adjacency[i];
		uint32_t id1 = triple_adjacency[i][0];
		uint32_t id2 = triple_adjacency[i][1];
		XPt3D p1 = XPt3D(points[id1][0], points[id1][1], points[id1][2]);
		XPt3D p2 = XPt3D(points[id2][0], points[id2][1], points[id2][2]);
		double max_d = 0;
		uint32_t furthest_point = 0;
		for (auto& p: pts_idx)
		  {
		    double d = d2(p1, XPt3D(points[p][0], points[p][1], points[p][2])) + d2(p2, XPt3D(points[p][0], points[p][1], points[p][2]));
		    if (d > max_d)
		      {
			max_d = d;
			furthest_point = p;
		      }
		  }
		anchor_vertices.emplace_back(furthest_point);
		triple_adjacency[i].emplace_back(furthest_point);
	      }
	  }

	for (auto& m: triple_adjacency)
	  {
	    std::cout << "component: " << m.first << " - " << std::flush;
	    for (auto& p: m.second)
	      std::cout << p << " " << std::flush;
	    std::cout << std::endl;
	    }

	// now, we check that all the points are close enough to the edges formed by the 3 anchor points per region
	double d_tresh = 0.5;
	for (uint32_t i = 0; i != ncomps + 1; ++i)
	  {
	    std::vector<uint32_t> apexes = triple_adjacency[i];
	    std::vector<uint32_t> pts_idx = comp2point_adjacency[i];
	    double dmax = 0;
	    uint32_t furthest_point = 0;
	    do
	      {
		dmax = 0;
		std::vector<uint32_t> apexes = triple_adjacency[i];	    
		for (auto& p: pts_idx) if(std::find(apexes.begin(), apexes.end(), p) == apexes.end())
		  {
		    XPt3D point = XPt3D(points[p][0], points[p][1], points[p][2]);
		    // project point on segment
		    // select a segment
		    double dmin = 1e9;
		    for (std::vector<uint32_t>::iterator it1 = apexes.begin(); it1 != apexes.end().operator -(1); ++it1)
		      {
			XPt3D p1 = XPt3D(points[*it1][0], points[*it1][1], points[*it1][2]);
			for (std::vector<uint32_t>::iterator it2 = it1.operator +(1); it2 != apexes.end(); ++it2)
			  {
			    XPt3D p2 = XPt3D(points[*it2][0], points[*it2][1], points[*it2][2]);
			    double t = ((point.X - p1.X) * (p2.X - p1.X) + (point.Y - p1.Y) * (p2.Y - p1.Y) + (point.Z - p1.Z) * (p2.Z - p1.Z)) / ((p2.X - p1.X) * (p2.X - p1.X) + (p2.Y - p1.Y) * (p2.Y - p1.Y) + (p2.Z - p1.Z));
			    double d = 0;
			    if (t < 0) d = d2(point,p1);
			    else if (t > 1) d = d2(point,p2);
			    else
			      {
				// compute point projection on segment [p1,p2]
				XPt3D proj = XPt3D(p1.X + t*(p2.X - p1.X), p1.Y + t*(p2.Y - p1.Y), p1.Z + t*(p2.Z - p1.Z));
				d = d2 (point, proj);
			      }
			    if (d < dmin) dmin = d;
			  }
		      }
		    if (dmin > dmax)
		      {
			dmax = dmin;
			furthest_point = p;
		      }
		  }
		if (dmax > d_tresh)
		  {
		    anchor_vertices.emplace_back(furthest_point);
		    triple_adjacency[i].emplace_back(furthest_point);
		  }
	      } while (dmax > d_tresh);
	  }

	for (auto& m: triple_adjacency)
	  {
	    std::cout << "component: " << m.first << " - " << std::flush;
	    for (auto& p: m.second)
	      std::cout << p << " " << std::flush;
	    std::cout << std::endl;
	    }*/

	/// \TODO contrained 3D Delaunay around anchored vertices
}

void store_plane_pursuit_tri(vvD *points, std::vector<std::vector<uint32_t> > *triangles, std::vector<Color> comp_colors, std::string name, std::vector<uint32_t> inComponent)
{
	// storing
	std::string out = "../../";
    ofstream fileOut(out+"plane_pursuit_"+name+".ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + out + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << points->size() << endl;
    fileOut << "property double x" << endl;
    fileOut << "property double y" << endl;
    fileOut << "property double z" << endl;
    fileOut << "element face " << triangles->size() << endl;
    fileOut << "property list uchar int vertex_index" << endl;
    fileOut << "property uchar red" << endl;
    fileOut << "property uchar green" << endl;
    fileOut << "property uchar blue" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(double);
    unsigned long vertex_buffer_size = vertex_bytesize * points->size();

    char * buffer = new char[vertex_buffer_size], * it = buffer;

	for (auto& p: *points)
	{
		Write<double>(it,p[0]);
		Write<double>(it,p[1]);
		Write<double>(it,p[2]);
	}
    cout << "Writing " << points->size() << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);


    unsigned int triangle_bytesize = sizeof(unsigned char) + 3*sizeof(int) + 3*sizeof(unsigned char);
    unsigned long triangle_buffer_size = triangle_bytesize * triangles->size();

    char * tri_buffer = new char[triangle_buffer_size], * tri_it = tri_buffer;

    for (int i = 0; i != triangles->size(); ++i)
      {
	std::vector<uint32_t> t = triangles->at(i);
	Write<unsigned char>(tri_it,3);
	Write<int>(tri_it,t[0]);
	Write<int>(tri_it,t[1]);
	Write<int>(tri_it,t[2]);
	Write<unsigned char>(tri_it,comp_colors[inComponent[i]].red);
	Write<unsigned char>(tri_it,comp_colors[inComponent[i]].green);
	Write<unsigned char>(tri_it,comp_colors[inComponent[i]].blue);
      }

    cout << "Writing " << triangles->size() << " triangles of size " << triangle_buffer_size << "=" << 1.e-6*triangle_buffer_size << "MB" << endl;
    fileOut.write(tri_buffer, triangle_buffer_size);
    
    
    cout << "Total " << 1.e-6*(vertex_buffer_size)+1.e-6*(triangle_buffer_size) << "MB" << endl;
    fileOut.close();
}

#include "libSteph/Ransac.h"
#include "stdlib.h"
#include "time.h"

#include "thread"
#include "mutex"

/// \brief mutex: used for parallelism
std::mutex mutx;


/// \brief select a triangle at random and outputs the plane it is based on
///
/// \param vtri: vector of triangles
/// \param mls
/// \param params
///
/// \return plane: selected plane
Plane random_plane_based_on_triangle(vT* vtri, XMls& mls, param params)
{
    mls.Load(0);
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    int pos = rand() % vtri->size();
    Triangle t = (*vtri)[pos];
    XPt3D p1 = mls.Pworld(0,t.i) - Pivot;
    XPt3D p2 = mls.Pworld(0,t.j) - Pivot;
    XPt3D p3 = mls.Pworld(0,t.k) - Pivot;
    //std::cout << p1.X << " " << p1.Y << " " << p1.Z << " " << p2.X << " " << p2.Y << " " << p2.Z << " " << p3.X << " " << p3.Y << " " << p3.Z << std::endl;
    Plane P = Plane(p1,p2,p3);
    //t.print();
    //P.print();
    mls.Free(0);
    return P;
}


/// \brief select a triangle at random and outputs the combinaison triangle/plane
///
/// \param vvD
///
/// \return pair of triangle / plane: the selected triangle and plane based on it
Plane random_plane(vvD* points, double tresh)
{
    int s = points->size();
    int pos1, pos2, pos3;
    for (int i=0; i != 3; ++i)
    {
        pos1 = rand() % s;
        pos2 = rand() % s;
        while (pos2 == pos1 )
        {
            pos2 = rand() % s;
        }
        pos3 = rand() % s;
        while (pos1 == pos3 || pos2 == pos3 )
        {
            pos3 = rand() % s;
        }
    }
    XPt3D p1 (points->at(pos1)[0],points->at(pos1)[1],points->at(pos1)[2]);
    XPt3D p2 (points->at(pos2)[0],points->at(pos2)[1],points->at(pos2)[2]);
    XPt3D p3 (points->at(pos3)[0],points->at(pos3)[1],points->at(pos3)[2]);
    Plane p(p1,p2,p3);
    p.p1 = pos1; p.p2 = pos2; p.p3 = pos3;
//    std::cout << pos1 << " " << pos2 << " " << pos3 << std::endl;
    return p;
}


/// \brief select a triangle at random and outputs the combinaison triangle/plane
///
/// \param vtri: vector of triangles
/// \param mls
/// \param params
///
/// \return pair of triangle / plane: the selected triangle and plane based on it
std::pair<Triangle,Plane> random_plane(vT* vtri, XMls& mls, param params)
{
    //mls.Load(0);
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    int pos = rand() % vtri->size();
    Triangle t = (*vtri)[pos];
    XPt3D p1 = mls.Pworld(0,t.i) - Pivot;
    XPt3D p2 = mls.Pworld(0,t.j) - Pivot;
    XPt3D p3 = mls.Pworld(0,t.k) - Pivot;
    Plane P = Plane(p1,p2,p3);
    //P.print();
    //mls.Free(0);
    return std::make_pair(t,P);
}


/// \brief find a plane based on 3 points selected at random
///
/// \param vp: vector of ints
/// \param mls
/// \param params
///
/// \return pair of triangle / plane: the 3 selected points and the plane based on them
std::pair<Triangle,Plane> random_plane(vI* vp, XMls& mls, param params)
{
    //mls.Load(0);
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    int pos1 = rand() % vp->size();
    int pos2 = rand() % vp->size();
    int pos3 = rand() % vp->size();
    XPt3D p1 = mls.Pworld(0,(*vp)[pos1]) - Pivot;
    XPt3D p2 = mls.Pworld(0,(*vp)[pos2]) - Pivot;
    XPt3D p3 = mls.Pworld(0,(*vp)[pos3]) - Pivot;
    Plane P = Plane(p1,p2,p3);
    return std::make_pair(Triangle(pos1,pos2,pos3),P);
}


/// \brief find the points that are in a mesh (vector of triangles)
///
/// \param vtri: vector of triangles
/// \param vint: vector of ints
///
/// \return vector of ints: all point indexes that belong to the mesh
vI* find_points_in_mesh(vT* vtri, vI* vint)
{
    for (Tit tit = vtri->begin(); tit != vtri->end(); ++tit)
    {
        vint->emplace_back(tit->i);
        vint->emplace_back(tit->j);
        vint->emplace_back(tit->k);

        //std::clock_t ss;
        //ss = std::clock();
        /*if (find(vint->begin(),vint->end(),tit->i) == vint->end())
            vint->emplace_back(tit->i);
        if (find(vint->begin(),vint->end(),tit->j) == vint->end())
            vint->emplace_back(tit->j);
        if (find(vint->begin(),vint->end(),tit->k) == vint->end())
            vint->emplace_back(tit->k);*/

    }

    sort( vint->begin(), vint->end() );
    vint->erase( unique( vint->begin(), vint->end() ), vint->end() );

    return vint;
}


/// \brief find the points that are in a mesh (vector of triangles)
///
/// \param vtri: vector of triangles
/// \param vint: vector of ints
///
/// \return vector of ints: all point indexes that belong to the mesh
/*vI_ref* find_points_in_mesh(vT_ref* vtri, vI_ref* vint)
{
    //vI* vint = new vI;
    for (vT_ref::iterator tit = vtri->begin(); tit != vtri->end(); ++tit)//Tit_ref tit = vtri->begin(); tit != vtri->end(); ++tit)
    {
        if (find(vint->begin(),vint->end(),(*tit)->i) == vint->end())
            vint->emplace_back((*tit)->i);
        if (find(vint->begin(),vint->end(),(*tit)->j) == vint->end())
            vint->emplace_back((*tit)->j);
        if (find(vint->begin(),vint->end(),(*tit)->k) == vint->end())
            vint->emplace_back((*tit)->k);
    }
    //std::cout << vint->size() << std::endl;
    return vint;
}*/


/// \brief select all points that are closer than a certain distance to the plane
///
/// \param p: plane
/// \param mls
/// \param points_in_mesh: vector of ints (remaining points)
/// \param e: max distance
/// \param params
///
/// \return vector of ints: points that are close enough to the plane indexes'
vI* compute_error(Plane p, XMls &mls, vI* points_in_mesh, float e, param params)
{
    //std::clock_t start;
    //start = std::clock();

    vI* points_near_plane = new vI;
    //mls.Load(0);
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    //p.print();
    for (Iit iit = points_in_mesh->begin(); iit != points_in_mesh->end(); ++iit)
    {
        if (p.distance_point_plane(mls.Pworld(0,*iit)-Pivot) < e)
            points_near_plane->emplace_back(*iit);
    }

    //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;

    //mls.Free(0);
    return points_near_plane;
}


/// \brief select all triangles that are closer than a certain distance to the plane
///
/// \param p: plane
/// \param mls
/// \param triangles: vector of triangles (all remaining triangles in the mesh)
/// \param e: max distance
/// \param params
///
/// \return vector of triangles: all triangles that are close enough to the plane
vT* compute_error(Plane p, XMls &mls, vT* triangles, float e, param params)
{
    vT* triangles_near_plane = new vT;
    //XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    for (Tit tit = triangles->begin(); tit != triangles->end(); ++tit)
        if (p.custom_ransac_distance(*tit,mls,params)<e)
            triangles_near_plane->emplace_back(*tit);
    return triangles_near_plane;
}


vI *compute_error(Plane p, vvD* points, float tresh)
{
    vI* close_points = new vI;
    int i = 0;
    for (vvD_iter it = points->begin(); it != points->end(); ++it, ++i)
    {
        if (p.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < tresh)
            close_points->emplace_back(i);
    }
    return close_points;
}


std::pair<vI *,double> compute_error_d(Plane p, vvD* points, float tresh)
{
    vI* close_points = new vI;
    double energy = 0;
    int i = 0;
    for (vvD_iter it = points->begin(); it != points->end(); ++it, ++i)
    {
        if (p.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < tresh*tresh)
        {
            close_points->emplace_back(i);
            energy += p.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) *1/(tresh*tresh);
//            std::cout << p.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) *1 << " " << std::flush;
        }
        else
            energy += 1;///(tresh*tresh);
    }
//    std::cout << close_points->size() << " " << energy << std::endl;
    return std::make_pair(close_points,energy);
}


/// \brief select all vertices of triangles that are close enough to the plane
///
/// \param p: plane
/// \param mls
/// \param triangles: vector of triangles
/// \param e: max distance
/// \param params
///
/// \return vector of ints: all triangles' vertices' indexes that are close enough to the plane
vI* compute_error_pt(Plane p, XMls &mls, vT* triangles, float e, param params)
{
    vI* triangles_near_plane = new vI;
    //XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
    for (Tit tit = triangles->begin(); tit != triangles->end(); ++tit)
    {
        if (p.custom_ransac_distance((*tit).i,mls,params)<e)
            triangles_near_plane->emplace_back(tit->i);
        if (p.custom_ransac_distance((*tit).j,mls,params)<e)
            triangles_near_plane->emplace_back(tit->j);
        if (p.custom_ransac_distance((*tit).k,mls,params)<e)
            triangles_near_plane->emplace_back(tit->k);
    }
    return triangles_near_plane;
}



/// \brief select planes at random and return the one with the highest number of close points
///
/// \param vtri: vector of triangles (mesh)
/// \param n: float: minimum number of points to qualify a plane as 'acceptable'
/// \param k: float: number of tested planes
/// \param e: float: maximum distance
/// \param mls
/// \param params
///
/// \return pair containing the best plane and its closest points' indexes
std::pair<Plane, vI> ransac(vT* vtri, float n, float k, float e, XMls& mls, param params)
{
    std::pair<Plane,vI> best_plane;
    best_plane.first = Plane();
    best_plane.second = {};
    vI* points_in_mesh = new vI;
    find_points_in_mesh(vtri,points_in_mesh);
    for (int i = 0; i < k; ++i)
    {
        Plane P = random_plane_based_on_triangle(vtri,mls,params);
        //P.print();
        vI* points_near_plane = compute_error(P,mls,points_in_mesh,e,params);
        if (points_near_plane->size() > n && points_near_plane->size() > best_plane.second.size())
            best_plane = std::make_pair(P,*points_near_plane);
    }
    return best_plane;
}


/// \brief find all points further than the distance allowed
///
/// \param points_in_mesh: vector of ints
/// \param points_near_plane: vector of ints
///
/// \return vector containing all point indexes that not considered close to the plane
vI* find_points_far_from_plane(vI* points_in_mesh, vI* points_near_plane)
{
    vI* points_far_from_plane = new vI;
    for (Iit iit = points_in_mesh->begin(); iit != points_in_mesh->end(); ++iit)
        if (!point_in_vI(points_near_plane,*iit))
            points_far_from_plane->emplace_back(*iit);
    return points_far_from_plane;
}


/// \brief find if a triangle is in a vector of triangles
///
/// \param triangles: vector of triangles
/// \param t: concerned triangle
///
/// \return true if the triangle is in the vector of triangles, else false
bool triangle_in_vT(vT *triangles, Triangle t)
{
    //std::cout << "auiahaaa" << std::endl;
    for (Tit tit = triangles->begin(); tit != triangles->end(); ++tit) if (*tit == t) return true;
    return false;
}


/// \brief compute RANSAC algorithm
///
/// \details
///     -# initialize random
///     -# find points in mesh
///     -# for number of plans
///         -# for each thread
///             -# for number of tries
///                 -# select a plan
///                 -# compute error
///                 -# if the plane is the best one
///                     -# keep it
///             -# store plane
///         -# from each thread best plane, select the best one
///         -# store the 3 points it is based on
///         -# find points remaining in mesh
///
/// \param vtri: vector of triangles: mesh
/// \param np: float: number of planes
/// \param n: minimum number of points per plane
/// \param k: float: number of tested planes per iteration
/// \param e: float: maximum distance
/// \param mls
/// \param params
///
/// \return a pair containing:
///             - vector of 3D points containing all associated triangles coordinates
///             - pair of selected planes and their associated triangles
std::pair<std::vector<XPt3D>,std::vector<std::pair<Plane,Triangle> > > ransacN(vT* vtri, float np, float n, float k, float e, XMls &mls, param params)
{
    mls.Load(0);
    //srand(time(NULL));
    std::vector<XPt3D> points;
    std::vector<std::pair<Plane,Triangle> > best_planes;
    vI* points_in_mesh = new vI;

    //std::clock_t start;
    //start = std::clock();

    points_in_mesh = find_points_in_mesh(vtri,points_in_mesh);

    //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;

    int nb_pts_used = points_in_mesh->size();
    std::cout << nb_pts_used << std::endl;

    for (int nb_plans = 0; nb_plans < np; ++nb_plans)
    {

        // parallel stuff - begin
        typedef std::vector<std::tuple<Plane,Triangle,vI*> > container;
        typedef container::iterator iter;

        auto worker = [&] (iter begin, iter end)
        {
            vI* vi = new vI;
            vi->resize(1);
            std::tuple<Plane,Triangle,vI*> best_plane = std::make_tuple(Plane(),Triangle(),vi);
            for (int i = 0; i < k; ++i)
                    {
                        std::pair<Triangle,Plane> tp = random_plane(vtri,mls,params);
                        Triangle t = tp.first;
                        Plane P = tp.second;

                        //std::clock_t start;
                        //start = std::clock();

                        vI* points_near_plane = compute_error(P,mls,points_in_mesh,e,params);

                        //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;

                        if (points_near_plane->size() > n && points_near_plane->size() > std::get<2>(best_plane)->size())
                            best_plane = std::make_tuple(P,t,points_near_plane);
                    }
            mutx.lock();
            *begin = best_plane;
            mutx.unlock();
        };

        std::vector<std::tuple<Plane,Triangle,vI*> > parallel_best_planes (std::thread::hardware_concurrency());
        std::vector<std::thread> threads(std::thread::hardware_concurrency());
        const int grainsize = parallel_best_planes.size() / std::thread::hardware_concurrency();

        auto work_iter = std::begin(parallel_best_planes);
        for(auto it = std::begin(threads); it != std::end(threads) - 1; ++it)
        {
            *it = std::thread(worker, work_iter, work_iter + grainsize);
            work_iter += grainsize;
        }
        threads.back() = std::thread(worker, work_iter, std::end(parallel_best_planes));

        for(auto&& i : threads)
            i.join();
        // parallel stuff - end


        vI* vi = new vI;
        vi->resize(1);
        std::tuple<Plane,Triangle,vI*> best_plane = std::make_tuple(Plane(),Triangle(),vi);
        for (auto& planes:parallel_best_planes)
            best_plane = std::get<2>(planes)->size() > std::get<2>(best_plane)->size() ? planes : best_plane;

        if (std::get<2>(best_plane)->size() > n)
        {
            //mls.Load(0);
            XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
            nb_pts_used = std::get<2>(best_plane)->size();
            Triangle t = std::get<1>(best_plane);
            XPt3D barycentre = std::get<0>(best_plane).barycenter(t,mls,params);
            XPt3D A = mls.Pworld(0,t.i);
            XPt3D B = mls.Pworld(0,t.j);
            XPt3D C = mls.Pworld(0,t.k);
            A = point_mutation(A,100,Pivot,barycentre);
            B = point_mutation(B,100,Pivot,barycentre);
            C = point_mutation(C,100,Pivot,barycentre);
            points.emplace_back(A);
            points.emplace_back(B);
            points.emplace_back(C);
            best_planes.emplace_back(std::make_pair(Plane(A,B,C),std::get<1>(best_plane)));
            points_in_mesh = find_points_far_from_plane(points_in_mesh,std::get<2>(best_plane));
            std::cout << points_in_mesh->size() << std::endl;
        }
    }
    mls.Free(0);
    return std::make_pair(points,best_planes);
}


/// \brief colorized version
std::tuple<std::vector<XPt3D>,std::vector<std::pair<Plane,Triangle> >,std::vector<std::pair<XPt3D,Color> > > ransacN_colorized(vT* vtri, float np, float n, float k, float e, XMls &mls, param params)
{
    std::vector<std::pair<XPt3D,Color> > colored_points;

    mls.Load(0);
    //srand(time(NULL));
    std::vector<XPt3D> points;
    std::vector<std::pair<Plane,Triangle> > best_planes;
    vI* points_in_mesh = new vI;

    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);

    for (uint i = 0; i != mls.NEcho(0); ++i)
        colored_points.emplace_back(std::make_pair(mls.Pworld(0,i),Color()));

    //std::clock_t start;
    //start = std::clock();

    points_in_mesh = find_points_in_mesh(vtri,points_in_mesh);

    //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;

    int nb_pts_used = points_in_mesh->size();
    std::cout << nb_pts_used << std::endl;

    for (int nb_plans = 0; nb_plans < np; ++nb_plans)
    {

        // parallel stuff - begin
        typedef std::vector<std::tuple<Plane,Triangle,vI*> > container;
        typedef container::iterator iter;

        auto worker = [&] (iter begin, iter end)
        {
            vI* vi = new vI;
            vi->resize(1);
            std::tuple<Plane,Triangle,vI*> best_plane = std::make_tuple(Plane(),Triangle(),vi);
            for (int i = 0; i < k; ++i)
                    {
                        std::pair<Triangle,Plane> tp = random_plane(vtri,mls,params);
                        Triangle t = tp.first;
                        Plane P = tp.second;

                        //std::clock_t start;
                        //start = std::clock();

                        vI* points_near_plane = compute_error(P,mls,points_in_mesh,e,params);

                        //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;

                        if (points_near_plane->size() > n && points_near_plane->size() > std::get<2>(best_plane)->size())
                            best_plane = std::make_tuple(P,t,points_near_plane);
                    }
            mutx.lock();
            *begin = best_plane;
            mutx.unlock();
        };

        std::vector<std::tuple<Plane,Triangle,vI*> > parallel_best_planes (std::thread::hardware_concurrency());
        std::vector<std::thread> threads(std::thread::hardware_concurrency());
        const int grainsize = parallel_best_planes.size() / std::thread::hardware_concurrency();

        auto work_iter = std::begin(parallel_best_planes);
        for(auto it = std::begin(threads); it != std::end(threads) - 1; ++it)
        {
            *it = std::thread(worker, work_iter, work_iter + grainsize);
            work_iter += grainsize;
        }
        threads.back() = std::thread(worker, work_iter, std::end(parallel_best_planes));

        for(auto&& i : threads)
            i.join();
        // parallel stuff - end


        vI* vi = new vI;
        vi->resize(1);
        std::tuple<Plane,Triangle,vI*> best_plane = std::make_tuple(Plane(),Triangle(),vi);
        for (auto& planes:parallel_best_planes)
            best_plane = std::get<2>(planes)->size() > std::get<2>(best_plane)->size() ? planes : best_plane;
        /*for (auto& p:(*std::get<2>(best_plane)))
        {
            XPt3D P = mls.Pworld(0,p);
            double d = std::get<0>(best_plane).distance_point_plane(P-Pivot);
            Color c =  nb_plans == 0 ? Color(255-50*d,0,0) : Color(0,0,255-50*d);
            colored_points[p] = std::make_pair(P,c);
        }*/
        if (std::get<2>(best_plane)->size() > n)
        {
            //mls.Load(0);

            nb_pts_used = std::get<2>(best_plane)->size();
            Triangle t = std::get<1>(best_plane);
            XPt3D barycentre = std::get<0>(best_plane).barycenter(t,mls,params);
            XPt3D A = mls.Pworld(0,t.i);
            XPt3D B = mls.Pworld(0,t.j);
            XPt3D C = mls.Pworld(0,t.k);
            A = point_mutation(A,100,Pivot,barycentre);
            B = point_mutation(B,100,Pivot,barycentre);
            C = point_mutation(C,100,Pivot,barycentre);
            points.emplace_back(A);
            points.emplace_back(B);
            points.emplace_back(C);
            best_planes.emplace_back(std::make_pair(Plane(A,B,C),std::get<1>(best_plane)));
            points_in_mesh = find_points_far_from_plane(points_in_mesh,std::get<2>(best_plane));
            std::cout << points_in_mesh->size() << std::endl;
        }
    }



    /// \note search for nearest plane
    double dm1 = 0;
    double dm2 = 0;
    double dm3 = 0;
    for (uint i = 0; i != mls.NEcho(0); ++i)
    {
        double d1 = best_planes[0].first.distance_point_plane(mls.Pworld(0,i));
        double d2 = best_planes[1].first.distance_point_plane(mls.Pworld(0,i));
        double d3 = best_planes[2].first.distance_point_plane(mls.Pworld(0,i));
        if (d1 > dm1) dm1 = d1;
        if (d2 > dm2) dm2 = d2;
        if (d3 > dm3) dm3 = d3;
    }
    for (uint i = 0; i != mls.NEcho(0); ++i)
    {
        double d1 = best_planes[0].first.distance_point_plane(mls.Pworld(0,i));
        double d2 = best_planes[1].first.distance_point_plane(mls.Pworld(0,i));
        double d3 = best_planes[2].first.distance_point_plane(mls.Pworld(0,i));
        unsigned char r = 255 * (1-std::pow(d1/dm1,1.0/8));
        unsigned char g = 255 * (1-std::pow(d3/dm3,1.0/8));
        unsigned char b = 255 * (1-std::pow(d2/dm2,1.0/8));
        colored_points[i] = std::make_pair(mls.Pworld(0,i),Color(r,g,b));
    }



    mls.Free(0);
    return std::make_tuple(points,best_planes,colored_points);
}




/// \brief segmentation version
std::tuple<
            std::vector<XPt3D>,
            std::vector<std::pair<Plane,Triangle> >,
            std::vector<std::pair<XPt3D,Color> >,
            std::vector<std::pair<XPt3D,Color> > >
ransac_segmentation(vT* vtri, float tresh, float k, float e, XMls &mls, param params)
{
    std::vector<std::pair<XPt3D,Color> > colored_points;
    std::vector<std::pair<XPt3D,Color> > project_points;

    mls.Load(0);
//    srand(time(NULL));
    std::vector<XPt3D> points;
    std::vector<XPt3D> vpoints;
    std::vector<std::pair<Plane,Triangle> > best_planes;
    vI* points_in_mesh = new vI;
    std::vector<Plane> planes;

    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);

    for (uint i = 0; i != mls.NEcho(0); ++i)
    {
        colored_points.emplace_back(std::make_pair(mls.Pworld(0,i)-Pivot,Color()));
        project_points.emplace_back(std::make_pair(mls.Pworld(0,i)-Pivot,Color()));
        vpoints.emplace_back(mls.Pworld(0,i)-Pivot);
    }

    points_in_mesh = find_points_in_mesh(vtri,points_in_mesh);

    int nb_pts_used = points_in_mesh->size();
    std::cout << nb_pts_used << std::endl;

    double old_dmean = 50;
    double dmean = 10;

    /// \note storing energy and number of planes
    ofstream energy;
    energy.open ("energy.csv");

    int nb_iter = 0;
    while (nb_iter < 2)
    //while (old_dmean-dmean > tresh)
    {nb_iter++;
        old_dmean = dmean;

        // parallel stuff - begin
        typedef std::vector<std::tuple<Plane,Triangle,vI*> > container;
        typedef container::iterator iter;

        auto worker = [&] (iter begin, iter end)
        {
            vI* vi = new vI;
            vi->resize(1);
            std::tuple<Plane,Triangle,vI*> best_plane = std::make_tuple(Plane(),Triangle(),vi);
            for (int i = 0; i < k; ++i)
                    {
                        std::pair<Triangle,Plane> tp = random_plane(vtri,mls,params);
                        Triangle t = tp.first;
                        Plane P = tp.second;

                        vI* points_near_plane = compute_error(P,mls,points_in_mesh,e,params);

                        if (points_near_plane->size() > std::get<2>(best_plane)->size())
                            best_plane = std::make_tuple(P,t,points_near_plane);
                    }
            mutx.lock();
            *begin = best_plane;
            mutx.unlock();
        };

        std::vector<std::tuple<Plane,Triangle,vI*> > parallel_best_planes (std::thread::hardware_concurrency());
        std::vector<std::thread> threads(std::thread::hardware_concurrency());
        const int grainsize = parallel_best_planes.size() / std::thread::hardware_concurrency();

        auto work_iter = std::begin(parallel_best_planes);
        for(auto it = std::begin(threads); it != std::end(threads) - 1; ++it)
        {
            *it = std::thread(worker, work_iter, work_iter + grainsize);
            work_iter += grainsize;
        }
        threads.back() = std::thread(worker, work_iter, std::end(parallel_best_planes));

        for(auto&& i : threads)
            i.join();
        // parallel stuff - end


        vI* vi = new vI;
        vi->resize(1);
        std::tuple<Plane,Triangle,vI*> best_plane = std::make_tuple(Plane(),Triangle(),vi);
        for (auto& p:parallel_best_planes)
            best_plane = std::get<2>(p)->size() > std::get<2>(best_plane)->size() ? p : best_plane;

            nb_pts_used = std::get<2>(best_plane)->size();
            Triangle t = std::get<1>(best_plane);
            XPt3D barycentre = std::get<0>(best_plane).barycenter(t,mls,params);
            XPt3D A = mls.Pworld(0,t.i);
            XPt3D B = mls.Pworld(0,t.j);
            XPt3D C = mls.Pworld(0,t.k);
            A = point_mutation(A,100,Pivot,barycentre);
            B = point_mutation(B,100,Pivot,barycentre);
            C = point_mutation(C,100,Pivot,barycentre);
            points.emplace_back(A);
            points.emplace_back(B);
            points.emplace_back(C);
            best_planes.emplace_back(std::make_pair(Plane(A,B,C),std::get<1>(best_plane)));
            points_in_mesh = find_points_far_from_plane(points_in_mesh,std::get<2>(best_plane));
            std::cout << points_in_mesh->size() << std::endl;

        planes.push_back(std::get<0>(best_plane));
        dmean = mean_distance_point_to_plane(mls,planes,params);

        //for (auto& p:planes) p.print();

        energy << planes.size() << "," << dmean << "\n";
    }

    energy.close();

    std::vector<Plane> vp = planes;

//    /// kmeans with planes
//    for (int i = 0; i != 5; i++)    // iterations
//    {
//        std::vector<std::vector<XPt3D> > vplanes(planes.size(),std::vector<XPt3D>(0));
//        for (auto& p3:vpoints)
//        {
//            double mind = 1e9;
//            int bestp = 0;
//            int pos = 0;

//            for (auto& p:vp)
//            {
//                if (p.distance_point_plane(p3) < mind)
//                {
//                    mind = p.distance_point_plane(p3);
//                    bestp = pos;
//                }
//                ++pos;
//            }
//            vplanes.at(bestp).emplace_back(p3);
//        }
//        int p = 0;
//        for (std::vector<std::vector<XPt3D> >::iterator it = vplanes.begin(); it != vplanes.end(); ++it, ++p)
//        {
//            std::cout << it->size() << std::endl;
//            vp[p] = Plane(&(*it));
//        }
//    }

    planes = vp;

    /// \note search for nearest plane
    std::vector<std::pair<Plane,Color> > plane2color;
    for (auto& p:planes)
    {
        unsigned char r = rand() % 255;
        unsigned char g = rand() % 255;
        unsigned char b = rand() % 255;
        plane2color.emplace_back(std::make_pair(p,Color(r,g,b)));
    }
    for (uint i = 0; i != mls.NEcho(0); ++i)
    {
        double dmin = 1e9;
        int pos = 0;
        std::vector<double> distances;
        Plane closest_plane = Plane();
        double closest_distance = 1e9;
        for (auto& p:planes)
        {
            distances.emplace_back(p.distance_point_plane(mls.Pworld(0,i)-Pivot));
            if (p.distance_point_plane(mls.Pworld(0,i)-Pivot) < closest_distance)
            {
                closest_plane = p;
                closest_distance = p.distance_point_plane(mls.Pworld(0,i)-Pivot);
            }
        }
        for (int j = 0; j != distances.size(); ++j)
            if (distances[j]<dmin)
            {
                dmin = distances[j];
                pos = j;
            }
        //std::cout << mls.Pworld(0,i).X - Pivot << " " << closest_plane.project_point_plane(mls.Pworld(0,i) - Pivot).X << std::endl;
        colored_points[i] = std::make_pair(mls.Pworld(0,i) - Pivot,plane2color[pos].second);
        project_points[i] = std::make_pair(closest_plane.project_point_plane(mls.Pworld(0,i) - Pivot),plane2color[pos].second);
    }


    mls.Free(0);
    return std::make_tuple(points,best_planes,colored_points,project_points);
}


/// \brief find bounding box of plane and use it to store it as a polygon
///
/// \param best_planes: vector of pairs of planes and their corresponding points
/// \param mls
///
/// \return vector of triangles containing for each plane, 3 points member of its bounding box
///
/// \warning deprecated
vT store_planes_as_triangles(std::vector<std::pair<Plane, vI> > best_planes, XMls &mls)
{
    vT planes;
    mls.Load(0);
    for (auto& best_plane:best_planes)
    {
        // find bounding box of points close to plane
        Plane P = best_plane.first;
        vI points = best_plane.second;
        vI bounding_box = find_bounding_box(P,points,mls);
        planes.emplace_back(Triangle(bounding_box[0],bounding_box[1],bounding_box[2]));
        planes.emplace_back(Triangle(bounding_box[0],bounding_box[3],bounding_box[2]));
    }
    mls.Free(0);
    return planes;
}



/// \brief find bounding box of a plane, knowing its closest poins
///
/// \param P: plane
/// \param points: vector of ints
/// \param mls
///
/// \return vector containing the bounding box (4 points) of the plane
///
/// \warning deprecated
vI find_bounding_box(Plane P, vI points, XMls &mls)
{
    /**\verbatim
      Y
      ^
      |
      | p4------------p3
      | |             |
      | |             |
      | |             |
      | |             |
      | p1------------p2
      L-------------------->   X
      \endverbatim
     */
    float xmin =  1e9, ymin =  1e9;
    float xmax = -1e9, ymax = -1e9;
    int p1 = 0, p2 = 0, p3 = 0, p4 = 0;
    for (auto& point:points)
    {
        XPt3D p = mls.Pworld(0,point);
        if (p.X < xmin)
        {
            if (p.Y < ymin)
            {
                p1 = point;
                xmin = p.X;
                ymin = p.Y;
            }
            else if (p.Y > ymax)
            {
                p4 = point;
                xmin = p.X;
                ymax = p.Y;
            }
        }
        else if (p.X > xmax)
        {
            if (p.Y < ymin)
            {
                p2 = point;
                xmax = p.X;
                ymin = p.Y;
            }
            else if (p.Y > ymax)
            {
                p3 = point;
                xmax = p.X;
                ymax = p.Y;
            }
        }
    }
    vI bounding_box = {p1,p2,p3,p4};
    return bounding_box;
}


/// \brief associate each plane to its bounding box
///
/// \param best_planes: vector containing pairs of planes and their associated points
/// \param mls
///
/// \return vector of planes, created by their bounding box
///
/// \warning deprecated
//vP store_planes(std::vector<std::pair<Plane, vI> > best_planes, XMls &mls)
//{
//    vP planes;
//    mls.Load(0);
//    for (auto& best_plane:best_planes)
//    {
//        // find bounding box of points close to plane
//        Plane P = best_plane.first;
//        vI points = best_plane.second;
//        vI bounding_box = find_bounding_box(P,points,mls);
//        //Plane P();
//        P.p1 = bounding_box[0];
//        P.p2 = bounding_box[1];
//        P.p3 = bounding_box[2];
//        P.p4 = bounding_box[3];
//        planes.emplace_back(P);
//    }
//    mls.Free(0);
//    return planes;
//}


/// \brief associate each plane to the triangle it is based on
///
/// \param best_planes: vector containing pairs of planes and their associated triangles
/// \param mls
///
/// \return vector of planes, created by the triangles
//vP store_planes(std::vector<std::pair<Plane, Triangle> > best_planes, XMls &mls)
//{
//    vP planes;
//    mls.Load(0);
//    for (auto& best_plane:best_planes)
//    {
//        // find bounding box of points close to plane
//        Plane P = best_plane.first;
//        Triangle t = best_plane.second;
//        //Plane P();
//        P.p1 = t.i;
//        P.p2 = t.j;
//        P.p3 = t.k;
//        //P.p4 = bounding_box[3];
//        planes.emplace_back(P);
//    }
//    mls.Free(0);
//    return planes;
//}


/// \brief computes RANSAC algorithm based on triangles
///
/// \param vtri: vector of triangles (mesh)
/// \param np: float: number of planes
/// \param n: float: minimum number of points per plane
/// \param k: float: number of iterations
/// \param e: float: maximum distance
/// \param mls
/// \param params
///
/// \return pair containing
///             -# vector of 3D points
///             -# vector of pairs of planes and their associated triangles
std::pair<std::vector<XPt3D>, std::vector<std::pair<Plane, Triangle> > > ransacN_from_triangles(vT *vtri, float np, float n, float k, float e, XMls &mls, param params)
{
    mls.Load(0);
    //srand(time(NULL));
    std::vector<XPt3D> points;
    std::vector<std::pair<Plane,Triangle> > best_planes;
    vT* triangles = vtri;
    vI* vp = new vI;
    vp = find_points_in_mesh(vtri,vp);
    int nb_triangles_used = triangles->size();

    for (int nb_plans = 0; nb_plans < np; ++nb_plans)
    {
        Triangle selected_t;
        std::tuple<Plane,Triangle,vT*> best_plane;
        vT* vt = new vT;
        best_plane = std::make_tuple(Plane(),Triangle(0,0,0),vt);
        for (int i = 0; i < k; ++i)
        {
            std::pair<Triangle,Plane> tp = random_plane(vp,mls,params);
            selected_t = tp.first;
            Plane P = tp.second;
            //P.print();
            //tp.first.print();
            vT* triangles_near_plane = compute_error(P,mls,triangles,e,params);
            if (triangles_near_plane->size() > n && triangles_near_plane->size() > std::get<2>(best_plane)->size())
                best_plane = std::make_tuple(P,selected_t,triangles_near_plane);
        }
        std::get<0>(best_plane).print();
        if (std::get<2>(best_plane)->size() > n)
        {
            //mls.Load(0);
            XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);
            nb_triangles_used = std::get<2>(best_plane)->size();
            Triangle t = std::get<1>(best_plane);
            XPt3D barycentre = std::get<0>(best_plane).barycenter(t,mls,params);
            XPt3D A = mls.Pworld(0,t.i);
            XPt3D B = mls.Pworld(0,t.j);
            XPt3D C = mls.Pworld(0,t.k);
            points.emplace_back(A);
            points.emplace_back(B);
            points.emplace_back(C);
            best_planes.emplace_back(std::make_pair(Plane(A,B,C),std::get<1>(best_plane)));
            triangles = find_triangles_far_from_plane(triangles,std::get<2>(best_plane));
            std::cout << triangles->size() << std::endl;
        }
    }
    mls.Free(0);
    return std::make_pair(points,best_planes);
}


/// \brief finds triangles further from the plane than the maximum distance
///
/// \param triangles: vector of triangles (mesh)
/// \param triangles_near_plane: vector of triangles close to selected plane
///
/// \return vector of triangles far from the selected plane
vT* find_triangles_far_from_plane(vT *triangles, vT *triangles_near_plane)
{
    vT* triangles_far_from_plane = new vT;
    for (Tit tit = triangles->begin(); tit != triangles->end(); ++tit)
        if (!triangle_in_vT(triangles_near_plane,*tit))
            triangles_far_from_plane->emplace_back(*tit);
    return triangles_far_from_plane;
}


/// \brief shift point coordinates
///
/// \details this function is used to store triangles on which selected planes are based
/// the points coordinates are shifted in order to increase the triangle size so that it is well visible
///
/// \param point: point to shift coordinates
/// \param mutation: the factor of shifting
/// \param Pivot: point used to remove registration problems
/// \param barycenter: barycenter of the triangle on which the selected plane is based
///
/// \return point which coordinates are shifted
XPt3D point_mutation(XPt3D point, float mutation, XPt3D Pivot, XPt3D barycenter)
{
    point.operator -=(Pivot);
    point.operator -=(barycenter);
    point.operator *=(mutation);
    point.operator +=(barycenter);
    point.operator +=(Pivot);
    return point;
}





std::pair<vI*,std::tuple<Plane,Triangle,vI*> > ransac_parallel_loop(vI* vp, XMls& mls, param params, int nb_iter, float error)
{
    // parallel stuff - begin
    typedef std::vector<std::tuple<Plane,Triangle,vI*> > container;
    typedef container::iterator iter;

    auto worker = [&] (iter begin, iter end)
    {
        vI* vi = new vI;
        vi->resize(1);
        std::tuple<Plane,Triangle,vI*> best_plane = std::make_tuple(Plane(),Triangle(),vi);
        for (int i = 0; i < nb_iter; ++i)
                {
                    std::pair<Triangle,Plane> tp = random_plane(vp,mls,params);
                    Triangle t = tp.first;
                    Plane P = tp.second;

                    vI* points_near_plane = compute_error(P,mls,vp,error,params);

                    if (points_near_plane->size() > std::get<2>(best_plane)->size())
                        best_plane = std::make_tuple(P,t,points_near_plane);
                }
        mutx.lock();
        *begin = best_plane;
        mutx.unlock();
    };

    std::vector<std::tuple<Plane,Triangle,vI*> > parallel_best_planes (std::thread::hardware_concurrency());
    std::vector<std::thread> threads(std::thread::hardware_concurrency());
    const int grainsize = parallel_best_planes.size() / std::thread::hardware_concurrency();

    auto work_iter = std::begin(parallel_best_planes);
    for(auto it = std::begin(threads); it != std::end(threads) - 1; ++it)
    {
        *it = std::thread(worker, work_iter, work_iter + grainsize);
        work_iter += grainsize;
    }
    threads.back() = std::thread(worker, work_iter, std::end(parallel_best_planes));

    for(auto&& i : threads)
        i.join();
    // parallel stuff - end


    vI* vi = new vI;
    vi->resize(1);
    std::tuple<Plane,Triangle,vI*> best_plane = std::make_tuple(Plane(),Triangle(),vi);
    for (auto& p:parallel_best_planes)
        best_plane = std::get<2>(p)->size() > std::get<2>(best_plane)->size() ? p : best_plane;

    vp = find_points_far_from_plane(vp,std::get<2>(best_plane));
    std::cout << vp->size() << std::endl;

    return std::make_pair(vp,best_plane);
}






std::vector<uint32> ransac_from_3d_points(std::vector<std::vector<double> > *points, int nb_planes)
{
    std::vector<uint32> inComponent;
    double tresh = 0.2;

    vP planes {nb_planes,Plane()};

    int nb_pts_min = points->size()/20;
std::cout << "prout" << std::endl;
        // parallel stuff - begin
        typedef std::vector<std::vector<std::tuple<Plane,vI*,double> > > container;
        typedef container::iterator iter;

        auto worker = [&] (iter begin, iter end)
        {
            std::vector<std::tuple<Plane,vI*,double> > best_planes;// = std::make_pair(Plane(),0);
            for (int i = 0; i < 20; ++i)
                    {
                        Plane p = random_plane(points,tresh);

                        std::pair<vI*,double> nb_points_near_plane = compute_error_d(p,points,0.2);
                        //std::cout << nb_points_near_plane.second << std::endl;

                        if (nb_points_near_plane.first->size() > nb_pts_min)
                            best_planes.emplace_back(std::make_tuple(p,nb_points_near_plane.first,nb_points_near_plane.second));
                    }
            mutx.lock();
            *begin = best_planes;
            mutx.unlock();
        };

        std::vector<std::vector<std::tuple<Plane,vI*,double> > > parallel_best_planes (std::thread::hardware_concurrency());
        std::vector<std::thread> threads(std::thread::hardware_concurrency());
        const int grainsize = parallel_best_planes.size() / std::thread::hardware_concurrency();

        auto work_iter = std::begin(parallel_best_planes);
        for(auto it = std::begin(threads); it != std::end(threads) - 1; ++it)
        {
            *it = std::thread(worker, work_iter, work_iter + grainsize);
            work_iter += grainsize;
        }
        threads.back() = std::thread(worker, work_iter, std::end(parallel_best_planes));

        for(auto&& i : threads)
            i.join();
        // parallel stuff - end
//std::cout << "prout" << std::endl;
        std::vector<std::tuple<Plane,vI*,double> > selected_planes;

//        std::tuple<Plane,vI*,double> best_plane;
        double min_energy = 1;
        for (std::vector<std::vector<std::tuple<Plane,vI*,double> > >::iterator it = parallel_best_planes.begin(); it != parallel_best_planes.end(); ++it)
            selected_planes.insert(selected_planes.end(),it->begin(),it->end());

        vvD* observations_temp = new vvD;
        for (int i = 0; i != nb_planes; ++i)
        {
            std::tuple<Plane,vI*,double> best_plane;
            min_energy = 1;
            for (auto plane_and_vi:selected_planes)
            {
                if (std::get<1>(plane_and_vi)->size() > 0)
                if (std::get<2>(plane_and_vi)/(2*std::get<1>(plane_and_vi)->size()) < min_energy)
                {
                    best_plane = plane_and_vi;
                    min_energy = std::get<2>(plane_and_vi);
                    //std::get<0>(best_plane).print();
                    //std::cout << min_energy << std::endl;
                }
            }

            planes[i] = std::get<0>(best_plane);
            std::get<0>(best_plane).print(); std::cout << std::get<1>(best_plane)->size() << std::endl;
//            if (planes[nb_planes] == Plane()) {} else break;
int j = 0;
            std::vector<std::tuple<Plane,vI*,double> > selected_planes_temp = selected_planes;
            selected_planes.clear();
            for (auto plane_and_vi:selected_planes_temp)
            {
                vI* temp = new vI;
                //temp->emplace_back(0);
                //std::cout << std::get<1>(plane_and_vi)->size() << " ";
                for (vI::const_iterator point = std::get<1>(plane_and_vi)->begin(); point != std::get<1>(plane_and_vi)->end(); ++point)
                {//std::cout << *point << " " << std::endl;
                    if (std::find(std::get<1>(best_plane)->begin(),std::get<1>(best_plane)->end(),*point) == std::get<1>(best_plane)->end())
                        temp->emplace_back(*point);
                }
                **&(std::get<1>(plane_and_vi)) = **&temp;
                observations_temp->clear();
                for (int i=0; i != points->size(); ++i)
                {
                    if (    std::find(std::get<1>(plane_and_vi)->begin(), std::get<1>(plane_and_vi)->end(),i) != std::get<1>(plane_and_vi)->end() &&
                            std::find(std::get<1>(best_plane  )->begin(), std::get<1>(best_plane  )->end(),i) == std::get<1>(best_plane  )->end()   )
                        observations_temp->emplace_back(std::vector<double>{points->at(i)[0],points->at(i)[1],points->at(i)[2]});
                }
                std::get<2>(plane_and_vi) = compute_error_d(std::get<0>(plane_and_vi),observations_temp,0.3).second;
                selected_planes.emplace_back(plane_and_vi);
                //
                //std::cout << j++ << std::endl;
                //remove_from_vec((std::get<1>(plane_and_vi)),(std::get<1>(best_plane)));
                //std::cout << std::get<1>(plane_and_vi)->size() << std::endl;
            }

        }

        for (vvD_iter it = points->begin(); it != points->end(); ++it)
        {
            double d_min = 1e9;
            uint32 closest_plane = 0;
            uint32 plane_pos = 0;
            for (auto& plane:planes)
            {
                //std::cout << plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) << " " << plane_pos << std::endl;
                if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                {
                    d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                    closest_plane = plane_pos;
                }
                ++plane_pos;
            }
            inComponent.emplace_back(closest_plane);
        }
//for (auto& c:inComponent) std::cout << c << " " << std::flush;
    return inComponent;
}




std::pair<std::vector<uint32>,uint32> ransac_from_3d_points_unparallelized(vvD *points, vvD* solution, double energy_tresh)
{
    std::vector<uint32> inComponent;
    double tresh = .1;
    int nb_planes = 0;
    double old_energy = points->size()*1e9;
    double new_energy = points->size()*1e8;
    double very_old_energy = old_energy;

    vP planes;
    vP planes_temp;

    int nb_pts_min = points->size()/500;


        vvD* observations_temp = new vvD;
        //while (new_energy / very_old_energy > energy_tresh)
        while ((old_energy - new_energy) / very_old_energy > energy_tresh || nb_planes < 3)
        {
            ++nb_planes;

            planes = planes_temp;
            planes_temp.clear();
            std::vector<std::vector<double> >* points_temp = new vvD;
            //**& points_temp = **&points;
            points_temp->clear();
            for (vvD_iter it = points->begin(); it != points->end(); ++it)
                points_temp->emplace_back(*it);
        for (int i = 0; i != nb_planes; ++i)
        {
//            nb_pts_min = points_temp->size()/10;
            std::vector<std::tuple<Plane,vI*,double> > best_planes;// = std::make_pair(Plane(),0);
            std::vector<std::tuple<Plane,vI*,double> > selected_planes;

            for (int i = 0; i < 1000; ++i)
                    {
                        Plane p = random_plane(points_temp,tresh);

                        std::pair<vI*,double> nb_points_near_plane = compute_error_d(p,points_temp,tresh);

                        if (nb_points_near_plane.first->size() > nb_pts_min)
                            selected_planes.emplace_back(std::make_tuple(p,nb_points_near_plane.first,nb_points_near_plane.second));
                    }


            // finding best plane
            std::tuple<Plane,vI*,double> best_plane;
            double min_energy = 1e49;
            for (auto plane_and_vi:selected_planes)
            {
                if (std::get<1>(plane_and_vi)->size() > 0)
                    if (std::get<2>(plane_and_vi) < min_energy)
                    {
                        best_plane = plane_and_vi;
                        min_energy = std::get<2>(plane_and_vi);
                    }
            }

            if (std::get<0>(best_plane).is_valid())
            planes_temp.emplace_back(std::get<0>(best_plane));

//            std::get<0>(best_plane).print();


            // finding remaining points
            vvD* points_temp_temp = new vvD;
            if (std::get<0>(best_plane).is_valid())
            for (vvD_iter point = points_temp->begin(); point != points_temp->end(); ++point)
            {
                if (std::get<0>(best_plane).distance_point_plane(XPt3D(point->at(0),point->at(1),point->at(2))) > tresh*tresh)
                {
                    std::vector<double> point_temp;
                    point_temp.emplace_back(point->at(0)); point_temp.emplace_back(point->at(1)); point_temp.emplace_back(point->at(2));
                    points_temp_temp->emplace_back(point_temp);
                }
            }
            else **&points_temp_temp = **&points_temp;
            points_temp->clear();

            for (vvD_iter it = points_temp_temp->begin(); it != points_temp_temp->end(); ++it)
            {
                points_temp->emplace_back(std::vector<double>{it->at(0),it->at(1),it->at(2)});
            }

            delete points_temp_temp;

        }


        // refining
        for (int i=0; i != 5; ++i)
        {
            vvP3* vvp3 = new vvP3;
            for (auto& plane:planes_temp)
                vvp3->emplace_back(new vP3);
            for (vvD_iter it = points->begin(); it != points->end(); ++it)
            {
                double d_min = 1e9;
                uint32 closest_plane = 0;
                uint32 plane_pos = 0;
                for (auto& plane:planes_temp)
                {
                    if (plane.is_valid())
                    if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                    {
                        d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                        closest_plane = plane_pos;
                    }
                    ++plane_pos;
                }
                if (closest_plane < vvp3->size())
                vvp3->at(closest_plane)->emplace_back(XPt3D(it->at(0),it->at(1),it->at(2)));
            }
            for (int j=0; j != planes_temp.size(); ++j)
            {
                if (j < vvp3->size())
                planes_temp.at(j) = Plane(vvp3->at(j));
            }
            delete vvp3;
        }


        // computing energy
            old_energy = new_energy;
            new_energy = 0;
            if (nb_planes == 1) very_old_energy = new_energy;
        for (vvD_iter it = points->begin(); it != points->end(); ++it)
        {
            double d_min = 1e9;
            uint32 closest_plane = 0;
            uint32 plane_pos = 0;
            for (auto& plane:planes_temp)
            {
//                plane.print();
                if (plane.is_valid())
                    if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                    {
                        d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                        closest_plane = plane_pos;
                    }
                ++plane_pos;
            }
            new_energy += d_min;
        }
        planes = planes_temp;
        std::cout << old_energy << " " << new_energy << std::endl;
        if (points_temp->size() < 3) break;

        delete points_temp;
        if (nb_planes == 1) {very_old_energy = new_energy; old_energy = new_energy;}
        }


        // computing solution
        solution->clear();
        for (vvD_iter it = points->begin(); it != points->end(); ++it)
        {
            double d_min = 1e9;
            uint32 closest_plane = 0;
            uint32 plane_pos = 0;
            XPt3D projected_point = XPt3D(it->at(0),it->at(1),it->at(2));
            for (auto& plane:planes)
            {
                if (plane.is_valid())
                    if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                    {
                        d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                        closest_plane = plane_pos;
                        projected_point = plane.project_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                    }
                ++plane_pos;
            }
            inComponent.emplace_back(closest_plane);
            solution->emplace_back(std::vector<double>{projected_point.X, projected_point.Y, projected_point.Z});
        }

        delete observations_temp;

    return std::make_pair(inComponent,nb_planes-1);
}







std::pair<std::vector<uint32>,uint32> ransac_from_3d_points_fast(vvD *points, vvD* solution, double energy_tresh)
{
    std::vector<uint32> inComponent;
    double tresh = .1;
    int nb_planes = 0;
    double old_energy = points->size()*10000;
    double new_energy = points->size()*1000;
    double very_old_energy = old_energy;


    vP planes;
    vP planes_temp;

    int nb_pts_min = points->size()/20;


        vvD* observations_temp = new vvD;
        //while (new_energy / very_old_energy > energy_tresh)
        while ((old_energy - new_energy) / very_old_energy > energy_tresh || nb_planes < 3)
        {
            ++nb_planes;

            planes = planes_temp;
            planes_temp.clear();
            std::vector<std::vector<double> >* points_temp = new vvD;
            //**& points_temp = **&points;
            points_temp->clear();
            for (vvD_iter it = points->begin(); it != points->end(); ++it)
                points_temp->emplace_back(*it);
        for (int i = 0; i != nb_planes; ++i)
        {
            nb_pts_min = points_temp->size()/10;

            // parallel stuff - begin
            typedef std::vector<std::vector<std::tuple<Plane,vI*,double> > > container;
            typedef container::iterator iter;

            auto worker = [&] (iter begin, iter end)
            {
                srand(1);
                std::vector<std::tuple<Plane,vI*,double> > best_planes;// = std::make_pair(Plane(),0);
                for (int i = 0; i < 200; ++i)
                        {
                            Plane p = random_plane(points_temp,tresh);

                            std::pair<vI*,double> nb_points_near_plane = compute_error_d(p,points_temp,tresh);

                            if (nb_points_near_plane.first->size() > nb_pts_min)
                                best_planes.emplace_back(std::make_tuple(p,nb_points_near_plane.first,nb_points_near_plane.second));
                        }

                if (best_planes.size() > 0)
                {
                mutx.lock();
                *begin = best_planes;
                mutx.unlock();
                }
            };

            std::vector<std::vector<std::tuple<Plane,vI*,double> > > parallel_best_planes (std::thread::hardware_concurrency());
            std::vector<std::thread> threads(std::thread::hardware_concurrency());
            const int grainsize = parallel_best_planes.size() / std::thread::hardware_concurrency();


            auto work_iter = std::begin(parallel_best_planes);
            for(auto it = std::begin(threads); it != std::end(threads) - 1; ++it)
            {
                *it = std::thread(worker, work_iter, work_iter + grainsize);
                work_iter += grainsize;
            }
            threads.back() = std::thread(worker, work_iter, std::end(parallel_best_planes));

            for(auto&& i : threads)
                i.join();
            // parallel stuff - end

            std::vector<std::tuple<Plane,vI*,double> > selected_planes;

    //        std::tuple<Plane,vI*,double> best_plane;
            for (std::vector<std::vector<std::tuple<Plane,vI*,double> > >::iterator it = parallel_best_planes.begin(); it != parallel_best_planes.end(); ++it)
                selected_planes.insert(selected_planes.end(),it->begin(),it->end());


            // finding best plane
            std::tuple<Plane,vI*,double> best_plane;
            double min_energy = 1e9;
            for (auto plane_and_vi:selected_planes)
            {
                if (std::get<1>(plane_and_vi)->size() > 0)
                    if (std::get<2>(plane_and_vi) < min_energy)
                    {
                        best_plane = plane_and_vi;
                        min_energy = std::get<2>(plane_and_vi);
                    }
            }

            if (std::get<0>(best_plane).is_valid())
            planes_temp.emplace_back(std::get<0>(best_plane));

//            std::get<0>(best_plane).print();


            // finding remaining points
            vvD* points_temp_temp = new vvD;
            if (std::get<0>(best_plane).is_valid())
            for (vvD_iter point = points_temp->begin(); point != points_temp->end(); ++point)
            {
                if (std::get<0>(best_plane).distance_point_plane(XPt3D(point->at(0),point->at(1),point->at(2))) > tresh*tresh)
                {
                    std::vector<double> point_temp;
                    point_temp.emplace_back(point->at(0)); point_temp.emplace_back(point->at(1)); point_temp.emplace_back(point->at(2));
                    points_temp_temp->emplace_back(point_temp);
                }
            }
            else **&points_temp_temp = **&points_temp;
            points_temp->clear();

            for (vvD_iter it = points_temp_temp->begin(); it != points_temp_temp->end(); ++it)
            {
                points_temp->emplace_back(std::vector<double>{it->at(0),it->at(1),it->at(2)});
            }

            delete points_temp_temp;
        }


        // refining
        for (int i=0; i != 5; ++i)
        {
            vvP3* vvp3 = new vvP3;
            for (auto& plane:planes_temp)
                vvp3->emplace_back(new vP3);
            for (vvD_iter it = points->begin(); it != points->end(); ++it)
            {
                double d_min = 1e9;
                uint32 closest_plane = 0;
                uint32 plane_pos = 0;
                for (auto& plane:planes_temp)
                {
                    if (plane.is_valid())
                    if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                    {
                        d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                        closest_plane = plane_pos;
                    }
                    ++plane_pos;
                }
                if (closest_plane < vvp3->size())
                vvp3->at(closest_plane)->emplace_back(XPt3D(it->at(0),it->at(1),it->at(2)));
            }
            for (int j=0; j != planes_temp.size(); ++j)
            {
                if (j < vvp3->size())
                planes_temp.at(j) = Plane(vvp3->at(j));
            }
            delete vvp3;
        }


        // computing energy
            old_energy = new_energy;
            new_energy = 0;
            if (nb_planes == 1) very_old_energy = new_energy;
        for (vvD_iter it = points->begin(); it != points->end(); ++it)
        {
            double d_min = 1e9;
            uint32 closest_plane = 0;
            uint32 plane_pos = 0;
            for (auto& plane:planes_temp)
            {
//                plane.print();
                if (plane.is_valid())
                    if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                    {
                        d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                        closest_plane = plane_pos;
                    }
                ++plane_pos;
            }
            new_energy += d_min;
        }
        planes = planes_temp;
        std::cout << old_energy << " " << new_energy << std::endl;
        if (points_temp->size() < 3) break;

        delete points_temp;
        if (nb_planes == 1) {very_old_energy = new_energy; old_energy = new_energy;}
        }


        // computing solution
        solution->clear();
        for (vvD_iter it = points->begin(); it != points->end(); ++it)
        {
            double d_min = 1e9;
            uint32 closest_plane = 0;
            uint32 plane_pos = 0;
            XPt3D projected_point = XPt3D(it->at(0),it->at(1),it->at(2));
            for (auto& plane:planes)
            {
                if (plane.is_valid())
                    if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                    {
                        d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                        closest_plane = plane_pos;
                        projected_point = plane.project_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                    }
                ++plane_pos;
            }
            inComponent.emplace_back(closest_plane);
            solution->emplace_back(std::vector<double>{projected_point.X, projected_point.Y, projected_point.Z});
        }

        delete observations_temp;

    return std::make_pair(inComponent,nb_planes-1);
}




std::pair<Plane, Plane> ransac2_from_3d_points_unparallelized(vvD *points)
{

    std::pair<Plane,Plane> planes2;
    float tresh = .5;
    int nb_planes = 2;

    vP planes;
    vP planes_temp;

    int nb_pts_min = points->size()/100;

        vvD* observations_temp = new vvD;

            planes = planes_temp;
            planes_temp.clear();
            std::vector<std::vector<double> >* points_temp = new vvD;
            points_temp->clear();
            for (vvD_iter it = points->begin(); it != points->end(); ++it)
                points_temp->emplace_back(*it);

        for (int i = 0; i != nb_planes; ++i)
        {
//            planes_temp.resize(nb_planes, Plane());
            nb_pts_min = points_temp->size()/100;
//            std::vector<std::tuple<Plane,vI*,double> > best_planes;
            std::vector<std::tuple<Plane,vI*,double> > selected_planes;

            if (points_temp->size() >= 3)
            for (int i = 0; i < 30; ++i)
                    {
                        Plane p = random_plane(points_temp,tresh);

                        std::pair<vI*,double> nb_points_near_plane = compute_error_d(p,points_temp,tresh);

                        if (nb_points_near_plane.first->size() > nb_pts_min)
                            selected_planes.emplace_back(std::make_tuple(p,nb_points_near_plane.first,nb_points_near_plane.second));
                    }
            else selected_planes.emplace_back(std::make_tuple(Plane(),new vI,1e9));


            // finding best plane
            std::tuple<Plane,vI*,double> best_plane;
            double min_energy = 1e9;
            for (auto plane_and_vi:selected_planes)
            {
                if (std::get<1>(plane_and_vi)->size() > 0)
                    if (std::get<2>(plane_and_vi) < min_energy)
                    {
                        best_plane = plane_and_vi;
                        min_energy = std::get<2>(plane_and_vi);
                    }
            }

            if (std::get<0>(best_plane).is_valid())
            planes_temp.emplace_back(std::get<0>(best_plane));

//            std::get<0>(best_plane).print();


            // finding remaining points
            vvD* points_temp_temp = new vvD;
            if (std::get<0>(best_plane).is_valid())
            for (vvD_iter point = points_temp->begin(); point != points_temp->end(); ++point)
            {
                if (std::get<0>(best_plane).distance_point_plane(XPt3D(point->at(0),point->at(1),point->at(2))) > tresh*tresh)
                {
                    std::vector<double> point_temp;
                    point_temp.emplace_back(point->at(0)); point_temp.emplace_back(point->at(1)); point_temp.emplace_back(point->at(2));
                    points_temp_temp->emplace_back(point_temp);
                }
            }
            else **&points_temp_temp = **&points_temp;
            points_temp->clear();

            for (vvD_iter it = points_temp_temp->begin(); it != points_temp_temp->end(); ++it)
            {
                points_temp->emplace_back(std::vector<double>{it->at(0),it->at(1),it->at(2)});
            }

            delete points_temp_temp;
        }



        // refining
        if (planes_temp.size() > 0)
        for (int i=0; i != 5; ++i)
        {
            std::vector<vP3>* vvp3 = new std::vector<vP3>;
            for (auto& plane:planes_temp)
            {
                vvp3->emplace_back(vP3());        // memory leak ?
            }
            for (vvD_iter it = points->begin(); it != points->end(); ++it)
            {
                double d_min = 1e9;
                uint32 closest_plane = 0;
                uint32 plane_pos = 0;
                for (auto& plane:planes_temp)
                {
                    if (plane.is_valid())
                    if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                    {
                        d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                        closest_plane = plane_pos;
                    }
                    ++plane_pos;
                }
//                if (closest_plane < vvp3->size())
                vvp3->at(closest_plane).emplace_back(XPt3D(it->at(0),it->at(1),it->at(2)));
            }
            for (int j=0; j != planes_temp.size(); ++j)
            {
//                if (j < vvp3->size())
                planes_temp.at(j) = Plane(&vvp3->at(j));
            }
            delete vvp3;
        }

        ++nb_planes;
        delete points_temp;

        if (planes_temp.size() > 0)
        {
            planes2.first = planes_temp[0];
            planes2.second = planes_temp[1];
        }
        else
        {
            planes2.first = Plane();
            planes2.second = Plane();
        }

        delete observations_temp;

    return planes2;
}




std::pair<Plane, Plane> ransac2_from_3d_points_fast(vvD *points)
{

    std::pair<Plane,Plane> planes2;
    float tresh = .1;
    int nb_planes = 2;

    vP planes;
    vP planes_temp;

    int nb_pts_min = points->size()/20;

        vvD* observations_temp = new vvD;

            planes = planes_temp;
            planes_temp.clear();
            std::vector<std::vector<double> >* points_temp = new vvD;
            points_temp->clear();
            for (vvD_iter it = points->begin(); it != points->end(); ++it)
                points_temp->emplace_back(*it);

        for (int i = 0; i != nb_planes; ++i)
        {
            nb_pts_min = points_temp->size()/10;

            // parallel stuff - begin
            typedef std::vector<std::vector<std::tuple<Plane,vI*,double> > > container;
            typedef container::iterator iter;

            auto worker = [&] (iter begin, iter end)
            {
                srand(1);
                std::vector<std::tuple<Plane,vI*,double> > best_planes;// = std::make_pair(Plane(),0);
                for (int i = 0; i < 20; ++i)
                        {
                            Plane p = random_plane(points_temp,tresh);

                            std::pair<vI*,double> nb_points_near_plane = compute_error_d(p,points_temp,tresh);

                            if (nb_points_near_plane.first->size() > nb_pts_min)
                                best_planes.emplace_back(std::make_tuple(p,nb_points_near_plane.first,nb_points_near_plane.second));
                        }

                if (best_planes.size() > 0)
                {
                mutx.lock();
                *begin = best_planes;
                mutx.unlock();
                }
            };

            std::vector<std::vector<std::tuple<Plane,vI*,double> > > parallel_best_planes (std::thread::hardware_concurrency());
            std::vector<std::thread> threads(std::thread::hardware_concurrency());
            const int grainsize = parallel_best_planes.size() / std::thread::hardware_concurrency();


            auto work_iter = std::begin(parallel_best_planes);
            for(auto it = std::begin(threads); it != std::end(threads) - 1; ++it)
            {
                *it = std::thread(worker, work_iter, work_iter + grainsize);
                work_iter += grainsize;
            }
            threads.back() = std::thread(worker, work_iter, std::end(parallel_best_planes));

            for(auto&& i : threads)
                i.join();
            // parallel stuff - end

            std::vector<std::tuple<Plane,vI*,double> > selected_planes;

    //        std::tuple<Plane,vI*,double> best_plane;
            for (std::vector<std::vector<std::tuple<Plane,vI*,double> > >::iterator it = parallel_best_planes.begin(); it != parallel_best_planes.end(); ++it)
                selected_planes.insert(selected_planes.end(),it->begin(),it->end());



            // finding best plane
            std::tuple<Plane,vI*,double> best_plane;
            double min_energy = 1e9;
            for (auto plane_and_vi:selected_planes)
            {
                if (std::get<1>(plane_and_vi)->size() > 0)
                    if (std::get<2>(plane_and_vi) < min_energy)
                    {
                        best_plane = plane_and_vi;
                        min_energy = std::get<2>(plane_and_vi);
                    }
            }

            if (std::get<0>(best_plane).is_valid())
            planes_temp.emplace_back(std::get<0>(best_plane));

//            std::get<0>(best_plane).print();


            // finding remaining points
            vvD* points_temp_temp = new vvD;
            if (std::get<0>(best_plane).is_valid())
            for (vvD_iter point = points_temp->begin(); point != points_temp->end(); ++point)
            {
                if (std::get<0>(best_plane).distance_point_plane(XPt3D(point->at(0),point->at(1),point->at(2))) > tresh*tresh)
                {
                    std::vector<double> point_temp;
                    point_temp.emplace_back(point->at(0)); point_temp.emplace_back(point->at(1)); point_temp.emplace_back(point->at(2));
                    points_temp_temp->emplace_back(point_temp);
                }
            }
            else **&points_temp_temp = **&points_temp;
            points_temp->clear();

            for (vvD_iter it = points_temp_temp->begin(); it != points_temp_temp->end(); ++it)
            {
                points_temp->emplace_back(std::vector<double>{it->at(0),it->at(1),it->at(2)});
            }

            delete points_temp_temp;
        }



        // refining
        if (planes_temp.size() > 0)
        for (int i=0; i != 5; ++i)
        {
            std::vector<vP3>* vvp3 = new std::vector<vP3>;
            for (auto& plane:planes_temp)
            {
                vvp3->emplace_back(vP3());        // memory leak ?
            }
            for (vvD_iter it = points->begin(); it != points->end(); ++it)
            {
                double d_min = 1e9;
                uint32 closest_plane = 0;
                uint32 plane_pos = 0;
                for (auto& plane:planes_temp)
                {
                    if (plane.is_valid())
                    if (plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2))) < d_min)
                    {
                        d_min = plane.distance_point_plane(XPt3D(it->at(0),it->at(1),it->at(2)));
                        closest_plane = plane_pos;
                    }
                    ++plane_pos;
                }
//                if (closest_plane < vvp3->size())
                vvp3->at(closest_plane).emplace_back(XPt3D(it->at(0),it->at(1),it->at(2)));
            }
            for (int j=0; j != planes_temp.size(); ++j)
            {
//                if (j < vvp3->size())
                planes_temp.at(j) = Plane(&vvp3->at(j));
            }
            delete vvp3;
        }

        ++nb_planes;
        delete points_temp;

        if (planes_temp.size() > 0)
        {
            planes2.first = planes_temp[0];
            planes2.second = planes_temp[1];
        }
        else
        {
            planes2.first = Plane();
            planes2.second = Plane();
        }

        delete observations_temp;

    return planes2;
}

#include "libSteph/Color.h"

Color::Color()
{
    red = 255;
    green = 255;
    blue = 255;
}

Color::Color(unsigned char r, unsigned char g, unsigned char b)
{
    red = r;
    green = g;
    blue = b;
}

Color::~Color()
{

}


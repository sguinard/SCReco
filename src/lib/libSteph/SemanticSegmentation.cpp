#include "libSteph/SemanticSegmentation.h"

double mean_distance_point_to_plane(XMls& mls, std::vector<Plane> planes, param params)
{
    double mean_distance = 0;
    XPt3D Pivot(params.pivot_E,params.pivot_N,params.pivot_H);

    for (uint i = 0; i != mls.NEcho(0); ++i)
    {
        double d = 1e5;
        for (auto& plane:planes)
            if (plane.distance_point_plane(mls.Pworld(0,i)-Pivot) < d)
                d = plane.distance_point_plane(mls.Pworld(0,i)-Pivot);
        mean_distance += d;
    }
    return mean_distance/mls.NEcho(0);
}

double quadratic_distance_point_point(XPt3D p1, XPt3D p2)
{
    return (std::pow(p1.X - p2.X,2) + std::pow(p1.Y - p2.Y,2) + std::pow(p1.Z - p2.Z,2));
}

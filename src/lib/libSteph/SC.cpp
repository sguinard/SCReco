#include "libSteph/SC.h"

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

SCReco::SCReco()
{

}

SCReco::SCReco(std::vector<Triangle> t, std::vector<std::tuple<uint, uint, uint> > e, std::vector<uint> p)
{
    SCReco SC;
    SC.ve = e;
    SC.vt = t;
    SC.vp = p;
}

std::vector<uint> SCReco::points_used()
{
    std::vector<uint> pu;
    for (auto& tri:vt)
    {
        pu.emplace_back(tri.i);
        pu.emplace_back(tri.j);
        pu.emplace_back(tri.k);
    }
    for (auto& e:ve)
    {
        pu.emplace_back(std::get<1>(e));
        pu.emplace_back(std::get<2>(e));
    }
    std::sort(pu.begin(),pu.end());
    pu.erase(unique(pu.begin(),pu.end()),pu.end());
    return pu;
}

void SCReco::find_triangles_and_edges(std::vector<std::map<uint, std::vector<SCR::edge> > > data, XMls &mls)
{
     std::tuple<std::vector<std::vector<Triangle> >,std::vector<std::tuple<uint,uint,uint> >,std::vector<std::vector<uint> > > out = SCR::compute_mesh_and_segments(data,mls);
     SCReco SC = SCReco(std::get<0>(out)[0],std::get<1>(out),std::get<2>(out)[0]);
}




#include "libSteph/MTriCoords.h"

XPt3D pmin(XPt3D p1, XPt3D p2)
{
  return sqrt(p1.X*p1.X + p1.Y*p1.Y + p1.Z*p1.Z) < sqrt(p2.X*p2.X + p2.Y*p2.Y + p2.Z*p2.Z) ? p1 : p2;
}

XPt3D pmax(XPt3D p1, XPt3D p2)
{
  return sqrt(p1.X*p1.X + p1.Y*p1.Y + p1.Z*p1.Z) < sqrt(p2.X*p2.X + p2.Y*p2.Y + p2.Z*p2.Z) ? p2 : p1;
}

MTriCoords::MTriCoords()
{

}

MTriCoords::MTriCoords(VTC* vtc)
{
  std::cout << "building map of tri coords" << std::endl;
  M.clear();
  for (VTC::iterator it = vtc->begin(); it != vtc->end(); ++it)
    { //std::cout << it->p1().X << " " << it->p2().X << " " << it->p3().X << std::endl;
      //std::cout << pmin(it->p1(),it->p2()).X << " " << pmin(it->p1(),it->p3()).X << " " << pmin(it->p2(),it->p3()).X << std::endl;
      pPP pPP1 = std::make_pair(pmin(it->p1(),it->p2()),pmax(it->p1(),it->p2()));
      pPP pPP2 = std::make_pair(pmin(it->p1(),it->p3()),pmax(it->p1(),it->p3()));
      pPP pPP3 = std::make_pair(pmin(it->p2(),it->p3()),pmax(it->p2(),it->p3()));
      //std::cout << &pPP1 << " " << &pPP2 << " " << &pPP3 << std::endl;
      M[pPP1].emplace_back(*it);
      M[pPP2].emplace_back(*it);
      M[pPP3].emplace_back(*it);
      if (std::make_pair(pmin(it->p1(),it->p2()), pmax(it->p1(),it->p2())) == std::make_pair(pmin(it->p1(),it->p3()), pmax(it->p1(),it->p3()))) std::cout << "meme cle " << std::flush;
      //std::cout << M[std::make_pair(std::min(it->p2(),it->p3(),cmpP()), std::max(it->p2(),it->p3(),cmpP()))].size() << std::endl;
    }
  std::cout << M.size() << std::endl;
  for (MTriCoords::iterator mit = M.begin(); mit != M.end(); ++mit)
    {std::cout << mit->first.first.X << " " << mit->first.second.X  <<  " - " << mit->second[3].p1().X << " " << mit->second[3].p2().X << " " << mit->second[3].p3().X << std::endl;
      std::cout << mit->second.size() << " " << std::flush;
      std::sort(mit->second.begin(), mit->second.end());
	std::cout << mit->second.begin()->p1().X << " " << std::flush;
        mit->second.erase( unique( mit->second.begin(), mit->second.end() ), mit->second.end() );
    }
  std::cout << "i'm ok" << std::endl;
}

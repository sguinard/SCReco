#include "libSteph/VSA.h"
#include "stdlib.h"
#include <ctime>
#include <algorithm>
#include <functional>
#include <array>

/*VSA::VSA()
{
	_tresh = -1;
}

VSA::VSA(uint32_t nb_regions, double tresh, vP3* points, vT* triangles)
{
	_nb_regions = nb_regions;
	_tresh = tresh;
	_component = std::vector<int32_t>(triangles->size(), -1);
        _points = (const vP3)(*points);
	_triangles = (const vT)(*triangles);
	for (uint32_t i = 0; i != _triangles.size(); ++i)
	  for (uint32_t j = i+1; j != _triangles.size(); ++j)
	    if (adjacent(_triangles[i],_triangles[j]))
	      tri_adj.emplace_back(std::make_pair(i,j));
}

VSA::~VSA()
{

}*/

bool VSA::is_valid()
{
	return (_tresh > 0 && _triangles.size() > 0) ? true : false;
}

void VSA::solve()
{
  double err_0 = iteration(true);
  double new_err = err_0;
  double old_err = err_0;
  for (uint32_t i = 0; i < _tresh; ++i)
  //while ( (new_err / err_0) >= _tresh)
    {
      old_err = new_err;
      new_err = iteration();
      std::cout << "Error evolution: " << err_0 << " " << old_err << " " << new_err << " " << _tresh << std::endl;
      }
  std::cout << "\nERROR FROM POINTS TO THEIR PROJECTIONS: " << compute_error_point() << std::endl;
}

double VSA::iteration(bool first_iter)
{
	double err = 0.;
	if (first_iter)
	{
	  std::cout << "VSA 1st iteration" << std::endl;
	  std::cout << "finding " << _nb_regions << " regions at random" << std::endl;
		_proxies = std::vector<Proxy>(_nb_regions,Proxy());
		// select k triangles at random
		for (uint32_t i = 0; i != _nb_regions; ++i)
		{
			uint32_t t_index = rand() % _triangles.size();
			while (this->_component[t_index] != -1)
				t_index = rand() % _triangles.size();
			this->_component[t_index] = i;
			//std::cout << t_index << " " << this->_component[t_index] << std::endl;
			const Triangle t = _triangles[t_index];
			vP3* points = new vP3;
			const XPt3D p1 = _points[t.i];
			const XPt3D p2 = _points[t.j];
			const XPt3D p3 = _points[t.k];
			points->emplace_back(p1); points->emplace_back(p2); points->emplace_back(p3);
			this->_proxies[i] = Proxy(points);
			delete points;
		}
		std::cout << "found k regions and computed their proxies" << std::endl;
		// compute proxy for each triangle
		propagation();
		std::cout << "propagated" << std::endl;
		err = compute_error_all();
		std::cout << err << std::endl;
	}
	else
	{
		// find nearest triangles for each proxy
	  for (int i = 0; i != _nb_regions; ++i)
	    {
	      vT* vtri = new vT;
	      for (uint32_t idx = 0; idx != _component .size(); ++idx)
		if (_component[idx] == i)
		  vtri->emplace_back(_triangles[idx]);
	      /*double min_e = 1e9;
	      Triangle best_tri = Triangle();
	      for (auto& t: *vtri)
		if (compute_error(i, t) < min_e)
		  {
		    min_e = compute_error_l21(i, t);
		    best_tri = t;
		    }*/
		  // proxy re-initialization
		  vP3* points = new vP3;
		  for (auto& tri: *vtri)
		    {
		      const XPt3D p1 = _points[tri.i];
		      const XPt3D p2 = _points[tri.j];
		      const XPt3D p3 = _points[tri.k];
		      points->emplace_back(p1); points->emplace_back(p2); points->emplace_back(p3);
		    }
		  this->_proxies[i] = Proxy(points);
		  double min_e = 1e9;
		  Triangle best_tri = Triangle();
		  for (auto& t: *vtri)
		    if (compute_error_l21(i, &t) < min_e)
		      {
			min_e = compute_error_l21(i, &t);
			best_tri = t;
		      }
		  for (uint32_t idx = 0; idx != _component .size(); ++idx)
		if (_component[idx] == i and _triangles[idx] != best_tri)
		  _component[idx] = -1;
	    }
		// growing region on these triangles
	      propagation();
		std::cout << "propagated" << std::endl;
		err = compute_error_all();
		std::cout << err << std::endl;
	}

	return err;
}


bool sortbyerr (std::pair<double,std::pair<uint32_t,uint32_t> > pa, std::pair<double,std::pair<uint32_t,uint32_t> > pb)
{ return pa.first < pb.first; }

void VSA::propagation()
{std::clock_t start; start = clock();
  double total_time = 0;
  std::vector<std::pair<double,std::pair<uint32_t, int32_t> > >* queue = new std::vector<std::pair<double,std::pair<uint32_t, int32_t> > >; // error, <index, comp>
  queue->reserve(_triangles.size());
  //for (auto& p: _proxies)
    //  std::cout << p.NX() << " " << p.NY() << " " << p.NZ() << std::endl;
  //std::clock_t prout = clock();
  for (uint32_t i = 0; i != _component.size(); ++i)
    {
      if (_component[i] != -1)
	{//start = clock();
	  vT* new_tri = new vT;
	  new_tri->reserve(100);// here we assume that a single triangle is not adjacent to more than 100 triangles ...
	  for (auto& edge: tri_adj)
	    {
	      if (edge.first == i and _component[edge.second] == -1)
		new_tri->emplace_back(_triangles[edge.second]);
	      else if (edge.second == i and _component[edge.first] == -1)
		new_tri->emplace_back(_triangles[edge.first]);
	    }
	  for (auto& t: *new_tri)
	    queue->insert(queue->begin(), std::make_pair(compute_error_l2(_component[i],&t), std::make_pair(uint32_t(std::find(_triangles.begin(), _triangles.end(),t) - _triangles.begin()), _component[i])));
	  delete new_tri;
	  //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
	}
    }
  //std::cout << "Time: " << (std::clock() - prout) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
  // sorting vector of errs
  //std::sort(queue->begin(), queue->end());
  //queue.erase(unique (queue.begin(), queue.end()), queue.end());
  //std::cout << "queue size: " << queue->size() << std::endl;
  //for (auto& q: *queue)
  //  std::cout << q.first << " " << q.second.first << " " << q.second.second << std::endl;
  int nb_iter = 0;
  while (std::find(_component.begin(), _component.end(), -1) != _component.end() && queue->size() > 0)
    {++nb_iter;
      std::vector<std::pair<double,std::pair<uint32_t, int32_t> > >::iterator it = std::min_element(queue->begin(), queue->end());
      std::clock_t start;
      start = clock();
      uint32_t i = it->second.first;//queue->at(0).second.first;
      uint32_t r = it->second.second;
      queue->erase(it);
      //std::cout << i << " " <<  it->first << " " << it->second.second << " - " << std::flush;
      if (_component[i] == -1)
	{//std::cout << "caca" << std::endl;
	  //std::clock_t c = clock();
	  _component[i] = r;//queue->at(0).second.second;
	  //std::cout << "duuuuuuuuuur" << std::endl;
	  vT* new_tri = new vT;
	  new_tri->reserve(100);
	  const Triangle t = _triangles[i];
	  std::vector<uint32_t>* v1 = new std::vector<uint32_t> (M[t.i]);
	  std::vector<uint32_t>* v2 = new std::vector<uint32_t> (M[t.j]);
	  std::vector<uint32_t>* v3 = new std::vector<uint32_t> (M[t.k]);
	  /*std::unordered_set<uint32_t>* s = new std::unordered_set<uint32_t>;
	  for (auto& p: *v1)
	    s->insert(p);
	  for (auto& p: *v2)
	    s->insert(p);
	  for (auto& p: *v3)
	    s->insert(p);
	    v1->assign( s->begin(), s->end() );*/
	  v1->reserve(v1->size() + v2->size() + v3->size());
	  v1->insert(v1->end(), v2->begin(), v2->end());
	  v1->insert(v1->end(), v3->begin(), v3->end());
	  std::sort(v1->begin(), v1->end());
	  v1->erase( unique(v1->begin(), v1->end()), v1->end());
	  //std::cout << "weeeee " << new_tri->size() << std::endl;
	  for (auto& idx : *v1)
	    if (adjacent(&t, &_triangles[idx]) && _component[idx] == -1)
	      new_tri->insert(new_tri->begin(),_triangles[idx]);
	  /*for (auto& edge: tri_adj)
	    {
	      if (edge.first == i and _component[edge.second] == -1)
		new_tri->emplace_back(_triangles[edge.second]);
	      else if (edge.second == i and _component[edge.first] == -1)
		new_tri->emplace_back(_triangles[edge.first]);
		}*/
	  //std::cout << "Adding " << new_tri->size() << " triangles" << std::endl; // only 2 ?
	  //queue->erase(it);
	  //c = clock();
	  //std::cout << new_tri->size() << " " << std::flush;
	  for (auto& tri: *new_tri)
	    queue->insert(queue->begin(), std::make_pair(compute_error_l2(_component[i],&tri), std::make_pair(tri_idx[std::tuple<uint32_t,uint32_t, uint32_t>(tri.i, tri.j, tri.k)]/*std::find(_triangles.begin(), _triangles.end(),tri) - _triangles.begin()*/, _component[i])));
	  //c = clock();
	  //uint32_t ttttt = std::find(_triangles.begin(), _triangles.end(),t) - _triangles.begin();
	  delete new_tri;
	  //std::find(_component.begin(), _component.end(), -1);
	  //std::cout << "Time: " << (std::clock() - c) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
	}

      //queue->erase(it/*queue->begin()*/);
	  //queue.shrink_to_fit();

	  //std::sort(queue->begin(), queue->end());
	  //queue.erase(unique (queue.begin(), queue.end()), queue.end());
	  //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;

    }
  std::cout << nb_iter << " iterations in " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
  std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
}

double VSA::compute_error_all()
{
	double err = 0.;
	uint32_t i = 0;
	//for (auto& p: _proxies)
	//  std::cout << p.X() << " " << p.Y() << " " << p.Z() << std::endl;
	for (auto& t: _triangles)
	{
	  //const XPt3D centroid = ((_points[t.i].operator +=(_points[t.j]).operator +=(_points[t.k])).operator /=(3));
		//std::cout << i << " " << std::flush;
		//std::cout << _component[i] << " - " << std::flush;
		//if (_component[i] < _triangles.size())
		  {
		    //Proxy p = _proxies[_component[i]];
		//std::cout << err << " " << std::flush;
		    err += compute_error_l2(_component[i], const_cast<Triangle*>(&t));++i;
		  }
	}
	return err;
}

double VSA::compute_error_l2(int32_t id_reg, Triangle* t)
{//std::clock_t c = clock();
  double err = -1;
  //std::cout << id_reg << " " << std::flush;
  if (id_reg == -1 /*&& t.is_valid()*/)
    return 0;
  err = 0;
  //std::cout << id_reg << " " << std::flush;
  Proxy prox = _proxies[id_reg];
  // distance from each point to the proxy
  const XPt3D p1 = _points[t->i];
  const double d1_ = (prox.NX()*p1.X + prox.NY()*p1.Y + prox.NZ()*p1.Z + prox.D()) / (prox.NX()*prox.NX() + prox.NY()*prox.NY() + prox.NZ()*prox.NZ());
  const XPt3D p2 = _points[t->j];
  const double d2_ = (prox.NX()*p2.X + prox.NY()*p2.Y + prox.NZ()*p2.Z + prox.D()) / (prox.NX()*prox.NX() + prox.NY()*prox.NY() + prox.NZ()*prox.NZ());
  const XPt3D p3 = _points[t->k];
  const double d3_ = (prox.NX()*p3.X + prox.NY()*p3.Y + prox.NZ()*p3.Z + prox.D()) / (prox.NX()*prox.NX() + prox.NY()*prox.NY() + prox.NZ()*prox.NZ());
  const double p = 0.5 * (d2(p1,p2) + d2(p1,p3) + d2(p2,p3));
  const double area = sqrt(p * (p-d2(p1,p2)) * (p-d2(p1,p3)) * (p-d2(p2,p3)));
  err = (d1_*d1_ + d2_*d2_ + d3_*d3_ + d1_*d2_ + d1_*d3_ + d2_*d3_) * area;
  err /= 6;
  //std::cout << prox.NX() << " " << prox.NY() << " " << prox.NZ() << " - " << std::flush;
  //std::cout << "Triangle : " << _points[t.i].X << " " << _points[t.i].Y << " " << _points[t.i].Z << " " << _points[t.j].X << " " << _points[t.j].Y << " " << _points[t.j].Z << " " << _points[t.k].X << " " << _points[t.k].Y << " " << _points[t.k].Z << std::endl;
  //std::cout << d1_ << " " << d2_ << " " << d3_ << " " << p << " " << area << " " << err << std::endl;
  /*XPt3D centroid = ((_points[t.i].operator +=(_points[t.j]).operator +=(_points[t.k])).operator /=(3));
  Proxy p = _proxies[_component[id_reg]];
  err += std::pow(centroid.X - p.X(),2);
  err += std::pow(centroid.Y - p.Y(),2);
  err += std::pow(centroid.Z - p.Z(),2);*/
  //std::cout << "Time: " << (std::clock() - c) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
  return err;
}


double VSA::compute_error_l21(uint32_t id_reg, Triangle* t)
{//std::clock_t c = clock();
  double err = -1;
  //if (id_reg != -1 /*&& t.is_valid()*/)
  //  return err;
  err = 0;
  if (id_reg == -1) return 0;
  Proxy prox = _proxies[id_reg];
  // distance from each point to the proxy
  vP3* pts = new vP3({_points[t->i],_points[t->j],_points[t->k]});
  Plane tri_p = Plane(pts);
  err = std::pow( abs(tri_p.X()) - abs(prox.NX()) ,2 ) + std::pow( abs(tri_p.Y()) - abs(prox.NY()) ,2 ) + std::pow( abs(tri_p.Z()) - abs(prox.NZ()) ,2 );
  //std::cout << "Triangle: " << _points[t.i].X << " " << _points[t.i].Y << " " << _points[t.i].Z << " " << _points[t.j].X << " " << _points[t.j].Y << " " << _points[t.j].Z << " " << _points[t.k].X << " " << _points[t.k].Y << " " << _points[t.k].Z << " " << err << " " << tri_p.X() << " " << tri_p.Y() << " " << tri_p.Z() << " " << prox.NX() << " " << prox.NY() << " " << prox.NZ() << std::endl;
  //std::cout << "Time: " << (std::clock() - c) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
  return err;
}




double VSA::compute_error_point()
{
  double err = 0;

  for (uint32_t p_idx = 0; p_idx != _points.size(); ++p_idx)//(auto& p_orig : _points)
        {
	  XPt3D p_proj = _points[p_idx];
	  XPt3D p_orig = _points[p_idx];
	  //uint32_t reg = _component[i];//_component[std::find(_points.begin(), _points.end(), p_orig) - _points.begin()];
	  //std::cout << reg << " " << std::flush;
	  std::vector<uint32_t> tris = M[p_idx];
	  std::vector<uint32_t> regs;
	  for (auto& t: tris)
	    regs.emplace_back(_component[t]);
	  //std::cout << regs.size() << " " ;
	  std::sort (regs.begin(), regs.end());
	  regs.erase( unique (regs.begin(), regs.end()), regs.end());
	  //std::cout << regs.size() << std::endl;

	  uint32_t comp = -1;
	  if (regs.size() == 1 && regs[0] != -1)
	    {
	      comp = regs[0];
	      uint32_t r = regs[0];
	      Plane plane(_proxies[r].NX(), _proxies[r].NY(), _proxies[r].NZ(), _proxies[r].D());
	      p_proj = plane.project_point_plane(p_orig);
	      //plane.print();
	      //std::cout << p_orig.X << " " << p_orig.Y << " " << p_orig.Z << " - " << p.X << " " << p.Y << " " << p.Z << std::endl;
	    }
	  else if (regs.size() > 1)
	    {
	      double min_nrg = 1e9;
	      uint32_t best_proxy = -1;
	      for (auto& r: regs)
		{
		  if (r != -1)
		    {
		      Plane plane(_proxies[r].NX(), _proxies[r].NY(), _proxies[r].NZ(), _proxies[r].D());
		      XPt3D proj = plane.project_point_plane(p_orig);
		      double d = d2(p_orig,p_proj);
		      if (d < min_nrg)
			{
			  min_nrg = d;
			  best_proxy = r;
			  comp = r;
			}
		      //p.operator +=(plane.project_point_plane(p_orig));
		    }
		  //else p.operator +=(p_orig);
		}
	      Plane plane(_proxies[best_proxy].NX(), _proxies[best_proxy].NY(), _proxies[best_proxy].NZ(), _proxies[best_proxy].D());
	      p_proj = plane.project_point_plane(p_orig);
	      //p.operator /=(regs.size());
	    }
	  else
	    {
	      double min_nrg = 1e9;
	      uint32_t best_proxy = -1;
	      uint32_t current_proxy = 0;
	      for (auto& p: _proxies)
		{
		  Plane plane(p.NX(), p.NY(), p.NZ(), p.D());
		  XPt3D proj = plane.project_point_plane(p_orig);
		  double d = d2(p_orig,p_proj);
		  if (d < min_nrg)
		    {
		      min_nrg = d;
		      best_proxy = current_proxy;
		    }
		  ++current_proxy;
		}
	      Plane plane(_proxies[best_proxy].NX(), _proxies[best_proxy].NY(), _proxies[best_proxy].NZ(), _proxies[best_proxy].D());
	      p_proj = plane.project_point_plane(p_orig);
	      //std::cout << min_nrg << " " << best_proxy << " " << d2(p_proj, p_orig) << " - " << std::flush;
	      }
	  err += d2(p_proj, p_orig);
        }
  
  return err;
}





std::vector<std::vector<std::pair<Triangle, Color> > > VSA::color_points()
{
    std::vector<std::vector<std::pair<Triangle, Color> > > points_colored;

    //std::cout << "Number of points: " << solution.size() << std::endl;

    std::vector<std::pair<uint32,Color> > comp_color;
    uint32_t n_comp = 0;
    for (int i=0; i != (int)_component.size(); i++)
      if (_component[i] > n_comp) n_comp = _component[i];
      
    for (uint32 i=0; i != n_comp/*(int)_component.size()*/; i++)
    {
        //std::cout << inComponent[i] << " " << std::flush;
        /*bool found_comp = false;
        for (std::vector<std::pair<uint32,Color> >::iterator it = comp_color.begin(); it != comp_color.end(); it++)
        {
            if (it->first == _component[i])
                found_comp = true;
        }
        if (!found_comp)*/
        {
            unsigned char r = rand() % 255;
            unsigned char g = rand() % 255;
            unsigned char b = rand() % 255;
            comp_color.push_back(std::make_pair(i,Color(r,g,b)));
        }
    }

    points_colored.resize(1);
    //int i=0;
    for (int p_index = 0; p_index != (int)_triangles.size(); p_index++)
    {
      Color color_of_comp;
            //std::cout << solution[p_index][0] << " " << solution[p_index][1] << " " << solution[p_index][2] << " " << inComponent[p_index] << std::endl;
            for (std::vector<std::pair<uint32,Color> >::iterator it = comp_color.begin(); it != comp_color.end(); it++)
            {
                if (it->first == _component[p_index])
                {
		  //std::cout << it->first << " " << static_cast<unsigned> ( it->second.red ) << " " << static_cast<unsigned> ( it->second.green ) << " " << static_cast<unsigned> ( it->second.blue ) << std::endl;

                    color_of_comp = it->second;
                    break;
                }
            }

            //std::cout << i << " " << std::flush;
            points_colored[0].emplace_back(std::make_pair(_triangles[p_index],color_of_comp));
    }
    std::cout << "Yay! I painted points     :hap:" << std::endl;
    return points_colored;
}



void VSA::store()
{

  int n_vertex = _points.size();

  std::vector<Color> colors;
  for (int i = 0; i != _nb_regions; ++i)
    colors.emplace_back(Color(rand()%255,rand()%255,rand()%255));

    std::string out = "../../";
    ofstream fileOut(out+"vsa.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + out + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property double x" << endl;
    fileOut << "property double y" << endl;
    fileOut << "property double z" << endl;
    fileOut << "element face " << _triangles.size() << endl;
    fileOut << "property list uchar int vertex_index" << endl;
    fileOut << "property uchar red" << endl;
    fileOut << "property uchar green" << endl;
    fileOut << "property uchar blue" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(double);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;

    int i = 0;
    for (auto& p : _points)
        {
            Write<double>(it,p.X);
            Write<double>(it,p.Y);
            Write<double>(it,p.Z);
        }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);


    unsigned int triangle_bytesize = sizeof(unsigned char) + 3*sizeof(int) + 3*sizeof(unsigned char);
    unsigned long triangle_buffer_size = triangle_bytesize * _triangles.size();

    char * tri_buffer = new char[triangle_buffer_size], * tri_it = tri_buffer;

    for (int i = 0; i != _triangles.size(); ++i)
      {
	Triangle t = _triangles[i];
	Write<unsigned char>(tri_it,3);
	Write<int>(tri_it,t.i);
	Write<int>(tri_it,t.j);
	Write<int>(tri_it,t.k);
	Write<unsigned char>(tri_it,colors[_component[i]].red);
	Write<unsigned char>(tri_it,colors[_component[i]].green);
	Write<unsigned char>(tri_it,colors[_component[i]].blue);
      }

    cout << "Writing " << _triangles.size() << " triangles of size " << triangle_buffer_size << "=" << 1.e-6*triangle_buffer_size << "MB" << endl;
    fileOut.write(tri_buffer, triangle_buffer_size);
    
    
    cout << "Total " << 1.e-6*(vertex_buffer_size)+1.e-6*(triangle_buffer_size) << "MB" << endl;
    fileOut.close();

}




/// \warning DO NOT USE
/// Problem not solved : a triangle has 3 points so a single point may belong to 3 triangles / regions
/// - how do we project it ?
void VSA::store_projections()
{

  int n_vertex = _points.size();

  std::vector<Color> colors;
  for (int i = 0; i != _nb_regions; ++i)
    colors.emplace_back(Color(rand()%255,rand()%255,rand()%255));

    std::string out = "../../";
    ofstream fileOut(out+"vsa_projection.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + out + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property double x" << endl;
    fileOut << "property double y" << endl;
    fileOut << "property double z" << endl;
    fileOut << "element face " << _triangles.size() << endl;
    fileOut << "property list uchar int vertex_index" << endl;
    fileOut << "property uchar red" << endl;
    fileOut << "property uchar green" << endl;
    fileOut << "property uchar blue" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(double);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;

    int i = 0;
    for (uint32_t p_idx = 0; p_idx != _points.size(); ++p_idx)//(auto& p_orig : _points)
        {
	  XPt3D p_orig = _points[p_idx];
	  //uint32_t reg = _component[i];//_component[std::find(_points.begin(), _points.end(), p_orig) - _points.begin()];
	  //std::cout << reg << " " << std::flush;
	  std::vector<uint32_t> tris = M[p_idx];
	  std::vector<uint32_t> regs;
	  for (auto& t: tris)
	    regs.emplace_back(_component[t]);
	  //std::cout << regs.size() << " " ;
	  std::sort (regs.begin(), regs.end());
	  regs.erase( unique (regs.begin(), regs.end()), regs.end());
	  //std::cout << regs.size() << std::endl;
	  
	  XPt3D p = XPt3D(0,0,0);
	  if (regs.size() == 1 && regs[0] != -1)
	    {
	      uint32_t r = regs[0];
	      Plane plane(_proxies[r].NX(), _proxies[r].NY(), _proxies[r].NZ(), _proxies[r].D());
	      p = plane.project_point_plane(p_orig);
	      //plane.print();
	      //std::cout << p_orig.X << " " << p_orig.Y << " " << p_orig.Z << " - " << p.X << " " << p.Y << " " << p.Z << std::endl;
	    }
	  if (regs.size() > 1)
	    {
	      double min_nrg = 1e9;
	      uint32_t best_proxy = -1;
	      for (auto& r: regs)
		{
		  if (r != -1)
		    {
		      Plane plane(_proxies[r].NX(), _proxies[r].NY(), _proxies[r].NZ(), _proxies[r].D());
		      XPt3D proj = plane.project_point_plane(p_orig);
		      double d = d2(p_orig,p);
		      if (d < min_nrg)
			{
			  min_nrg = d;
			  best_proxy = r;
			}
		      //p.operator +=(plane.project_point_plane(p_orig));
		    }
		  //else p.operator +=(p_orig);
		}
	      Plane plane(_proxies[best_proxy].NX(), _proxies[best_proxy].NY(), _proxies[best_proxy].NZ(), _proxies[best_proxy].D());
	      p = plane.project_point_plane(p_orig);
	      //p.operator /=(regs.size());
	    }
	  if (p == XPt3D(0,0,0)) p = p_orig;
	  //std::cout << p_orig.X << " " << p_orig.Y << " " << p_orig.Z << " - " << p.X << " " << p.Y << " " << p.Z << std::endl;
            Write<double>(it,p.X);
            Write<double>(it,p.Y);
            Write<double>(it,p.Z);
	    
        }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);


    unsigned int triangle_bytesize = sizeof(unsigned char) + 3*sizeof(int) + 3*sizeof(unsigned char);
    unsigned long triangle_buffer_size = triangle_bytesize * _triangles.size();

    char * tri_buffer = new char[triangle_buffer_size], * tri_it = tri_buffer;

    for (int i = 0; i != _triangles.size(); ++i)
      {
	Triangle t = _triangles[i];
	Write<unsigned char>(tri_it,3);
	Write<int>(tri_it,t.i);
	Write<int>(tri_it,t.j);
	Write<int>(tri_it,t.k);
	Write<unsigned char>(tri_it,colors[_component[i]].red);
	Write<unsigned char>(tri_it,colors[_component[i]].green);
	Write<unsigned char>(tri_it,colors[_component[i]].blue);
      }

    cout << "Writing " << _triangles.size() << " triangles of size " << triangle_buffer_size << "=" << 1.e-6*triangle_buffer_size << "MB" << endl;
    fileOut.write(tri_buffer, triangle_buffer_size);
    
    
    cout << "Total " << 1.e-6*(vertex_buffer_size)+1.e-6*(triangle_buffer_size) << "MB" << endl;
    fileOut.close();

}




void VSA::store_points_colored()
{

  int n_vertex = _points.size();

  std::vector<Color> colors;
  for (int i = 0; i != _nb_regions; ++i)
    colors.emplace_back(Color(rand()%255,rand()%255,rand()%255));

    std::string out = "../../";
    ofstream fileOut(out+"vsa_points_colored.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + out + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property double x" << endl;
    fileOut << "property double y" << endl;
    fileOut << "property double z" << endl;
    fileOut << "property float component" << endl;
    //fileOut << "property uchar red" << endl;
    //fileOut << "property uchar green" << endl;
    //fileOut << "property uchar blue" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(double) + sizeof(float);//3*sizeof(unsigned char);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;

    int i = 0;
    for (uint32_t p_idx = 0; p_idx != _points.size(); ++p_idx)//(auto& p_orig : _points)
        {
	  XPt3D p_orig = _points[p_idx];
	  //uint32_t reg = _component[i];//_component[std::find(_points.begin(), _points.end(), p_orig) - _points.begin()];
	  //std::cout << reg << " " << std::flush;
	  std::vector<uint32_t> tris = M[p_idx];
	  std::vector<uint32_t> regs;
	  regs.clear();
	  for (auto& t: tris)
	    regs.emplace_back(_component[t]);
	  if (tris.size() != regs.size()) std::cout << tris.size() << " " << regs.size() << " - " ;
	  std::sort (regs.begin(), regs.end());
	  regs.erase( unique (regs.begin(), regs.end()), regs.end());
	  //std::cout << regs.size() << std::endl;
	  //if (tris.size() != regs.size()) std::cout << tris.size() << " " << regs.size() << " - " ;

	  float comp = -1;
	  XPt3D p = XPt3D(0,0,0);
	  if (regs.size() == 1 && regs[0] != -1)
	    {
	      comp = regs[0];
	      uint32_t r = regs[0];
	      Plane plane(_proxies[r].NX(), _proxies[r].NY(), _proxies[r].NZ(), _proxies[r].D());
	      p = plane.project_point_plane(p_orig);
	      //plane.print();
	      //std::cout << p_orig.X << " " << p_orig.Y << " " << p_orig.Z << " - " << p.X << " " << p.Y << " " << p.Z << std::endl;
	    }
	  if (regs.size() > 1)
	    {
	      double min_nrg = 1e9;
	      uint32_t best_proxy = -1;
	      for (auto& r: regs)
		{
		  if (r != -1)
		    {
		      Plane plane(_proxies[r].NX(), _proxies[r].NY(), _proxies[r].NZ(), _proxies[r].D());
		      XPt3D proj = plane.project_point_plane(p_orig);
		      double d = d2(p_orig,p);
		      if (d < min_nrg)
			{
			  min_nrg = d;
			  best_proxy = r;
			  comp = r;
			}
		      //p.operator +=(plane.project_point_plane(p_orig));
		    }
		  //else p.operator +=(p_orig);
		}
	      Plane plane(_proxies[best_proxy].NX(), _proxies[best_proxy].NY(), _proxies[best_proxy].NZ(), _proxies[best_proxy].D());
	      p = plane.project_point_plane(p_orig);
	      //p.operator /=(regs.size());
	    }
	  if (p == XPt3D(0,0,0)) p = p_orig;
	  //std::cout << p_orig.X << " " << p_orig.Y << " " << p_orig.Z << " - " << p.X << " " << p.Y << " " << p.Z << std::endl;
            Write<double>(it,p.X);
            Write<double>(it,p.Y);
            Write<double>(it,p.Z);
	    //std::cout << comp << " " << std::flush;
	    if (comp != -1)
	      {
		Write<float>(it,comp);
		//Write<unsigned char>(it,colors[_component[comp]].red);
		//Write<unsigned char>(it,colors[_component[comp]].green);
		//Write<unsigned char>(it,colors[_component[comp]].blue);
	      }
	    else
	      {
		Write<float>(it,-1);
		//Write<unsigned char>(it,colors[0].red);
		//Write<unsigned char>(it,colors[0].green);
		//Write<unsigned char>(it,colors[0].blue);
		}
        }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    
    
    cout << "Total " << 1.e-6*(vertex_buffer_size) << "MB" << endl;
    fileOut.close();

}

#include "libSteph/Proxy.h"

Proxy::Proxy()
{
    this->_x = XPt3D(0,0,0);
    this->_n = XPt3D(0,0,0);
    this->_d = 0.;
}

Proxy::Proxy(XPt3D x, XPt3D n)
{
    this->_x = x;
    this->_n = n;
    this->_d = - (x.X*n.X + x.Y*n.Y + x.Z*n.Z);
}

Proxy::Proxy(vP3 *points)
{
  //std::cout << points->at(0).X << " " << points->at(1).X << " " << points->at(2).X << std::endl;
  XPt3D c = XPt3D(0,0,0);
  for (auto& p: *points)
    c.operator +=(p);
  c.operator /=(points->size());
  this->_x = c;
  Plane p = Plane(points);
    this->_n = XPt3D(p.X(), p.Y(), p.Z());
    this->_d = - (_x.X*_n.X + _x.Y*_n.Y + _x.Z*_n.Z);
    //std::cout << _n.X << " " << _n.Y << " " << _n.Z << " " << c.X << " " << c.Y << " " << c.Z << std::endl;
}

double Proxy::X()
{
    return this->_x.X;
}

double Proxy::Y()
{
    return this->_x.Y;
}

double Proxy::Z()
{
    return this->_x.Z;
}

double Proxy::NX()
{
    return this->_n.X;
}

double Proxy::NY()
{
    return this->_n.Y;
}

double Proxy::NZ()
{
    return this->_n.Z;
}

double Proxy::D()
{
	return this->_d;
}

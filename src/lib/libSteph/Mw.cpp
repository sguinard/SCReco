#include "libSteph/Mw.h"

/// \brief default constructor
Mw::Mw()
{

}

/// \brief constructor from map of wedges
Mw::Mw(std::vector<Wedge> vw)
{
    M.clear();
    for (auto & w:vw)
    {
        //w.print();
        M[w.e1].emplace_back(w);
        M[w.e2].emplace_back(w);
        M[w.e3].emplace_back(w);
        M[w.e4].emplace_back(w);
    }
    for (Mw::iterator mit = M.begin(); mit != M.end(); ++mit)
    {
        //std::cout << mit->second.size() << " " << std::flush;
        std::sort(mit->second.begin(), mit->second.end());
        mit->second.erase( unique( mit->second.begin(), mit->second.end() ), mit->second.end() );
        //for (std::vector<Wedge>::iterator wit = mit->second.begin(); wit != mit->second.end(); ++wit) wit->print();
    }
}

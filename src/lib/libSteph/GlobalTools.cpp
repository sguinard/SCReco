#include "libSteph/GlobalTools.h"

/// \file GlobalTools.cpp
///
/// \brief file with function definitions from GlobalTools.h


/// \brief find if a point is in a vector of ints
///
/// \param points: vector containing point indexes
/// \param p: the index of the concerned point
///
/// \return true if p is in points, else false
bool point_in_vI(vI* points, int p)
{
    for (Iit iit = points->begin(); iit != points->end(); ++iit) if (*iit == p) return true;
    return false;
}


/// \brief search if a point is a vertex of a triangle
///
/// \param t: triangle
/// \param p: index of point
///
/// \return true if p is a vertex of t, else false
///
/// @note hesitate to monoline it ... (just kidding, that's the kind of things that makes you go to hell)
bool search_point_in_triangle(Triangle t, uint p)
{
    return t.i == p ? true :
                      t.j == p ? true :
                                 t.k == p ? true :
                                            false;
}

/// @warning sounds good, doesn't work ...
bool find_edge(std::tuple<uint,uint,uint> ENIT, int b, int p)
{
    /*for (std::vector<std::tuple<uint,uint,uint> >::iterator it = ENIT.begin(); it != ENIT.end(); ++it) /// @note find a way to iterate on tuple ...
    {
        if (std::get<0>(*it) == b && (std::get<1>(*it) == p || std::get<2>(*it) == p)) return true;
    }
    return false;*/
}

/// \brief find if a point is in a vector of triangles
///
/// \param v_tri: vector of triangles
/// \param p: point index
///
/// \return true if p is in v_tri, else false
///
/// \note i was prolly drunk when is named this function
bool find_edge(std::vector<Triangle> v_tri, uint p)
{
    //return std::binary_search(v_tri.begin(), v_tri.end(), p, search_point_in_triangle) ? true : false;
    for (std::vector<Triangle>::iterator it = v_tri.begin(); it != v_tri.end(); ++it)
        if (it->i == p || it->j == p || it->k == p)
            return true;
    return false;
}

/// \brief find if an edge is in a vector of triangles
///
/// \param vt: vector of triangles
/// \param e: edge
///
/// \return true if e is in vt, else false
bool find_edge_in_triangles(std::vector<Triangle>* vt, SCR::edge e)
{
    //std::clock_t start;
    //start = std::clock();
    for (std::vector<Triangle>::iterator it = vt->begin(); it != vt->end(); ++it)
    {
        int i = it->i, j=it->j, k=it->k, n1=e.node_begin, n2=e.node_end;
        if (

                (   i == n1 && (j == n2 || k == n2)  ) ||
                (   j == n1 && (i == n2 || k == n2)  ) ||
                (   k == n1 && (j == n2 || i == n2)  )

                //find_edge_in_triangle(*it,e)
                ) return true;
    }
    //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
    return false;
}

/// \brief find if an edge is in a triangle
///
/// \param vt: triangle
/// \param e: edge
///
/// \return true if e is n vt, else false
bool find_edge_in_triangle(Triangle vt, SCR::edge e)
{
    return
            (   vt.i == e.node_begin && (vt.j == e.node_end || vt.k == e.node_end)  ) ||
            (   vt.j == e.node_begin && (vt.i == e.node_end || vt.k == e.node_end)  ) ||
            (   vt.k == e.node_begin && (vt.j == e.node_end || vt.i == e.node_end)  ) ?
                true : false ;
}


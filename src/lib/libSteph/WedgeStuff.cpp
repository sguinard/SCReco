#include "libSteph/WedgeStuff.h"

#include "thread"
#include "mutex"
#include <ctime>

std::mutex mtx;

/// @brief find all adjacent wedges of a vector of wedges, given a specific wedge
///
/// @param Mw M : map of wedges
/// @param Wedge w : wedge to search adjacency
///
/// @return std::vector<Wedge> : vector containing all adjacent wedges of w
std::vector<std::pair<Wedge,edge_number> > find_wedge_m(const Mw* M, Wedge w)
{
    std::vector<std::pair<Wedge,edge_number> > adjacency;
    std::vector<Wedge> vw;

    //std::clock_t start;
    //start = std::clock();
    for (const auto& wit:(*M)[w.e1])
    {
        //std::clock_t    start;
        //start = std::clock();
        edge_number e = w.place_adjacent(wit); /// @note 0.01 ms
        /*w.print();
        wit.print();
        std::cout << e << std::endl;*/
        //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
        //std::clock_t start;
        //start = std::clock();
        if (e != edge_number::None && (adjacency.empty() || find(vw.begin(),vw.end(),wit) != vw.end())) /// @note 0.001 ms
        {
            /*w.print();
            wit.print();*/

            /*std::cout <<" " << e << " ";
            std::cout << std::endl;*/

            adjacency.emplace_back(std::make_pair(wit,e));
            vw.emplace_back(wit);
        }
        //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
    }
    //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
    for (const auto& wit:(*M)[w.e2])
    {
        edge_number e = w.place_adjacent(wit);
        /*w.print();
        wit.print();
        std::cout << e << std::endl;*/
        if (e != edge_number::None && (adjacency.empty() || find(vw.begin(),vw.end(),wit) == vw.end()))
        {
            adjacency.emplace_back(std::make_pair(wit,e));
            vw.emplace_back(wit);
        }
    }
    for (const auto& wit:(*M)[w.e3])
    {
        edge_number e = w.place_adjacent(wit);
        /*w.print();
        wit.print();
        std::cout << e << std::endl;*/
        if (e != edge_number::None && (adjacency.empty() || find(vw.begin(),vw.end(),wit) == vw.end()))
        {
            /*w.print();
            wit.print();*/

            /*std::cout <<" " << e << " ";
            std::cout << std::endl;*/

            adjacency.emplace_back(std::make_pair(wit,e));
            vw.emplace_back(wit);
        }
    }
    for (const auto& wit:(*M)[w.e4])
    {
        edge_number e = w.place_adjacent(wit);
        /*w.print();
        wit.print();
        std::cout << e << std::endl;*/
        if (e != edge_number::None && (adjacency.empty() || find(vw.begin(),vw.end(),wit) == vw.end()))
        {
            /*w.print();
            wit.print();*/

            /*std::cout <<" " << e << " ";
            std::cout << std::endl;*/

            adjacency.emplace_back(std::make_pair(wit,e));
            vw.emplace_back(wit);
        }
    }
    //std::cout << std::endl;
    //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
    return adjacency;
}


/// @brief test parallelize function
/*std::vector<Wedge> check_continuity(XMls & mls, std::vector<Wedge> vw)
{

    std::vector<Wedge> svw;

    int n=0;

    for (auto& wit:vw)
    {++n;
        std::vector<std::pair<Wedge,edge_number> > adjacency = find_wedge_m(M,wit); /// @note time this function and optimise if time > 0.05s
        //adjacency.erase( unique( adjacency.begin(), adjacency.end() ), adjacency.end() );

        XPt3D normal = compute_normal(mls,wit); /// @note same here
        //std::cout << adjacency.size() << " " << std::flush;

        /// @note compute each normal for adjacency wedges
        ///       and find if semi-contunuity in 2 orthogonal directions

        if (adjacency.size() > 0)
        {//std::cout << n << " " << std::flush;
            std::vector<XPt3D> adjacency_normals;
            for (auto & pair_adjacent_wedge:adjacency) adjacency_normals.emplace_back(compute_normal(mls,pair_adjacent_wedge.first));

            /// @note compute angle between normals and look for semi-continuity
            int i = 0;
            std::vector<edge_number> semi_continuity;
            for (auto & adjacent_normal:adjacency_normals)
            {
                if (abs(normal.X * adjacent_normal.X + normal.Y * adjacent_normal.Y + normal.Z * adjacent_normal.Z) > 0.5) // keep wedge
                    semi_continuity.emplace_back(adjacency[i].second);
                ++i;
            }

            if ( (  find(semi_continuity.begin(),semi_continuity.end(),edge_number::Left)   != semi_continuity.end() && (
                    find(semi_continuity.begin(),semi_continuity.end(),edge_number::Top)    != semi_continuity.end() ||
                    find(semi_continuity.begin(),semi_continuity.end(),edge_number::Bottom) != semi_continuity.end()    )) ||
                 (  find(semi_continuity.begin(),semi_continuity.end(),edge_number::Right)  != semi_continuity.end() && (
                    find(semi_continuity.begin(),semi_continuity.end(),edge_number::Top)    != semi_continuity.end() ||
                    find(semi_continuity.begin(),semi_continuity.end(),edge_number::Bottom) != semi_continuity.end()    ))
                 )
                svw.emplace_back(wit);
        }

        if (n%100 == 0)
            std::cout << n << " " << std::flush;

    }

    return svw;

}*/


/// @brief Compute equivalent of c1 regularity for edge, but here for wedges
///        i.e. we compute wedge normals and look for semi-continuity with neighbourhing wedges
///        if a wedge is semi-continuus in 2 different directions we keep it
///        otherwise we remove it
///
/// @param std::vector<Wedge> vector containing all possible wedges
///
/// @return std::vector<Wedge> vector containing all remaining wedges
std::vector<Wedge> wedge_thresholding(XMls & mls, std::vector<Wedge> vw, double omega)
{
    mls.Load(0);
    std::vector<Wedge> svw;

    std::cout << "vw" << " " << vw.size() << std::endl;

    typedef std::vector<Wedge> container;
    typedef container::const_iterator iter;

    container v = vw;

    const Mw* M = new const Mw(vw);

    auto worker = [&] (iter begin, iter end) {
        int n = 0;
        for (auto& wit:v)
        {++n;

            mtx.lock();

            std::vector<std::pair<Wedge,edge_number> > adjacency = find_wedge_m(M,wit);

            XPt3D normal = compute_normal(mls,wit); /// @note same here
            //std::cout << adjacency.size() << " " << std::flush;

            mtx.unlock();

            /// @note compute each normal for adjacency wedges
            ///       and find if semi-contunuity in 2 orthogonal directions



            if (adjacency.size() >= 2)
            {//std::cout << n << " " << std::flush;
                std::vector<XPt3D> adjacency_normals;
                for (auto & pair_adjacent_wedge:adjacency) adjacency_normals.emplace_back(compute_normal(mls,pair_adjacent_wedge.first));

                /// @note compute angle between normals and look for semi-continuity
                int i = 0;
                std::vector<edge_number> semi_continuity;
                semi_continuity.clear();
                //mtx.lock();
                for (auto & adjacent_normal:adjacency_normals)
                {
                    //std::cout << adjacency[i].second << "-" << std::flush;
                    //std::cout << abs(normal.X * adjacent_normal.X + normal.Y * adjacent_normal.Y + normal.Z * adjacent_normal.Z) << " " << std::flush;
                    if (abs(normal.X * adjacent_normal.X + normal.Y * adjacent_normal.Y + normal.Z * adjacent_normal.Z) < omega) // keep wedge
                    {
                        semi_continuity.emplace_back(adjacency[i].second);
                        //std::cout << adjacency[i].second << " " << std::flush;
                    }
                    ++i;
                }

                bool pair_c = false;
                bool impair_c = false;
                for (auto& c:semi_continuity)
                {
                    //std::cout << c << " " << std::flush;
                    if (c%2 == 0)
                        pair_c = true;
                    if (c%2 == 1)
                        impair_c = true;
                }
                //std::cout << std::endl;
                if (/*semi_continuity.size() >= 2*/pair_c && impair_c)
                {
                    //std::cout << "a" << std::flush;
                    mtx.lock();
                    svw.emplace_back(wit);
                    mtx.unlock();
                }
            }

            //if (n%1000 == 0)
            //    std::cout << n << " " << std::flush;
        }



      };

    // parallel
    std::vector<std::thread> threads(8);
    const int grainsize = v.size() / 8;

    auto work_iter = std::begin(v);
    for(auto it = std::begin(threads); it != std::end(threads) - 1; ++it)
    {
        *it = std::thread(worker, work_iter, work_iter + grainsize);
        work_iter += grainsize;
    }
    threads.back() = std::thread(worker, work_iter, std::end(v));

    for(auto&& i : threads)
        i.join();

    delete M;

    std::cout << "svw : " << svw.size() << std::endl;

    mls.Free(0);

    return svw;
}


/// @brief function for the conversion of quadruplets of self-connected edges into wedges
///
/// @param v_edges: vector containing map of edges for each acquisition block
/// @param mls
///
/// @return vector of wedges
std::vector<Wedge>* convert_edge_list_to_wedge(std::vector<std::map<uint,std::vector<SCR::edge> >* >* v_edges, XMls& mls)
{
    std::map<uint,std::vector<SCR::edge> >* edges = v_edges->operator [](0);

    int PPL = mls.PulsePerLine();
    std::cout << "PPL : " << PPL << std::endl;

    //std::sort( edges.begin(), edges.end() );
    //edges.erase( unique( edges.begin(), edges.end() ), edges.end() );

    std::vector<Wedge>* vw = new std::vector<Wedge>;
    //std::map<uint,std::vector<Triangle> > mtri;

    mls.Load(0);

    for (std::map<uint,std::vector<SCR::edge> >::iterator it = edges->begin(); it != edges->end(); ++it)    // 1st point
    {
        uint echo1 = it->first;
        for (std::vector<SCR::edge>::iterator e1 = edges->operator [](echo1).begin(); e1 != edges->operator [](echo1).end(); ++e1)//auto& e1:edges[echo1]) // 1st edge
            for (std::vector<SCR::edge>::iterator e2 = edges->operator [](echo1).begin(); e2 != edges->operator [](echo1).end(); ++e2) // 2nd edge
            {
                uint echo2 = SCR::find_common_node(*e1,*e2);  // common node
                if (echo2 != 0xffffffff && echo2 != echo1)
                    for (std::vector<SCR::edge>::iterator e3 = edges->operator [](echo2).begin(); e3 != edges->operator [](echo2).end(); ++e3) // 3rd edge
                    {
                        uint echo3 = SCR::find_common_node(*e2,*e3) == e3->node_begin ? e3->node_end : e3->node_begin;
                        if (echo3 != 0xffffffff && echo3 != echo2 && echo3 != echo1 && (echo3 - echo1 + echo3 - echo2) < PPL+2)
                            for (std::vector<SCR::edge>::iterator e4 = edges->operator [](echo3).begin(); e4 != edges->operator [](echo3).end(); ++e4)
                            {
                                uint echo4 = SCR::find_common_node(*e4,*e1) == e4->node_begin ? e4->node_end : e4->node_begin;
                                if (echo4 != echo1 && echo4 != echo2 && echo4 != echo3)
                                {
                                    std::vector<uint> v{echo1,echo2,echo3,echo4};
                                    std::sort (v.begin(), v.end());
                                    Wedge w = Wedge(v[0],v[1],v[2],v[3]);
                                    if ((vw->empty() || vw->back() != w) && (int)(w.e4 - w.e1) < PPL+2 && !find_wedge_in_vec(vw,w))
                                        vw->emplace_back(w);
                                }
                            }
                    }
            }
    }

    mls.Free(0);
    std::cout << "Edge map converted to wedge vector" << std::endl;
    return vw;
}


/// @brief function for the conversion of pairs of triangles into wedges
///
/// @param vtri: vector of triangles
/// @param mls
///
/// @return vector of wedges
std::vector<Wedge> convert_triangle_list_to_wedge(std::vector<Triangle> vtri, XMls& mls)
{
    int PPL = mls.PulsePerLine();
    std::cout << "PPL : " << PPL << std::endl;

    std::sort( vtri.begin(), vtri.end() );
    vtri.erase( unique( vtri.begin(), vtri.end() ), vtri.end() );

    std::vector<Wedge> vw;
    //std::map<uint,std::vector<Triangle> > mtri;

    mls.Load(0);

    Mtri M(vtri);
    for (Mtri::iterator mit = M.begin(); mit != M.end(); ++mit)
        for (std::vector<Triangle>::iterator tit = mit->second.begin(); tit != mit->second.end(); ++tit)
            for (std::vector<Triangle>::iterator tit2 = tit.operator +(1); tit2 != mit->second.end(); ++tit2)
                if (adjacent(*tit,*tit2))
                {
                    std::vector<uint> pts;
                    pts.emplace_back(tit->i);
                    pts.emplace_back(tit->j);
                    pts.emplace_back(tit->k);
                    pts.emplace_back(tit2->i);
                    pts.emplace_back(tit2->j);
                    pts.emplace_back(tit2->k);
                    std::sort(pts.begin(),pts.end());
                    pts.erase( unique( pts.begin(), pts.end() ), pts.end() );

                    /*for (std::vector<uint>::iterator vit = pts.begin(); vit != pts.end(); ++vit)
                        std::cout << *vit << " " << std::flush;
                    std::cout << "\n";*/

                    mit->second.erase(tit, mit->second.end());
                    mit->second.erase(tit2, mit->second.end());


                    std::vector<uint>::iterator vit = pts.begin();
                    Wedge w = Wedge(XEchoIndex(*(vit++)),*(vit++),*(vit++),*(vit++));
                    if ((vw.empty() || vw.back() != w) && w.e4 - w.e1 < 4000)
                        vw.emplace_back(w);

                    break;
                }

    mls.Free(0);
    std::cout << "Triangle vector converted to wedge vector" << std::endl;
    return vw;
}


/// @brief function for the conversion of wedges and edges to triangles and edges
///
/// @param mls
/// @param vw: vector of wedges
/// @param edges: map of all possible edges
/// @param b: block we work on
/// @param mt: map of triangles
/// @param eps: new threshold on edges
///
/// @return tuple containing a vector of wedges, a vector of remaining edges and a vector of points used in any simplex oh higher dimension
std::tuple<std::vector<Triangle>, std::vector<std::tuple<uint,uint,uint> >, std::vector<uint> > convert_wedge_list_to_triangle(XMls& mls, std::vector<Wedge> vw,
                                                                                                                               std::map<uint,std::vector<SCR::edge> >* edges,
                                                                                                                               XBlockIndex b, Mtri mt, double eps)
{
    std::cout << vw.size() << " " ;
    std::vector<Triangle>* vtri = new std::vector<Triangle>;

    std::map<uint,std::vector<SCR::edge> > edges_copy;
    std::map<uint,std::vector<SCR::edge> > edges_used;
    std::vector<std::tuple<uint,uint,uint> > ve;
    std::vector<uint> points_used;

    mls.Load(0);

    for (auto & w:vw)
    {
        //w.print();

        Triangle t(w.e1,w.e2,w.e3);
        //std::cout << t.perimeter(mls) << " " << t.area(mls) << " ----- " << std::flush;
        //if (t.perimeter(mls)/t.area(mls) > 10)
        //if (find_triangle(mt[w.e1],t))
        {
            std::vector<uint> p;
            p.emplace_back(w.e1);
            p.emplace_back(w.e2);
            p.emplace_back(w.e3);
            std::sort(p.begin(),p.end());

            vtri->emplace_back(Triangle(p[0],p[1],p[2]));

            edges_used[w.e1].emplace_back(SCR::edge(b,w.e1,w.e4));
            edges_used[w.e2].emplace_back(SCR::edge(b,w.e1,w.e4));

            edges_used[w.e1].emplace_back(SCR::edge(b,w.e1,w.e2));
            edges_used[w.e2].emplace_back(SCR::edge(b,w.e1,w.e2));

            edges_used[w.e1].emplace_back(SCR::edge(b,w.e1,w.e3));
            edges_used[w.e3].emplace_back(SCR::edge(b,w.e1,w.e3));

            edges_used[w.e2].emplace_back(SCR::edge(b,w.e2,w.e3));
            edges_used[w.e3].emplace_back(SCR::edge(b,w.e2,w.e3));

            points_used.emplace_back(w.e1);
            points_used.emplace_back(w.e2);
            points_used.emplace_back(w.e3);
        }
        t = Triangle(w.e4,w.e2,w.e3);
        //if (t.perimeter(mls)/t.area(mls) > 10)
        //if (find_triangle(mt[w.e4],t))
        {
            std::vector<uint> p;
            p.emplace_back(w.e2);
            p.emplace_back(w.e4);
            p.emplace_back(w.e3);
            //std::sort(p.begin(),p.end());

            vtri->emplace_back(Triangle(p[0],p[1],p[2]));

            edges_used[w.e1].emplace_back(SCR::edge(b,w.e1,w.e4));
            edges_used[w.e2].emplace_back(SCR::edge(b,w.e1,w.e4));

            edges_used[w.e2].emplace_back(SCR::edge(b,w.e2,w.e3));
            edges_used[w.e3].emplace_back(SCR::edge(b,w.e2,w.e3));

            edges_used[w.e2].emplace_back(SCR::edge(b,w.e2,w.e4));
            edges_used[w.e4].emplace_back(SCR::edge(b,w.e2,w.e4));

            edges_used[w.e3].emplace_back(SCR::edge(b,w.e3,w.e4));
            edges_used[w.e4].emplace_back(SCR::edge(b,w.e3,w.e4));

            points_used.emplace_back(w.e4);
            points_used.emplace_back(w.e2);
            points_used.emplace_back(w.e3);
        }
    }

    std::sort(vtri->begin(),vtri->end());
    vtri->erase(unique(vtri->begin(),vtri->end()),vtri->end());

    //std::cout << vtri->size() << " ";



    for (auto& edges_point: *edges)
        for (auto& e:edges_point.second)
            if (    !find_edge(edges_used[e.node_begin],*e) &&
                    !find_edge(edges_used[e.node_end  ],*e) &&
                    !find_edge(edges_used[e.node_begin],SCR::edge(e.node_end,e.node_begin)) &&
                    !find_edge(edges_used[e.node_end  ],SCR::edge(e.node_end,e.node_begin)) )
                edges_copy[e.node_begin].emplace_back(e);

    //mls.Load(0);

    for (auto& edges_point: edges_copy)
        for (auto& e:edges_point.second)
            //if (edges[e.node_begin].size() > 1 && edges[e.node_end].size() > 1)
            {
                XPt3D p1 = SCR::e2p(e,mls);
                for (auto& ea: edges->operator [](e.node_begin))
                {
                    XPt3D p2 = SCR::e2p(ea,mls);
                    if (abs(p1.X * p2.X + p1.Y * p2.Y + p1.Z * p2.Z) < eps)
                    {
                        ve.emplace_back(std::make_tuple(b,e.node_begin,e.node_end));
                        points_used.emplace_back(e.node_begin);
                        points_used.emplace_back(e.node_end);
                    }
                }
                for (auto& ea: edges->operator [](e.node_end))
                {
                    XPt3D p2 = SCR::e2p(ea,mls);
                    if (abs(p1.X * p2.X + p1.Y * p2.Y + p1.Z * p2.Z) < eps)
                    {
                        ve.emplace_back(std::make_tuple(b,e.node_begin,e.node_end));
                        points_used.emplace_back(e.node_begin);
                        points_used.emplace_back(e.node_end);
                    }
                }
            }

    mls.Free(0);

    std::sort(points_used.begin(),points_used.end());
    points_used.erase(unique(points_used.begin(),points_used.end()),points_used.end());

    //std::cout << points_used.size() << " points used on " << mls.NEcho(0) << std::endl;

    return std::make_tuple(*vtri,ve,points_used);

}

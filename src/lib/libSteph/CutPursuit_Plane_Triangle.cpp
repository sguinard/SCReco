#include "libSteph/CutPursuit.h"

#include "../extern/cut-pursuit-master/include/API.h"
//#include "libSteph/CutPursuit_Plane_Triangle.h"

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "libSteph/MIvP3.h"
#include "libSteph/ConvexHull.h"

#include "time.h"

using namespace CP;

// observations
/*std::vector<TriangleCoords> find_observations(vT* vtri, vvD* observations)
{
  std::vector<TriangleCoords> tri_obs;
  for (vT_iter t = vtri->begin(); t != vtri->end(); ++t)
    {
      vP3 pts { XPt3D(observations->at(t->i)[0], observations->at(t->i)[1], observations->at(t->i)[2]),
	  XPt3D(observations->at(t->j)[0], observations->at(t->j)[1], observations->at(t->j)[2]),
	  XPt3D(observations->at(t->k)[0], observations->at(t->k)[1], observations->at(t->k)[2])};
      tri_obs.emplace_back(TriangleCoords(&pts));
    }
  return tri_obs;
  }


// Eu & Ev
std::pair<std::vector<uint32_t>,std::vector<uint32_t> > find_edges_tri(std::vector<TriangleCoords> vtri)
{
  struct myP3comp
  {
    bool operator() (const XPt3D p1, const XPt3D p2) const
    {
      return p1.X < p2.X ? true : p1.X == p2.X ? p1.Y < p2.Y ? true : p1.Y == p2.Y ? p1.Z <= p2.Z ? true : false : false : false;
    }
  };
    std::vector<uint32_t> edges_begin;
    std::vector<uint32_t> edges_end;

    std::map<XPt3D,uint32_t,myP3comp> index_order;
    uint32_t idx = 0;
    for (auto& tri:vtri)
    {
      if (index_order.size() > 0)
	{
	  XPt3D p1 = tri.p1();
	  if (index_order.find(p1) == index_order.end())
	    index_order.at(p1) = idx++;
	  XPt3D p2 = tri.p2();
	  if (index_order.find(p2) == index_order.end())
	    index_order.at(p2) = idx++;
	  XPt3D p3 = tri.p3();
	  if (index_order.find(p3) == index_order.end())
	    index_order.at(p3) = idx++;
	}
    }

    for (std::vector<TriangleCoords>::iterator tri_it1 = vtri.begin(); tri_it1 != vtri.end(); ++tri_it1)
    {
      for (std::vector<TriangleCoords>::iterator tri_it2 = tri_it1; tri_it2 != vtri.end(); ++tri_it2)
	if (tri_it1->intersects(*tri_it2))
	  {
	    edges_begin;
	  }
    }
    return std::make_pair(edges_begin,edges_end);
}




void cut_pursuit
                (const uint32_t n_nodes,    // mls.necho()
                 const uint32_t n_edges,    // each echo that is connected with SCR ?
                 const uint32_t nObs,       // 3
                 std::vector<TriangleCoords> &observation, // coordinates
                 const std::vector<uint32_t> &Eu,               // first nodes for edges
                 const std::vector<uint32_t> &Ev,               // second nodes for edges
                 const std::vector<double> &edgeWeight,          // vector of ones
                 const std::vector<double> &nodeWeight,          // vector of ones
                 std::vector<TriangleCoords> &solution,    // void vector
                 const double lambda,        // 0.5 ?
                 const double mode,          // 0
                 const double speed,         // 3
                 const double verbose,       // 1 default, 2 for debug
                 std::vector<uint32> inComponent)
{   //C-style ++ interface
    //std::srand (time(NULL));
    if (verbose > 0)
    {
        std::cout << "L0-CUT PURSUIT";
    }
    //--------parameterization---------------------------------------------
//    for (auto& c:inComponent) std::cout << c << " " << std::flush;    // ok
    CutPursuit_Plane_Triangle<double>* cp = dynamic_cast<CutPursuit_Plane_Triangle<double>* >(create_CP(mode, verbose));
    vvD* observations = new vvD;
    for (auto& tri: observation) observations->emplace_back(std::vector<double>{
	                                                      tri.p1().X, tri.p1().Y, tri.p1().Z,
							      tri.p2().X, tri.p2().Y, tri.p2().Z,
							      tri.p3().X, tri.p3().Y, tri.p3().Z});
    set_speed(cp, speed, verbose);
    set_up_CP(cp, n_nodes, n_edges, nObs, *observations, Eu, Ev
	      ,edgeWeight, nodeWeight); 
    cp->parameter.reg_strenth = lambda;
    cp->parameter.max_ite_main = 10;
    cp->parameter.backward_step = true;
    //-------run the optimization------------------------------------------
    /*!   cp->run_plane(inComponent);
    //------------write the solution-----------------------------
    VertexAttributeMap<double> vertex_attribute_map = boost::get(
            boost::vertex_bundle, cp->main_graph);

//for (auto& c:inComponent) std::cout << c << " " << std::flush;
    VertexIterator<double> ite_nod = boost::vertices(cp->main_graph).first;
    for(uint32_t ind_nod = 0; ind_nod < n_nodes; ind_nod++ )
    {

        inComponent[*ite_nod] =
        vertex_attribute_map[*ite_nod].in_component;
        //std::cout << inComponent[*ite_nod] << " " << std::flush;

        for(uint32_t ind_dim=0; ind_dim < nObs; ind_dim++)
        {
            solution[ind_nod][ind_dim] = vertex_attribute_map[*ite_nod].value[ind_dim];
        }
        //std::cout << std::endl;
        ite_nod++;
    }

    std::pair<std::vector<std::vector<std::pair<XPt3D, uint32> > >,std::vector<std::vector<std::pair<XPt3D, uint32> > > > points_colored = color_points(solution,observation,inComponent);

    //WritePlyPointColor(points_colored.first,"_values.ply");
    //WritePlyPointColor(points_colored.second,"_observations.ply");
    
    delete cp;
    return;
}



std::vector<std::vector<std::pair<XPt3D, Color> > > color_points_tri(std::vector<std::vector<double> > solution, std::vector<uint32> inComponent)
{
    std::vector<std::vector<std::pair<XPt3D, Color> > > points_colored;

    //std::cout << "Number of points: " << solution.size() << std::endl;

    std::vector<std::pair<uint32,Color> > comp_color;
    for (int i=0; i != (int)inComponent.size(); i++)
    {
        //std::cout << inComponent[i] << " " << std::flush;
        bool found_comp = false;
        for (std::vector<std::pair<uint32,Color> >::iterator it = comp_color.begin(); it != comp_color.end(); it++)
        {
            if (it->first == inComponent[i])
                found_comp = true;
        }
        if (!found_comp)
        {
            unsigned char r = rand() % 255;
            unsigned char g = rand() % 255;
            unsigned char b = rand() % 255;
            comp_color.push_back(std::make_pair(inComponent[i],Color(r,g,b)));
        }
    }

    points_colored.resize(1);
    //int i=0;
    for (int p_index = 0; p_index != (int)solution.size(); p_index++)
    {
        if (solution[p_index].size() == 3)
        {
            Color color_of_comp;
            //std::cout << solution[p_index][0] << " " << solution[p_index][1] << " " << solution[p_index][2] << " " << inComponent[p_index] << std::endl;
            for (std::vector<std::pair<uint32,Color> >::iterator it = comp_color.begin(); it != comp_color.end(); it++)
            {
                if (it->first == inComponent[p_index])
                {
                    //std::cout << it->first << " " << static_cast<unsigned> ( it->second.red ) << " " << static_cast<unsigned> ( it->second.green ) << " " << static_cast<unsigned> ( it->second.blue ) << std::endl;

                    color_of_comp = it->second;
                    break;
                }
            }

            XPt3D p ((double)solution[p_index][0],(double)solution[p_index][1],(double)solution[p_index][2]);
            //std::cout << i << " " << std::flush;
            points_colored[0].emplace_back(std::make_pair(p,color_of_comp));
        }
    }
    std::cout << "Yay! I painted points     :hap:" << std::endl;
    return points_colored;
}




std::pair<std::vector<std::vector<std::pair<XPt3D, Color> > >,std::vector<std::vector<std::pair<XPt3D, Color> > > > color_points_tri(std::vector<std::vector<double> > solution,
                                                                                                                                 std::vector<std::vector<double> > observation,
                                                                                                                                 std::vector<uint32> inComponent,
                                                                                                                                 uint32 nb_comps)
{
    //std::cout << "Cate enters the shed and see a paint bucket" << std::endl;
//std::cout << solution.size() - observation.size() << std::endl;
    //int nb_pts_obs = 0;

    std::vector<std::vector<std::pair<XPt3D, Color> > > values;
    std::vector<std::vector<std::pair<XPt3D, Color> > > observations;

    //std::cout << "Number of points: " << solution.size() << std::endl;

    std::vector<std::pair<uint32,Color> > comp_color;
    for (int i = 0; i != nb_comps; ++i)
        comp_color.emplace_back(std::make_pair(i,Color(rand() % 255,
                                                       rand() % 255,
                                                       rand() % 255)));

    std::cout << comp_color.size() << " colors" << std::endl;

    values.resize(1);
    observations.resize(1);

    for (int p_index = 0; p_index != (int)observation.size(); p_index++)
    {
        if (observation[p_index].size() == 3)
        {
            Color color_of_comp;
            for (std::vector<std::pair<uint32,Color> >::iterator it = comp_color.begin(); it != comp_color.end(); it++)
            {
                //std::cout << it->first << " " << static_cast<unsigned> ( it->second.red ) << " " << static_cast<unsigned> ( it->second.green ) << " " << static_cast<unsigned> ( it->second.blue ) << std::endl;
                //std::cout << it->first << " " << inComponent[i] << std::endl;
                if (it->first == inComponent[p_index])
                {
                    //std::cout << it->first << " " << " " << static_cast<unsigned> ( it->second.red ) << " " << static_cast<unsigned> ( it->second.green ) << " " << static_cast<unsigned> ( it->second.blue ) << std::endl;

                    color_of_comp = it->second;
                    break;
                }
            }


            XPt3D p (solution[p_index][0],solution[p_index][1],solution[p_index][2]);
            XPt3D p2 (observation[p_index][0],observation[p_index][1],observation[p_index][2]);
            //std::cout << i << " " << std::flush;
            values[0].emplace_back(std::make_pair(p,color_of_comp));
            observations[0].emplace_back(std::make_pair(p2,color_of_comp));
        }
    }
    //std::cout << "Cate is happy because he toppled the bucket and can now play with it" << std::endl;
//std::cout << "Yay! I painted points     :hap:" << std::endl;
    return std::make_pair(values,observations);
}



std::pair<std::vector<std::vector<std::pair<XPt3D, uint32> > >,std::vector<std::vector<std::pair<XPt3D, uint32> > > > color_points_tri(
                                                                                                                                std::vector<std::vector<double> > solution,
                                                                                                                                std::vector<std::vector<double> > observation,
                                                                                                                                std::vector<uint32> inComponent)

{
    //std::cout << "Cate enters the shed and see a paint bucket" << std::endl;
//std::cout << solution.size() - observation.size() << std::endl;
    //int nb_pts_obs = 0;

    std::vector<std::vector<std::pair<XPt3D, uint32> > > values;
    std::vector<std::vector<std::pair<XPt3D, uint32> > > observations;

    values.resize(1);
    observations.resize(1);

    for (int p_index = 0; p_index != (int)observation.size(); p_index++)
    {
        if (observation[p_index].size() == 3)
        {
            XPt3D p (solution[p_index][0],solution[p_index][1],solution[p_index][2]);
            XPt3D p2 (observation[p_index][0],observation[p_index][1],observation[p_index][2]);
            //std::cout << i << " " << std::flush;
            values[0].emplace_back(std::make_pair(p,inComponent[p_index]));
            observations[0].emplace_back(std::make_pair(p2,inComponent[p_index]));
        }
    }
    //std::cout << "Cate is happy because he toppled the bucket and can now play with it" << std::endl;
//std::cout << "Yay! I painted points     :hap:" << std::endl;
    return std::make_pair(values,observations);
}

*/

#include <ctime>
#include <iostream>
#include <limits>
#include <stdio.h>
#include <tuple>

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"

#include "libSteph/Sensortools.h"

using namespace SCR;

/// \file SensorTools.cpp
///
/// \brief this file contains the definitions of the functions in SensotTools.h


/// \brief count the number of points
///
/// \param mls
///
/// \return number of vertices in the mls
int count_vertex(XMls & mls)
{

    int n_vertex = 0;

    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<n_pulse; pulse_idx++)
        {
            for (int i=0; i<mls.NbOfEcho(block_idx,pulse_idx);i++)
            {
                ++n_vertex;
            }
        }
        mls.Free(block_idx);
    }

    return n_vertex;
}


/// \brief write .ply file containing edges
///
/// \param mls
/// \param edges: vector of pairs of echoes (edges)
/// \param n_vertex: number of vertices
/// \param params
void WritePlyEdge(XMls & mls, std::vector<std::pair<uint,uint> >edges,
              int n_vertex, param params)
{
    std::string out = params.output;
    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_edges.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    int n_edges = 0;
    for (std::vector<std::pair<uint,uint> >::iterator it = edges.begin(); it != edges.end(); ++it)
        if ((*it).first <= (uint)n_vertex && (*it).first >= (uint)0 && (*it).second <= (uint)n_vertex && (*it).second >= (uint)0)
            ++n_edges;

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }
    fileOut << "element edge " << n_edges << endl;
    fileOut << "property int vertex1" << endl;
    fileOut << "property int vertex2" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    XFloatAttrib * p_refl=NULL;
    if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            for (XEchoIndex echo = mls.IdxFirstEcho(block_idx,pulse_idx); echo <= mls.IdxLastEcho(block_idx,pulse_idx); ++echo)
            {
                //XEchoIndex last_echo_idx = mls.IdxLastEcho(block_idx, pulse_idx);
                XPt3D Pw = mls.Pworld(block_idx, echo);
                float e=Pw.X-params.pivot_E, n=Pw.Y-params.pivot_N, h=Pw.Z-params.pivot_H;
                if (pulse_idx == 0)
                    cout << e << " " << n << " " << h << endl;
                Write<float>(it, e);
                Write<float>(it, n);
                Write<float>(it, h);
                if(params.add_rgb == 1) // r g b mode
                {
                    float g = 12.75f*(p_refl->at(block_idx)[echo]+20.f); // rescale [-20,0] to [0,256] TODO: parameters
                    if(g<0.f) g=0.f; else if(g>255.f) g=255.f;
                    unsigned char ug = g;
                    Write<unsigned char>(it, ug);
                    Write<unsigned char>(it, ug);
                    Write<unsigned char>(it, ug);
                }
                else if(params.add_rgb == 2) // quality mode
                {
                    Write<float>(it, p_refl->at(block_idx)[echo]);
                }
            }
        }
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // edges list
    unsigned int edge_bytesize = 2*sizeof(int)+3*sizeof(unsigned char);
    unsigned long edge_buffer_size = edge_bytesize*edges.size();
    buffer = new char[edge_buffer_size]; it = buffer;
    for(auto & ed:edges)
    {
        //Write<unsigned char>(it, 2);
        if (ed.first < (uint)n_vertex && ed.second < (uint)n_vertex && ed.first >= (uint)0 && ed.second >= (uint)0)
        {
            Write<int>(it, ed.first);
            Write<int>(it, ed.second);
        }
        //std::cout << ed.first << "-" << ed.second << " ";
    }
    cout << "Writing " << n_edges << " edges of size " << edge_buffer_size << "=" << 1.e-6*edge_buffer_size << "MB" << endl;
    fileOut.write(buffer, edge_buffer_size);
    delete buffer;
    cout << "Total " << 1.e-6*(vertex_buffer_size+edge_buffer_size) << "MB" << endl;
    fileOut.close();
}


/// \brief writes .ply file containing edges
///
/// \remark same as #WritePlyEdge but working on multiple blocks
///
/// \param mls
/// \param edges: vector containing a vector of edges for each block
/// \param n_vertex: number of vertices
/// \param params
void WritePlySegment(XMls & mls, std::vector<std::tuple<uint,uint,uint> >edges,
              int n_vertex, param params)
{
    std::string out = params.output;
    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_edges.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    int n_edges = 0;
    for (std::vector<std::tuple<uint,uint,uint> >::iterator it = edges.begin(); it != edges.end(); ++it)
        if (std::get<1>(*it) <= (uint)n_vertex && std::get<1>(*it) >= (uint)0 && std::get<2>(*it) <= (uint)n_vertex && std::get<2>(*it) >= (uint)0)
            ++n_edges;

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_edges << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "property float x1" << endl;
    fileOut << "property float y1" << endl;
    fileOut << "property float z1" << endl;
    /*if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }*/
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 6*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_edges;
    //XFloatAttrib * p_refl=NULL;
    //if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    //if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    //else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);


        for (std::vector<std::tuple<uint,uint,uint> >::iterator pit = edges.begin(); pit != edges.end(); ++pit)
        {
            //std::cout << pit->first << " " << pit->second << " - " << std::flush;
            if (std::get<1>(*pit) < mls.NPulse(block_idx) && std::get<2>(*pit) < mls.NPulse(block_idx) && std::get<1>(*pit) > 0 && std::get<2>(*pit) > 0
                    && std::get<0>(*pit) == block_idx)
            {
                XPt3D P  = mls.Pworld(block_idx,std::get<1>(*pit));
                XPt3D P1 = mls.Pworld(block_idx,std::get<2>(*pit));

                float PX = P.X-params.pivot_E, PY = P.Y-params.pivot_N, PZ = P.Z-params.pivot_H;
                float P1X = P1.X-params.pivot_E, P1Y = P1.Y-params.pivot_N, P1Z = P1.Z-params.pivot_H;

                Write<float>(it,PX);
                Write<float>(it,PY);
                Write<float>(it,PZ);
                Write<float>(it,P1X);
                Write<float>(it,P1Y);
                Write<float>(it,P1Z);

                //std::cout << PX << " " << P1X << " " << PY << " " << P1Y << " " << PZ << " " << P1Z << " - " << std::flush;
            }
        }

        mls.Free(block_idx);
    }
    cout << "Writing " << n_edges << " edges of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    cout << "Total " << 1.e-6*(vertex_buffer_size) << "MB" << endl;
    fileOut.close();
}


/// \brief writes .ply file with only points
///
/// \note used only for the remaining points of the simplicial complex (points that not part of any triangle nor edge)
///
/// \param mls
/// \param points: vector contianing remaining points for each block
/// \param params
void WritePlyPoint(XMls & mls, std::vector<std::vector<XPt3D> > points, param params)
{
    int n_vertex = 0;

    for (std::vector<std::vector<XPt3D> >::iterator it = points.begin(); it != points.end(); ++it)
        n_vertex += it->size();

    std::string out = params.output;
    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_points.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    /*if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }*/
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    //XFloatAttrib * p_refl=NULL;
    //if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    //if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    //else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;

    int NPS = 0;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        for (std::vector<XPt3D>::iterator pit = points[block_idx].begin(); pit != points[block_idx].end(); ++pit)
        {
            XPt3D P  = *pit;

            float PX = P.X-params.pivot_E, PY = P.Y-params.pivot_N, PZ = P.Z-params.pivot_H;

            Write<float>(it,PX);
            Write<float>(it,PY);
            Write<float>(it,PZ);
        }
        mls.Load(block_idx);
        NPS += mls.NEcho(block_idx);
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    cout << "Total " << 1.e-6*(vertex_buffer_size) << "MB" << endl;
    fileOut.close();
}


/// \brief writes .ply file with only points
///
/// \note used only for the remaining points of the simplicial complex (points that not part of any triangle nor edge)
///
/// \param points: vector contianing remaining points for each block
void WritePlyPoint(std::vector<std::vector<double> > points)
{
    int n_vertex = 0;

    //for (std::vector<std::vector<double> >::iterator it = points.begin(); it != points.end(); ++it)
        n_vertex = points.size();

    std::string out = "../../";
    //for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_points.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + out+"_points.ply" + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;

        for (std::vector<std::vector<double> >::iterator pit = points.begin(); pit != points.end(); ++pit)
        {
            //XPt3D P  = *pit;

            float PX = pit->at(0), PY = pit->at(1), PZ = pit->at(2);

            Write<float>(it,PX);
            Write<float>(it,PY);
            Write<float>(it,PZ);
        }

    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    cout << "Total " << 1.e-6*(vertex_buffer_size) << "MB" << endl;
    fileOut.close();
}


/// \brief writes .ply file with only points
///
/// \note used only for the remaining points of the simplicial complex (points that not part of any triangle nor edge)
///
/// \param mls
/// \param points: vector contianing remaining points for each block
/// \param params
void WritePlyPointColor(XMls & mls, std::vector<std::vector<std::pair<XPt3D,Color> > > points, param params, std::string filename)
{
    int n_vertex = 0;

    for (std::vector<std::vector<std::pair<XPt3D,Color> > >::iterator it = points.begin(); it != points.end(); ++it)
        n_vertex += it->size();

    std::string out = params.output;
    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+filename);
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property double x" << endl;
    fileOut << "property double y" << endl;
    fileOut << "property double z" << endl;
    fileOut << "property uchar red" << endl;
    fileOut << "property uchar green" << endl;
    fileOut << "property uchar blue" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(double) + 3*sizeof(char);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    //XFloatAttrib * p_refl=NULL;
    //if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    //if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    //else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;

    int NPS = 0;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        for (std::vector<std::pair<XPt3D,Color> >::iterator pit = points[block_idx].begin(); pit != points[block_idx].end(); ++pit)
        {
            XPt3D P  = (*pit).first;

            double PX = P.X, PY = P.Y, PZ = P.Z;

            //std::cout << PX << " " << PY << " " << PZ << std::endl;

            Write<double>(it,PX);
            Write<double>(it,PY);
            Write<double>(it,PZ);
            Write<unsigned char>(it,pit->second.red);
            Write<unsigned char>(it,pit->second.green);
            Write<unsigned char>(it,pit->second.blue);
        }
        mls.Load(block_idx);
        NPS += mls.NEcho(block_idx);
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    cout << "Total " << 1.e-6*(vertex_buffer_size) << "MB" << endl;
    fileOut.close();
}




/// \brief writes .ply file with only points
///
/// \note used only for the remaining points of the simplicial complex (points that not part of any triangle nor edge)
///
/// \param points: vector contianing remaining points for each block
/// \param filename: name of the file where points are stored
void WritePlyPointColor(std::vector<std::vector<std::pair<XPt3D,uint32> > > points, std::string filename)
{
    int n_vertex = 0;

    for (std::vector<std::vector<std::pair<XPt3D,uint32> > >::iterator it = points.begin(); it != points.end(); ++it)
        n_vertex += it->size();

    std::string out = "";//../../";
    ofstream fileOut(out+filename);
    if(!fileOut.good())
    {
        cout << "Cannot open " + out + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "property float component" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float) + 1*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;

    int i = 0;
        for (std::vector<std::pair<XPt3D,uint32> >::iterator pit = points[0].begin(); pit != points[0].end(); ++pit)
        {
            XPt3D P  = (*pit).first;

            float PX = P.X, PY = P.Y, PZ = P.Z;

            Write<float>(it,PX);
            Write<float>(it,PY);
            Write<float>(it,PZ);
            Write<float>(it,pit->second);
        }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    cout << "Total " << 1.e-6*(vertex_buffer_size) << "MB" << endl;
    fileOut.close();
}





/// \brief writes .ply file with only points
///
/// \note used only for the remaining points of the simplicial complex (points that not part of any triangle nor edge)
///
/// \param points: vector contianing remaining points for each block
/// \param filename: name of the file where points are stored
void WritePlyPointColor(std::vector<std::vector<std::pair<XPt3D,Color> > > points, std::string filename)
{
    int n_vertex = 0;

    for (std::vector<std::vector<std::pair<XPt3D,Color> > >::iterator it = points.begin(); it != points.end(); ++it)
        n_vertex += it->size();

    std::string out = "../../";
    ofstream fileOut(out+filename);
    if(!fileOut.good())
    {
        cout << "Cannot open " + out + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property double x" << endl;
    fileOut << "property double y" << endl;
    fileOut << "property double z" << endl;
    fileOut << "property uchar red" << endl;
    fileOut << "property uchar green" << endl;
    fileOut << "property uchar blue" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(double) + 3*sizeof(char);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    //XFloatAttrib * p_refl=NULL;
    //if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    //if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    //else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;

    int i = 0;
//    int NPS = 0;
//    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
//    {
        for (std::vector<std::pair<XPt3D,Color> >::iterator pit = points[0].begin(); pit != points[0].end(); ++pit)
        {
            XPt3D P  = (*pit).first;

            double PX = P.X, PY = P.Y, PZ = P.Z;

            Write<double>(it,PX);
            Write<double>(it,PY);
            Write<double>(it,PZ);
            Write<unsigned char>(it,pit->second.red);
            Write<unsigned char>(it,pit->second.green);
            Write<unsigned char>(it,pit->second.blue);
        }
//        mls.Load(block_idx);
//        NPS += mls.NEcho(block_idx);
//        mls.Free(block_idx);
//    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    cout << "Total " << 1.e-6*(vertex_buffer_size) << "MB" << endl;
    fileOut.close();
}


/// \brief writes .ply file with planes
///
/// \note planes are stored as triangles
/// \n these triangles are the ones planes are based on, but enlarged
///
/// \param points: vector containing all the points of triangles, ordered as plane order
/// \param params
/// \param mls
void StorePlanes(std::vector<XPt3D> points, param params, XMls& mls)
{
    int npoints = points.size();
    int plane_size = npoints/3;
    std::string out = params.output;
    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_planes.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << npoints << endl;
    fileOut << "property double x" << endl;
    fileOut << "property double y" << endl;
    fileOut << "property double z" << endl;
    if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }
    fileOut << "element face " << plane_size << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(double);
    unsigned long vertex_buffer_size = vertex_bytesize * npoints;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
        //mls.Load(0);
        for (auto& p:points)
        {
            //XPt3D Pw = mls.Pworld(0,p);
            float e=p.X-params.pivot_E, n=p.Y-params.pivot_N, h=p.Z-params.pivot_H;
            Write<double>(it, e);
            Write<double>(it, n);
            Write<double>(it, h);
        }
    cout << "Writing " << npoints << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int plane_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long plane_buffer_size = plane_bytesize*plane_size;
    buffer = new char[plane_buffer_size]; it = buffer;
        //std::vector<Plane> blk_plane = v_plane;
        //std::cout << blk_plane.size() << std::endl;
        int n = 0;
        for(int i=0; i<plane_size;++i)
        {
            Write<unsigned char>(it, 3);
            Write<int>(it, n++);
            Write<int>(it, n++);
            Write<int>(it, n++);
        }
    cout << "Writing " << plane_size << " planes of size " << plane_buffer_size << "=" << 1.e-6*plane_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+plane_buffer_size) << "MB" << endl;
    fileOut.write(buffer, plane_buffer_size);
    fileOut.close();
}


/// \brief writes a .ply file with planes
///
/// \param mls
/// \param v_plane: vector of planes
/// \param vpoints: vector of points
/// \param n_vertex: number of vertices
/// \param params
///
/// \warning deprecated
//void StorePlanes(XMls & mls, vector<Plane> v_plane,
//              int n_vertex, param params)
//{
//    mls.Load(0);
//    int plane_size = v_plane.size();
//    int npoints = 0;
//    std::vector<int> points = {};
//    std::vector<int> conversion = {};
//    int n = 0;
//    for (auto& p:v_plane)
//    {
//        points.emplace_back(p.p1);
//        if (std::find(points.begin(),points.end(),p.p1)==points.end())
//        {
//            points.emplace_back(p.p1);
//            conversion.emplace_back(n++);
//        }
//        else
//            conversion.emplace_back(*(std::find(points.begin(),points.end(),p.p1)));
//        if (std::find(points.begin(),points.end(),p.p2)==points.end())
//        {
//            points.emplace_back(p.p2);
//            conversion.emplace_back(n++);
//        }
//        else
//            conversion.emplace_back(*(std::find(points.begin(),points.end(),p.p2)));
//        if (std::find(points.begin(),points.end(),p.p3)==points.end())
//        {
//            points.emplace_back(p.p3);
//            conversion.emplace_back(n++);
//        }
//        else
//            conversion.emplace_back(*(std::find(points.begin(),points.end(),p.p3)));
//    }
//    npoints = points.size();

//    std::string out = params.output;
//    for (int i=0; i<4; ++i) out.pop_back();
//    ofstream fileOut(out+"_planes.ply");
//    if(!fileOut.good())
//    {
//        cout << "Cannot open " + params.output + " for writing\n";
//        return;
//    }

//    // write text header
//    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
//    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
//    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
//    fileOut << "element vertex " << npoints << endl;
//    fileOut << "property float x" << endl;
//    fileOut << "property float y" << endl;
//    fileOut << "property float z" << endl;
//    if(params.add_rgb == 1)
//    {
//        fileOut << "property uchar red" << endl;
//        fileOut << "property uchar green" << endl;
//        fileOut << "property uchar blue" << endl;
//    } else if(params.add_rgb == 2)
//    {
//        fileOut << "property float quality" << endl;
//    }
//    fileOut << "element face " << plane_size << endl;
//    fileOut << "property list uchar int vertex_indices" << endl;
//    fileOut << "end_header" << endl;
//    // vertex list
//    unsigned int vertex_bytesize = 3*sizeof(float);
//    unsigned long vertex_buffer_size = vertex_bytesize * npoints;
//    XFloatAttrib * p_refl=NULL;
//    if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
//    if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * npoints;
//    else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * npoints;

//    char * buffer = new char[vertex_buffer_size], * it = buffer;
//        //mls.Load(0);
//        for (auto& p:points)
//        {
//            XPt3D Pw = mls.Pworld(0,p);
//            float e=Pw.X-params.pivot_E, n=Pw.Y-params.pivot_N, h=Pw.Z-params.pivot_H;
//            Write<float>(it, e);
//            Write<float>(it, n);
//            Write<float>(it, h);
//        }
//        //mls.Free(0);
//    cout << "Writing " << npoints << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
//    fileOut.write(buffer, vertex_buffer_size);
//    delete buffer;
//    // triangle list
//    unsigned int plane_bytesize = sizeof(unsigned char)+3*sizeof(int);
//    unsigned long plane_buffer_size = plane_bytesize*plane_size;
//    buffer = new char[plane_buffer_size]; it = buffer;
//        std::vector<Plane> blk_plane = v_plane;
//        std::cout << blk_plane.size() << std::endl;
//        n = 0;
//        for(auto & plane:blk_plane)
//        {
//            //std::cout << plane.p1 << " " << plane.p2 << " " << plane.p3 << " " << plane.p4 << " - " << std::flush;
//            Write<unsigned char>(it, 3);
//            Write<int>(it, conversion[n++]);
//            Write<int>(it, conversion[n++]);
//            Write<int>(it, conversion[n++]);
//            //Write<int>(it, plane.p4);
//        }
//    cout << "Writing " << plane_size << " planes of size " << plane_buffer_size << "=" << 1.e-6*plane_buffer_size << "MB" << endl;
//    cout << "Total " << 1.e-6*(vertex_buffer_size+plane_buffer_size) << "MB" << endl;
//    fileOut.write(buffer, plane_buffer_size);
//    fileOut.close();
//    mls.Free(0);
//}


/// \brief writes .ply file with a mesh
///
/// \param mls
/// \param v_tri: vector contianing for each block the liste of triangles
/// \param n_vertex: number of vertices
/// \param params
void WritePly(XMls & mls, vector<vector<Triangle> > v_tri,
              int n_vertex, param params)
{
    int tri_size = 0;
    for (std::vector<std::vector<Triangle> >::iterator vit = v_tri.begin(); vit != v_tri.end(); ++vit)
    {
        std::cout << vit->size() << std::endl;
        tri_size += vit->size();
    }

    std::string out = params.output;
    for (int i=0; i<4; ++i) out.pop_back();
    ofstream fileOut(out+"_triangles.ply");
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }
    fileOut << "element face " << tri_size << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    XFloatAttrib * p_refl=NULL;
    if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    std::vector<int> nb_points_per_block;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            for (XEchoIndex echo = mls.IdxFirstEcho(block_idx,pulse_idx); echo <= mls.IdxLastEcho(block_idx,pulse_idx); ++echo)
            {
                //XEchoIndex last_echo_idx = mls.IdxLastEcho(block_idx, pulse_idx);
                XPt3D Pw = mls.Pworld(block_idx, echo);
                float e=Pw.X-params.pivot_E, n=Pw.Y-params.pivot_N, h=Pw.Z-params.pivot_H;
                if (pulse_idx == 0)
                    cout << e << " " << n << " " << h << endl;
                Write<float>(it, e);
                Write<float>(it, n);
                Write<float>(it, h);
                if(params.add_rgb == 1) // r g b mode
                {
                    float g = 12.75f*(p_refl->at(block_idx)[echo]+20.f); // rescale [-20,0] to [0,256] TODO: parameters
                    if(g<0.f) g=0.f; else if(g>255.f) g=255.f;
                    unsigned char ug = g;
                    Write<unsigned char>(it, ug);
                    Write<unsigned char>(it, ug);
                    Write<unsigned char>(it, ug);
                }
                else if(params.add_rgb == 2) // quality mode
                {
                    Write<float>(it, p_refl->at(block_idx)[echo]);
                }
            }
        }
        nb_points_per_block.push_back(mls.NEcho(block_idx));
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*tri_size;
    buffer = new char[tri_buffer_size]; it = buffer;
    int NPS = 0;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        std::vector<Triangle> blk_tri = v_tri[block_idx];
        std::cout << blk_tri.size() << std::endl;
        for(auto & tri:blk_tri)
        {
            //std::cout << tri.i << " " << tri.j << " " << tri.k << " - " << std::flush;
            Write<unsigned char>(it, 3);
            Write<int>(it, tri.i+NPS);
            Write<int>(it, tri.j+NPS);
            Write<int>(it, tri.k+NPS);
        }
        NPS += nb_points_per_block[block_idx];
    }
    cout << "Writing " << tri_size << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}


/// \brief writes .ply file with triangles and edges
///
/// \param mls
/// \param edges: vector of edges
/// \param v_tri: vector of triangles
/// \param n_vertex: number of vertices
/// \param params
///
/// \note monoblock
void WritePly(XMls & mls, std::vector<std::pair<uint,uint> >edges, vector<Triangle> v_tri,
              int n_vertex, param params)
{
    ofstream fileOut(params.output);
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    int n_edges = 0;
    for (std::vector<std::pair<uint,uint> >::iterator it = edges.begin(); it != edges.end(); ++it)
        if ((*it).first < (uint)n_vertex && (*it).first > (uint)0 && (*it).second < (uint)n_vertex && (*it).second > (uint)0)
            ++n_edges;

    int n_tri = 0;
    for (std::vector<Triangle>::iterator it = v_tri.begin(); it != v_tri.end(); ++it)
        if ((*it).i < (uint)n_vertex && (*it).i >= 0 && (*it).j < (uint)n_vertex && (*it).j >= 0 && (*it).k < (uint)n_vertex && (*it).k >= 0)
            ++n_tri;

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }
    fileOut << "element edge " << n_edges << endl;
    fileOut << "property int vertex1" << endl;
    fileOut << "property int vertex2" << endl;
    //fileOut << "element face " << n_tri << endl;
    //fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    XFloatAttrib * p_refl=NULL;
    if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            XEchoIndex last_echo_idx = mls.IdxLastEcho(block_idx, pulse_idx);
            XPt3D Pw = mls.Pworld(block_idx, last_echo_idx);
            float e=(float)Pw.X-(float)params.pivot_E, n=(float)Pw.Y-(float)params.pivot_N, h=(float)Pw.Z-(float)params.pivot_H;
            //if (e < 1e-6 || n < 1e-6 || h < 1e-6)
            //    std::cout << e << " " << n << " " << h << " - " << std::flush;
            if (pulse_idx == 0)
                cout << e << " " << n << " " << h << endl;
            Write<float>(it, e);
            Write<float>(it, n);
            Write<float>(it, h);
            if(params.add_rgb == 1) // r g b mode
            {
                float g = 12.75f*(p_refl->at(block_idx)[last_echo_idx]+20.f); // rescale [-20,0] to [0,256] TODO: parameters
                if(g<0.f) g=0.f; else if(g>255.f) g=255.f;
                unsigned char ug = g;
                Write<unsigned char>(it, ug);
                Write<unsigned char>(it, ug);
                Write<unsigned char>(it, ug);
            }
            else if(params.add_rgb == 2) // quality mode
            {
                Write<float>(it, p_refl->at(block_idx)[last_echo_idx]);
            }
        }
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // edges list
    unsigned int edge_bytesize = 2*sizeof(int);
    unsigned long edge_buffer_size = edge_bytesize*edges.size();
    buffer = new char[edge_buffer_size]; it = buffer;
    for(auto & ed:edges)
    {
        //Write<unsigned char>(it, 2);
        if (ed.first < (uint)n_vertex && ed.second < (uint)n_vertex && ed.first >= (uint)0 && ed.second >= (uint)0)
        {
            Write<int>(it, ed.first);
            Write<int>(it, ed.second);
        }
        //std::cout << ed.first << "-" << ed.second << " ";
    }
    cout << "Writing " << n_edges << " edges of size " << edge_buffer_size << "=" << 1.e-6*edge_buffer_size << "MB" << endl;
    fileOut.write(buffer, edge_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(uint);
    unsigned long tri_buffer_size = tri_bytesize*v_tri.size();
    buffer = new char[tri_buffer_size]; it = buffer;
    /*for(auto & tri:v_tri)
    {
        if (tri.i+0 < (uint)n_vertex && tri.i+0 >= (uint)0 && tri.j+0 < (uint)n_vertex && tri.j+0 >= (uint)0 && tri.k+0 < (uint)n_vertex && tri.k+0 >= (uint)0)
        {
            //std::cout << tri.i+0 << "-" << tri.j+0 << "-" << tri.k+0 << " ";
            Write<unsigned char>(it, 3);
            Write<int>(it, tri.i+0);
            Write<int>(it, tri.j+0);
            Write<int>(it, tri.k+0);
        }
    }
    cout << "Writing " << n_tri << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;*/
    cout << "Total " << 1.e-6*(vertex_buffer_size+/*tri_buffer_size+*/edge_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}


/// \brief writes .ply file with edges (associated to their score) and triangles
///
/// \param mls
/// \param edges: odd pair of vector of edges and score
/// \param v_tri: vector of triangles
/// \param n_vertex: number of vertices
/// \param params
///
/// \warning deprecated
/// \warning edges are not stored this way anymore anywhere in the library hence the compatibility with other functions is not ensured
/// @bug vertex coordinates are trunkated or rounded to 2/3 digits in x and y
void WritePly(XMls & mls, std::pair<std::vector<std::pair<uint,uint> >, std::vector<double> > edges_score, vector<Triangle> v_tri,
              int n_vertex, param params)
{

    std::vector<std::pair<uint,uint> > edges = edges_score.first;
    std::vector<double> score = edges_score.second;

    ofstream fileOut(params.output);
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }

    int n_edges = 0;
    for (std::vector<std::pair<uint,uint> >::iterator it = edges.begin(); it != edges.end(); ++it)
        if ((*it).first < (uint)n_vertex && (*it).first > (uint)0 && (*it).second < (uint)n_vertex && (*it).second > (uint)0)
            ++n_edges;

    int n_tri = 0;
    for (std::vector<Triangle>::iterator it = v_tri.begin(); it != v_tri.end(); ++it)
        if ((*it).i < (uint)n_vertex && (*it).i >= 0 && (*it).j < (uint)n_vertex && (*it).j >= 0 && (*it).k < (uint)n_vertex && (*it).k >= 0)
            ++n_tri;

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.ept_folder << " secs " << mls.FirstSecond() << " to " << mls.LastSecond() << endl;
    fileOut << "comment IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    if(params.add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(params.add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }
    fileOut << "element edge " << n_edges << endl;
    fileOut << "property int vertex1" << endl;
    fileOut << "property int vertex2" << endl;
    fileOut << "property uchar red" << endl;
    fileOut << "property uchar green" << endl;
    fileOut << "property uchar blue" << endl;
    fileOut << "element face " << n_tri << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    XFloatAttrib * p_refl=NULL;
    if(params.add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    if(params.add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    else if(params.add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            for (XEchoIndex echo = mls.IdxFirstEcho(block_idx,pulse_idx); echo <= mls.IdxLastEcho(block_idx,pulse_idx); ++echo)
            {
                //XEchoIndex last_echo_idx = mls.IdxLastEcho(block_idx, pulse_idx);
                XPt3D Pw = mls.Pworld(block_idx, echo);
                float e=Pw.X-params.pivot_E, n=Pw.Y-params.pivot_N, h=Pw.Z-params.pivot_H;
                if (pulse_idx == 0)
                    cout << e << " " << n << " " << h << endl;
                Write<float>(it, e);
                Write<float>(it, n);
                Write<float>(it, h);
                if(params.add_rgb == 1) // r g b mode
                {
                    float g = 12.75f*(p_refl->at(block_idx)[echo]+20.f); // rescale [-20,0] to [0,256] TODO: parameters
                    if(g<0.f) g=0.f; else if(g>255.f) g=255.f;
                    unsigned char ug = g;
                    Write<unsigned char>(it, ug);
                    Write<unsigned char>(it, ug);
                    Write<unsigned char>(it, ug);
                }
                else if(params.add_rgb == 2) // quality mode
                {
                    Write<float>(it, p_refl->at(block_idx)[echo]);
                }
            }
        }
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // edges list
    unsigned int edge_bytesize = 2*sizeof(int)+3*sizeof(unsigned char);
    unsigned long edge_buffer_size = edge_bytesize*edges.size();
    buffer = new char[edge_buffer_size]; it = buffer;
    int score_it = 0;
    for(auto & ed:edges)
    {
        //Write<unsigned char>(it, 2);
        if (ed.first < (uint)n_vertex && ed.second < (uint)n_vertex && ed.first >= (uint)0 && ed.second >= (uint)0)
        {
            //if (ed.first >= 73670 && ed.first <= 73700) std::cout << ed.first << " " << ed.second << std::endl;
            //else if (ed.second>= 73670 && ed.second<= 73700) std::cout << ed.first << " " << ed.second << std::endl;
            Write<int>(it, ed.first);
            Write<int>(it, ed.second);
            double s = score[score_it++];
            Write<unsigned char>(it,(unsigned char)(s*255));
            Write<unsigned char>(it,(unsigned char)(s*255));
            Write<unsigned char>(it,(unsigned char)(0));
        }
        //std::cout << ed.first << "-" << ed.second << " ";
    }
    cout << "Writing " << n_edges << " edges of size " << edge_buffer_size << "=" << 1.e-6*edge_buffer_size << "MB" << endl;
    fileOut.write(buffer, edge_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(uint);
    unsigned long tri_buffer_size = tri_bytesize*v_tri.size();
    buffer = new char[tri_buffer_size]; it = buffer;
    for(auto & tri:v_tri)
    {
        if (tri.i+0 < (uint)n_vertex && tri.i+0 >= (uint)0 && tri.j+0 < (uint)n_vertex && tri.j+0 >= (uint)0 && tri.k+0 < (uint)n_vertex && tri.k+0 >= (uint)0)
        {
            //std::cout << tri.i+0 << "-" << tri.j+0 << "-" << tri.k+0 << " ";
            Write<unsigned char>(it, 3);
            Write<int>(it, tri.i+0);
            Write<int>(it, tri.j+0);
            Write<int>(it, tri.k+0);
        }
    }
    cout << "Writing " << n_tri << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+tri_buffer_size+edge_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}


/// \brief writes .off file with triangles
///
/// \param mls
/// \param v_tri: vector of triangles
/// \param n_vertex: number of vertices
/// \param params
void WriteOff(XMls & mls, vector<Triangle> v_tri,
              int n_vertex, param params)
{
    ofstream fileOut(params.output);
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }
    // write text header
    fileOut << "OFF" << endl;
    fileOut << "# Generated from " << params.ept_folder << " secs " << params.i_start << " to " << params.i_end << endl;
    fileOut << "# IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl << endl;
    // number of edges is not used in OFF but Meshlab fails if absent. It is long to compute so we provide an estimate (exact for watertight meshes)
    fileOut << n_vertex << " " << v_tri.size() << " " << v_tri.size() << endl;
    // vertex list
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            XPt3D Pw = mls.Pworld(block_idx, mls.IdxLastEcho(block_idx, pulse_idx));
            float e=Pw.X-params.pivot_E, n=Pw.Y-params.pivot_N, h=Pw.Z;
            fileOut << e << " " << n << " " << h << endl;
            if (pulse_idx == 0)
                cout << e << " " << n << " " << h << endl;
        }
        mls.Free(block_idx);
    }

    // triangle list
    for(auto & tri:v_tri) fileOut << "3 " << tri.i << " " << tri.j << " " << tri.k << endl;
    fileOut.close();
}


/// \brief writes .off file with edges
///
/// \warning never tested
///
/// \param mls
/// \param edges: vector of edges
/// \param n_vertex: number of vertices
/// \param params
void WriteOff(XMls & mls, std::vector<std::pair<uint,uint> >edges,
              int n_vertex, param params)
{
    ofstream fileOut(params.output);
    if(!fileOut.good())
    {
        cout << "Cannot open " + params.output + " for writing\n";
        return;
    }
    // write text header
    fileOut << "OFF" << endl;
    fileOut << "# Generated from " << params.ept_folder << " secs " << params.i_start << " to " << params.i_end << endl;
    fileOut << "# IGN offset Pos " << params.pivot_E << " " << params.pivot_N << " " << params.pivot_H << endl << endl;
    // number of edges is not used in OFF but Meshlab fails if absent. It is long to compute so we provide an estimate (exact for watertight meshes)
    fileOut << n_vertex << " " << edges.size() << " " << edges.size() << endl;
    // vertex list
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            XPt3D Pw = mls.Pworld(block_idx, mls.IdxLastEcho(block_idx, pulse_idx));
            float e=Pw.X-params.pivot_E, n=Pw.Y-params.pivot_N, h=Pw.Z;
            fileOut << e << " " << n << " " << h << endl;
            if (pulse_idx == 0)
                cout << e << " " << n << " " << h << endl;
        }
        mls.Free(block_idx);
    }

    // triangle list
    for(auto & tri:edges) fileOut << "2 " << tri.first << " " << tri.second << endl;
    fileOut.close();
}


/// \brief Given 3 echoes, computes maximum edge size between these echoes
///
/// \param mls
/// \param b1: 1st block
/// \param id1: 1st echo
/// \param b2: 2nd block
/// \param id2: 2nd echo
/// \param b3: 3rd block
/// \param id3: 3rd echo
///
/// \return max edge size between the 3 pulses
float Maxedgesize_echo(XMls & mls, XBlockIndex b1, XEchoIndex id1, XBlockIndex b2, XEchoIndex id2, XBlockIndex b3, XEchoIndex id3)
{
    XPt3D P1 = mls.Pworld(b1, id1);
    XPt3D P2 = mls.Pworld(b2, id2);
    XPt3D P3 = mls.Pworld(b3, id3);
    double d12 = (P1-P2).Norme();
    double d23 = (P2-P3).Norme();
    double d31 = (P3-P1).Norme();
    return max(d12,max(d23,d31));
}

//float Maxedgesize(XMls & mls, XBlockIndex b, XEchoIndex id1, XEchoIndex id2, XEchoIndex id3)
//{
//    return Maxedgesize(mls, b, id1, b, id2, b, id3);
//}


/// \brief Given 3 pulses, computes maximum edge size between these pulses
///
/// \param mls
/// \param b1: 1st block
/// \param id1: 1st pulse
/// \param b2: 2nd block
/// \param id2: 2nd pulse
/// \param b3: 3rd block
/// \param id3: 3rd pulse
///
/// \return max edge size between the 3 pulses
float Maxedgesize(XMls & mls, XBlockIndex b1, XPulseIndex id1, XBlockIndex b2, XPulseIndex id2, XBlockIndex b3, XPulseIndex id3)
{
    XPt3D P1 = mls.Pworld(b1, mls.IdxLastEcho(b1, id1));
    XPt3D P2 = mls.Pworld(b2, mls.IdxLastEcho(b2, id2));
    XPt3D P3 = mls.Pworld(b3, mls.IdxLastEcho(b3, id3));
    double d12 = (P1-P2).Norme();
    double d23 = (P2-P3).Norme();
    double d31 = (P3-P1).Norme();
    return max(d12,max(d23,d31));
}


/// \brief calls Maxedgesize functions when all points are in a same block
///
/// \param mls
/// \param b: block
/// \param id1: 1st pulse
/// \param id2: 2nd pulse
/// \param id3: 3rd pulse
///
/// \return maximum edge size
float Maxedgesize(XMls & mls, XBlockIndex b, XPulseIndex id1, XPulseIndex id2, XPulseIndex id3)
{
    //std::cout << Maxedgesize(mls, b, id1, b, id2, b, id3) << " " << std::flush;
    return Maxedgesize(mls, b, id1, b, id2, b, id3);
}


/// \brief make a wedge based on the four vertices ABXY = (AXB)+(ABY) triangles
///
/// \param mls
/// \param v_tri: vector of triangles
/// \param vv_pulse_with_echo_idx: vector containing each echo per block
/// \param tri_treshold: max length
/// \param ba: block of pulse 1
/// \param pa: index of pulse 1
/// \param bb: block of pulse 2
/// \param pb: index of pulse 2
/// \param bx: block of pulse 3
/// \param px: index of pulse 3
/// \param by: block of pulse 4
/// \param py: index of pulse 4
void MakeWedge(XMls & mls, vector<Triangle> & v_tri, vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
               XBlockIndex ba, XPulseIndex pa, XBlockIndex bb, XPulseIndex pb,
               XBlockIndex bx, XPulseIndex px, XBlockIndex by, XPulseIndex py)
{
    if(mls.NbOfEcho(ba, pa)==0 || mls.NbOfEcho(bb, pb) == 0) return;
    //cout << "ABXY (" << ba << "," << pa << ")("<< bb << "," << pb << ")("<< bx << "," << px << ")("<< by << "," << py << ")" << endl;
    int A=vv_pulse_with_echo_idx[ba][pa], B=vv_pulse_with_echo_idx[bb][pb];
    int X=vv_pulse_with_echo_idx[bx][px], Y=vv_pulse_with_echo_idx[by][py];
    if(mls.NbOfEcho(bx, px)>0)
    {
        if(Maxedgesize(mls, ba, pa, bb, pb, bx, px) < tri_threshold)
            v_tri.push_back(Triangle(A, B, X));
    }
    if(mls.NbOfEcho(by, py)>0)
    {
        if(Maxedgesize(mls, ba, pa, bb, pb, by, py) < tri_threshold)
            v_tri.push_back(Triangle(A, Y, B));
    }
}


/// \brief make a wedge based on the four vertices ABXY = (AXB)+(ABY) triangles
///
/// \param mls
/// \param v_tri: vector of triangles
/// \param vv_pulse_with_echo_idx: vector containing each echo per block
/// \param tri_treshold: max length
/// \param ba: block of echo 1
/// \param pa: index of echo 1
/// \param bb: block of echo 2
/// \param pb: index of echo 2
/// \param bx: block of echo 3
/// \param px: index of echo 3
/// \param by: block of echo 4
/// \param py: index of echo 4
///
/// \warning deprecated
void MakeWedge(XMls & mls, vector<Triangle> & v_tri, vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
               XBlockIndex ba, XEchoIndex pa, XBlockIndex bb, XEchoIndex pb,
               XBlockIndex bx, XEchoIndex px, XBlockIndex by, XEchoIndex py)
{
    //if(mls.NbOfEcho(ba, pa)==0 || mls.NbOfEcho(bb, pb) == 0) return;
    //cout << "ABXY (" << ba << "," << pa << ")("<< bb << "," << pb << ")("<< bx << "," << px << ")("<< by << "," << py << ")" << endl;
    /*int A=pa, B=pb;
    int X=px, Y=py;
    if(mls.NbOfEcho(bx, px)>0)
    {
        if(Maxedgesize(mls, ba, pa, bb, pb, bx, px) < tri_threshold)
            v_tri.push_back(Triangle(A, B, X));
    }
    if(mls.NbOfEcho(by, py)>0)
    {
        if(Maxedgesize(mls, ba, pa, bb, pb, by, py) < tri_threshold)
            v_tri.push_back(Triangle(A, Y, B));
    }*/
}


/// \brief vain tentative to display a progress bar
///
/// \param progression: int
/// \param max: int
///
/// \note actually it works but displays percentage on a new line each time
void progress (int progression, int max)
{
    //std::cout << progression << " " << max << "\n" << std::flush;
    int barWidth = 100;

    std::cout << "[";
    int pos = barWidth * ((int)progression/(int)max);
    for (int i = 0; i < barWidth; ++i) {
        if (i < pos) std::cout << "=";
        else if (i == pos) std::cout << ">";
        else std::cout << " ";
    }
    std::cout << "] " << int(progression/max * 100) << "%";
    std::cout << '\r' ;
    std::cout << std::flush;
}


/// \brief displays an exemple of how our maxflow algorithm works
///
/// \note Yay ! i display ascii art
void maxflow_help()
{
    std::cout   << " *  "
                << " *            source\n"
                << " *            /    \\ \n"
                << " *    c0(n0) /      \\  c0(n1)\n"
                << " *          /        \\ \n"
                << " *        n0          | \n"
                << " *         | \\        | \n"
                << " *         | c1(n0,n1)| \n"
                << " *         |        \\ | \n"
                << " *         |          n1 \n"
                << " *          \\        / \n"
                << " *  1-c0(n0) \\      / 1-c0(n1) \n"
                << " *            \\    / \n"
                << " *             sink \n"
                << " *  "
    << std::endl;
}

#include <iostream>
#include "libSteph/QPBO_Optim/QPBO_Optim.h"
#include "QPBO.h"

/// graph construction
/// with qpbo
///
//    E[3] = get_score_quad(ch1lab,chnlab);
//    E[2] = get_score_quad(ch1lab,lalpha);
//    E[1] = get_score_quad(lalpha,chnlab);
//    E[0] = get_score_quad(lalpha,lalpha);

//    double E_x1 = E[0] - E[2];
//    double E_bx2 = E[3] - E[2];
//    // Quadratic term should be positif
//    double E_quad = -E[0] + E[1] + E[2] - E[3];


//    if(E_x1 > 0)
//      g->add_tweights(c1Id, 0 , MULT*E_x1*coef);
//    else
//      g->add_tweights(c1Id,-1*MULT*E_x1*coef, 0);
//    if(E_bx2 > 0)
//      g->add_tweights(cnId, MULT*E_bx2*coef, 0 );
//    else
//      g->add_tweights(cnId,0, -1*MULT*E_bx2*coef);
//    g->add_edge(c1Id, cnId,    /* capacities */ MULT*E_quad*coef ,0);

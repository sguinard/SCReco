#include "libSteph/SensorTopologyTools.h"

#include "thread"
#include "mutex"

std::mutex amutex;

/// \file SensorTopologyTools.cpp
///
/// \details This file contains the implementation of the SensorTopology class

/// CONSTRUCTORS

/// \fn default constructor
///
/// \details place the pivot at (0,0,0) and create void vectors of points, edges and triangles.
SensorTopology::SensorTopology()
{
    pivot = XPt3D(0,0,0);
    observations = vvD{std::vector<double>(0)};
    edges = std::vector<edge>{edge(0,0)};
    triangles = std::vector<Triangle>{Triangle(0,0,0)};
    wedges = std::vector<Wedge>{Wedge(0,0,0,0)};
}

/// \fn constructor from vector of points
///
/// \details place the pivot at (0,0,0), copy vector of points and create void vectors edges and triangles.
SensorTopology::SensorTopology(vvD *_observations)
{
    pivot = XPt3D(0,0,0);
    observations = *_observations;
    edges = std::vector<edge>{edge(0,0)};
    triangles = std::vector<Triangle>{Triangle(0,0,0)};
    wedges = std::vector<Wedge>{Wedge(0,0,0,0)};
}

/// \fn constructor from pivot and vector of points
///
/// \details save pivot coordinates, copy observations and create void vectors of edges and triangles
SensorTopology::SensorTopology(XPt3D _pivot, vvD *_observations)
{
    pivot = _pivot;
    observations = *_observations;
    edges = std::vector<edge>{edge(0,0)};
    triangles = std::vector<Triangle>{Triangle(0,0,0)};
}

/// \fn constructor from pivot, vector points and edges
///
/// \details save pivot coordinates, copy observations and edges and create void vector of triangles
SensorTopology::SensorTopology(XPt3D _pivot, vvD *_observations, std::vector<edge> *_edges)
{
    pivot = _pivot;
    observations = *_observations;
    edges = *_edges;
    triangles = std::vector<Triangle>{Triangle(0,0,0)};
    wedges = std::vector<Wedge>{Wedge(0,0,0,0)};
}

/// \fn constructor from pivot, vector points and triangles
///
/// \details save pivot coordinates, copy observations and triangles and create void vector of edges
SensorTopology::SensorTopology(XPt3D _pivot, vvD *_observations, std::vector<Triangle> *_triangles)
{
    pivot = _pivot;
    observations = *_observations;
    edges = std::vector<edge>{edge(0,0)};
    triangles = *_triangles;
    wedges = std::vector<Wedge>{Wedge(0,0,0,0)};
}

/// \fn constructor from pivot, vector points, edges and triangles
///
/// \details save pivot coordinates, copy observations, edges and triangles
SensorTopology::SensorTopology(XPt3D _pivot, vvD *_observations, std::vector<edge> *_edges, std::vector<Triangle> *_triangles)
{
    pivot = _pivot;
    observations = *_observations;
    edges = *_edges;
    triangles = *_triangles;
    wedges = std::vector<Wedge>{Wedge(0,0,0,0)};
}

/// \fn default destructor
///
/// \details deletes points, edges and triangles.
SensorTopology::~SensorTopology()
{

}

void SensorTopology::get_triangles(std::vector<Triangle> *_triangles)
{
    *_triangles = triangles;
}

/// \fn check if observations exists and are not trivial
///
/// \todo check if observations are trivial: vector full of 0s or numeric_limits or all points identical - stuff like that
bool SensorTopology::has_valid_observations()
{
    return observations.size() > 0 ? true : false;
}

/// \fn check if edges exists and are not trivial
///
/// \todo check if edges are trivial: edges on 1 single point, stuff like that
bool SensorTopology::has_valid_edges()
{
    return edges.size() > 0 ? true : false;
}

/// \fn check if triangles exists and are not trivial
///
/// \todo check if triangles are trivial: triangle on 1 or 2 points, stuff like that
bool SensorTopology::has_valid_triangles()
{
    return triangles.size() > 0 ? true : false;
}


/// \fn finds the centroid of a point cloud
XPt3D SensorTopology::find_centroid()
{
    XPt3D centroid;
    for (auto &p:observations)
	centroid.operator +=(XPt3D(p.at(0),p.at(1),p.at(2)));
    centroid.operator /=(observations.size());
    return centroid;
}


/// \fn computes the standard deviation of a point cloud - useful for edge thresholding
double SensorTopology::compute_standard_deviation()
{
    XPt3D centroid = find_centroid();
    double std = 0;
    for (auto &p:observations)
	std += (centroid.X - p.at(0))*(centroid.X - p.at(0)) + (centroid.Y - p.at(1))*(centroid.Y - p.at(1)) + (centroid.Z * p.at(2))*(centroid.Z * p.at(2));
    std /= observations.size();
    return sqrt(std);
}


/// \fn computes the normal of a given wedge
XPt3D SensorTopology::compute_wedge_normal(Wedge w)
{
    XPt3D p1, p2, p3;
    p1.X = observations.at(w.e1)[0];
    p1.Y = observations.at(w.e1)[1];
    p1.Z = observations.at(w.e1)[2];
    p2.X = observations.at(w.e2)[0];
    p2.Y = observations.at(w.e2)[1];
    p2.Z = observations.at(w.e2)[2];
    p3.X = observations.at(w.e3)[0];
    p3.Y = observations.at(w.e3)[1];
    p3.Z = observations.at(w.e3)[2];

    return cross_product(p2.operator-=(p1),p3.operator-=(p1));
}


/// \fn computes edges between points
///
/// \todo write this method
void SensorTopology::compute_edges(double c0_tresh, double c1_tresh, uint32 nb_pulses_per_line)
{
    double d_tresh = compute_standard_deviation()/10;
    if (this->has_valid_observations())
    {
        edges.clear();

        for (uint32 p_index = 0; p_index != observations.size(); ++p_index)
        {
            bool added_next_point = false;
            bool added_next_line = false;
            bool added_next_line_next_point = false;
        // c0 regularity
	    XPt3D p1;   p1.X = observations.at(p_index)[0];
	                p1.Y = observations.at(p_index)[1];
	                p1.Z = observations.at(p_index)[2];
            // edge with next point?
            if (p_index < observations.size()-1)
            {
                XPt3D p2;   p2.X = observations.at(p_index+1)[0];
                            p2.Y = observations.at(p_index+1)[1];
                            p2.Z = observations.at(p_index+1)[2];
			    if (c0(pivot,p1,p2) > c0_tresh && d2(p1,p2) < d_tresh)
                {
                    edges.emplace_back(edge(p_index,p_index+1));
                    added_next_point = true;
                }
            }
            // edge with next line?
            if (p_index < observations.size()-nb_pulses_per_line)
            {
                XPt3D p2;   p2.X = observations.at(p_index+nb_pulses_per_line)[0];
                            p2.Y = observations.at(p_index+nb_pulses_per_line)[1];
                            p2.Z = observations.at(p_index+nb_pulses_per_line)[2];
			    if (c0(pivot,p1,p2) > c0_tresh && d2(p1,p2) < d_tresh)
                {
                    edges.emplace_back(edge(p_index,p_index+nb_pulses_per_line));
                    added_next_line = true;
                }
            }
            // edge with next line+1?
            if (p_index < observations.size()-nb_pulses_per_line-1)
            {
                XPt3D p2;   p2.X = observations.at(p_index+nb_pulses_per_line+1)[0];
                            p2.Y = observations.at(p_index+nb_pulses_per_line+1)[1];
                            p2.Z = observations.at(p_index+nb_pulses_per_line+1)[2];
			    if (c0(pivot,p1,p2) > c0_tresh && d2(p1,p2) < d_tresh)
                {
                    edges.emplace_back(edge(p_index,p_index+nb_pulses_per_line+1));
                    added_next_line_next_point = true;
                }
            }

        // c1 regularity
            // edge with next point? - before
            if (p_index < observations.size()-1 && p_index > 0 && !added_next_point)
            {
                XPt3D p2;   p2.X = observations.at(p_index+1)[0];
                            p2.Y = observations.at(p_index+1)[1];
                            p2.Z = observations.at(p_index+1)[2];
                XPt3D p3;   p3.X = observations.at(p_index-1)[0];
                            p3.Y = observations.at(p_index-1)[1];
                            p3.Z = observations.at(p_index-1)[2];
		if (c1(p1,p2,p3) < (c1_tresh*c0_tresh*c0(pivot,p1,p2)/(c0_tresh-c0(pivot,p1,p2))))
                {
                    edges.emplace_back(edge(p_index,p_index+1));
                    added_next_point = true;
                }
            }
            //edge with next point? - after
            if (p_index < observations.size()-2 && !added_next_point)
            {
                XPt3D p2;   p2.X = observations.at(p_index+1)[0];
                            p2.Y = observations.at(p_index+1)[1];
                            p2.Z = observations.at(p_index+1)[2];
                XPt3D p3;   p3.X = observations.at(p_index+2)[0];
                            p3.Y = observations.at(p_index+2)[1];
                            p3.Z = observations.at(p_index+2)[2];
	        if (c1(p1,p2,p3) < (c1_tresh*c0_tresh*c0(pivot,p1,p2)/(c0_tresh-c0(pivot,p1,p2))))
                {
                    edges.emplace_back(edge(p_index,p_index+1));
                    added_next_point = true;
                }
            }
            // edge with next line? - before
            if (p_index < observations.size()-nb_pulses_per_line && p_index > nb_pulses_per_line && !added_next_line)
            {
                XPt3D p2;   p2.X = observations.at(p_index+nb_pulses_per_line)[0];
                            p2.Y = observations.at(p_index+nb_pulses_per_line)[1];
                            p2.Z = observations.at(p_index+nb_pulses_per_line)[2];
                XPt3D p3;   p3.X = observations.at(p_index-nb_pulses_per_line)[0];
                            p3.Y = observations.at(p_index-nb_pulses_per_line)[1];
                            p3.Z = observations.at(p_index-nb_pulses_per_line)[2];
		if (c1(p1,p2,p3) < (c1_tresh*c0_tresh*c0(pivot,p1,p2)/(c0_tresh-c0(pivot,p1,p2))))
                {
                    edges.emplace_back(edge(p_index,p_index+nb_pulses_per_line));
                    added_next_line = true;
                }
            }
            // edge with next line? - after
            if (p_index < observations.size()-nb_pulses_per_line*2 && !added_next_line)
            {
                XPt3D p2;   p2.X = observations.at(p_index+nb_pulses_per_line)[0];
                            p2.Y = observations.at(p_index+nb_pulses_per_line)[1];
                            p2.Z = observations.at(p_index+nb_pulses_per_line)[2];
                XPt3D p3;   p3.X = observations.at(p_index+2*nb_pulses_per_line)[0];
                            p3.Y = observations.at(p_index+2*nb_pulses_per_line)[1];
                            p3.Z = observations.at(p_index+2*nb_pulses_per_line)[2];
	        if (c1(p1,p2,p3) < (c1_tresh*c0_tresh*c0(pivot,p1,p2)/(c0_tresh-c0(pivot,p1,p2))))
                {
                    edges.emplace_back(edge(p_index,p_index+nb_pulses_per_line));
                    added_next_line = true;
                }
            }
            // edge with next line next point? - before
            if (p_index < observations.size()-nb_pulses_per_line-1 && p_index > nb_pulses_per_line+1 && !added_next_line_next_point)
            {
                XPt3D p2;   p2.X = observations.at(p_index+nb_pulses_per_line+1)[0];
                            p2.Y = observations.at(p_index+nb_pulses_per_line+1)[1];
                            p2.Z = observations.at(p_index+nb_pulses_per_line+1)[2];
                XPt3D p3;   p3.X = observations.at(p_index-nb_pulses_per_line-1)[0];
                            p3.Y = observations.at(p_index-nb_pulses_per_line-1)[1];
                            p3.Z = observations.at(p_index-nb_pulses_per_line-1)[2];
	        if (c1(p1,p2,p3) < (c1_tresh*c0_tresh*c0(pivot,p1,p2)/(c0_tresh-c0(pivot,p1,p2))))
                {
                    edges.emplace_back(edge(p_index,p_index+nb_pulses_per_line));
                    added_next_line_next_point = true;
                }
            }
            // edge with next line next point? - after
            if (p_index < observations.size()-2*nb_pulses_per_line-2 && !added_next_line_next_point)
            {
                XPt3D p2;   p2.X = observations.at(p_index+nb_pulses_per_line+1)[0];
                            p2.Y = observations.at(p_index+nb_pulses_per_line+1)[1];
                            p2.Z = observations.at(p_index+nb_pulses_per_line+1)[2];
                XPt3D p3;   p3.X = observations.at(p_index+2*nb_pulses_per_line+2)[0];
                            p3.Y = observations.at(p_index+2*nb_pulses_per_line+2)[1];
                            p3.Z = observations.at(p_index+2*nb_pulses_per_line+2)[2];
		if (c1(p1,p2,p3) < (c1_tresh*c0_tresh*c0(pivot,p1,p2)/(c0_tresh-c0(pivot,p1,p2))))
                {
                    edges.emplace_back(edge(p_index,p_index+nb_pulses_per_line));
                    added_next_line_next_point = true;
                }
            }
        }
    }
    else
    {
        std::cerr << "Observations are not valid - cannot compute edges." << std::endl;
        return;
    }
    std::cout << "I counted " << edges.size() << " edges" << std::endl;
    return;
}

/// \fn computes triangles between points
///
/// \todo write this method
void SensorTopology::compute_triangles_from_edges()
{    
    if (this->has_valid_observations() && this->has_valid_edges())
    {
        triangles.clear();
        std::map<uint32,std::vector<edge> >* edge_map = new std::map<uint32,std::vector<edge> >;
        // browse edges and store them in a map of vertex indices - vector of edges
        for (auto& e: edges)
        {
            if (e.is_valid())
            {
                uint32 index1 = e.node_begin;
                uint32 index2 = e.node_end;
                edge_map->operator[](index1).emplace_back(e);   // initialization when edge_map->at(index1) is void
                edge_map->operator[](index2).emplace_back(e);
            }
        }

        // create triangles for triplets of self-connected edges
        for (std::map<uint32,std::vector<edge> >::iterator eit = edge_map->begin(); eit != edge_map->end(); ++eit)
        {
            uint32 index1 = eit->first;
            for (auto& e: eit->second)
            {
                uint32 index2 = e.node_begin == index1 ? e.node_end : e.node_begin;
		//std::cout << index1 << " " << index2 << std::endl;
                for (auto& e2: edge_map->at(index2))
                {
                    uint32 index3 = e2.node_begin == index2 ? e2.node_end : e2.node_begin;
		    //std::cout << index3 << std::endl;
                    edge e3 (index1,index3);
                    if (std::find(edge_map->at(index1).begin(), edge_map->at(index1).end(), e3) != edge_map->at(index1).end() && is_triangle(e,e2,e3))
                        triangles.emplace_back(Triangle(index1,index2,index3));
                }
            }
        }
    }
    else
    {
        std::cerr << "Observations or Edges are not valid - cannot compute triangles." << std::endl;
        return;
    }
    return;
}


/// \fn compute wedges (sets of 4 self-connected edges) for regularization
///
/// \todo write this function
void SensorTopology::compute_wedges()
{
    if (has_valid_observations() && has_valid_edges())
	{
	    wedges.clear();
	    std::map<uint32,std::vector<edge> >* edge_map = new std::map<uint32,std::vector<edge> >;
	    for (auto& e: edges)
		{
		    uint32 index1 = e.node_begin;
		    uint32 index2 = e.node_end;
		    edge_map->operator[](index1).emplace_back(e);
		    edge_map->operator[](index2).emplace_back(e);
		}

	    for (std::map<uint32,std::vector<edge> >::iterator eit = edge_map->begin(); eit != edge_map->end(); ++eit)
		{
		    uint32 index1 = eit->first;
		    for (auto& e: eit->second)
			{
			    uint32 index2 = e.node_begin == index1 ? e.node_end : e.node_begin;
			    for (auto& e2: edge_map->at(index2))
				{
				    uint32 index3 = e2.node_begin == index2 ?
					e2.node_end == index1 ? ~(uint32)0 : (uint32)e2.node_end :
					e2.node_begin == index1 ? ~(uint32)0 : (uint32)e2.node_begin;
				    if (index3 == ~(uint32)0) break;
				    for (auto& e3: edge_map->at(index3))
					{
					    uint32 index4 = e3.node_begin == index3 ?
						e3.node_end == index1 ? ~(uint32)0 :
						e3.node_end == index2 ? ~(uint32)0 : (uint32)e3.node_end :
						e3.node_begin == index1 ? ~(uint32)0 :
						e3.node_begin == index2 ? ~(uint32)0 : (uint32)e3.node_begin;
					    if (index4 == ~(uint32)0) break;
					    edge e4(index4,index1);
					    if (std::find(edge_map->at(index1).begin(), edge_map->at(index1).end(), e4) != edge_map->at(index1).end())
						wedges.emplace_back(Wedge(index1,index2,index3,index4));
					}
				}
			}
		}
	}

    else
	{
	    std::cerr << "Observations or Edges are not valid - cannot compute wedges" << std::endl;
	}
    
    return;
}


/// \fn filter wedges according to adjacency to other wedges
///
/// \todo write this function
void SensorTopology::filter_wedges(double wedge_tresh)
{
    typedef std::vector<Wedge> container;
    typedef container::const_iterator iter;

    container vw = wedges;
    container vw_temp = wedges;
    // creates map of wedges
    const Mw* mw = new const Mw(vw);

    wedges.clear();

    auto worker = [&] (iter begin, iter end)
	{
	    int n = 0;
	    for (auto& wit:vw)
		{
		    ++n;
		    amutex.lock();
		    std::vector<std::pair<Wedge,edge_number> > adjacency = find_wedge_m(mw,wit);
		    XPt3D normal = compute_wedge_normal(wit); /// \todo add this function
		    //normal.Normalise();
		    amutex.unlock();

		    if (adjacency.size() >= 2)
			{
			    std::vector<XPt3D> adjacency_normals;
			    for (auto &pair_adjacent_wedge: adjacency)
				{
				    adjacency_normals.emplace_back(compute_wedge_normal(pair_adjacent_wedge.first));
				    //(adjacency_normals.back()-1).Normalise();
				}

			    /// \note compute angle between normals and look for continuity
			    int i=0;
			    std::vector<edge_number> semi_continuity;
			    semi_continuity.clear();

			    for (auto &adjacent_normal: adjacency_normals)
				if (abs(normal.X * adjacent_normal.X + normal.Y * adjacent_normal.Y + normal.Z * adjacent_normal.Z) < wedge_tresh)
				    semi_continuity.emplace_back(adjacency.at(i).second);
			    ++i;

			    bool odd = false, even = false;
			    for (auto &c: semi_continuity)
				c%2 == 0 ? odd = true : even = true;
			    if (odd && even)
				{
				    amutex.lock();
				    vw_temp.emplace_back(wit);
				    amutex.unlock();
				}
			}
		}
	};
    
    // calling parallel stuff
    std::vector<std::thread> threads(std::thread::hardware_concurrency());
    const int grainsize = vw.size() / std::thread::hardware_concurrency();
    auto work_iter = std::begin(vw);

    for (auto it = std::begin(threads); it != std::end(threads)-1; ++it)
	{
	    *it = std::thread(worker, work_iter, work_iter+grainsize);
	    work_iter += grainsize;
	}
    threads.back() = std::thread(worker, work_iter, std::end(vw));

    for (auto &&i: threads)
	i.join();

    wedges = vw_temp;
    delete mw;
    std::cout << "I haz " << wedges.size() << " wedges" << std::endl;
    return;
}


/// \fn computes triangles from set of wedges (4 self-connected edges)
///
/// \todo write this function
void SensorTopology::compute_triangles_from_wedges()
{
    triangles.clear();

    for (auto &w: wedges)
	{
	    triangles.emplace_back(Triangle(w.e1,w.e2,w.e3));
	    triangles.emplace_back(Triangle(w.e4,w.e2,w.e3));
	}

    std::sort(triangles.begin(),triangles.end());
    triangles.erase(unique(triangles.begin(), triangles.end()), triangles.end());
}


/// \fn computes the whole simplicial complexes reconstruction pipeline
///
/// \todo write this function
void SensorTopology::compute_simplicial_complexes_reconstruction_from_observations(double c0_tresh, double c1_tresh, double wedge_tresh, double edge_tresh, uint32 nb_pulses_per_line)
{
    // find edges
    compute_edges(c0_tresh, c1_tresh, nb_pulses_per_line);

    // find wedges
    compute_wedges();

    // filter wedges
    filter_wedges(wedge_tresh);

    // find triangles from wedges
    compute_triangles_from_wedges();
}

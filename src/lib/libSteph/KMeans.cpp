#include "libSteph/KMeans.h"
#include "map"

#include <ctime>

/// \brief Default constructor
KMeans::KMeans()
{
    vP3 points;
    vP  planes;
    energy_trivial = -1;
    energy = -1;
}

/// \brief Constructor from vector of 3D points
/// This constructor also computes the 1st kmeans iteration
/// in order to compute trivial energy
///
/// \param v_points : reference to a vector of 3D points
KMeans::KMeans(vP3 *v_points)
{
    points = *v_points;
    vP planes;
    energy_trivial = 0;
    energy = 1e12;

    // 1st iteration of KMeans
    /*Plane p(v_points);
    for (vP3_iter point_iter = points.begin(); point_iter != points.end(); point_iter++)
        energy_trivial += p.quadratic_distance_point_plane(*point_iter);*/
    nth_iteration(1);
    energy_trivial = energy;
}

/// \brief Destructor
KMeans::~KMeans()
{

}


/// \brief find if a given point index is in a map of points indexes
///
/// \param m : map of point indexes
/// \param p : point index
bool KMeans::find_point_in_map_of_planes_and_vi(MPvI* m, int p)
{
    for (MPvI::iterator m_iter = m->begin(); m_iter != m->end(); ++m_iter)
        if (std::find(m_iter->second.begin(), m_iter->second.end(), p) != m_iter->second.end())
            return true;
    return false;
}


/// \brief kmeans algorithm for n clusters
///
/// \param n : number of clusters
void KMeans::nth_iteration(int n)
{
    std::cout << "KMeans iteration n°" << n << std::endl;

    vI* list_points = new vI;
    vP* list_planes = new vP;
    vvP3* points_by_planes = new vvP3;

    for (int i = 0; i != n; i++)
        points_by_planes->emplace_back(new vP3);

    // find points for each plane
    for (int dim = 0; dim != n; dim++)
        for (int i = 0; i != 3; i++)
        {
            int pos = rand() % points.size();
            while (point_in_vI(list_points,pos)) pos = rand() % points.size();
            list_points->emplace_back(pos);
        }

    // create planes
    for (unsigned int i = 0; i < list_points->size(); i+=3)
        list_planes->emplace_back(Plane(points.at(list_points->at(i)),points.at(list_points->at(i+1)),points.at(list_points->at(i+2))));

    // refine planes
    for (int i = 0; i != 10; i++)
    {
        points_by_planes->clear();
        for (int i = 0; i != n; i++)
            points_by_planes->emplace_back(new vP3);

        // associate each point to its closest plane and compute energy
        energy = 0;
        for (vP3_iter point_iter = points.begin(); point_iter != points.end(); point_iter++)
        {
            int plane_pos = 0;
            int closest_plane = -1;
            double closest_distance = 1e6;
            for (vP_iter plane_iter = list_planes->begin(); plane_iter != list_planes->end(); plane_iter++, plane_pos++)
            {
                double d = plane_iter->quadratic_distance_point_plane(*point_iter);
                if (d < closest_distance)
                {
                    closest_distance = d;
                    closest_plane = plane_pos;
                }
            }
            energy += closest_distance;
            points_by_planes->at(closest_plane)->emplace_back(*point_iter);
        }

        // re compute planes
        int plane_index = 0;
        for (vP_iter plane_iter = list_planes->begin(); plane_iter != list_planes->end(); plane_iter++, plane_index++)
            *plane_iter = Plane(points_by_planes->at(plane_index));

    }

    std::cout << "KMeans energy: " << energy << " " << energy/energy_trivial << std::endl;

    planes = *list_planes;

    delete list_points;
    delete list_planes;

}

/// \brief kmeans algorithm for n clusters
///
/// \param n : number of clusters
void KMeans::nth_iteration_ransac(int n)
{
    std::cout << "KMeans iteration n°" << n << std::endl;

    vI* list_points = new vI;
    vI* list_points_ransac = new vI;
    vP* list_planes = new vP;
    vP* list_planes_ransac = new vP;
    vvP3* points_by_planes = new vvP3;

    for (int i = 0; i != n; i++)
        points_by_planes->emplace_back(new vP3);

    // find points for each plane
    for (int dim = 0; dim != n*100; dim++)
    {
        for (int i = 0; i != 3; i++)
        {
            int pos = rand() % points.size();
            while (point_in_vI(list_points,pos)) pos = rand() % points.size();
            list_points->emplace_back(pos);
        }
        list_points_ransac->emplace_back(0);
    }

    // create planes
    for (int i = 0; i < list_points->size(); i+=3)
        list_planes_ransac->emplace_back(Plane(points.at(list_points->at(i)),points.at(list_points->at(i+1)),points.at(list_points->at(i+2))));
    //list_planes_ransac->at(0).print();
    // evaluate planes
    for (vP3_iter point_iter = points.begin(); point_iter != points.end(); point_iter++)
    {
        //std::cout << point_iter->X << " " << std::flush;
        int plane_pos = 0;
        int closest_plane = -1;
        double closest_distance = 1e6;
        for (vP_iter plane_iter = list_planes_ransac->begin(); plane_iter != list_planes_ransac->end(); plane_iter++, plane_pos++)
        {
            double d = plane_iter->quadratic_distance_point_plane(*point_iter);
            if (d < closest_distance)
            {//plane_iter->print();
                closest_distance = d;
                closest_plane = plane_pos;
                //std::cout << d << " " << closest_distance << " - " << std::flush;
            }
            if (d < 0.1) list_points_ransac->at(plane_pos)+=1;
            //std::cout << closest_distance << " " << std::flush;
        }
        if (closest_distance < 0.1)
        list_points_ransac->at(closest_plane)+=1;
        //std::cout << closest_distance << " " << std::flush;
    }
    // select best ones
    vI* best_planes = new vI;
    for (int i = 0; i != n; i++)
    {
        int best_plane = -1;
        int highest_nb_points = -1;
        int pos = 0;
        for (vI_iter it = list_points_ransac->begin(); it != list_points_ransac->end(); it++, pos++)
        {
            //std::cout << *it << " " << std::flush;
            if (*it > highest_nb_points && ( best_planes->empty() || std::find(best_planes->begin(),best_planes->end(),pos) == best_planes->end()) )
            {
                best_plane = pos;
                highest_nb_points = *it;
                //std::cout << best_plane << " " << std::flush;
            }
        }
        best_planes->emplace_back(best_plane);
        std::cout << best_plane << " " << std::flush;
    }
    for (vI_iter it = best_planes->begin(); it != best_planes->end(); it++)
        list_planes->emplace_back(list_planes_ransac->at(*it));
    //std::cout << best_planes->at(0) << std::endl;

    //std::cout << list_planes->size() << " planes" << std::endl;
    //list_planes->begin()->print();
    //list_planes->end()->print();

    // refine planes
    for (int i = 0; i != 10; i++)
    {
        points_by_planes->clear();
        for (int i = 0; i != n; i++)
            points_by_planes->emplace_back(new vP3);

        // associate each point to its closest plane and compute energy
        energy = 0;
        for (vP3_iter point_iter = points.begin(); point_iter != points.end(); point_iter++)
        {
            int plane_pos = 0;
            int closest_plane = -1;
            double closest_distance = 1e6;
            for (vP_iter plane_iter = list_planes->begin(); plane_iter != list_planes->end(); plane_iter++, plane_pos++)
            {
                double d = plane_iter->quadratic_distance_point_plane(*point_iter);
                if (d < closest_distance)
                {
                    closest_distance = d;
                    closest_plane = plane_pos;
                }
            }
            energy += closest_distance;
            points_by_planes->at(closest_plane)->emplace_back(*point_iter);
        }
        //std::cout << energy << std::endl;

        // re compute planes
        int plane_index = 0;
        //for (vP_iter plane_iter = list_planes->begin(); plane_iter != list_planes->end(); plane_iter++, plane_index++)
        //    *plane_iter = Plane(points_by_planes->at(plane_index));

    }

    std::cout << "KMeans energy: " << energy << " " << energy/energy_trivial << std::endl;

    planes = *list_planes;

    delete list_points;
    delete list_planes;

}




/// \brief kmeans algorithm for n clusters
///
/// \param n : number of clusters
/// \param d_tresh : a threshold used ot associate a point to a plane if it is close enough.
/// We do this in order not to penalize 2 planes that are REALLY close to a same object by associating to each, only half the points of the object as closest point
/// But, using this parameter, we reward the closest plane by counting twice the point
/// \param verbose : let the algorithm tel you the details of the computation. Default = true
///
/// \details
/// -# initialize containers
/// -# find 3 random points and create \f$ n \times 100\f$ planes
/// -# for each dimension
///     -# evaluate plane and keep their closest points
///     -# select best one
///     -# remove and save it
/// -# refine planes
///     -# associate each point to its closest point and compute energy
///     -# re-compute planes based on their set of closest points
void KMeans::nth_iteration_ransac_custom(int n, double d_tresh, bool verbose = true)
{

    std::cout << "KMeans iteration n°" << n << std::endl;


    // initialize stuff
    if (verbose) std::cout << "Initializing stuff" << std::endl;
    vI* points_used = new vI;
    vI* list_points = new vI;
    vI* list_points_ransac = new vI;
    vI* best_planes = new vI;
    vP* list_planes = new vP;
    vP* list_planes_ransac = new vP;
    vP3 points_temp;
    vvP3* points_by_planes = new vvP3;
    MPvI* point_close_to_plane = new MPvI;
    MPvI* point_close_to_plane_kept = new MPvI;


    // find points for each plane
    if (verbose) std::cout << "Finding points for each plane" << std::endl;
    for (int i = 0; i != n; i++)
        points_by_planes->emplace_back(new vP3);


    // find points for each plane
    for (int dim = 0; dim != n*1000; dim++)
    {
        for (int i = 0; i != 3; i++)
        {
            int pos = rand() % points.size();
            while (point_in_vI(list_points,pos)) pos = rand() % points.size();
            list_points->emplace_back(pos);
        }
        list_points_ransac->emplace_back(0);
    }


    // create planes
    if (verbose) std::cout << "Creating planes" << std::endl;
    for (int i = 0; i < list_points->size(); i+=3)
        list_planes_ransac->emplace_back(Plane(points.at(list_points->at(i)),points.at(list_points->at(i+1)),points.at(list_points->at(i+2))));

    for (vP_iter p_iter = list_planes_ransac->begin(); p_iter != list_planes_ransac->end(); ++p_iter)
        //if (point_close_to_plane->find(*p_iter) != point_close_to_plane->end())
            point_close_to_plane->operator [](*p_iter).emplace_back(-1);


    // for each dimension
    if (verbose) std::cout << "KMeans main loop" << std::endl;
    points_temp = points;
    for (int dim = 0; dim != n; dim++)
    {

        for (vP_iter p_iter = list_planes_ransac->begin(); p_iter != list_planes_ransac->end(); ++p_iter)
            if (point_close_to_plane->find(*p_iter) != point_close_to_plane->end())
                point_close_to_plane->operator [](*p_iter).clear();

        // evaluate planes
        if (verbose) std::cout << "Evaluating planes for iteration nb°" << dim+1 << std::endl;
        int nb_point = 0;
        for (vP3_iter point_iter = points_temp.begin(); point_iter != points_temp.end(); point_iter++, nb_point++)
        {
            //std::cout << point_iter->X << " " << std::flush;
            int plane_pos = 0;
            int closest_plane = -1;
            double closest_distance = 1e6;
            for (vP_iter plane_iter = list_planes_ransac->begin(); plane_iter != list_planes_ransac->end(); plane_iter++, plane_pos++)
            {
                double d = plane_iter->quadratic_distance_point_plane(*point_iter);
                // we only count points if they are close to a plane
                // but to not penalize 2 planes that will be good for each point,
                // we increment number of points for borh planes
                if (d < closest_distance)
                {
                    closest_distance = d;
                    closest_plane = plane_pos;
                }

                //std::clock_t    start;
                //start = std::clock();

                if (d < d_tresh /*&& std::find(points_used->begin(), points_used->end(), nb_point) == points_used->end()*/ /*!find_point_in_map_of_planes_and_vi(point_close_to_plane_kept,nb_point)*/)
                {
                    list_points_ransac->at(plane_pos)+=1;
                    point_close_to_plane->operator [](*plane_iter).emplace_back(nb_point);
                }

                //std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
            }
            //if (closest_distance < 0.1)
            //list_points_ransac->at(closest_plane)+=1;
            //point_close_to_plane->operator [](list_planes_ransac->at(closest_plane)).emplace_back(nb_point);
        }


        // select best one
        if (verbose) std::cout << "Selecting best planes" << std::endl;
        int best_plane = -1;
        int highest_nb_points = -1;
        int pos = 0;
        for (vI_iter it = list_points_ransac->begin(); it != list_points_ransac->end(); it++, pos++)
        {
            if (*it > highest_nb_points /*&& ( best_planes->empty() || std::find(best_planes->begin(),best_planes->end(),pos) == best_planes->end())*/ )
            {
                best_plane = pos;
                highest_nb_points = *it;
            }
        }
        best_planes->emplace_back(best_plane);
        Plane p = list_planes_ransac->operator [](best_plane);
        point_close_to_plane_kept->operator [](p) = point_close_to_plane->operator [](p);
        points_used->insert(points_used->end(), point_close_to_plane->operator [](p).begin(), point_close_to_plane->operator [](p).end());
        std::cout << best_plane << " " << std::flush;

        nb_point = 0;
        points_temp.clear();
        for (vP3_iter point_iter = points.begin(); point_iter != points.end(); point_iter++, nb_point++)
        {
            if (std::find(points_used->begin(), points_used->end(), nb_point) == points_used->end())
                points_temp.emplace_back(*point_iter);
        }

        // remove best one and store it
    }


    for (vI_iter it = best_planes->begin(); it != best_planes->end(); it++)
        list_planes->emplace_back(list_planes_ransac->at(*it));


//    // refine planes
//    if (verbose) std::cout << "Refining planes" << std::endl;
//    for (int i = 0; i != 5; i++)
//    {
//        points_by_planes->clear();
//        for (int j = 0; j != n; j++)
//            points_by_planes->emplace_back(new vP3);

//        // associate each point to its closest plane and compute energy
//        energy = 0;
//        for (vP3_iter point_iter = points.begin(); point_iter != points.end(); point_iter++)
//        {
//            int plane_pos = 0;
//            int closest_plane = -1;
//            double closest_distance = 1e6;
//            for (vP_iter plane_iter = list_planes->begin(); plane_iter != list_planes->end(); plane_iter++, plane_pos++)
//            {
//                double d = plane_iter->quadratic_distance_point_plane(*point_iter);
//                if (d < closest_distance)
//                {
//                    closest_distance = d;
//                    closest_plane = plane_pos;
//                }
//            }
//            energy += closest_distance;
//            points_by_planes->at(closest_plane)->emplace_back(*point_iter);
//        }

//        // re compute planes
//        int plane_index = 0;
//        for (vP_iter plane_iter = list_planes->begin(); plane_iter != list_planes->end(); plane_iter++, plane_index++)
//            *plane_iter = Plane(points_by_planes->at(plane_index));

//    }


    std::cout << "KMeans energy: " << energy << " " << energy/energy_trivial << std::endl;

    planes = *list_planes;

}


/// \brief function for solving kmeans problem, given a certain precision
///
/// \param treshold : ratio of energy to stop kmeans, vary between 0 and 1
void KMeans::solve(double treshold)
{
    int dim = 1;
    std::cout << "KMeans trivial energy: " << energy_trivial << std::endl;
    for (int i=0; i!=3; i++)
    //while (energy / energy_trivial > treshold)
        nth_iteration_ransac_custom(dim++,.3);
}

/// \brief function for storing the clusters and color points according to their belonging region
///
/// \param mls
/// \param params
void KMeans::store(XMls& mls, param params)
{
    std::vector<std::vector<std::pair<XPt3D,Color> > > colored_points;
    std::vector<std::vector<std::pair<XPt3D,Color> > > project_points;
    std::vector<std::pair<XPt3D,Color> > blabla;
    colored_points.emplace_back(blabla);
    project_points.emplace_back(blabla);
    vC* colors = new vC;

    for (unsigned int i = 0; i != planes.size(); i++)
    {
        unsigned char r = rand() % 255;
        unsigned char g = rand() % 255;
        unsigned char b = rand() % 255;
        colors->emplace_back(Color(r,g,b));
    }

    for (vP3_iter point_iter = points.begin(); point_iter != points.end(); point_iter++)
    {
        double min_d = 1e6;
        int min_plane_idx = -1;
        int current_plane_idx = 0;
        XPt3D coords;
        XPt3D project;
        for (vP_iter plane_iter = planes.begin(); plane_iter != planes.end(); plane_iter++, current_plane_idx++)
        {
            double d = plane_iter->quadratic_distance_point_plane(*point_iter);
            if (d < min_d)
            {
                min_d = d;
                min_plane_idx = current_plane_idx;
                coords = *point_iter;
                project = plane_iter->project_point_plane(*point_iter);
                //std::cout << abs(coords.X - point_iter->X) << " " << abs(coords.Y - point_iter->Y) << " " << abs(coords.Z - point_iter->Z) << std::endl;
            }
        }
        colored_points[0].emplace_back(std::make_pair(coords/*point_iter*/,colors->at(min_plane_idx)));
        project_points[0].emplace_back(std::make_pair(project/*point_iter*/,colors->at(min_plane_idx)));
    }

    WritePlyPointColor(mls,colored_points,params,"_observations.ply");
    WritePlyPointColor(mls,project_points,params,"_values.ply");
}

/// \brief function for storing the clusters and color points according to their belonging region
///
/// \param mls
/// \param params
///
/// \warning not implemented yet
void KMeans::color_points(XMls& mls, param params, std::vector<uint32> inComponent, std::vector<Triangle> vt)
{

}

#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"
#include "libSteph/Wedge.h"

//////////////////////////
/// @brief Constructors
//////////////////////////

/// @brief Default constructor
/// @note 0xffffffff is max uint value, this way we do not mistake with actual points
Wedge::Wedge()
{
    e1 = ~0;
    e2 = ~0;
    e3 = ~0;
    e4 = ~0;
}

/// @brief Standard constructor with 2 triangles
/*Wedge::Wedge(Triangle t1, Triangle t2)
{
    if (adjacent(t1,t2))
    {
        this->t1 = t1;
        this->t2 = t2;
    }
    else Wedge();
}*/

/// @brief Standard constructor with 4 points
/*Wedge::Wedge(XPt3D p1, XPt3D p2, XPt3D p3, XPt3D p4)
{
    this->p1 = p1;
    this->p2 = p2;
    this->p3 = p3;
    this->p4 = p4;
}*/

/// @brief Standard constructor with 1 block and 4 echoes
/*Wedge::Wedge(XMls &mls, XBlockIndex b, XEchoIndex e1, XEchoIndex e2, XEchoIndex e3, XEchoIndex e4)
{
    mls.Load(b);            /// @note thinking about sorting echoes to ensure acquisition order
    this->p1 = mls.Pworld(b,e1);
    this->p2 = mls.Pworld(b,e2);
    this->p3 = mls.Pworld(b,e3);
    this->p4 = mls.Pworld(b,e4);
    mls.Free(b);
}*/

Wedge::Wedge(XEchoIndex e1, XEchoIndex e2, XEchoIndex e3, XEchoIndex e4)
{
    std::vector<XEchoIndex> e;
    e.emplace_back(e1);
    e.emplace_back(e2);
    e.emplace_back(e3);
    e.emplace_back(e4);
    std::sort (e.begin(), e.end());
    std::vector<XEchoIndex>::iterator vit = e.begin();
    this->e1 = *vit;++vit;
    this->e2 = *vit;++vit;
    this->e3 = *vit;++vit;
    this->e4 = *vit;++vit;
}

/// @brief Destructor
Wedge::~Wedge()
{

}

///////////////////////
/// @brief operators
///////////////////////

/// @brief equality operator for wedges
bool Wedge::operator ==(Wedge w)
{
    return this->e1 == w.e1 && this->e2 == w.e2 && this->e3 == w.e3 && this->e4 == w.e4 ? true : false;
}

/// @brief non-equality operator for wedges
bool Wedge::operator !=(Wedge w)
{
    return !(this->operator ==(w));
}

bool Wedge::operator <(Wedge w)
{
    return  e1 < w.e1 ? true :
                e2 < w.e2 ? true :
                    e3 < w.e3 ? true :
                        e4 < w.e4 ? true : false;
}

///////////////////////////////////
/// @brief custom adjacency search
///////////////////////////////////

/// @brief check wether 2 wedges are adjacent (i.e. they a common edge)
///
/// @param Wedge w : wedge to check adjacency with
///
/// @return bool : true if wedges are adjacent, false else
bool Wedge::is_adjacent(Wedge w)
{
        // edges are p1-p2, p1-p3, p2-p4 and p3-p4
    return this->operator !=(w) && (
        ( e1 == w.e3 && e2 == w.e4 ) ||
        ( e1 == w.e2 && e3 == w.e4 ) ||
        ( e4 == w.e2 && e3 == w.e1 ) ||
        ( e4 == w.e3 && e2 == w.e1 ) )
            ? true : false;
}

/// @brief check wether 2 wedges are adjacent (i.e. they a common edge)
///
/// @param Wedge w : wedge to check adjacency with
///
/// @return edge_number : where the intersection happen, -1 if no intersection
edge_number Wedge::place_adjacent(Wedge w)
{
    //this->print();
    //w.print();
        // edges are p1-p2, p1-p3, p2-p4 and p3-p4
    if ( this->operator !=(w) )
    {
        if ( e1 == w.e3 && e2 == w.e4 ) return edge_number::Left;
        if ( e1 == w.e2 && e3 == w.e4 ) return edge_number::Bottom;
        if ( e4 == w.e2 && e3 == w.e1 ) return edge_number::Right;
        if ( e4 == w.e3 && e2 == w.e1 ) return edge_number::Top;
    }
    return edge_number::None;
}

/// @brief do i really need to explain what it does ?
const void Wedge::print() const
{
    std::cout << e1 << " " << e2 << " " << e3 << " " << e4 << std::endl;
}

///////////////////////////////
/// @brief functions on wedges
///////////////////////////////

/// @brief find all adjacent wedges of a vector of wedges, given a specific wedge
///
/// @param std::vector<Wedge> vw : vector of wedges
/// @param Wedge : wedge to search adjacency
///
/// @return std::vector<Wedge> : vector containing all adjacent wedges of w
std::vector<std::pair<Wedge,edge_number> > find_wedge(std::vector<Wedge> vw, Wedge w)
{
    std::vector<std::pair<Wedge,edge_number> > adjacency;
    for (std::vector<Wedge>::iterator wit = vw.begin(); wit != vw.end(); ++wit)
        if (w.is_adjacent(*wit))
            adjacency.emplace_back(std::make_pair(*wit,w.place_adjacent(*wit)));
    return adjacency;
}

/// @brief compute wedge 2 main directions
///
/// @param Wedge w : w to compute on
///
/// @return Xpt3D normal of wedge
XPt3D compute_normal(XMls & mls, Wedge w)
{
    XPt3D v1 = mls.Pworld(0,w.e2) - mls.Pworld(0,w.e1);
    XPt3D v2 = mls.Pworld(0,w.e3) - mls.Pworld(0,w.e1);

    return cross_product(v1, v2);
}

/// @brief Compute normal of a wedge given its 2 main directions
///
/// @param XPt3D v1 : 1st direction
/// @param XPt3D v2 : 2nd direction
///
/// @return XPt3D normals
XPt3D cross_product(XPt3D v1, XPt3D v2)
{
    XPt3D normale;
    normale.X = v1.Y * v2.Z - v1.Z * v2.Y;
    normale.Y = v1.Z * v2.X - v1.X * v2.Z;
    normale.Z = v1.X * v2.Y - v1.Y * v2.X;
    return normale;
}

/// \brief find if a wedge is in a vector of wedges
///
/// \param vw: vector of wedges
/// \param w: wedge
///
/// \return true if w is in vw, else false
bool find_wedge_in_vec(std::vector<Wedge>* vw, Wedge w)
{
    bool r = false;
    for (auto & wedge:*vw)
        if (    (   w.e1 == wedge.e1 && ( w.e2 == wedge.e2 && (w.e3 == wedge.e3 || w.e4 == wedge.e4) ) || ( w.e3 == wedge.e3 && w.e4 == wedge.e4) )   ||
                (   w.e2 == wedge.e2 && w.e3 == wedge.e3 && w.e4 == wedge.e4)   )
            return true;
    return false;
}

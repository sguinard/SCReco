#include "libSteph/TinyPlyManagement.h"
#include "libSteph/Config.h"
#include "libSteph/Triangle_Coords.h"

void PlyFile2Vector(const std::string & filename, PlyFile* file, SCR::vvD* observations, std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* edges)
{
    std::ifstream ss(filename, std::ios::binary);

    if (ss.fail()) throw std::runtime_error("failed to open " + filename);

    //PlyFile file;
    file->parse_header(ss);

    std::cout << "........................................................................\n";
    for (auto c : file->get_comments()) std::cout << "Comment: " << c << std::endl;
    for (auto e : file->get_elements())
    {
        std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
        for (auto p : e.properties) std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")" << std::endl;
    }
    std::cout << "........................................................................\n";

    // Tinyply 2.0 treats incoming data as untyped byte buffers. It's now
    // up to users to treat this data as they wish. See below for examples.
    std::shared_ptr<PlyData> vertices, normals, faces, texcoords;

    // The header information can be used to programmatically extract properties on elements
    // known to exist in the file header prior to reading the data. For brevity of this sample, properties
    // like vertex position are hard-coded:
    try { vertices = file->request_properties_from_element("vertex", { "x", "y", "z" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

//    try { normals = file->request_properties_from_element("vertex", { "nx", "ny", "nz" }); }
//    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

//    try { texcoords = file->request_properties_from_element("vertex", { "u", "v" }); }
//    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    try { faces = file->request_properties_from_element("face", { "vertex_indices" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    manual_timer read_timer;

    read_timer.start();
    file->read(ss);
    read_timer.stop();

    std::cout << "Parsing took " << read_timer.get() / 1000.f << " seconds: " << std::endl;
    if (vertices) std::cout << "\tRead " << vertices->count << " total vertices "<< std::endl;
    if (normals) std::cout << "\tRead " << normals->count << " total vertex normals " << std::endl;
    if (texcoords) std::cout << "\tRead " << texcoords->count << " total vertex texcoords " << std::endl;
    if (faces) std::cout << "\tRead " << faces->count << " total faces (triangles) " << std::endl;

    // Example: type 'conversion' to your own native types - Option A

    const size_t numVerticesBytes = vertices->buffer.size_bytes();
    std::vector<float3> verts(vertices->count);
    std::memcpy(verts.data(), vertices->buffer.get(), numVerticesBytes);

    for (auto& vertex:verts)
        observations->emplace_back(std::vector<double> {(double)vertex.x, (double)vertex.y, (double)vertex.z});

    //for (auto& obs:*observations)
    //    std::cout << obs[0] << " " << obs[1] << " " << obs[2] << " - " << std::flush;

    const size_t numFacesBytes = faces->buffer.size_bytes();
    std::vector<mesh_triangle> facs(faces->count);
    std::memcpy(facs.data(), faces->buffer.get(), numFacesBytes);


    std::vector<uint32_t> edges_begin;
    std::vector<uint32_t> edges_end;


    for (auto& tri:facs)
    {
        edges_begin.emplace_back(tri.v1);
        edges_begin.emplace_back(tri.v1);
        edges_begin.emplace_back(tri.v2);

        edges_end.emplace_back(tri.v2);
        edges_end.emplace_back(tri.v3);
        edges_end.emplace_back(tri.v3);
    }

    *edges = std::make_pair(edges_begin,edges_end);

}



typedef std::vector<double> vD;

void PlyFile2Vector(const std::string & filename, PlyFile* file, SCR::vvD* points, SCR::vvD* observations, std::vector<std::vector<uint32_t> >* triangles)
{
    std::ifstream ss(filename, std::ios::binary);

    if (ss.fail()) throw std::runtime_error("failed to open " + filename);

    //PlyFile file;
    file->parse_header(ss);

    std::cout << "........................................................................\n";
    for (auto c : file->get_comments()) std::cout << "Comment: " << c << std::endl;
    for (auto e : file->get_elements())
    {
        std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
        for (auto p : e.properties) std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")" << std::endl;
    }
    std::cout << "........................................................................\n";

    // Tinyply 2.0 treats incoming data as untyped byte buffers. It's now
    // up to users to treat this data as they wish. See below for examples.
    std::shared_ptr<PlyData> vertices, normals, faces, texcoords;

    // The header information can be used to programmatically extract properties on elements
    // known to exist in the file header prior to reading the data. For brevity of this sample, properties
    // like vertex position are hard-coded:
    try { vertices = file->request_properties_from_element("vertex", { "x", "y", "z" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    try { faces = file->request_properties_from_element("face", { "vertex_indices" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    manual_timer read_timer;

    read_timer.start();
    file->read(ss);
    read_timer.stop();

    std::cout << "Parsing took " << read_timer.get() / 1000.f << " seconds: " << std::endl;
    if (vertices) std::cout << "\tRead " << vertices->count << " total vertices "<< std::endl;
    if (normals) std::cout << "\tRead " << normals->count << " total vertex normals " << std::endl;
    if (texcoords) std::cout << "\tRead " << texcoords->count << " total vertex texcoords " << std::endl;
    if (faces) std::cout << "\tRead " << faces->count << " total faces (triangles) " << std::endl;

    // Example: type 'conversion' to your own native types - Option A

    const size_t numVerticesBytes = vertices->buffer.size_bytes();
    std::vector<double3> verts(vertices->count);
    std::memcpy(verts.data(), vertices->buffer.get(), numVerticesBytes);

    for (auto& vertex:verts)
        points->emplace_back(std::vector<double> {(double)vertex.x, (double)vertex.y, (double)vertex.z});

    const size_t numFacesBytes = faces->buffer.size_bytes();
    std::vector<mesh_triangle> facs(faces->count);
    std::memcpy(facs.data(), faces->buffer.get(), numFacesBytes);
    
    for (auto& tri: facs)
    {
		triangles->emplace_back(std::vector<uint32_t>{tri.v1, tri.v2, tri.v3});
		vD p1 = points->at(tri.v1); vD p2 = points->at(tri.v2); vD p3 = points->at(tri.v3);
		p1.at(0) += p2.at(0) + p3.at(0);
		p1.at(1) += p2.at(1) + p3.at(1);
		p1.at(2) += p2.at(2) + p3.at(2);
		p1.at(0) /= 3; p1.at(1) /= 3; p1.at(2) /= 3;
		observations->emplace_back(p1);
    }

}






void PlyFile2Vector(const std::string & filename, PlyFile* file, SCR::vvD* points, SCR::vvD* observations, std::pair<std::vector<uint32_t>, std::vector<uint32_t> >* edges)
{
    std::ifstream ss(filename, std::ios::binary);

    if (ss.fail()) throw std::runtime_error("failed to open " + filename);

    //PlyFile file;
    file->parse_header(ss);

    std::cout << "........................................................................\n";
    for (auto c : file->get_comments()) std::cout << "Comment: " << c << std::endl;
    for (auto e : file->get_elements())
    {
        std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
        for (auto p : e.properties) std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")" << std::endl;
    }
    std::cout << "........................................................................\n";

    // Tinyply 2.0 treats incoming data as untyped byte buffers. It's now
    // up to users to treat this data as they wish. See below for examples.
    std::shared_ptr<PlyData> vertices, normals, faces, texcoords;

    // The header information can be used to programmatically extract properties on elements
    // known to exist in the file header prior to reading the data. For brevity of this sample, properties
    // like vertex position are hard-coded:
    try { vertices = file->request_properties_from_element("vertex", { "x", "y", "z" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    try { normals = file->request_properties_from_element("vertex", { "nx", "ny", "nz" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

//    try { texcoords = file->request_properties_from_element("vertex", { "u", "v" }); }
//    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    try { faces = file->request_properties_from_element("face", { "vertex_indices" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    manual_timer read_timer;

    read_timer.start();
    file->read(ss);
    read_timer.stop();

    std::cout << "Parsing took " << read_timer.get() / 1000.f << " seconds: " << std::endl;
    if (vertices) std::cout << "\tRead " << vertices->count << " total vertices "<< std::endl;
    if (normals) std::cout << "\tRead " << normals->count << " total vertex normals " << std::endl;
    if (texcoords) std::cout << "\tRead " << texcoords->count << " total vertex texcoords " << std::endl;
    if (faces) std::cout << "\tRead " << faces->count << " total faces (triangles) " << std::endl;

    // Example: type 'conversion' to your own native types - Option A

    const size_t numVerticesBytes = vertices->buffer.size_bytes();
    std::vector<float3> verts(vertices->count);
    std::memcpy(verts.data(), vertices->buffer.get(), numVerticesBytes);

    for (auto& vertex:verts)
        points->emplace_back(std::vector<double> {(double)vertex.x, (double)vertex.y, (double)vertex.z});
        
        
    const size_t numNormalsBytes = normals->buffer.size_bytes();
    std::vector<float3> norms(normals->count);
    std::memcpy(norms.data(), normals->buffer.get(), numNormalsBytes);

    for (auto& n:norms)
        observations->emplace_back(std::vector<double> {(double)n.x, (double)n.y, (double)n.z});
        

    const size_t numFacesBytes = faces->buffer.size_bytes();
    std::vector<mesh_triangle> facs(faces->count);
    std::memcpy(facs.data(), faces->buffer.get(), numFacesBytes);


    std::vector<uint32_t> edges_begin;
    std::vector<uint32_t> edges_end;


    for (auto& tri:facs)
    {
        edges_begin.emplace_back(tri.v1);
        edges_begin.emplace_back(tri.v1);
        edges_begin.emplace_back(tri.v2);

        edges_end.emplace_back(tri.v2);
        edges_end.emplace_back(tri.v3);
        edges_end.emplace_back(tri.v3);
    }

    *edges = std::make_pair(edges_begin,edges_end);

}









void PlyFile2Vector(const std::string & filename, PlyFile* file, SCR::vP3* observations, SCR::vT* triangles)
{
    std::ifstream ss(filename, std::ios::binary);

    if (ss.fail()) throw std::runtime_error("failed to open " + filename);

    //PlyFile file;
    file->parse_header(ss);

    std::cout << "........................................................................\n";
    for (auto c : file->get_comments()) std::cout << "Comment: " << c << std::endl;
    for (auto e : file->get_elements())
    {
        std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
        for (auto p : e.properties) std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")" << std::endl;
    }
    std::cout << "........................................................................\n";

    // Tinyply 2.0 treats incoming data as untyped byte buffers. It's now
    // up to users to treat this data as they wish. See below for examples.
    std::shared_ptr<PlyData> vertices, normals, faces, texcoords;

    // The header information can be used to programmatically extract properties on elements
    // known to exist in the file header prior to reading the data. For brevity of this sample, properties
    // like vertex position are hard-coded:
    try { vertices = file->request_properties_from_element("vertex", { "x", "y", "z" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

//    try { normals = file->request_properties_from_element("vertex", { "nx", "ny", "nz" }); }
//    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

//    try { texcoords = file->request_properties_from_element("vertex", { "u", "v" }); }
//    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    try { faces = file->request_properties_from_element("face", { "vertex_indices" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    manual_timer read_timer;

    read_timer.start();
    file->read(ss);
    read_timer.stop();

    std::cout << "Parsing took " << read_timer.get() / 1000.f << " seconds: " << std::endl;
    if (vertices) std::cout << "\tRead " << vertices->count << " total vertices "<< std::endl;
    if (normals) std::cout << "\tRead " << normals->count << " total vertex normals " << std::endl;
    if (texcoords) std::cout << "\tRead " << texcoords->count << " total vertex texcoords " << std::endl;
    if (faces) std::cout << "\tRead " << faces->count << " total faces (triangles) " << std::endl;

    // Example: type 'conversion' to your own native types - Option A

    const size_t numVerticesBytes = vertices->buffer.size_bytes();
    std::vector<float3> verts(vertices->count);
    std::memcpy(verts.data(), vertices->buffer.get(), numVerticesBytes);

    for (auto& vertex:verts)
        observations->emplace_back(XPt3D((double)vertex.x, (double)vertex.y, (double)vertex.z));

    const size_t numFacesBytes = faces->buffer.size_bytes();
    std::vector<mesh_triangle> facs(faces->count);
    std::memcpy(facs.data(), faces->buffer.get(), numFacesBytes);


    for (auto& tri:facs)
    {
		triangles->emplace_back(Triangle(tri.v1,tri.v2,tri.v3));
    }
    std::sort(triangles->begin(), triangles->end());
    triangles->erase (unique (triangles->begin(), triangles->end()), triangles->end());

}








void PlyFile2Vector(const std::string & filename, PlyFile* file, std::vector<TriangleCoords> *observations)
{
	std::ifstream ss(filename, std::ios::binary);

    if (ss.fail()) throw std::runtime_error("failed to open " + filename);

    //PlyFile file;
    file->parse_header(ss);

    std::cout << "........................................................................\n";
    for (auto c : file->get_comments()) std::cout << "Comment: " << c << std::endl;
    for (auto e : file->get_elements())
    {
        std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
        for (auto p : e.properties) std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")" << std::endl;
    }
    std::cout << "........................................................................\n";

	// Tinyply 2.0 treats incoming data as untyped byte buffers. It's now
    // up to users to treat this data as they wish. See below for examples.
    std::shared_ptr<PlyData> vertices, normals, faces, texcoords;

    // The header information can be used to programmatically extract properties on elements
    // known to exist in the file header prior to reading the data. For brevity of this sample, properties
    // like vertex position are hard-coded:
    try { vertices = file->request_properties_from_element("vertex", { "x", "y", "z" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }
    
    try { faces = file->request_properties_from_element("face", { "vertex_indices" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    manual_timer read_timer;

    read_timer.start();
    file->read(ss);
    read_timer.stop();

    std::cout << "Parsing took " << read_timer.get() / 1000.f << " seconds: " << std::endl;
    if (vertices) std::cout << "\tRead " << vertices->count << " total vertices "<< std::endl;
    if (faces) std::cout << "\tRead " << faces->count << " total faces (triangles) " << std::endl;

    // Example: type 'conversion' to your own native types - Option A

    const size_t numVerticesBytes = vertices->buffer.size_bytes();
    std::vector<float3> verts(vertices->count);
    std::memcpy(verts.data(), vertices->buffer.get(), numVerticesBytes);
    
    const size_t numFacesBytes = faces->buffer.size_bytes();
    std::vector<mesh_triangle> facs(faces->count);
    std::memcpy(facs.data(), faces->buffer.get(), numFacesBytes);

    for (auto& tri:facs)
    {
        XPt3D p1(verts[tri.v1].x, verts[tri.v1].y, verts[tri.v1].z);
        XPt3D p2(verts[tri.v2].x, verts[tri.v2].y, verts[tri.v2].z);
        XPt3D p3(verts[tri.v3].x, verts[tri.v3].y, verts[tri.v3].z);
        vP3 v{p1,p2,p3};
	observations->emplace_back(TriangleCoords(&v));
    }

}



void PlyFile2Vector(const std::string & filename, PlyFile* file, SCR::vvD* observations)
{
    std::ifstream ss(filename, std::ios::binary);

    if (ss.fail()) throw std::runtime_error("failed to open " + filename);

    //PlyFile file;
    file->parse_header(ss);

    std::cout << "........................................................................\n";
    for (auto c : file->get_comments()) std::cout << "Comment: " << c << std::endl;
    for (auto e : file->get_elements())
    {
        std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
        for (auto p : e.properties) std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")" << std::endl;
    }
    std::cout << "........................................................................\n";

    // Tinyply 2.0 treats incoming data as untyped byte buffers. It's now
    // up to users to treat this data as they wish. See below for examples.
    std::shared_ptr<PlyData> vertices, normals, faces, texcoords;

    // The header information can be used to programmatically extract properties on elements
    // known to exist in the file header prior to reading the data. For brevity of this sample, properties
    // like vertex position are hard-coded:
    try { vertices = file->request_properties_from_element("vertex", { "x", "y", "z" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    manual_timer read_timer;

    read_timer.start();
    file->read(ss);
    read_timer.stop();

    std::cout << "Parsing took " << read_timer.get() / 1000.f << " seconds: " << std::endl;
    if (vertices) std::cout << "\tRead " << vertices->count << " total vertices "<< std::endl;
    if (normals) std::cout << "\tRead " << normals->count << " total vertex normals " << std::endl;
    if (texcoords) std::cout << "\tRead " << texcoords->count << " total vertex texcoords " << std::endl;
    if (faces) std::cout << "\tRead " << faces->count << " total faces (triangles) " << std::endl;

    // Example: type 'conversion' to your own native types - Option A

    const size_t numVerticesBytes = vertices->buffer.size_bytes();
    std::vector<float3> verts(vertices->count);
    std::memcpy(verts.data(), vertices->buffer.get(), numVerticesBytes);

    for (auto& vertex:verts)
        observations->emplace_back(std::vector<double> {(double)vertex.x, (double)vertex.y, (double)vertex.z});

}

void createmesh(const string &filename, PlyFile *file, SCR::vvD* observations, std::vector<Triangle> *v_tri)
{
    std::ifstream ss(filename, std::ios::binary);

    if (ss.fail()) throw std::runtime_error("failed to open " + filename);

    //PlyFile file;
    file->parse_header(ss);

    std::cout << "........................................................................\n";
    for (auto c : file->get_comments()) std::cout << "Comment: " << c << std::endl;
    for (auto e : file->get_elements())
    {
        std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
        for (auto p : e.properties) std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")" << std::endl;
    }
    std::cout << "........................................................................\n";

    // Tinyply 2.0 treats incoming data as untyped byte buffers. It's now
    // up to users to treat this data as they wish. See below for examples.
    std::shared_ptr<PlyData> vertices, normals, faces, texcoords;

    // The header information can be used to programmatically extract properties on elements
    // known to exist in the file header prior to reading the data. For brevity of this sample, properties
    // like vertex position are hard-coded:
    try { vertices = file->request_properties_from_element("vertex", { "x_laser", "y_laser", "z_laser" }); }
    catch (const std::exception & e) { std::cerr << "tinyply exception: " << e.what() << std::endl; }

    manual_timer read_timer;

    read_timer.start();
    file->read(ss);
    read_timer.stop();

    std::cout << "Parsing took " << read_timer.get() / 1000.f << " seconds: " << std::endl;
    if (vertices) std::cout << "\tRead " << vertices->count << " total vertices "<< std::endl;

    const size_t numVerticesBytes = vertices->buffer.size_bytes();
    std::vector<float3> verts(vertices->count);
    std::memcpy(verts.data(), vertices->buffer.get(), numVerticesBytes);

    for (auto& vertex:verts)
        observations->emplace_back(std::vector<double> {(double)vertex.x, (double)vertex.y, (double)vertex.z});
}

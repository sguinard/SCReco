FIND_PATH(CP_INCLUDE_DIR CutPursuit.h
  ${EXTERN_DIR}/cut-pursuit-master/include/
)
FIND_LIBRARY(CP_LIBRARY libcp.so
  ${EXTERN_DIR}/cut-pursuit-master/build/src/
)


IF (CP_INCLUDE_DIR)
  SET(CP_INCLUDE_FOUND "YES")
  MESSAGE(STATUS "Found Cut Pursuit include dir: ${CP_INCLUDE_DIR}")
ENDIF (CP_INCLUDE_DIR)

IF (CP_LIBRARY)
  SET(CP_LIB_FOUND "YES")
  MESSAGE(STATUS "Found CP lib: ${CP_LIBRARY}")
ENDIF (CP_LIBRARY)

IF(CP_INCLUDE_FOUND)
  SET(CP_FOUND "YES")
ENDIF(CP_INCLUDE_FOUND)
